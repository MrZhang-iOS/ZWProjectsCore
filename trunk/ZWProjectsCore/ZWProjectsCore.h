//
//  ZWProjectsCore.h
//  ZWProjectsCore
//
//  Created by ZhangWei on 16/9/13.
//  Copyright © 2016年 ZhangWei. All rights reserved.
//

#import <UIKit/UIKit.h>

#if __has_include(<ZWProjectsCore/ZWProjectsCore.h>)
//! Project version number for ZWProjectsCore.
FOUNDATION_EXPORT double ZWProjectsCoreVersionNumber;

//! Project version string for ZWProjectsCore.
FOUNDATION_EXPORT const unsigned char ZWProjectsCoreVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ZWProjectsCore/PublicHeader.h>
#import <ZWProjectsCore/ZWProjectsCoreMacros.h>
#import <ZWProjectsCore/ZWPinyin.h>
#import <ZWProjectsCore/NSArray+ZhangWei.h>
#import <ZWProjectsCore/NSData+ZhangWei.h>
#import <ZWProjectsCore/NSDate+ZhangWei.h>
#import <ZWProjectsCore/NSDate+ZWUtilities.h>
#import <ZWProjectsCore/NSDictionary+ZhangWei.h>
#import <ZWProjectsCore/NSMutableAttributedString+ZhangWei.h>
#import <ZWProjectsCore/NSNumber+ZhangWei.h>
#import <ZWProjectsCore/NSString+ZhangWei.h>
#import <ZWProjectsCore/NSTimer+ZhangWei.h>
#import <ZWProjectsCore/UIButton+ZhangWei.h>
#import <ZWProjectsCore/UIColor+ZhangWei.h>
#import <ZWProjectsCore/UIFont+ZhangWei.h>
#import <ZWProjectsCore/UIImage+ZhangWei.h>
#import <ZWProjectsCore/UIImageView+ZhangWei.h>
#import <ZWProjectsCore/UIView+ZhangWei.h>
#import <ZWProjectsCore/UIViewController+ZWPresentQueue.h>
#import <ZWProjectsCore/UIViewController+ZhangWei.h>
#import <ZWProjectsCore/ZWProjectsCoreFunctions.h>
#import <ZWProjectsCore/ZWCountryCodeManager.h>
#import <ZWProjectsCore/ZWCreateGifWithImage.h>
#import <ZWProjectsCore/ZWDirectoryWatcher.h>
#import <ZWProjectsCore/ZWEncryption.h>
#import <ZWProjectsCore/ZWLocalizedLanguage.h>
#import <ZWProjectsCore/ZWResourcesManager.h>
#import <ZWProjectsCore/ZWSendEmailManager.h>
#import <ZWProjectsCore/ZWSendSMSManager.h>
#import <ZWProjectsCore/UIZWBaseView.h>
#import <ZWProjectsCore/UIView+ZWDebug.h>
#import <ZWProjectsCore/UIZWBaseButton.h>
#import <ZWProjectsCore/UIZWBaseCollectionReusableView.h>
#import <ZWProjectsCore/UIZWBaseCollectionViewCell.h>
#import <ZWProjectsCore/UIZWBaseLabel.h>
#import <ZWProjectsCore/UIZWBaseTableViewCell.h>
#import <ZWProjectsCore/UIZWBaseTableViewHeaderFooterView.h>
#import <ZWProjectsCore/UIZWUniversalCell.h>
#import <ZWProjectsCore/UIZWAnimationPopView.h>
#import <ZWProjectsCore/UIZWCloseBoardBar.h>
#import <ZWProjectsCore/UIZWTopNavBar.h>
#import <ZWProjectsCore/UIZWBaseNavigationController.h>
#import <ZWProjectsCore/UIZWBaseViewController.h>
#import <ZWProjectsCore/UIZWTopNavBarCtrl.h>
#import <ZWProjectsCore/UIZWGifView.h>
#import <ZWProjectsCore/UIZWNoDelayTableView.h>
#import <ZWProjectsCore/UIZWTextLimitField.h>
#import <ZWProjectsCore/UIZWTextLimitView.h>
#import <ZWProjectsCore/UIZWZoomAspectImageView.h>

#else

#import "ZWProjectsCoreMacros.h"
#import "ZWPinyin.h"
#import "NSArray+ZhangWei.h"
#import "NSData+ZhangWei.h"
#import "NSDate+ZhangWei.h"
#import "NSDate+ZWUtilities.h"
#import "NSDictionary+ZhangWei.h"
#import "NSMutableAttributedString+ZhangWei.h"
#import "NSNumber+ZhangWei.h"
#import "NSString+ZhangWei.h"
#import "NSTimer+ZhangWei.h"
#import "UIButton+ZhangWei.h"
#import "UIColor+ZhangWei.h"
#import "UIFont+ZhangWei.h"
#import "UIImage+ZhangWei.h"
#import "UIImageView+ZhangWei.h"
#import "UIView+ZhangWei.h"
#import "UIViewController+ZWPresentQueue.h"
#import "ZWProjectsCoreFunctions.h"
#import "ZWCountryCodeManager.h"
#import "ZWCreateGifWithImage.h"
#import "ZWDirectoryWatcher.h"
#import "ZWEncryption.h"
#import "ZWLocalizedLanguage.h"
#import "ZWResourcesManager.h"
#import "ZWSendEmailManager.h"
#import "ZWSendSMSManager.h"
#import "UIZWBaseView.h"
#import "UIView+ZWDebug.h"
#import "UIZWBaseButton.h"
#import "UIZWBaseCollectionReusableView.h"
#import "UIZWBaseCollectionViewCell.h"
#import "UIZWBaseLabel.h"
#import "UIZWBaseTableViewCell.h"
#import "UIZWBaseTableViewHeaderFooterView.h"
#import "UIZWUniversalCell.h"
#import "UIZWAnimationPopView.h"
#import "UIZWCloseBoardBar.h"
#import "UIZWTopNavBar.h"
#import "UIZWBaseNavigationController.h"
#import "UIZWBaseViewController.h"
#import "UIZWTopNavBarCtrl.h"
#import "UIZWGifView.h"
#import "UIZWNoDelayTableView.h"
#import "UIZWTextLimitField.h"
#import "UIZWTextLimitView.h"
#import "UIZWZoomAspectImageView.h"

/**
 *💡【如果在"主应用"和"应用扩展"中使用这些源文件，请在"应用扩展"的BuildSettings中增加如下宏定义】💡
 *1. 注意工程预编译开关，创建Target时预编译功能默认是关闭的。
 *① TARGETS → Build Settings -> Apple LLVM 9.0 - Language打开Precompile Prefix Header为YES
 *② TARGETS → Build Settings -> Apple LLVM 9.0 - Language配置Prefix Header路径。
 *2. 如果工程包含多个应用扩展，建议使用动态库，可以有效减少打包后的ipa所占Size大小，否则资源及相关类模块都会被多次复用拷贝。
 *3. (可选配置)TARGETS → Build Settings -> Preprocessor Macro 配置SV_APP_EXTENSIONS——第三方库SVProgressHUD针对"应用扩展"定义的逻辑宏。
 *
 *🌟以上无论是Github模块的头文件还是自定义模块的头文件都不得单独关联到非模板类中，因为它们可能会被编译到到*.framework中作为通用模块供其他项目使用，
 *一旦私自关联且使用*.framework动态库时，工程编译报错❌，提示文件找不到相关类文件的错误。
 */

#endif

/*
 动态库有 dynamic shared libraries, shared objects, or dynamically linked libraries。我们以 OS X 为例，当 App 启动时，操作系统内核会将 App 代码和数据载入新进程(也就是操作系统为 App 创建的新进程)的地址空间。
 与此同时呢，操作系统内核也会把动态加载器(Dynamic Loader) 载入进程，由动态加载器来完成加载 App 依赖的动态库。不过在启动阶段，动态加载器只会根据静态链接器中记录的 App 已链接的依赖库的名字，然后使用依赖库的 install name 来查找它们是否在文件系统中存在。
 如果不存在或不兼容，App 启动过程会中断。动态库被完全载入内存，是在代码里使用它的时候。
 所以相对静态库来说，使用动态库链接的 App 启动过程会更快。

 
 💡【使用此*.framework动态库的注意事项】💡
     1. 如果*.framework中有category类，则使用它的主程序或者应用扩展编译虽然成功但是运行调用时会导致crash，需要在BuildSettings的other Linker flags添上参数 -ObjC。⭐️比如该动态库中就存在category类，引用关联的工程就需要添加【-ObjC】
         －ObjC：加了这个参数后，链接器就会把静态/动态库中所有的Objective-C类和分类都加载到最后的可执行文件中。
         －all_load：会让链接器把所有找到的目标文件都加载到可执行文件中，但是千万不要随便使用这个参数！假如你使用了不止一个静态库文件，然后又使用了这个参数，那么你很有可能会遇到ld: duplicate symbol错误，因为不同的库文件里面可能会有相同的目标文件，所以建议在遇到-ObjC失效的情况下使用-force_load参数。
         -force_load：所做的事情跟-all_load其实是一样的，但是-force_load需要指定要进行全部加载的库文件的路径，这样的话，你就只是完全加载了一个库文件，不影响其余库文件的按需加载。
     2. *.framework静态库直接关联到项目中并增加静态库头文件就可以使用静态库中封装的方法了；
        *.framework动态库只能在Target->General->Embedded Binaries中添加关联，如果主程序存在"应用扩展"，不可以对"应用扩展"单独关联*.framework动态库，直接在"应用扩展"中增加*.framework动态库头文件就可以使用动态库中封装的方法了。
     3. ⭐️检查*.framework动态库文件中携带的图片(见ZWResourcesManager模块应该包含至少一个*.bundle文件)和语言国际化(见ZWLocalizedLanguage模块应该包含至少一个*.bundle文件)资源是否完整，如果缺少请参见【创建模板库相关的配置】第3条。
     4. ⭐️如果需要添加更多的资源文件，可以直接打开已经创建好的*.framework动态库，找到相应的*.bundle文件，右键"显示包内容"，将资源文件置于其中即可，无需再次编译创建*.framework动态库。建议最好添加资源文件到工程后重新编译生成新的*.framework，以免后期重新编译造成部分资源丢失。
     5. (可选配置项)如果在"应用扩展"中使用SVProgressHUD的源文件，请在"应用扩展"的BuildSettings中增加如下宏定义：
         ① TARGETS → Build Settings -> Preprocessor Macro 配置SV_APP_EXTENSIONS——第三方库SVProgressHUD针对"应用扩展"定义的逻辑宏。
     6. 注意工程预编译开关，新创建Target时预编译功能默认是关闭的。
         ① TARGETS → Build Settings -> Apple LLVM 9.0 - Language打开Precompile Prefix Header为YES
         ② TARGETS → Build Settings -> Apple LLVM 9.0 - Language配置Prefix Header路径。
     7. 如果引用该*.framework动态库的工程同时包括App和App Extension，建议在生成*.framework时勾选【Allow app extension API only】，否则编译时报警告。详情参见【创建模板库后相关的配置】第1-⑦条。
     8. ⭐️如果引用了通用*.framework动态库的工程在上传时报错ITMS-90087，请在引用的App工程后添加脚本后再重新打包上传。详情参见【创建模板库后相关的配置】第8条。
 
 
 💡【创建模板库相关的配置】💡
     1. Build Settings 配置
         ① Mach-O Type: 默认是Dynamic Library，可以按需手动修改成Static Library（静态库）。
             Xcode 生成五种 Mach-O 类型的二进制文件
             1️⃣ Executable                 可执行二进制文件
             2️⃣ Dynamic Library            动态库
             3️⃣ Bundle                     非独立二进制文件，显式加载
             4️⃣ Static Library             静态库
             5️⃣ Relocatable Object File    可重定位的目标文件，中间结果
         ② iOS Deployment target:framework支持的最低iOS版本。
         ③ (💕脚本编译通用库时配置，直接Archive编译的库无需配置)如果需要支持Bitcode，需要在Deployment中打开 Deployment Postprocessing配置为YES(可以去除符号缩减app的大小)，并设置Strip Style为Debugging Symbols，Custom Compiler Flags中Other C Flags添加-fembed-bitcode参数。
         ④ (可选配置项)Link With Standard Libraries 设置为YES，那么编译器在链接过程中会自动使用标准库的链接器。
         ⑤ (可选配置项)Dead Code Stripping 设置为YES，是对程序编译出的可执行二进制文件中没有被实际使用的代码进行Strip操作，能够一定程度上对程序安装包进行优化，只是优化的效果一般，对于一些比较小的项目甚至没有什么优化体现。
         ⑥ (可选配置项)Product Name: 最终编译出来的Framework文件的名称。默认为 $(TARGET_NAME)
         ⑦ Require Only App-Extension-Safe API 配置为YES，即TARGETS → General → Deployment Info → 勾选【Allow app extension API only】，能消除引用关联该*.framework动态库且包含应用扩展的工程编译时报的警告(提示linking against a dylib which is not safe for use in application extensions)
         ⑧ (可选配置项)Inhibit All Warnings 设置为YES，隐藏所有警告
     2. Build Phases 配置
         ① 在Link Binary With Libraries中添加自定义库的第三方依赖库（其他主程序或应用扩展在使用这个自定义*.framework时，Target中也需要另行关联这些第三方依赖库）。
            例：FMDB需要系统动态库libz.tbd和libsqlite3.tbd支持，如果是创建静态库，无需添加依赖，否则编译报错，在使用该静态库的程序或者应用扩展中再添加依赖；如果是创建动态库，则直接在工程中添加依赖关系，在使用该动态库的程序或者应用扩展中无需再添加依赖
         ② Compile Sources 包含了需要封装的实现代码。
         ③ Copy Bundle Resources:项目中使用到的资源文件，图片，XIB文件，plist文件等。
         ④ headers:将需要暴露出来的头文件添加到public分组下，需要隐藏的头文件添加到private分组下，把Public中的头文件增加到总头文件中，即此文件。
     3. *.framework动态库引入的源码中如果有附带资源，尽量为其创建*.bundle文件夹，关于*.framework动态库中资源(如*.plist文件或者*.bundle文件)的读取方式可以参考SVProgressHUD、MJRefresh、SAMKeychain等模块。
        SVProgressHUD不可以编译到*.framework静态库中，因为它包含有自己独有的SVProgressHUD.bundle文件，在静态库中会导致加载失败返回nil，在运行时参数不接受nil会导致Crash。
        如该动态库中ZWLocalizedLanguage及ZWResourcesManager模块都附带有至少一个*.bundle文件，内部有图片和语言国际化资源。
     4. 该模板库的总头文件必须与关联的Target同名，否则编译报错。
     5. 该总头文件必须关联TARGETS → Build Phases → Headers → Public部分的所有头文件，如果不想暴露出来的头文件请置于TARGETS → Build Phases → Headers → Private部分，否则在使用此*.framework动态库编译时会产生警告(提示Umbrella header for module 'ZWProjectsCore' does not include header '*.h')。
     6. 因为SVProgressHUD模块涉及到App和App Extension互斥的代码（通过配置宏SV_APP_EXTENSIONS实现控制，具体请参见源码），所以不合适将这种类似的Github库封装到自定义*.framework中（编译生成的*.framework库已经把源码编译到二进制文件中了，二进制文件不再识别编译时使用的宏）
     7. ⭐️因为Build Settings中Bitcode配置默认为YES，且Bitcode只默认在Archive下编译。在debug和release下并不会，所以通过CMD+B编译生成的*.framework在被App(Bitcode也设置为YES)关联使用时，虽然可以正常编译通过也可以在真机或模拟器上运行，但是一旦通过Archive发布App时会编译报错，提示*.framework不支持Bitcode(报错信息类似如下日志 bitcode bundle could not be generated because ... framework was built without full bitcode. All frameworks and dylibs for bitcode must be generated from XCode Archive or Install build file...)。
     8. ⭐️一般来说，创建Dynamic/Static library，都是需要同时兼容模拟器和真机的，而Xcode并未提供便利的一键生成同时兼容模拟器和真机的executable binary的方式，而是要单独编译后合并模拟器和真机的Dynamic/Static library，然而在编译到模拟器时不支持Archive，所以必须完成1-③配置。为了减少这些复杂的流程，可以创建Aggregate Target添加Run Script脚本(代号X)，在Scheme中选中Aggregate Target编译(Command+B或者Archive均可，💡注意💡选择Debug还是Release模式)后直接生成支持多种运行环境的Dynamic/Static library。⭐️这种通用的库在被引用关联后，打包提交到App Store时会报错，因为App Store不允许ipa文件存在i386模拟器模块，为了解决这个问题，还需要在引用关联通用库工程的App Target添加Run Script脚本(代号Y)剔除通用库中冗余的架构部分(如果包含应用扩展，App Extension Target不需要执行此操作)，添加了此脚本后不影响工程在模拟器上编译运行App。
 
 
【创建模板库前的准备知识】
     1. iOS里动态库的文件格式：*.dylib和*.framework（动态库在制作的时候可以直接包含静态库，也能自动link所需要的依赖库）。
         Framework是Cocoa/Cocoa Touch程序中使用的一种资源打包方式，可以将代码文件、头文件、资源文件、说明文档等集中在一起，方便开发者使用。一般如果是静态Framework的话，资源打包进Framework是读取不了的。
         *.framework静态库和*.a文件都是直接编译进可执行文件里面了，所以无法加载到这些静态库文件夹，里面的资源也就无法获取了。只有*.framework动态库能在*.app下面的Framework文件夹下看到，并可以读取*.framework里的资源文件。
     2. iOS里静态库的文件格式：*.a和*.framework(静态库很大的一个优点是减少耦合性，因为静态库中是不可以包含其他静态库的，使用的时候要另外导入它的依赖库，最大限度的保证了每一个静态库都是独立的，不会重复引用)。
     3. 系统的*.framework是动态库，比如Cocoa/Cocoa Touch开发框架本身提供了大量的Framework，比如Foundation.framework/UIKit.framework/AppKit.framework等。需要注意的是，这些framework无一例外都是动态库。
        自定义*.framework在iOS8之前必须是静态块，iOS8之后自定义的*.framework可以是动态库也可以是静态库。（iOS8之前苹果是禁止项目中使用自定义动态库的，否则不允许上架AppStore, 在iOS8后苹果开放了动态库，可能是因为App和Extension需要共享动态库）
     4. *.a是一个纯二进制的文件，且不可以直接使用，必须结合.h文件使用；而*.framework中除了有二进制文件之外还有资源文件，它是可以直接使用的，所以一般建议创建*.framework动态库，上面已经说过*.framework静态库的资源是无法读取的。
     5. CPU构架是厂商给属于同一系列的CPU产品定的一个规范, 主要目的是为了区分不同类型CPU的重要指示, 模拟器上的构架和真机上的构架不是一样的, 如果库的构架和测试项目对应的模拟器或者真机上的构架不对应就会报错 "Undefined symbols for architecture arm64/i386"。
         在终端查看*.framework文件输入指令(首先cd到指定的文件路径) lipo -info xxx.framework/xxxxFramework
         在终端查看*.a文件输入指令(首先cd到指定的文件路径) lipo -info xxx.a
 
         模拟器架构：
             i386 : iPhone 4s ~ 5
             x86_64 : iPhone 5s ~ 7Plus
         真机架构：
             armv6 : iPhone、iPhone 2、iPhone 3G、iPod Touch(第一代)、iPod Touch(第二代)
             armv7 : iPhone 3Gs、iPhone 4、iPhone 4s、iPad、iPad 2
             armv7s(armv7兼容armv7s) : iPhone 5、iPhone 5c
             arm64 : iPhone 5s、iPhone 6、iPhone 6 Plus、iPhone 6s、iPhone 6s Plus、iPad Air、iPad Air2、iPad mini2、iPad mini3及最新设备
 
 
【参考资料，注意仅作参考使用】
 https://juejin.im/post/5acefc0e6fb9a028e1205688
 https://www.jianshu.com/p/cbec1da24585
 https://www.jianshu.com/p/a37328bcee8f
 http://xummer26.com/blog/create-framework-on-Xcode-8.html
 https://www.jianshu.com/p/1cb4c4fe5481
 https://www.jianshu.com/p/e1ce400434e4
 https://www.jianshu.com/p/216b2ecb50b2
 http://openfibers.github.io/blog/2016/01/14/fucking-bitcode/
 https://stackoverflow.com/questions/31233395/ios-library-to-bitcode?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
 https://www.jianshu.com/p/588cb9a614ae
 https://www.jianshu.com/p/e2765d0b2484
 https://www.jianshu.com/p/04c7612e20ab
 http://blog.cocoachina.com/article/68838
 https://www.jianshu.com/p/216b2ecb50b2

*/
