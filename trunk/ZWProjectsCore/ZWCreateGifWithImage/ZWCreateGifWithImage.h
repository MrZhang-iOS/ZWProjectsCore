//
//  ZWCreateGifWithImage.h
//  ZWProjectsCore
//
//  Created by ZhangWei on 16/1/15.
//  Copyright © 2016年 ZhangWei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ZWCreateGifWithImage : NSObject

/**
 *  用一些UIImage对象创建一个Gif文件，最终的尺寸为所有图片最大的宽和最大的高，宽高可能不同属一张图片
 *
 *  @param path       Gif被创建的路径
 *  @param images     合成Gif的UIImage模型
 *  @param fDelay     Gif文件图片延迟切换的时间
 *  @param nLoopTimes Gif播放的次数 -1表示仅播放一次 0表示无线循环，1表示播放一次后再循环一次
 *  @param handle     创建成功后的回调
 */
+ (void)createGifAtPath:(NSString*)path
             withImages:(NSArray <UIImage*> *)images
           delaySeconds:(CGFloat)fDelay
              loopTimes:(int)nLoopTimes
        completedHandle:(void(^)(BOOL isSuccess, NSData* gifData))handle;

@end
