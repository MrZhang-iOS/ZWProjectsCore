//
//  ZWCreateGifWithImage.m
//  ZWProjectsCore
//
//  Created by ZhangWei on 16/1/15.
//  Copyright © 2016年 ZhangWei. All rights reserved.
//

#import "ZWCreateGifWithImage.h"
#import <ImageIO/ImageIO.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "ZWProjectsCoreFunctions.h"
#import "UIImage+ZhangWei.h"

@implementation ZWCreateGifWithImage

+ (void)createGifAtPath:(NSString*)path
             withImages:(NSArray <UIImage*> *)images
           delaySeconds:(CGFloat)fDelay
              loopTimes:(int)nLoopTimes
        completedHandle:(void(^)(BOOL isSuccess, NSData* gifData))handle;
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^
                   {
                       if (path.length > 0 && [images count] > 0)
                       {
                           CGFloat fMaxWidth = 0;
                           CGFloat fMaxHeigth = 0;
                           NSMutableArray* newImages = [NSMutableArray array];
                           for (UIImage* imageTemp in images)
                           {
                               CGSize imageSize = imageTemp.size;
                               if (imageSize.width > fMaxWidth)
                               {
                                   fMaxWidth = imageSize.width;
                               }
                               if (imageSize.height > fMaxHeigth)
                               {
                                   fMaxHeigth = imageSize.height;
                               }
                           }
                           CGFloat scale = [UIScreen mainScreen].scale;
                           fMaxWidth = ceil(fMaxWidth/scale);
                           fMaxHeigth = ceil(fMaxHeigth/scale);
                           for (UIImage* imageTemp in images)
                           {
                               UIImage* newImage = [imageTemp zw_zoomWithSize:CGSizeMake(fMaxWidth, fMaxHeigth) aspect:YES];
                               if (newImage)
                               {
                                   [newImages addObject:newImage];
                               }
                           }
                           
                           NSUInteger nImagesCount = [newImages count];
                           CGImageDestinationRef destination = CGImageDestinationCreateWithURL((CFURLRef)[NSURL fileURLWithPath:path],
                                                                                               kUTTypeGIF,
                                                                                               nImagesCount,
                                                                                               NULL);
                           
                           NSDictionary *frameProperties = [NSDictionary dictionaryWithObject:[NSDictionary dictionaryWithObject:[NSNumber numberWithFloat:fDelay] forKey:(NSString *)kCGImagePropertyGIFDelayTime]
                                                                                       forKey:(NSString *)kCGImagePropertyGIFDictionary];
                           NSDictionary *gifProperties = [NSDictionary dictionaryWithObject:[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:nLoopTimes] forKey:(NSString *)kCGImagePropertyGIFLoopCount]
                                                                                     forKey:(NSString *)kCGImagePropertyGIFDictionary];
                           
                           for (UIImage* tempImage in newImages)
                           {
                               CGImageDestinationAddImage(destination, tempImage.CGImage, (CFDictionaryRef)frameProperties);
                           }
                           CGImageDestinationSetProperties(destination, (CFDictionaryRef)gifProperties);
                           CGImageDestinationFinalize(destination);
                           CFRelease(destination);
                           
                           BOOL bExist = zw_isFileExistsAtPath(path);
                           NSData* gifData = nil;
                           if (bExist)
                           {
                               gifData = [NSData dataWithContentsOfFile:path];
                           }
                           
                           dispatch_async(dispatch_get_main_queue(), ^
                                          {
                                              if (handle)
                                              {
                                                  handle(bExist, gifData);
                                              }
                                          });
                       }
                       else
                       {
                           dispatch_async(dispatch_get_main_queue(), ^
                                          {
                                              if (handle)
                                              {
                                                  handle(NO, nil);
                                              }
                                          });
                       }
                   });
}

@end
