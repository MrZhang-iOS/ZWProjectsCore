/*
2014年注：
    模拟器33种可选语言、实体机34种可选语言
    以下列表用来在ZWLanguageStrings.plist文件查找创建语言文件夹所指定的字符串名称
    Settings->General->International->Language:
                                     zh-Hans    简体中文
                                     zh-Hant    繁体中文
                                     en         美式英文
                                     en-GB      英式英文
                                     fr         法文
                                     de         德文
                                     ja         日文
                                     nl         荷兰文
                                     it         意大利文
                                     es         西班牙文
                                     pt         葡萄牙文
                                     pt-PT      葡萄牙(葡萄牙)文
                                     da         丹麦文
                                     fi         芬兰文
                                     nb         挪威博克马尔文
                                     sv         瑞典文
                                     ko         韩文 
                                     ru         俄文
                                     pl         波兰文
                                     tr         土耳其文
                                     uk         乌克兰文
                                     ar         阿拉伯文
                                     hr         克罗地亚文
                                     cs         捷克文
                                     el         希腊文
                                     he         希伯来文
                                     ro         罗马尼亚文
                                     sk         斯洛伐克文
                                     th         泰文
                                     id         印度尼西亚文 
                                     ca         加泰罗尼亚文
                                     hu         匈牙利文
                                     vi         越南文
                                     ms         马来文（注：模拟器中没有此语言）

 
说明：
    💡使用此模块，项目必须关联ZWDefaultLanguage.bundle文件💡
    此模块实现语言+图片本地化，默认根据设备语言的切换自动选择本地化bundle，如果没有就使用默认文件ZWDefaultLanguage.bundle,
    用户可以设置此模块不根据系统语言的切换自动选择本地化bundle，也可以自定义bundle，如果自定义bundle，则此模块
    将不会根据系统语言的切换自动选择本地化bundle。

    例：设备的系统语言为“简体中文”，且该模块支持自动切换，则首先查找“SimplifiedChinese.bundle”，
    如果存在，表示“简体中文”已经被本地化了，否则使用默认Bundle——ZWDefaultLanguage.bundle。

    使用步骤：
    一、关联ZWLocalizedLanguage.h|ZWLocalizedLanguage.m|ZWLanguageStrings.plist|ZWLanguageShortCodes.plist
    二、如果你需要本地化日语和韩语两种语言，请对照ZWLanguageStrings.plist文件创建对应语言的*.bundle及*.strings文件，即“Japanese.bundle”、“Korean.bundle”
    三、请确保项目已关联ZWDefaultLanguage.bundle文件及其内部已存在ZWDefaultLanguage.strings文件
    四、如果添加了相应语言的*.bundle及*.strings文件，则ZWLanguageShortCodes.plist中需要配置相应的键值信息，否则以默认语言匹配缩写值
*/

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

UIKIT_EXTERN NSString* const ZWLocalizedLanguageMacro_DefaultValue;

#define ZWLOCALIZE(key) [[ZWLocalizedLanguage sharedZWLocalizedLanguage] localizedStringForKey:key]
#define ZWLOCALIZE_EX(key,value) [[ZWLocalizedLanguage sharedZWLocalizedLanguage] localizedStringForKey:key defaultValue:value]
#define ZWLOCALIZEInstance [ZWLocalizedLanguage sharedZWLocalizedLanguage]

@interface ZWLocalizedLanguage : NSObject

//自定义当前语言国际化文件名，必须创建与文件名同名的*.bundle和*.strings文件。否则自定义当前语言国际化文件失败，则使用默认路径
@property(nonatomic,copy)NSString *localizedBundleName;
@property(nonatomic,copy)NSString* (^configHighPriorityDataSavedDirecorty)(void);//配置高优先级数据读取或者保存的路径文件夹，如果不设置此属性默认将保存到沙盒路径下

//实例化对象
+ (ZWLocalizedLanguage*)sharedZWLocalizedLanguage;

//根据系统语言自动切换获取匹配的语言国际化文件,默认为YES,即读取对应语言的文件夹，如果没有则使用默认文件夹
- (void)autoAdjustAccordingToSystem:(BOOL)bFlag;
- (BOOL)isAutoAdjustAccordingToSystem;

//获取当前语言国际化文件中的键值。
//参数一表示返回值对应的键，
//参数二表示如果参数一在当前语言国际化文件中无法找到匹配的值时，将以此参数作为返回值
//返回值：如果参数一在当前语言国际化文件中存在且有匹配值，则返回匹配值，否则返回参数二的值
- (NSString*)localizedStringForKey:(NSString *)pkey defaultValue:(NSString*)pStrValue;
- (NSString*)localizedStringForKey:(NSString *)pkey;
//语言国际化的Key是否存在
- (BOOL)isLocalizedKeyExist:(NSString *)pkey;

//获取当前语言国际化文件所属的bundle路径下其他文件的路径（图片、html、声音等）
//(如：图片或者Html文件 参数为文件的完整名字即包括后缀。例：@"icon.png"或@"image.jpg")
- (NSString*)localizedFilePathWithName:(NSString*)pFileName;

//获取当前语言国际化文件所属的bundle
- (NSBundle*)localizedBundle;

//获取当前语言国际化文件的标记 如en_US
- (NSString*)currentUseShortLanguage;
//获取根据系统语言应该使用的语言国际化文件标记 如en_US
- (NSString*)shortLanguageAccordingToSystem;
//获取当前项目使用的所有语言国际化文件的标记
- (NSArray<NSString*>*)allShortLanguages;
//根据语言国际化文件的标记获取对应资源文件束的名字，不带后缀。如参数为nil或者空字符串将返回ZWLocalizedLanguage_DefaultLanguage
- (NSString*)bundleNameWithShortLanguage:(NSString*)shortLanguage;

//对高优先级的数据设置新的值，如果为nil，旧的值不会清空
- (BOOL)addHighPriorityData:(NSDictionary*)newDicData;

@end
