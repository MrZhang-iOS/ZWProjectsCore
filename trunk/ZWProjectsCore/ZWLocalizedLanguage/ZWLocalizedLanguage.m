#import "ZWLocalizedLanguage.h"
#import "ZWProjectsCoreMacros.h"
#import "ZWProjectsCoreFunctions.h"
#import "NSArray+ZhangWei.h"

#if !__has_feature(objc_arc)
#error This class requires automatic reference counting
#endif

NSString *const ZWLocalizedLanguageMacro_DefaultValue = @"DefaultValue";
NSString *const ZWLocalizedLanguage_DefaultLanguage = @"ZWDefaultLanguage";
NSString *const ZWLocalizedLanguage_MappingPlistFile = @"ZWLanguageStrings.plist";
NSString *const ZWLocalizedLanguage_MappingShortKeyPlistFile = @"ZWLanguageShortCodes.plist";

static ZWLocalizedLanguage *g_pZWLocalizedLanguageInstance = nil;

@interface ZWLocalizedLanguage ()

@property(nonatomic,strong)NSBundle* excuteBundle;
@property(nonatomic,strong)NSDictionary* languageMatchedFileNamesMapping;
@property(nonatomic,strong)NSDictionary* languageMatchedShortNamesMapping;

@property(nonatomic,strong)NSDictionary *appResourcesData;
@property(nonatomic,strong)NSDictionary *highPriorityData;
@property(nonatomic,strong)NSBundle* bundleInAppResources;
@property(nonatomic,assign)BOOL bAutoAdjustFlag;

@end

@implementation ZWLocalizedLanguage

+ (ZWLocalizedLanguage*)sharedZWLocalizedLanguage
{
    static dispatch_once_t pred;
    dispatch_once(&pred, ^ {
        g_pZWLocalizedLanguageInstance = [[ZWLocalizedLanguage alloc] init];
    });
    return g_pZWLocalizedLanguageInstance;
}

- (BOOL)addHighPriorityData:(NSDictionary*)newDicData;
{
    BOOL dataAddedSucc = NO;
    if (newDicData == nil || [newDicData allKeys].count == 0)
    {
        dataAddedSucc = YES;
    }
    else
    {
        NSMutableDictionary* existData = [NSMutableDictionary dictionaryWithDictionary:self.highPriorityData];
        if (existData == nil)
        {
            existData = [[NSMutableDictionary alloc] init];
        }
        if ([existData allKeys].count > 0)
        {
            NSArray* allKeysInNewDicData = [newDicData allKeys];
            for (NSString* keyInNewDicData in allKeysInNewDicData)
            {
                if (keyInNewDicData && [keyInNewDicData isKindOfClass:[NSString class]])
                {
                    [existData setValue:[newDicData valueForKey:keyInNewDicData] forKey:keyInNewDicData];
                }
            }
            dataAddedSucc = [existData writeToFile:[self highPriorityDataPath] atomically:YES];
            if (dataAddedSucc)
            {
                self.highPriorityData = existData;
            }
        }
        else
        {
            dataAddedSucc = [newDicData writeToFile:[self highPriorityDataPath] atomically:YES];
            if (dataAddedSucc)
            {
                self.highPriorityData = newDicData;
            }
        }
    }    
    return dataAddedSucc;
}

//根据系统语言自动切换获取本地化文件的路径,默认为YES
- (void)autoAdjustAccordingToSystem:(BOOL)bFlag;
{
    _bAutoAdjustFlag = bFlag;
    [self checkFileExist];
}

- (BOOL)isAutoAdjustAccordingToSystem;
{
    return _bAutoAdjustFlag;
}

- (void)setLocalizedBundleName:(NSString *)localizedBundleName;
{
    _localizedBundleName = localizedBundleName.copy;
    [self autoAdjustAccordingToSystem:NO];
}

- (void)setConfigHighPriorityDataSavedDirecorty:(NSString *(^)(void))configHighPriorityDataSavedDirecorty;
{
    _configHighPriorityDataSavedDirecorty = [configHighPriorityDataSavedDirecorty copy];
    [self autoAdjustAccordingToSystem:_bAutoAdjustFlag];
}

- (NSString *)localizedStringForKey:(NSString *)pkey defaultValue:(NSString*)pStrValue;
{
    if ( pkey.length <= 0 )
    {
        return pStrValue;
    }
    
    //优先从服务器获取的语言国际化文件中取Key值
    NSString * value = [self.highPriorityData valueForKey:pkey];
    if (value == nil)//允许key的后台配置为空字符串，所以可能为@""
    {
        value = [self.appResourcesData valueForKey:pkey];
    }

    if (value == nil)//允许key的后台配置为空字符串，所以可能为@""
    {
        value = pStrValue;
    }
    
    return value;
}

- (NSString*)localizedStringForKey:(NSString *)pkey
{
    return [self localizedStringForKey:pkey defaultValue:ZWLocalizedLanguageMacro_DefaultValue];
}

- (BOOL)isLocalizedKeyExist:(NSString *)pkey;
{
    BOOL isExist = NO;
    if ( pkey.length <= 0 )
    {
        return isExist;
    }
    
    //优先从服务器获取的语言国际化文件中取Key值
    isExist = [[self.highPriorityData allKeys] zw_containsString:pkey];
    //如果服务器获取的语言国际化文件不存在再从本地获取Key
    if (isExist == NO)
    {
        isExist = [[self.appResourcesData allKeys] zw_containsString:pkey];
    }
    
    return isExist;
}

- (NSString*)localizedFilePathWithName:(NSString*)pFileName;
{
    return [self.bundleInAppResources pathForResource:pFileName ofType:nil];
}

- (NSBundle*)localizedBundle;
{
    return self.bundleInAppResources;
}

#pragma mark - Private

- (id)init
{
    if (( self = [super init] ) != nil)
    {
        //获取当前类的NSBundle，因为该模块可能会被编译到*.framework库中
        //可以把*.framework动态库想象为主程序中的一个文件夹。在没有framework的主程序中，资源是直接放在根目录下的。但是，引入了*.framework动态库之后，就像把资源移动了相应的子目录。这样路径就发生了变化，NSBundle这个类就是为了区分这种变化。
        //*.framework动态库内部的资源如果用mainBundle取，路径就错了，所以一般使用bundleForClass:方法定位指定类所在bundle，然后读取其下的资源文件
        //注：*.framework静态库在主程序中不可以当做一个文件夹，因为*.framework静态库不允许添加资源文件，这也是SVProgressHUD在编译成*.framework静态库运行时crash的原因
        self.excuteBundle = [NSBundle bundleForClass:[self class]];
        self.languageMatchedFileNamesMapping = [NSDictionary dictionaryWithContentsOfFile:[self.excuteBundle pathForResource:ZWLocalizedLanguage_MappingPlistFile ofType:nil]];
        self.languageMatchedShortNamesMapping = [NSDictionary dictionaryWithContentsOfFile:[self.excuteBundle pathForResource:ZWLocalizedLanguage_MappingShortKeyPlistFile ofType:nil]];
        [self autoAdjustAccordingToSystem:YES];
    }
    return self;
}

- (NSString*)languageAccordingToSystem;
{
    //获取设备当前使用语言，以下方法是经过多方验证通过的，请勿轻易变更（包括ZWLanguageStrings.plist文件的内容）。
    //不同iOS系统版本获取语言返回的字符串略有差异，详情见以下链接。https://www.jianshu.com/p/424eb17a20d1
    //不同系统版本下获取的当前语言字符串
    //iOS7如下
    //zh-Hans: 简体
    //zh-Hant: 繁体
    //iOS8如下
    //zh-Hans: 简体
    //zh-Hant: 繁体
    //zh-HK: 香港繁体（增加）
    //iOS9如下
    //zh-Hans-CN: 简体（改变）
    //zh-Hant-CN: 繁体（改变）
    //zh-HK: 香港繁体
    //zh-TW: 台湾繁体（增加）
    //iOS 10如下
    //zh-Hans-CN    简体
    //yue-Hans-CN  粤语 简体 （增加）
    //yue-Hant-CN   粤语 繁体 （增加）
    //zh-Hant-CN     繁体
    //zh-Hant-MO    澳门繁体 （改变）
    //zh-Hant-TW    台湾繁体 （改变）
    //zh-Hant-HK     香港繁体 （改变）
    NSString* pStrCurrentLanguage = [NSLocale preferredLanguages].firstObject;
    
    if (zw_ios9OrLater())
    {
        /**
         *  iOS9开始，系统返回的语言格式增加了地区字符串标识
         */
        NSMutableArray* ios9Component = [NSMutableArray arrayWithArray:[pStrCurrentLanguage componentsSeparatedByString:@"-"]];
        if ([ios9Component count] > 1)
        {
            [ios9Component removeLastObject];
        }
        pStrCurrentLanguage = [ios9Component componentsJoinedByString:@"-"];
    }
    
    NSArray* pAllKeysInPlist = [self.languageMatchedFileNamesMapping allKeys];
    BOOL isLanguageExistInPlistFile = NO;
    for (NSString* key in pAllKeysInPlist)
    {
        if ([key isEqualToString:pStrCurrentLanguage])
        {
            isLanguageExistInPlistFile = YES;
            break;
        }
    }
    
    NSString* pLanguageMatchedFileName = [self.languageMatchedFileNamesMapping valueForKey:pStrCurrentLanguage];
    
    if (!isLanguageExistInPlistFile || pLanguageMatchedFileName.length <= 0)
    {
        NSLog(@"The language \"【%@】\" is not exist in【ZWLanguageStrings.plist】file,please check the plist file! Otherwise it will use default.", pStrCurrentLanguage);
        //语言国际化对应文件名配置文件中没有找到设备当前的语言,取默认语言及默认文件名
        return ZWLocalizedLanguage_DefaultLanguage;
    }
    else
    {
        NSString *languageMatchedBundlePath = [self.excuteBundle pathForResource:pLanguageMatchedFileName ofType:@"bundle"];
        NSBundle *languageMatchedBundle = [NSBundle bundleWithPath:languageMatchedBundlePath];
        
        NSString* pStrResourceCurrentFile = [languageMatchedBundle pathForResource:pLanguageMatchedFileName ofType:@"strings"];
        if (languageMatchedBundle && [[NSFileManager defaultManager] fileExistsAtPath:pStrResourceCurrentFile])
        {
            return pLanguageMatchedFileName;
        }
        else
        {
            NSLog(@"The bundle for language【%@】is not exist, so it will use【%@】bundle.", pStrCurrentLanguage, ZWLocalizedLanguage_DefaultLanguage);
            return ZWLocalizedLanguage_DefaultLanguage;
        }
    }
}

- (void)checkFileExist
{
    if (_bAutoAdjustFlag)
    {
        _localizedBundleName = [self languageAccordingToSystem];
    }
    else
    {
        if (_localizedBundleName.length <= 0)
        {
            _localizedBundleName = ZWLocalizedLanguage_DefaultLanguage;
        }
        NSString* pStrResourceCurrentFile = [self resourceLanguageFilePathInResourceBundle];
        if (![[NSFileManager defaultManager] fileExistsAtPath:pStrResourceCurrentFile])
        {
            NSLog(@"The bundle【%@】is not exist, so it will use【%@】bundle.", _localizedBundleName, ZWLocalizedLanguage_DefaultLanguage);
            _localizedBundleName = ZWLocalizedLanguage_DefaultLanguage;
        }
    }
    
    self.appResourcesData = [NSDictionary dictionaryWithContentsOfFile:[self resourceLanguageFilePathInResourceBundle]];
    self.highPriorityData = [NSDictionary dictionaryWithContentsOfFile:[self highPriorityDataPath]];
    self.bundleInAppResources = [NSBundle bundleWithPath:[self resourceLanguageBundlePathInResourceBundle]];
}

- (NSString*)currentUseShortLanguage;
{
    NSString* shortLanguage = [self.languageMatchedShortNamesMapping valueForKey:_localizedBundleName];
    if (shortLanguage.length <= 0)
    {
        shortLanguage = [self.languageMatchedShortNamesMapping objectForKey:ZWLocalizedLanguage_DefaultLanguage];
    }
    if (shortLanguage.length <= 0)
    {
        shortLanguage = @"en_US";
    }
    return shortLanguage;
}

- (NSString*)shortLanguageAccordingToSystem;
{
    NSString* shortLanguage = [self.languageMatchedShortNamesMapping objectForKey:[self languageAccordingToSystem]];
    if (shortLanguage.length <= 0)
    {
        shortLanguage = [self.languageMatchedShortNamesMapping objectForKey:ZWLocalizedLanguage_DefaultLanguage];
    }
    if (shortLanguage.length <= 0)
    {
        shortLanguage = @"en_US";
    }
    return shortLanguage;
}

- (NSArray<NSString*>*)allShortLanguages;
{
    NSArray* allValues = [self.languageMatchedShortNamesMapping allValues];
    NSMutableArray<NSString*>* allShortLanguages = [[NSMutableArray alloc] init];
    for (NSString* tempValue in allValues)
    {
        if (tempValue && [tempValue isKindOfClass:[NSString class]])
        {
            [allShortLanguages addObject:tempValue];
        }
    }
    if (allShortLanguages.count > 0)
    {
        return allShortLanguages;
    }
    return nil;
}

- (NSString*)bundleNameWithShortLanguage:(NSString*)shortLanguage;
{
    NSString* pLanguageMatchedFileName = ZWLocalizedLanguage_DefaultLanguage;
    if (shortLanguage == nil || ![shortLanguage isKindOfClass:[NSString class]] || shortLanguage.length <= 0)
    {
        return pLanguageMatchedFileName;
    }
    
    NSArray* allKeys = [self.languageMatchedShortNamesMapping allKeys];
    for (NSString* tempKey in allKeys)
    {
        if (tempKey && [tempKey isKindOfClass:[NSString class]])
        {
            NSString* tempValue = [self.languageMatchedShortNamesMapping valueForKey:tempKey];
            if (tempValue && [tempValue isKindOfClass:[NSString class]] && [tempValue isEqualToString:shortLanguage])
            {
                pLanguageMatchedFileName = tempKey;
                break;
            }
        }
    }
    
    if (pLanguageMatchedFileName == nil || pLanguageMatchedFileName.length <= 0)
    {
        pLanguageMatchedFileName = ZWLocalizedLanguage_DefaultLanguage;
    }
    
    return pLanguageMatchedFileName;
}

- (NSString*)resourceLanguageBundlePathInResourceBundle;
{
    return [self.excuteBundle pathForResource:_localizedBundleName ofType:@"bundle"];
}

- (NSString*)resourceLanguageFilePathInResourceBundle;
{
    NSBundle *languageMatchedBundle = [NSBundle bundleWithPath:[self resourceLanguageBundlePathInResourceBundle]];
    return [languageMatchedBundle pathForResource:_localizedBundleName ofType:@"strings"];
}

- (NSString*)highPriorityDataPath;
{
    NSString* directoryPath = nil;
    if (self.configHighPriorityDataSavedDirecorty)
    {
        NSString* userConfigDirectory = self.configHighPriorityDataSavedDirecorty();
        if (zw_isFileExistsAtPath(userConfigDirectory))
        {
            directoryPath = userConfigDirectory;
        }
    }
    if (directoryPath == nil)
    {
        directoryPath = zw_getLibraryPath();
    }    
    return [directoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.strings", _localizedBundleName]];
}

@end
