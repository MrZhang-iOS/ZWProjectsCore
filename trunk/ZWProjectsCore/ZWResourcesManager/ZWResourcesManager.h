//
//  ZWResourcesManager.h
//  ZWProjectsCore
//
//  Created by ZhangWei on 15/4/21.
//  Copyright (c) 2015年 ZhangWei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

static NSString *const ZWResourcesManagerMacro_DefaultBundle = @"ZWDefaultResources.bundle";

@interface ZWResourcesManager : NSObject

/**
 通过资源名来获取资源文件路径
 */
+ (NSString*)resourceFilePathWithName:(NSString*)fileName;

/**
通过资源名来获取资源数据
 */
+ (NSData*)resourceFileDataWithName:(NSString*)fileName;

/**
通过图片资源名来获取图片，类似系统方法[UIImage imageNamed:@"XXX"]，图片会自动缓存处理
 */
+ (UIImage*)imageWithName:(NSString*)imageName;

/**
 通过图片资源名来获取图片，但不存在缓存
 */
+ (UIImage*)imageNoMemoryCachesWithName:(NSString*)imageName;

/**
 通过图片资源名来获取可以从中心开始水平和垂直拉伸的图片
 */
+ (UIImage*)stretchableImageWithName:(NSString*)imageName;

/**
 获取当前的资源Bundle名字 默认为ZWResourcesManagerMacro_DefaultBundle常量字符串的值
 */
+ (NSString*)currentResouceBundleName;

/**
 使用指定的新的资源名替换掉当前使用的资源Bundle，App重新启动后自动恢复默认资源名，即ZWResourcesManagerMacro_DefaultBundle常量字符串的值
 该值如果为@""、nil和非NSString值时，将使用默认资源名，如果新的资源名在项目中没有找到时，会通过断言提示开发人员。
 */
+ (BOOL)changeResourceBundle:(NSString*)bundleName;

/**
 乔布斯头像和苹果Logo融合设计的图片，一般用来测试使用
 */
+ (UIImage*)steveJobsInAppleLogo;

@end


/*
 
 该模块主要是用来管理项目自带的公用资源，如图片等。这些资源都是直接放在工程目录下面特定的Bundle文件中。
 它的好处是
     1.可以避免资源冲突，有效的避免了开发过程中因其他项目的资源重名导致的获取资源异常。
     2.开发人员对资源的增删改不用再理会是否关联到了项目工程文件。
     3.图片资源能够快速释放，减少了内存的浪费；页面全屏背景图可以优先考虑使用这种方式管理图片。
  ⭐️ 4.这里的资源可以供多个进程使用，如项目App和它的多个应用扩展，可以共用此一份资源文件（避免相同资源被重复打包），减少发布项目所占的Size
 它的缺点是
     1.Bundle文件是静态的，不参与项目编译，图片等资源不受保护。
     2.使用imageNamed:加载图片时效率较Assetcatalog差。
 
 🌟 请确保项目已关联ZWDefaultResources.bundle文件，使用该模块请保证项目中有Bundle文件存在，且文件名与ZWResourcesManagerMacro_DefaultBundle的值一样。
 
 ========================================💡Asset Catalog💡=================================================================================================================================================================
 AssetCatalog，另一种管理资源的方式，如果将图片直接放在工程目录下面，项目打包后图片文件也是散落在包里面，而且不会对图片进行压缩，而如果放在xcassets中，在打包后会将这些图片（除了AppIcon和LaunchImage，这两种图片是直接放在包中的）统一压缩成一个Assets.car的文件，大大减小包的大小，具体是几倍的关系我记不清了，但是相当的可观。Image Set实质就是一个文件夹，里面包括不同分辨率的图片和一个Contents.json文件，内容如下包含着这个图片集中图片文件的基本信息。
 它的优点是
     1.用一个高度优化的特殊格式来存所有图片（生成一个*.car文件），而不是一个一个的单独的图片资源，会更少的涉及频繁Disk I/O操作，且会按需下载适合设备机型的合适分辨率的图片资源。
     2.安全性高，图片等资源得到一定程度保护（Asset,car不易打开，可以用cartool开源命令行工具解压）。
     3.图片在多处使用时，仅被创建一次，减少了沙盒的读取操作；页面中频繁使用的图片k可以优先考虑使用这种方式管理图片。
     4.减少App占用的空间。
     5.可以手动调整图片的拉伸区域。
     6.不同的AssetCatalog下如果存在同名的ImageSet，编译时会报警告⚠️，但是如果和系统AppIcon或者LaunchImage同名，编译时不会报警告⚠️，且使用时以系统为准显示。
     7.jpg格式的图片也可以类似png格式的图片管理。相对于jpg，iOS对png的支持较好，例如，如果从Assets.xcassets以外的地方加载图片，必须在文件名后加扩展名。例如：
        [UIImage imageNamed:@"pic"]; // 错误，图片未能正确加载
        [UIImage imageNamed:@"pic.jpg"]; // 正确
 它的缺点是
     1.不能通过路径加载图片，只支持[UIImage imageNamed]的方式实例化。
     2.图片都缓存在内存中，由系统管理。
     3.无法参与全局搜索。

 注意：⚠️NSDataAsset只能在Deployment Target为iOS9及之上使用，否则初始化之后为nil（已亲测证实）。
 例：主目录中的Gifs命名空间文件夹下有DataSet名为laugh，内部有L000.gif文件；（以下示例已亲测）
    ① [[NSDataAsset alloc] initWithName:@"Gifs/laugh"];//可获取到数据
    ② [[NSBundle mainBundle] pathForResource:@"Gifs/L000.gif" ofType:nil];//⚠️无法获取到文件路径，返回nil
    ③ [[NSBundle mainBundle] pathForResource:@"Gifs/laugh" ofType:nil];//⚠️无法获取到文件路径，返回nil
    ④ [[NSDataAsset alloc] initWithName:@"Gifs/L000.gif"];//⚠️无法获取到数据，返回nil
 
    主目录中DataSet名为smile，内部有S000.gif文件；
    ① [[NSDataAsset alloc] initWithName:@"smile"];//可获取到数据
    ② [[NSBundle mainBundle] pathForResource:@"S000.gif" ofType:nil];//可以获取到路径
    ③ [[NSBundle mainBundle] pathForResource:@"smile" ofType:nil];//⚠️无法获取到文件路径，返回nil
    ④ [[NSDataAsset alloc] initWithName:@"S000.gif"];//⚠️无法获取到数据，返回nil
 ============================================================================================================================================================================================================================
 */
