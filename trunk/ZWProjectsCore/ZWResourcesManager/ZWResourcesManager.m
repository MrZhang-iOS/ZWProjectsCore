//
//  ZWResourcesManager.m
//  ZWProjectsCore
//
//  Created by ZhangWei on 15/4/21.
//  Copyright (c) 2015年 ZhangWei. All rights reserved.
//

#import "ZWResourcesManager.h"

@interface ZWResourcesManager ()

@property(nonatomic,copy)NSString* bundleName;
@property(nonatomic,strong)NSBundle* pResourceBundle;

@end

@implementation ZWResourcesManager

+ (NSString*)resourceFilePathWithName:(NSString*)fileName;
{
    if (fileName && [fileName isKindOfClass:[NSString class]] && fileName.length > 0)
    {
        return [[ZWResourcesManager sharedZWResourcesManager].pResourceBundle pathForResource:fileName ofType:nil];
    }
    return nil;
}

+ (NSData*)resourceFileDataWithName:(NSString*)fileName;
{
    if (fileName && [fileName isKindOfClass:[NSString class]] && fileName.length > 0)
    {
        return [NSData dataWithContentsOfFile:[ZWResourcesManager resourceFilePathWithName:fileName]];
    }
    return nil;
}

+ (UIImage*)imageWithName:(NSString*)imageName;
{
    if (imageName && [imageName isKindOfClass:[NSString class]] && imageName.length > 0)
    {
        UIImage* image = [UIImage imageNamed:[NSString stringWithFormat:@"%@/%@", [ZWResourcesManager sharedZWResourcesManager].bundleName,imageName]];
        if (image == nil)
        {
            image = [ZWResourcesManager imageNoMemoryCachesWithName:imageName];
        }
        return image;
    }
    return nil;
}

+ (UIImage*)imageNoMemoryCachesWithName:(NSString*)imageName;
{
    if (imageName && [imageName isKindOfClass:[NSString class]] && imageName.length > 0)
    {
        return [UIImage imageWithContentsOfFile:[ZWResourcesManager resourceFilePathWithName:imageName]];
    }
    return nil;
}

+ (UIImage*)stretchableImageWithName:(NSString*)imageName;
{
    UIImage* pTempImage = [ZWResourcesManager imageWithName:imageName];
    pTempImage = [pTempImage stretchableImageWithLeftCapWidth:pTempImage.size.width/2.0 topCapHeight:pTempImage.size.height/2.0];
    return pTempImage;
}

+ (NSString*)currentResouceBundleName;
{
    return [ZWResourcesManager sharedZWResourcesManager].bundleName;
}

+ (BOOL)changeResourceBundle:(NSString*)bundleName;
{
    [ZWResourcesManager sharedZWResourcesManager].bundleName = bundleName;
    return [[ZWResourcesManager sharedZWResourcesManager].bundleName isEqualToString:bundleName];
}

+ (UIImage*)steveJobsInAppleLogo;
{
    return [ZWResourcesManager imageWithName:@"RememberSteveJobs.png"];
}

#pragma mark - PrivateFuntions
+ (ZWResourcesManager*)sharedZWResourcesManager;
{
    static ZWResourcesManager* g_pSharedResourceBundleManager;
    static dispatch_once_t pred;
    dispatch_once(&pred, ^ {
        g_pSharedResourceBundleManager = [[ZWResourcesManager alloc] init];
    });
    return g_pSharedResourceBundleManager;
}

- (id)init
{
    if (self = [super init])
    {
        self.bundleName = ZWResourcesManagerMacro_DefaultBundle;
    }
    return self;
}

- (void)setBundleName:(NSString *)bundleName;
{
    if (bundleName == nil || ![bundleName isKindOfClass:[NSString class]] || bundleName.length <= 0)
    {
        bundleName = ZWResourcesManagerMacro_DefaultBundle;
    }
    _bundleName = [bundleName copy];
    
    //获取当前类的NSBundle，因为该模块可能会被编译到*.framework库中
    //可以把*.framework动态库想象为主程序中的一个文件夹。在没有framework的主程序中，资源是直接放在根目录下的。但是，引入了*.framework动态库之后，就像把资源移动了相应的子目录。这样路径就发生了变化，NSBundle这个类就是为了区分这种变化。
    //*.framework动态库内部的资源如果用mainBundle取，路径就错了，所以一般使用bundleForClass:方法定位指定类所在bundle，然后读取其下的资源文件
    //注：*.framework静态库在主程序中不可以当做一个文件夹，因为*.framework静态库不允许添加资源文件，这也是SVProgressHUD在编译成*.framework静态库运行时crash的原因
    NSString* resourceBundlePath = [[NSBundle bundleForClass:[self class]] pathForResource:_bundleName ofType:nil];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if ([fileManager fileExistsAtPath:resourceBundlePath])
    {
        self.pResourceBundle = [NSBundle bundleWithPath:resourceBundlePath];
    }
    else
    {
        NSString* failedText = [NSString stringWithFormat:@"🌟开发人员请注意🌟没有在项目中找打BundleName为【%@】的资源容器", bundleName];
        NSLog(@"设置资源路径失败：%@", failedText);
        if ([_bundleName isEqualToString:ZWResourcesManagerMacro_DefaultBundle])
        {
            NSAssert([fileManager fileExistsAtPath:resourceBundlePath], failedText);
        }
        else
        {
            self.bundleName = ZWResourcesManagerMacro_DefaultBundle;
        }
    }
}
@end
