//
//  ZWPinyin.h
//  ZWProjectsCore
//
//  Created by ZhangWei on 11/5/5.
//  Copyright (c) 2011年 ZhangWei. All rights reserved.
//

char zw_pinyinFirstLetter(unsigned short hanzi);
int zw_isUnicharHanzi(unsigned short hanzi);


/**
 * #import "ZWPinyin.h"
 *
 * NSString *hanyu = @"中国共产党万岁！";
 * for (int i = 0; i < [hanyu length]; i++)
 * {
 *     printf("%c", zw_pinyinFirstLetter([hanyu characterAtIndex:i]));
 * }
 *
 */
