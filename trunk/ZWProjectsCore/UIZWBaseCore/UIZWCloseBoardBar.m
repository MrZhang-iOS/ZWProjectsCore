//
//  UIZWCloseBoardBar.m
//  ZWProjectsCore
//
//  Created by ZhangWei on 15/5/31.
//  Copyright (c) 2015年 ZhangWei. All rights reserved.
//

#import "UIZWCloseBoardBar.h"
#import "UIButton+ZhangWei.h"
#import <Masonry/Masonry.h>

@interface UIZWCloseBoardBar ()

@property (nonatomic, strong) UIButton *rightButton;

- (void)rightButtonAction:(id)sender;

@end

@implementation UIZWCloseBoardBar

- (id)initWithFrame:(CGRect)frame;
{
    if (self = [super initWithFrame:frame])
    {
        self.backgroundColor = [UIColor blackColor];
        self.contentMode = UIViewContentModeScaleAspectFill;
        self.clipsToBounds = YES;
        self.userInteractionEnabled = YES;
        
        _rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_rightButton zw_expandEventSize:CGSizeMake(10, 10)];
        [_rightButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_rightButton setTitle:@"关闭" forState:UIControlStateNormal];
        [_rightButton addTarget:self action:@selector(rightButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_rightButton];
        [_rightButton mas_makeConstraints:^(MASConstraintMaker *make) {
            if (@available(iOS 11.0, *))
            {
                make.right.equalTo(self.mas_safeAreaLayoutGuideRight).offset(-10);
            }
            else
            {
                make.right.equalTo(self).offset(-10);
            }
            make.top.bottom.equalTo(self);
        }];
    }
    return self;
}

- (void)rightButtonAction:(id)sender;
{
    if (self.rightButtonBlock)
    {
        self.rightButtonBlock(self, sender);
    }
}

@end
