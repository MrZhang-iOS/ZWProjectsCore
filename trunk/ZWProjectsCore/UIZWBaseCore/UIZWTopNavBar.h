//
//  UIZWTopNavBar.h
//  ZWProjectsCore
//
//  Created by ZhangWei on 16/5/1.
//  Copyright © 2016年 ZhangWei. All rights reserved.
//

#import "UIZWBaseView.h"

@interface UIZWTopNavBar : UIZWBaseView

/**
 中间的标题栏。
 【文字内容】默认nil，居中
 【文字内容颜色】默认黑色
 【文字内容字体】默认系统字体粗体17
 */
@property (nonatomic, strong, readonly) UILabel *titleLabel;

/**
 左侧按钮，默认隐藏。
 【文字内容】默认“返回”
 【文字内容颜色】默认MyMacro_Color_SystemBlue
 【文字内容字体】默认系统字体粗体15
 【图片】默认为nil
 */
@property (nonatomic, strong, readonly) UIButton *leftButton;
@property (nonatomic, assign) CGSize leftButtonSize;//默认为CGSizeZero, 表示按钮Size随文字内容调整。一旦设置此属性，左边按钮将按设置的size显示。
@property (nonatomic, assign) CGPoint leftButtonOffset;//默认为CGPointZero
@property (nonatomic, copy) void(^leftButtonHandler)(UIZWTopNavBar *view, UIButton *button);

/**
 右侧按钮，默认隐藏。
 【文字内容】默认“返回”
 【文字内容颜色】默认MyMacro_Color_SystemBlue
 【文字内容字体】默认系统字体粗体15
 【图片】默认为nil
 */
@property (nonatomic, strong, readonly) UIButton *rightButton;
@property (nonatomic, assign) CGSize rightButtonSize;//默认为CGSizeZero, 表示按钮Size随文字内容调整。一旦设置此属性，右边按钮将按设置的size显示。
@property (nonatomic, assign) CGPoint rightButtonOffset;//默认为CGPointZero
@property (nonatomic, copy) void(^rightButtonHandler)(UIZWTopNavBar *view, UIButton *button);

/**
 底部分割线
 */
@property (nonatomic, strong, readonly) UIImageView *separateLine;
@property (nonatomic, assign) NSInteger separateLineHeight;//默认为1

/**
 @abstract 该方法自带了指定构造器宏 NS_DESIGNATED_INITIALIZER
 
 什么是指定构造器宏？
 http://sejasonwang.github.io/2016/01/27/designated-initalizer/
 http://www.swiftyper.com/2016/08/02/understanding-designated-initializer-in-objective-c/
 http://www.jianshu.com/p/b142caa44382
 */
- (instancetype)initWithFrame:(CGRect)frame NS_DESIGNATED_INITIALIZER;

@end

/*
 背景默认透明
 */
