//
//  UIZWBaseButton.h
//
//  Created by ZhangWei on 2020/11/5.
//  Copyright © 2020 ZhangWei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIZWBaseButton : UIButton

//MARK: 支持关键字高亮显示
@property (nonatomic, strong) UIColor *highlightedKeywordsColor;//高亮关键字的颜色，默认使用系统亮色文字，即[UIColor lightTextColor]
@property (nonatomic, strong) UIFont *highlightedKeywordsFont;//高亮关键字的字体，默认使用title当前字体
@property (nonatomic, copy) NSString *highlightedKeywords;//高亮关键字。如果highlightedKeywords属性有值且长度大于0时，调用【setTitle:forState:】方法将自动设置关键字高亮显示

//MARK: 支持不同状态独立字体
@property (nonatomic, strong) UIFont *titleFontForNormal;
@property (nonatomic, strong) UIFont *titleFontForHighlighted;
@property (nonatomic, strong) UIFont *titleFontForSelected;
@property (nonatomic, strong) UIFont *titleFontForDisabled;

@end

/*
 该控件的的设计目的：
 1.按钮上的title如果包含了指定的关键字，可以使关键字区别于title按指定的颜色和字体显示。
 2.按钮在选中、高亮、不可用状态时，默认不支持变更title的字体。
 */
