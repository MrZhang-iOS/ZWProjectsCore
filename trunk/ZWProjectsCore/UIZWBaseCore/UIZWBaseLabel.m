//
//  UIZWBaseLabel.m
//
//  Created by ZhangWei on 2020/7/27.
//  Copyright © 2020 ZhangWei. All rights reserved.
//

#import "UIZWBaseLabel.h"
#import "NSMutableAttributedString+ZhangWei.h"

@interface UIZWBaseLabel ()

@property (nonatomic, copy) NSString *originText;
@property (nonatomic, strong) UIColor *textColorForDefault;

@end

@implementation UIZWBaseLabel
@synthesize highlightedKeywordsColor = _highlightedKeywordsColor;
@synthesize highlightedKeywordsFont = _highlightedKeywordsFont;

- (void)setTextColor:(UIColor *)textColor;
{
    [super setTextColor:textColor];
    self.textColorForDefault = textColor;
}

- (UIColor *)textColorForDefault
{
    if (_textColorForDefault == nil)
    {
        //⚠️使用self.textColor获取当前文字颜色，在当前显示的文字已经是富文本时获取的颜色可能不稳定。故而保存UILabel的文字颜色。
        _textColorForDefault = self.textColor?:[UIColor darkTextColor];
    }
    return _textColorForDefault;
}

- (void)setHighlightedKeywordsColor:(UIColor *)highlightedKeywordsColor;
{
    _highlightedKeywordsColor = highlightedKeywordsColor;
    self.text = self.originText;
}

- (UIColor *)highlightedKeywordsColor;
{
    if (nil == _highlightedKeywordsColor)
    {
        return [UIColor lightTextColor];
    }
    return _highlightedKeywordsColor;
}

- (void)setHighlightedKeywordsFont:(UIFont *)highlightedKeywordsFont;
{
    _highlightedKeywordsFont = highlightedKeywordsFont;
    self.text = self.originText;
}

- (UIFont *)highlightedKeywordsFont;
{
    if (nil == _highlightedKeywordsFont)
    {
        return self.font?:[UIFont systemFontOfSize:[UIFont labelFontSize]];
    }
    return _highlightedKeywordsFont;
}

- (void)setHighlightedKeywords:(NSString *)highlightedKeywords;
{
    _highlightedKeywords = [highlightedKeywords copy];
    
    [self setText:self.originText];
}

- (void)setText:(NSString *)text;
{
    self.originText = text;
    if (text.length > 0 && self.highlightedKeywords && [self.highlightedKeywords isKindOfClass:[NSString class]] && self.highlightedKeywords.length > 0)
    {
        [super setText:nil];
        NSMutableAttributedString* tmpText = [NSMutableAttributedString zw_createWithString:text
                                                                                       font:self.font
                                                                                      color:self.textColorForDefault];
        [tmpText zw_addFont:self.highlightedKeywordsFont substring:self.highlightedKeywords];
        [tmpText zw_addColor:self.highlightedKeywordsColor substring:self.highlightedKeywords];
        [self setAttributedText:tmpText];        
    }
    else
    {
        [self setAttributedText:nil];
        [super setText:text];
    }
}

@end
