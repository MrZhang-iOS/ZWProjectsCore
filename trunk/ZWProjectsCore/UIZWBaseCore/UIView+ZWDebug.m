//
//  UIView+ZWDebug.m
//
//  Created by ZhangWei on 2020/11/5.
//  Copyright © 2020 ZhangWei. All rights reserved.
//

#import "UIView+ZWDebug.h"
#import "UIColor+ZhangWei.h"
#import <objc/runtime.h>
#import <Masonry/Masonry.h>

static const void *PropertyKey_debugText = &PropertyKey_debugText;
static const void *PropertyKey_debugLabel = &PropertyKey_debugLabel;

@implementation UIView (ZWDebug)
@dynamic debugText;

- (void)setDebugText:(NSString *)debugText;
{
    objc_setAssociatedObject(self, PropertyKey_debugText, debugText, OBJC_ASSOCIATION_COPY_NONATOMIC);
    if (debugText && [debugText isKindOfClass:[NSString class]] && debugText.length > 0)
    {
        self.debugLabel.text = debugText;
    }
}

- (NSString *)debugText
{
    return objc_getAssociatedObject(self, PropertyKey_debugText);
}

- (UILabel *)debugLabel;
{
#ifdef DEBUG
    UILabel *tmpLabel = objc_getAssociatedObject(self, PropertyKey_debugLabel);
    if (nil == tmpLabel)
    {
        tmpLabel = [[UILabel alloc] init];
        [tmpLabel setTextAlignment:NSTextAlignmentCenter];
        [tmpLabel setTextColor:[UIColor lightGrayColor]];
        [self addSubview:tmpLabel];
        [tmpLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self);
        }];
        objc_setAssociatedObject(self, PropertyKey_debugLabel, tmpLabel, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        
        self.backgroundColor = [UIColor zw_randomColor];
    }
    
    return tmpLabel;
#else
    return nil;
#endif
}

@end
