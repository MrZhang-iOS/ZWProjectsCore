//
//  UIZWNoDelayTableView.h
//  ZWProjectsCore
//
//  Created by ZhangWei on 16/8/7.
//  Copyright © 2016年 ZhangWei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIZWNoDelayTableView : UITableView

/**
 @abstract 因项目开发人员流动性大，为了便于维护代码，不建议使用Xib和Storyboard创建UI，统一使用源码管理UI。
 
 @warning This method is unavaialble. Please use【-initWithFrame: style:】OR【-initWithFrame:】OR【-init:】instead.
 */
- (instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE;//不能通过此方法初始化

/**
 @abstract 该方法自带了指定构造器宏 NS_DESIGNATED_INITIALIZER
 
 什么是指定构造器宏？
 http://sejasonwang.github.io/2016/01/27/designated-initalizer/
 http://www.swiftyper.com/2016/08/02/understanding-designated-initializer-in-objective-c/
 http://www.jianshu.com/p/b142caa44382
 */
- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style NS_DESIGNATED_INITIALIZER;

@end

/*
 
 UIScrollView会在接受到手势是延迟150ms来判断该手势是否能触发UIScrollView的滑动事件；反之值为NO时，UIScrollView会立马将接受到的手势分发到子视图上。
 所以在它上面的UIButton空间的高亮状态没有那么及时，封装该类的目的就是为了解决这个问题
 
 http://www.jianshu.com/p/b4331f06bd34
 
 */
