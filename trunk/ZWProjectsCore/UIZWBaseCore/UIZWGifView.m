//
//  UIZWGifView.m
//  ZWProjectsCore
//
//  Created by ZhangWei on 16/1/16.
//  Copyright © 2016年 ZhangWei. All rights reserved.
//

#import "UIZWGifView.h"
#import <ImageIO/ImageIO.h>
#import "NSTimer+ZhangWei.h"
#import "ZWProjectsCoreMacros.h"

#define UIZWGifViewMacro_DelayInterval 0.5

@interface UIZWGifView ()
{
    CGImageSourceRef _gif;
    size_t _index;
    size_t _count;
}

@property (nonatomic, strong) NSTimer *timer;

@end

@implementation UIZWGifView

- (void)dealloc;
{
    [self pauseGif];
    if (_gif)
    {
        CFRelease(_gif);
        _gif = nil;
    }
}

- (instancetype)initWithFrame:(CGRect)frame;
{
    if (self = [super initWithFrame:frame])
    {
        _gifData = nil;
        _loopCount = 0;
        _delayInterval = 0;
        _durationInterval = 0;
        _isPlaying = NO;
        _gifSize = CGSizeZero;
    }
    return self;
}

- (void)setGifData:(NSData *)gifData
{
    _gifData = gifData;
    
    if (gifData)
    {
        if (_gif)
        {
            CFRelease(_gif);
            _gif = nil;
        }
        _gif = CGImageSourceCreateWithData((__bridge CFDataRef)gifData, nil);
        
        _count = CGImageSourceGetCount(_gif);
        
        if (_count > 0)
        {
            CFDictionaryRef gifProps = CGImageSourceCopyPropertiesAtIndex(_gif, 0, nil);
            if (self.delayInterval <= 0)
            {
                CFDictionaryRef gifDic = CFDictionaryGetValue(gifProps, kCGImagePropertyGIFDictionary);
                NSNumber* delayInterval = CFDictionaryGetValue(gifDic, kCGImagePropertyGIFDelayTime);
                if (delayInterval)
                {
                    _delayInterval = [delayInterval floatValue];
                }
                else
                {
                    _delayInterval = UIZWGifViewMacro_DelayInterval;
                }
            }
            _durationInterval = _delayInterval*_count;
            
            NSNumber * w = CFDictionaryGetValue(gifProps, @"PixelWidth");
            NSNumber * h =CFDictionaryGetValue(gifProps, @"PixelHeight");
            if (w && h)
            {
                _gifSize = CGSizeMake(w.intValue, h.intValue);
            }
            
            if (gifProps)
            {
                CFRelease(gifProps);
            }
        }
    }
    [self stopGif];
}

- (void)setLoopCount:(NSInteger)loopCount
{
    if (_loopCount != loopCount)
    {
        _loopCount = loopCount;
    }
}

- (void)setDelayInterval:(CGFloat)delayInterval
{
    if (_delayInterval != delayInterval && delayInterval > 0)
    {
        _delayInterval = delayInterval;
        
        if (_count)
        {
            _durationInterval = delayInterval*_count;
            
            [self resumeGif];
        }
    }
}

- (void)setDurationInterval:(CGFloat)durationInterval
{
    if (_durationInterval != durationInterval && durationInterval > 0)
    {
        _durationInterval = durationInterval;
        if (_count > 0)
        {
            _delayInterval = durationInterval/_count;
            
            [self resumeGif];
        }
    }
}

- (void)play;
{
    if (_count <= 0)
    {
        [self pauseGif];
        return;
    }
    if (self.loopCount <= -1)
    {
        if (_index >= _count)
        {
            [self pauseGif];
            return;
        }
    }
    else if (self.loopCount == 0)
    {
        if (_index >= _count)
        {
            _index = 0;
        }
    }
    else
    {
        if (_index >= _count*self.loopCount)
        {
            [self pauseGif];
            return;
        }
    }
    _isPlaying = YES;
    size_t nIndex = _index%_count;
    CGImageRef ref = CGImageSourceCreateImageAtIndex(_gif, nIndex, nil);
    self.layer.contents = (__bridge id)ref;
    CFRelease(ref);
    _index ++;
}

#pragma mark - Public Functions
- (void)playGif;
{
    _index = 0;
    [_timer invalidate];
    _timer = nil;
    
    __weakSelf(self);
    _timer = [NSTimer zw_timerWithTimeInterval:self.delayInterval
                                        block:^{
        [weakself play];
    }
                                      repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
}

- (void)stopGif;
{
    [self pauseGif];
    
    _index = 0;
    
    [self play];
    _isPlaying = NO;
}

- (void)pauseGif;
{
    _isPlaying = NO;
    [_timer invalidate];
    _timer = nil;
}

- (void)resumeGif;
{
    [self pauseGif];
    
    __weakSelf(self);
    _timer = [NSTimer zw_scheduledTimerWithTimeInterval:self.delayInterval
                                                 block:^{
        [weakself play];
    }
                                               repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
    [_timer fire];
}
@end
