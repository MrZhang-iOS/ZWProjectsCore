//
//  UIZWTextLimitField.m
//  ZWProjectsCore
//
//  Created by ZhangWei on 2017/5/15.
//  Copyright © 2017年 ZhangWei. All rights reserved.
//

#import "UIZWTextLimitField.h"

@interface UIZWTextLimitField ()
<UITextFieldDelegate>

@property (nonatomic, assign) BOOL delegateWorking;

@end

@implementation UIZWTextLimitField
//@dynamic:作用于在@implementation内部，与@synthesize不同，使用@dynamic声明时相当于告诉编译器getter setter方法由用户自己生成。
//如果声明为@dynamic而没有手动生成getter setter方法编译的时候不报错，但是在运行时如果使用.语法去调用该属性时会崩溃。
//之所以在运行时才会发生崩溃是因为OC具有动态绑定特性。只有在运行时才会去确认具体的调用方法。

- (void)dealloc;
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)initWithFrame:(CGRect)frame;
{
    if (self = [super initWithFrame:frame])
    {
        _maxLength = NSIntegerMax;
        _delegateWorking = YES;
        self.delegate = self;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(observeUITextFieldTextDidChangeNotification:) name:UITextFieldTextDidChangeNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(observeUITextFieldTextDidBeginEditingNotification:) name:UITextFieldTextDidBeginEditingNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(observeUITextFieldTextDidEndEditingNotification:) name:UITextFieldTextDidEndEditingNotification object:nil];
    }
    return self;
}

- (void)setDelegate:(id<UITextFieldDelegate>)delegate;
{
    [super setDelegate:delegate];
    if (self == self.delegate)
    {
        self.delegateWorking = YES;
    }
    else
    {
        self.delegateWorking = NO;
    }
}

//重写【系统设置文本】方法的作用是，防止控件字符超过上限
- (void)setText:(NSString *)text
{
    void (^task)(void) = ^{
        NSString* copyText = text.copy;
        //如果没有self.markedTextRange == nil限制，在汉字输入联想时程序会挂掉
        if (self.markedTextRange == nil)//markedTextRange方法必须在主线程执行，否则可能导致crash
        {
            if (self.maxLength < copyText.length)
            {
                copyText = [copyText substringToIndex:self.maxLength];
            }
        }
        BOOL isModified = ![self.text isEqualToString:copyText];
        [super setText:copyText];
        if (isModified)
        {
            if (self.textModifiedHandler)
            {
                self.textModifiedHandler(self);
            }
        }
    };
    
    if (strcmp(dispatch_queue_get_label(DISPATCH_CURRENT_QUEUE_LABEL), dispatch_queue_get_label(dispatch_get_main_queue())) == 0)
    {
        // 主队列
        task();
    }
    else
    {
        // 非主队列
        dispatch_async(dispatch_get_main_queue(), task);
    }
}

- (void)setMaxLength:(NSUInteger)maxLength;
{
    if (_maxLength != maxLength)
    {
        _maxLength = maxLength;
        
        if (maxLength < self.text.length)
        {
            self.text = self.text;
        }
    }
}

/*
 对于 iOS 系统自带的键盘，有时候它在输入框中填入的是占位字符（已被高亮选中起来），等用户选中键盘上的候选词时，再替换为真正输入的字符.
 这会带来一个问题：比如输入框限定最多只能输入 10 位，当已经输入 9 个汉字的时候，使用系统拼音键盘则第 10 个字的拼音就打不了（因为剩余的 1 位无法输入完整的拼音）。
 怎么办呢？
 上面提到，输入框中的拼音会被高亮选中起来，所以我们可以根据 UITextField 的 markedTextRange 属性判断是否存在高亮字符，
 如果有则不进行字数统计和字符串截断操作。我们通过监听 UIControlEventEditingChanged 事件来对输入框内容的变化进行相应处理，如下
 */
- (void)observeUITextFieldTextDidChangeNotification:(NSNotification *)info;
{
    UIZWTextLimitField* pTempField = info.object;
    if (pTempField != self)
    {
        return;
    }
    
    // text field 的内容
    NSString *contentText = pTempField.text;
    // 获取高亮内容的范围
    UITextRange *selectedRange = [pTempField markedTextRange];
    // 这行代码 可以认为是 获取高亮内容的长度
    NSInteger markedTextLength = [pTempField offsetFromPosition:selectedRange.start toPosition:selectedRange.end];
    // 没有高亮内容时,对已输入的文字进行操作
    if (markedTextLength == 0)
    {
        // 如果 text field 的内容长度大于我们限制的内容长度
        if (contentText.length > self.maxLength)
        {
            NSRange rangeRange = [contentText rangeOfComposedCharacterSequencesForRange:NSMakeRange(0, self.maxLength)];
            pTempField.text = [contentText substringWithRange:rangeRange];
        }
        else
        {
            if (self.textModifiedHandler)
            {
                self.textModifiedHandler(self);
            }
        }
    }    
}

//通知在代理之后执行
- (void)observeUITextFieldTextDidBeginEditingNotification:(NSNotification *)info;
{
    UIZWTextLimitField* pTempField = info.object;
    if (pTempField != self)
    {
        return;
    }
    
    if (!self.delegateWorking && self.textFieldDidBeginEditingBlock)
    {
        self.textFieldDidBeginEditingBlock(self);
    }
}

//通知在代理之后执行
- (void)observeUITextFieldTextDidEndEditingNotification:(NSNotification *)info;
{
    UIZWTextLimitField* pTempField = info.object;
    if (pTempField != self)
    {
        return;
    }
    
    if (!self.delegateWorking && self.textFieldDidEndEditingBlock)
    {
        self.textFieldDidEndEditingBlock(self);
    }
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;        // return NO to disallow editing.
{
    if (self.textFieldShouldBeginEditingBlock)
    {
        return self.textFieldShouldBeginEditingBlock(self);
    }
    else
    {
        return YES;
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField;           // became first responder
{
    if (self.textFieldDidBeginEditingBlock)
    {
        self.textFieldDidBeginEditingBlock(self);
    }
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField;          // return YES to allow editing to stop and to resign first responder status. NO to disallow the editing session to end
{
    if (self.textFieldShouldEndEditingBlock)
    {
        return self.textFieldShouldEndEditingBlock(self);
    }
    else
    {
        return YES;
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField;             // may be called if forced even if shouldEndEditing returns NO (e.g. view removed from window) or endEditing:YES called
{
    if (self.textFieldDidEndEditingBlock)
    {
        self.textFieldDidEndEditingBlock(self);
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;   // return NO to not change text
{
    //如果有汉字，则联想字词不计数输入限制
    //删除操作时，string.length == 0
    if (string.length == 0 || textField.markedTextRange != nil)
    {
        return YES;
    }
    
    //UIZWTextLimitField类虽然已经重构了setText方法，直接赋值就可以预防超过文字上限，但这样会导致光标永远停在最后的字符上。
    //如果用户的目的是从字符串中间输入一段新的文字则每输入一个字符时需要调整光标位置，这样用户体验就很差了
    NSString* newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (self.maxLength < newString.length)
    {
        textField.text = newString;
        //返回YES，则将会把新的字符串自动拼接在旧的字符串上
        //返回NO，则会舍弃新的字符串
        return NO;
    }
    
    //返回YES，则将会把新的字符串自动拼接在旧的字符串上
    //返回NO，则会舍弃新的字符串
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField;               // called when clear button pressed. return NO to ignore (no notifications)
{
    if (self.textFieldShouldClearBlock)
    {
        return self.textFieldShouldClearBlock(self);
    }
    else
    {
        return YES;
    }
}

//iOS在默认情况下点击输入框会弹出键盘，但必须实现textFieldShouldReturn:(UITextField*)方法里才可以收键盘。
- (BOOL)textFieldShouldReturn:(UITextField *)textField;
{
    if (self.textFieldShouldResignDidReturnBlock)
    {
        BOOL shouldReturn = self.textFieldShouldResignDidReturnBlock(self);
        if (shouldReturn)
        {
            [self resignFirstResponder];
        }
        return shouldReturn;
    }
    else
    {
        [self resignFirstResponder];
        return YES;
    }
}

@end
