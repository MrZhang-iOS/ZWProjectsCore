//
//  UIZWAnimationPopView.h
//  ZWProjectsCore
//
//  Created by ZhangWei on 15/5/15.
//  Copyright (c) 2015年 ZhangWei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIZWAnimationPopView : UIView

/**
 *  因动画需要，请将子视图置于该视图下
 */
@property (nonatomic, strong, readonly) UIView *popBoardView;

/**
 @abstract 因项目开发人员流动性大，为了便于维护代码，不建议使用Xib和Storyboard创建UI，统一使用源码管理UI。
 
 @warning This method is unavaialble. Please use【-init:】OR【-initWithFrame:】instead.
 */
- (instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE;

/**
 @abstract 该方法自带了指定构造器宏 NS_DESIGNATED_INITIALIZER
 
 什么是指定构造器宏？
 http://sejasonwang.github.io/2016/01/27/designated-initalizer/
 http://www.swiftyper.com/2016/08/02/understanding-designated-initializer-in-objective-c/
 http://www.jianshu.com/p/b142caa44382
 */
- (instancetype)initWithFrame:(CGRect)frame NS_DESIGNATED_INITIALIZER;

//弹出式效果
- (void)showInView:(UIView*)view withAnimation:(BOOL)bAnimation;

//从window上弹出
- (void)showWithAnimation:(BOOL)bAnimation;

- (void)dismissWithAnimation:(BOOL)bAnimation;

@end
