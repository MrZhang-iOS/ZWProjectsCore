//
//  UIZWTextLimitView.h
//  ZWProjectsCore
//
//  Created by ZhangWei on 15/7/31.
//  Copyright (c) 2015年 ZhangWei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIZWTextLimitView : UITextView

/**
 *  用来限制文字输入框的文字总数，默认为NSIntegerMax
 */
@property (nonatomic, assign) NSUInteger maxLength;

/*
 *  文本框内的文字发生变动后的回调，包括调用【setText】方法对text的变更。
 */
@property (nonatomic, copy) void(^textModifiedHandler)(UIZWTextLimitView *view);

/**
 *  虚拟键盘上的【Return】键被点击后是否结束响应的回调
 *  返回值YES时，关闭键盘（即，取消第一响应）
 *  默认NO，点击键盘上的【Return】键可以换行
 *  ⚠️该block块受delegate重置的影响
 */
@property (nonatomic, copy) BOOL(^textViewShouldResignDidReturnBlock)(UIZWTextLimitView *view);

/**
 *  是否允许开始文本编辑
 *  默认YES
 *  ⚠️该block块受delegate重置的影响
 */
@property (nonatomic, copy) BOOL(^textViewShouldBeginEditingBlock)(UIZWTextLimitView *view);

/**
 *  是否允许结束文本编辑
 *  默认YES
 *  ⚠️该block块受delegate重置的影响
 */
@property (nonatomic, copy) BOOL(^textViewShouldEndEditingBlock)(UIZWTextLimitView *view);

/**
 *  文字即将变更时的回调
 */
@property (nonatomic, copy) void(^textViewDidBeginEditingBlock)(UIZWTextLimitView *view);

/**
 *  文字结束变更时的回调
 */
@property (nonatomic, copy) void(^textViewDidEndEditingBlock)(UIZWTextLimitView *view);

/**
 *  焦点发生改变的回调
 *  ⚠️该block块受delegate重置的影响
 */
@property (nonatomic, copy) void(^textViewDidChangeSelectionBlock)(UIZWTextLimitView *view);

/**
 @abstract 因项目开发人员流动性大，为了便于维护代码，不建议使用Xib和Storyboard创建UI，统一使用源码管理UI。
 
 @warning This method is unavaialble. Please use【-initWithStyle: reuseIdentifier:】OR【-initWithFrame:】OR【-init:】instead.
 */
- (instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE;

/**
 @abstract 该方法自带了指定构造器宏 NS_DESIGNATED_INITIALIZER
 什么是指定构造器宏？
 http://sejasonwang.github.io/2016/01/27/designated-initalizer/
 http://www.swiftyper.com/2016/08/02/understanding-designated-initializer-in-objective-c/
 http://www.jianshu.com/p/b142caa44382
 */
- (instancetype)initWithFrame:(CGRect)frame NS_DESIGNATED_INITIALIZER;

@end


/**
 * 该类为UITextView控件设置了可输入文字字数上限的属性
 *⚠️注意内部已默认配置了delegate属性，如果修改了该属性，会导致部分block受影响。
 */
