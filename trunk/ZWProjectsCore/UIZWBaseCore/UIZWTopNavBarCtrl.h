//
//  UIZWTopNavBarCtrl.h
//  ZWProjectsCore
//
//  Created by ZhangWei on 16/5/2.
//  Copyright © 2016年 ZhangWei. All rights reserved.
//

#import "UIZWBaseViewController.h"
#import "UIZWTopNavBar.h"

@interface UIZWTopNavBarCtrl : UIZWBaseViewController

/**
 *置于levelHighestView上。⚠️包含状态栏和导航栏
 */
@property (nonatomic, strong, readonly) UIImageView *topBoardView;
@property (nonatomic, strong, readonly) UIZWTopNavBar *topBar;//置于topBarBoardView上，但与mainView的顶部对齐
@property (nonatomic, assign, readonly) BOOL showTopBar;//默认为YES

- (void)setShowTopBar:(BOOL)showTopBar withAnimation:(BOOL)animation;

@end

/*
 该类控制器levelHighestView上已带自定义导航栏.
 所有置于mainView和fullCtrlView上的子视图都将显示在导航栏下方.
 UI排版布局时，其他显示控件top.equalTo置于topBoardView、topBarBoardView、topBar任一底边mas_bottom均可。
 */
