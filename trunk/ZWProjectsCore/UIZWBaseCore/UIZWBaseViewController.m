//
//  UIZWBaseViewController.m
//  ZWProjectsCore
//
//  Created by ZhangWei on 15/5/3.
//  Copyright (c) 2015年 ZhangWei. All rights reserved.
//

#import "UIZWBaseViewController.h"
#import <Masonry/Masonry.h>
#import "ZWProjectsCoreMacros.h"
#import "ZWProjectsCoreFunctions.h"

#if !__has_feature(objc_arc)
#error This class requires automatic reference counting
#endif

@interface UIZWBaseViewController ()

@property (nonatomic, assign) BOOL isVisible;
@property (nonatomic, strong) UIImageView *levelHighestView;
/// 在loadView方法调用之前充当levelHighestView属性的替身，在loadView方法调用之后才与levelHighestView属性合并
@property (nonatomic, strong) UIImageView *levelHighestImageView;

#pragma mark 以下视图为自定义UI依附的主要两种视图，一种带状态栏，一种不带状态栏，请自行选择使用
@property (nonatomic, strong) UIView *fullCtrlView;
@property (nonatomic, strong) UIView *mainView;

@property (nonatomic, strong) UIView *virtualKeyboardView;
@property (nonatomic, assign) CGFloat fVirtualKeyboardHeight;

#pragma mark 辅助关闭虚拟键盘的栏
@property (nonatomic, strong) UIZWCloseBoardBar *virtualKeyboardCloseBar;

@end

@implementation UIZWBaseViewController

#pragma mark - LifeCycle
- (void)dealloc;
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder;
{
    if (self = [super initWithCoder:aDecoder]) {
        [self baseViewControllerInit];
    }
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil;
{
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        [self baseViewControllerInit];
    }
    return self;
}

- (void)baseViewControllerInit;
{
    _isVisible = NO;
    
    /**
     iOS7以后系统趋向于尽可能完全利用有限的屏幕以展示更多的用户数据，所以UINavigationBar和UITabBar默认都是半透明模糊效果，UIScrollView中的内容在它们后面也是隐约可见的。
     在此情况下，系统对于作为controller view的第一个subView的全屏UIScrollView，会自动处理其contentInset，使其头部和尾部的内容起始和末尾不会被UINavigationBar和UITabBar挡住。
     automaticallyAdjustsScrollViewInsets默认为YES
     */
    //self.automaticallyAdjustsScrollViewInsets = NO;//iOS11已经被弃用
    
    /**
     指定视图边缘要延伸的方向，默认值为UIRectEdgeAll，四周边缘均延伸至全屏幕大小，就是说，如果即使视图中上有navigationBar，下有tabBar，那么视图仍会延伸覆盖到四周的区域。
     一般为了不让UIView延伸到它们下面，属性设置为 UIRectEdgeNone
     */
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    /**
     导航栏透明和半透明效果，translucent默认为YES；
     当edgesForExtendedLayout为UIRectEdgeNone时导航栏会变灰，translucent为NO即可解决该问题
     */
    self.navigationController.navigationBar.translucent = NO;
    
    _statusBarHidden = NO;
    _fVirtualKeyboardHeight = 0;
    _autoLayoutViewForVirtualKeyboard = NO;
    _showVirtualKeyboardCloseBar = YES;
    
#pragma mark 初始化控制器的各级视图
    _levelHighestImageView = [[UIImageView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [_levelHighestImageView setUserInteractionEnabled:YES];
    [_levelHighestImageView setContentMode:UIViewContentModeScaleAspectFill];
    [_levelHighestImageView setBackgroundColor:[UIColor whiteColor]];
    [_levelHighestImageView setClipsToBounds:YES];//此代码很重要，可在AddChildCtrl时得知
    
    _fullCtrlView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [_fullCtrlView setBackgroundColor:[UIColor clearColor]];
    [_levelHighestImageView addSubview:_fullCtrlView];
    
#pragma mark 初始化控制器的虚拟键盘相关视图
    _virtualKeyboardView = [[UIView alloc] init];
    [_virtualKeyboardView setBackgroundColor:[UIColor clearColor]];
    [_levelHighestImageView addSubview:_virtualKeyboardView];
    [_virtualKeyboardView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.levelHighestImageView);
        make.height.mas_equalTo([self getVirtualKeyboardViewHeight]);
    }];
    //注意_fullCtrlView视图的布局依赖_virtualKeyboardView，所以调整到这里布局
    [_fullCtrlView mas_makeConstraints:^(MASConstraintMaker *make){
        make.top.left.right.equalTo(self.levelHighestImageView);
        make.bottom.equalTo(self.virtualKeyboardView.mas_top);
    }];
    
    __weak typeof(self) wSelf = self;

    _virtualKeyboardCloseBar = [[UIZWCloseBoardBar alloc] init];
    _virtualKeyboardCloseBar.rightButtonBlock = ^(UIZWCloseBoardBar *view, UIButton *button) {
        [wSelf resignFirstResponder];
        zw_closeVirtualKeyboard();
    };
    [_virtualKeyboardView addSubview:_virtualKeyboardCloseBar];
    [_virtualKeyboardCloseBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(self.virtualKeyboardView);
        make.height.mas_equalTo(UIVirtualKeyboardCloseBarDefaultHeight);
    }];
    
#pragma mark 以下监听如果涉及到性能问题，可以通过属性开关来选择控制达到优化效果。
#pragma mark 应用程序常用的通知监听
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(observeApplicationSignificantTimeChange:) name:UIApplicationSignificantTimeChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(observeApplicationResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(observeApplicationDidEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(observeApplicationWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(observeApplicationBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(observeApplicationUserDidTakeScreenShot:) name:UIApplicationUserDidTakeScreenshotNotification object:nil];
}

//💡该接口仅供外部其他模块调用，内部方法请通过指针调用
- (UIImageView*)levelHighestView;
{
    return _levelHighestImageView;
}

/*
 loadView
 1.什么时候被调用？
 每次访问UIViewController的view(比如controller.view、self.view)而且view为nil，loadView方法就会被调用。
 2.有什么作用？
 loadView方法是用来负责创建UIViewController的view
 
 在控制器加载view的时候,系统默认做法:访问self.view如果view==nil就会调用loadView方法,所以会产生以下需要特别注意的地方:
 首先系统默认做法,访问self.view,如果view==nil就会调用loadView方法
 系统底层view的get方法是通知loadView方法创建控制器的view.
 1.判断当前的控制器是否从StoryBoard加载的.如果是,从StoryBoad当中创建view.
 2.如果不是,还会判断当前控制器,是否有xib进行描述.如果有,就会从Xib里面创建View
 3.如果也不是xib来描述的,那么它就会给创建一个空的UIView.
 在开发当中一旦重写了loadView,需要自己创建View.
 重写loadView一般用在:
 1.当控制器的view一进来就是显示一个图片时
 2.当控制器一进来就去加载一个网页时.
 特别注意:loadView 重写时不调用[super loadView]容易访问到空view;
 而访问self.view,会访问空的view,会导致死循环exc_bad_access(code = 2....)的错误
 */
- (void)loadView;
{
    //特别注意:如果view为nil,调用self.view方法会造成死循环
    _levelHighestView = _levelHighestImageView;
    self.view = _levelHighestView;
    [self checkLevelViewsLayout];
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
}

- (void)setStatusBarHidden:(BOOL)statusBarHidden;
{
    if (_statusBarHidden != statusBarHidden)
    {
        _statusBarHidden = statusBarHidden;
        [self setNeedsStatusBarAppearanceUpdate];
        
        if (_levelHighestView)
        {
            [self checkLevelViewsLayout];
            [self.view setNeedsLayout];
            [self.view layoutIfNeeded];
        }
    }
}

- (CGFloat)getStatusBarHeight;
{
    //不能使用系统获取的状态栏高度，因为他们在通话、录音、个人设备热点时返回的高度不再是20，而是40
    //CGFloat statusBarHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
    CGFloat statusBarHeight = 20;
    if(self.statusBarHidden)
    {
        statusBarHeight = 0;
    }
    else
    {
        //iPhone X在横屏时，即使设置状态栏显示，但状态栏还是会隐藏，所以safeAreaInset.top为0，其他已知设备的状态栏还是保持20高度；
        //iPhone X在竖屏时，safeAreaInset.top为44，其他已知设备的状态栏还是保持20高度；
        //综上，通过读取self.view.safeAreaInsets.top可以完美解决适配状态栏的高度问题
        if (@available(iOS 11.0, *))
        {
            //iPhoneX获取的safeAreaInsets正常，但在其他设备上获取的safeAreaInsets全为UIEdgeInsetsZero
            if (zw_isNoHomeButtonStyleScreen())
            {
                /// 此处使用self.view.safeAreaInsets.top，有时会出现错误值。
                /// 如竖屏进入iPhone X应用的控制器A，此时值为44，push到控制器B，后旋转成横屏，虽然控制器B此值为0，但此时控制器A此值仍然是44，
                /// 导致返回到控制器A时，横屏下的A顶部仍然后44高度的空白。所以还是读取keywindow的值保险
                /// 刘海款式iPhone竖屏 safeAreaInsets = (top = 44, left = 0, bottom = 34, right = 0)
                /// 刘海款式iPhone横屏 safeAreaInsets = (top = 0, left = 44, bottom = 21, right = 44)
                /// 刘海款式iPadPro竖屏 safeAreaInsets = (top = 24, left = 0, bottom = 20, right = 0)
                /// 刘海款式iPadPro横屏 safeAreaInsets = (top = 24, left = 0, bottom = 20, right = 0)
                UIWindow* keywindow = zw_sharedApplicationWindow();
                statusBarHeight = keywindow.safeAreaInsets.top;
            } else {
                /// 以下代码为了解决iOS13系统开始，所有设备在横屏时状态栏强制隐藏的问题。
                if (@available(iOS 13.0, *)) {
                    statusBarHeight = zw_sharedApplication().windows.firstObject.windowScene.statusBarManager.statusBarFrame.size.height;
                } else {
                    statusBarHeight = zw_sharedApplication().statusBarFrame.size.height;
                }
            }
        }
    }
    return ceil(statusBarHeight);
}

- (CGFloat)getVirtualKeyboardViewHeight;
{
    CGFloat currentHeight = 0;
    if (self.autoLayoutViewForVirtualKeyboard && self.isFirstResponder)
    {
        currentHeight += self.fVirtualKeyboardHeight;
        if (self.showVirtualKeyboardCloseBar && self.fVirtualKeyboardHeight > 0)
        {
            self.virtualKeyboardCloseBar.hidden = NO;
            currentHeight += UIVirtualKeyboardCloseBarDefaultHeight;
        }
        else
        {
            self.virtualKeyboardCloseBar.hidden = YES;
        }
    }
    return ceil(currentHeight);
}

- (BOOL)prefersStatusBarHidden;
{
    return self.statusBarHidden;
}

- (UIStatusBarStyle)preferredStatusBarStyle;
{
    return UIStatusBarStyleDefault;
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation
{
    return UIStatusBarAnimationFade;
}

- (void)setAutoLayoutViewForVirtualKeyboard:(BOOL)autoLayoutViewForVirtualKeyboard;
{
    if (_autoLayoutViewForVirtualKeyboard != autoLayoutViewForVirtualKeyboard)
    {
        _autoLayoutViewForVirtualKeyboard = autoLayoutViewForVirtualKeyboard;
        
        if (_autoLayoutViewForVirtualKeyboard)
        {
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(observeApplicationKeyboardWillShow:)name:UIKeyboardWillShowNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(observeApplicationKeyboardShow:)name:UIKeyboardDidShowNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(observeApplicationKeyboardWillChangeFrame:) name:UIKeyboardWillChangeFrameNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(observeApplicationKeyboardChangeFrame:) name:UIKeyboardDidChangeFrameNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(observeApplicationKeyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(observeApplicationKeyboardHide:) name:UIKeyboardDidHideNotification object:nil];
        }
        else
        {
            [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
            [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
            [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
            [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidChangeFrameNotification object:nil];
            [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
            [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
        }
        
        if (_levelHighestView)
        {
            [self checkLevelViewsLayout];
            [self.view setNeedsLayout];
            [self.view layoutIfNeeded];
        }
    }
}

- (void)setShowVirtualKeyboardCloseBar:(BOOL)showVirtualKeyboardCloseBar;
{
    if (_showVirtualKeyboardCloseBar != showVirtualKeyboardCloseBar)
    {
        _showVirtualKeyboardCloseBar = showVirtualKeyboardCloseBar;
        
        if (_levelHighestView && self.autoLayoutViewForVirtualKeyboard)
        {
            [self checkLevelViewsLayout];
            [self.view setNeedsLayout];
            [self.view layoutIfNeeded];
        }
    }
}

- (void)checkLevelViewsLayout;
{
    [_virtualKeyboardView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo([self getVirtualKeyboardViewHeight]);
    }];
    
    [_mainView mas_updateConstraints:^(MASConstraintMaker *make){
        make.top.equalTo(self.fullCtrlView.mas_top).offset([self getStatusBarHeight]);
    }];
}

- (void)viewWillAppear:(BOOL)animated;
{
    [super viewWillAppear:animated];
    
    if (self.autoLayoutViewForVirtualKeyboard)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(observeApplicationKeyboardWillShow:)name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(observeApplicationKeyboardShow:)name:UIKeyboardDidShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(observeApplicationKeyboardWillChangeFrame:) name:UIKeyboardWillChangeFrameNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(observeApplicationKeyboardChangeFrame:) name:UIKeyboardDidChangeFrameNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(observeApplicationKeyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(observeApplicationKeyboardHide:) name:UIKeyboardDidHideNotification object:nil];
    }
    
    self.isVisible = YES;
}

- (void)viewWillDisappear:(BOOL)animated;
{
    [super viewWillDisappear:animated];
    
    if (self.autoLayoutViewForVirtualKeyboard)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidChangeFrameNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
    }
    
    self.isVisible = NO;
}

- (void)viewDidDisappear:(BOOL)animated;
{
    [super viewDidDisappear:animated];
    
    //页面跳转时虚拟键盘会自动关闭（通知虚拟键盘隐藏时UI做相应的显示变更）
    [self applicationKeyboardHide:nil];
}

- (BOOL)isFirstResponder;
{
    return YES;
}

#pragma mark - Getter/Setting
- (UIView *)mainView {
    if (!_mainView) {
        _mainView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        [_mainView setBackgroundColor:[UIColor clearColor]];
        /// 直接addSubview可能会盖住先前附加在fullCtrlView上的子视图，而无法和他们进行交互。
        [self.fullCtrlView insertSubview:_mainView atIndex:0];
        [_mainView mas_makeConstraints:^(MASConstraintMaker *make){
            make.top.equalTo(self.fullCtrlView.mas_top).offset([self getStatusBarHeight]);
            make.left.right.bottom.equalTo(self.fullCtrlView);
        }];
    }
    return _mainView;
}

#pragma mark - 逻辑方法

- (void)applicationSignificantTimeChange:(NSNotification*)aNotification;
{
}

- (void)applicationResignActive:(NSNotification*)aNotification;
{
}

- (void)applicationDidEnterBackground:(NSNotification*)aNotification;
{
}

- (void)applicationWillEnterForeground:(NSNotification*)aNotification;
{
}

- (void)applicationBecomeActive:(NSNotification*)aNotification;
{
}

- (void)applicationUserDidTakeScreenShot:(NSNotification*)aNotification;
{
}

- (void)applicationKeyboardWillShow:(NSNotification*)aNotification;
{
    NSDictionary *userInfo = [aNotification userInfo];
    CGRect rect = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat keyboardDuration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve keyboardAnimation = [userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    CGFloat virtualKeyboardHeight = CGRectGetHeight(rect);
    self.fVirtualKeyboardHeight = floor(virtualKeyboardHeight);
    
    if (_levelHighestView && self.autoLayoutViewForVirtualKeyboard && self.isFirstResponder)
    {
        [self checkLevelViewsLayout];
        
        [UIView animateWithDuration:keyboardDuration
                         animations:^ {
            [UIView setAnimationBeginsFromCurrentState:YES];
            [UIView setAnimationCurve:keyboardAnimation];
            [self.view setNeedsLayout];
            [self.view layoutIfNeeded];
        }];
    }
}

- (void)applicationKeyboardShow:(NSNotification*)aNotification;
{
    NSDictionary *userInfo = [aNotification userInfo];
    CGRect rect = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    CGFloat virtualKeyboardHeight = CGRectGetHeight(rect);
    self.fVirtualKeyboardHeight = floor(virtualKeyboardHeight);
    
    if (_levelHighestView && self.autoLayoutViewForVirtualKeyboard && self.isFirstResponder)
    {
        [self checkLevelViewsLayout];
        [self.view setNeedsLayout];
        [self.view layoutIfNeeded];
    }
}

- (void)applicationKeyboardWillChangeFrame:(NSNotification*)aNotification;
{
    NSDictionary *userInfo = [aNotification userInfo];
    CGRect rect = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    CGFloat virtualKeyboardHeight = CGRectGetHeight(rect);
    self.fVirtualKeyboardHeight = floor(virtualKeyboardHeight);
    
    if (_levelHighestView && self.autoLayoutViewForVirtualKeyboard && self.isFirstResponder)
    {
        [self checkLevelViewsLayout];
        [self.view setNeedsLayout];
        [self.view layoutIfNeeded];
    }
}

- (void)applicationKeyboardChangeFrame:(NSNotification*)aNotification;
{
    NSDictionary *userInfo = [aNotification userInfo];
    CGRect rect = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    CGFloat virtualKeyboardHeight = CGRectGetHeight(rect);
    self.fVirtualKeyboardHeight = floor(virtualKeyboardHeight);
    
    if (_levelHighestView && self.autoLayoutViewForVirtualKeyboard && self.isFirstResponder)
    {
        [self checkLevelViewsLayout];
        [self.view setNeedsLayout];
        [self.view layoutIfNeeded];
    }
}

- (void)applicationKeyboardWillHide:(NSNotification*)aNotification;
{
    self.fVirtualKeyboardHeight = 0;
    
    if (_levelHighestView && self.autoLayoutViewForVirtualKeyboard)
    {
        [self checkLevelViewsLayout];
        [self.view setNeedsLayout];
        [self.view layoutIfNeeded];
    }
}

- (void)applicationKeyboardHide:(NSNotification*)aNotification;
{
    self.fVirtualKeyboardHeight = 0;
    
    if (_levelHighestView && self.autoLayoutViewForVirtualKeyboard)
    {
        [self checkLevelViewsLayout];
        [self.view setNeedsLayout];
        [self.view layoutIfNeeded];
    }
}

- (void)backToPreViewCtrl:(BOOL)animation;
{
    //页面跳转时虚拟键盘会自动关闭（通知虚拟键盘隐藏时UI做相应的显示变更）
    [self applicationKeyboardHide:nil];
    
    if (self.presentingViewController)
    {
        if ([self.navigationController.viewControllers count] >= 2)
        {
            [self.navigationController popViewControllerAnimated:animation];
        }
        else
        {
            [self dismissViewControllerAnimated:animation completion:nil];
        }        
    }
    else
    {
        if ([self.navigationController.viewControllers count] >= 2)
        {
            [self.navigationController popViewControllerAnimated:animation];
        }
    }
}

- (void)orientationToPortrait:(UIInterfaceOrientation)orientation;
{
    SEL selector = NSSelectorFromString(@"setOrientation:");
    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[UIDevice instanceMethodSignatureForSelector:selector]];
    [invocation setSelector:selector];
    [invocation setTarget:[UIDevice currentDevice]];
    NSInteger val = orientation;
    [invocation setArgument:&val atIndex:2];//前两个参数已被target和selector占用
    [invocation invoke];
}

- (BOOL)shouldAutorotate;
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations;
{
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation;
{
    return UIInterfaceOrientationPortrait;
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id <UIViewControllerTransitionCoordinator>)coordinator;
{
    __weak typeof(self) wSelf = self;
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
        NSLog(@"转屏前调入");
    }
                                 completion:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
        NSLog(@"转屏后调入");
        [wSelf checkLevelViewsLayout];
        [wSelf.view setNeedsLayout];
        [wSelf.view layoutIfNeeded];
    }];
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
}

#pragma mark - ObserveNotifications
- (void)observeApplicationSignificantTimeChange:(NSNotification*)aNotification;
{
    [self applicationSignificantTimeChange:aNotification];
}

- (void)observeApplicationResignActive:(NSNotification*)aNotification;
{
    [self applicationResignActive:aNotification];
}

- (void)observeApplicationDidEnterBackground:(NSNotification*)aNotification;
{
    [self applicationDidEnterBackground:aNotification];
}

- (void)observeApplicationWillEnterForeground:(NSNotification*)aNotification;
{
    [self applicationWillEnterForeground:aNotification];
}

- (void)observeApplicationBecomeActive:(NSNotification*)aNotification;
{
    [self applicationBecomeActive:aNotification];
}

- (void)observeApplicationUserDidTakeScreenShot:(NSNotification*)aNotification;
{
    if (self.isViewLoaded && self.view.window && self.isVisible)
    {
        [self applicationUserDidTakeScreenShot:aNotification];
    }
}

- (void)observeApplicationKeyboardWillShow:(NSNotification*)aNotification;
{
    if (self.isViewLoaded && self.view.window && self.isVisible)
    {
        [self applicationKeyboardWillShow:aNotification];
    }
}

- (void)observeApplicationKeyboardShow:(NSNotification*)aNotification;
{
    if (self.isViewLoaded && self.view.window && self.isVisible)
    {
        [self applicationKeyboardShow:aNotification];
    }
}

- (void)observeApplicationKeyboardWillChangeFrame:(NSNotification*)aNotification;
{
    if (self.isViewLoaded && self.view.window && self.isVisible)
    {
        [self applicationKeyboardWillChangeFrame:aNotification];
    }
}

- (void)observeApplicationKeyboardChangeFrame:(NSNotification*)aNotification;
{
    if (self.isViewLoaded && self.view.window && self.isVisible)
    {
        [self applicationKeyboardChangeFrame:aNotification];
    }
}

- (void)observeApplicationKeyboardWillHide:(NSNotification*)aNotification;
{
    if (self.isViewLoaded && self.view.window && self.isVisible)
    {
        [self applicationKeyboardWillHide:aNotification];
    }
}

- (void)observeApplicationKeyboardHide:(NSNotification*)aNotification;
{
    if (self.isViewLoaded && self.view.window && self.isVisible)
    {
        [self applicationKeyboardHide:aNotification];
    }
}

@end
