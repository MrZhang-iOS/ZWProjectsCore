//
//  UIZWBaseCollectionViewCell.m
//
//  Created by ZhangWei on 2020/10/16.
//  Copyright © 2020 ZhangWei. All rights reserved.
//

#import "UIZWBaseCollectionViewCell.h"
#import <Masonry/Masonry.h>

@interface UIZWBaseCollectionViewCell ()

@property (nonatomic, strong) UIView *customContentView;

@end

@implementation UIZWBaseCollectionViewCell

+ (CGSize)perfectSize;
{
    return CGSizeMake(80, 44);
}

- (instancetype)initWithFrame:(CGRect)frame;
{
    if (self = [super initWithFrame:frame])
    {
        self.backgroundColor = [UIColor clearColor];
        
        _customInsets = UIEdgeInsetsZero;
        
        _customContentView = [[UIView alloc] init];
        [self addSubview:_customContentView];
        [_customContentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self).insets(self.customInsets);
        }];
    }
    return self;
}

- (void)setCustomInsets:(UIEdgeInsets)customInsets;
{
    if (!UIEdgeInsetsEqualToEdgeInsets(_customInsets, customInsets))
    {
        _customInsets = customInsets;
        [_customContentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self).insets(self.customInsets);
        }];
    }
}

@end
