//
//  UIZWBaseLabel.h
//
//  Created by ZhangWei on 2020/7/27.
//  Copyright © 2020 ZhangWei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIZWBaseLabel : UILabel

//MARK: 支持关键字高亮显示
@property (nonatomic, strong) UIColor *highlightedKeywordsColor;//高亮关键字的颜色，默认使用系统亮色文字，即[UIColor lightTextColor]
@property (nonatomic, strong) UIFont *highlightedKeywordsFont;//高亮关键字的字体，默认使用title当前字体
@property (nonatomic, copy) NSString *highlightedKeywords;//高亮关键字。如果highlightedKeywords属性有值且长度大于0时，调用【setText:】方法将自动设置关键字高亮显示

@end

/*
 该控件的的设计目的：
 UILabel上的text如果包含了指定的关键字，可以使关键字区别于text按指定的颜色和字体显示。
 */
