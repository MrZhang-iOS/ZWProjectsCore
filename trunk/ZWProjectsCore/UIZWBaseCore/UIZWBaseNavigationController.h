//
//  UIZWBaseNavigationController.h
//  ZWProjectsCore
//
//  Created by ZhangWei on 16/5/30.
//  Copyright © 2016年 ZhangWei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIZWBaseNavigationController : UINavigationController

@property (nonatomic, assign) BOOL slipRightToPopEnable;//默认为YES，从边缘向右滑动可返回上级控制器

@end

/*
 默认隐藏了顶部导航栏和底部工具栏
 */
