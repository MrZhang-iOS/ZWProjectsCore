//
//  UIZWTopNavBarCtrl.m
//  ZWProjectsCore
//
//  Created by ZhangWei on 16/5/2.
//  Copyright © 2016年 ZhangWei. All rights reserved.
//

#import "UIZWTopNavBarCtrl.h"
#import <Masonry/Masonry.h>
#import "ZWProjectsCoreMacros.h"

@interface UIZWTopNavBarCtrl ()

@property (nonatomic, strong) UIImageView *topBoardView;
@property (nonatomic, strong) UIView *topBarBoardView;//置于topBoardView上。⚠️仅包含导航栏(用来管理topBar，控制它的显示或隐藏的动画)
@property (nonatomic, strong) UIZWTopNavBar *topBar;

@end

@implementation UIZWTopNavBarCtrl

- (instancetype)init;
{
    if (self = [super init])
    {
        _showTopBar = YES;
        
        _topBoardView = [[UIImageView alloc] init];
        _topBoardView.userInteractionEnabled = YES;
        [_topBoardView setBackgroundColor:[UIColor clearColor]];
        [self.levelHighestView addSubview:_topBoardView];
        [_topBoardView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.equalTo(self.levelHighestView);
        }];
        
        _topBarBoardView = [[UIView alloc] init];
        _topBarBoardView.clipsToBounds = YES;
        [_topBarBoardView setBackgroundColor:[UIColor clearColor]];
        [_topBoardView addSubview:_topBarBoardView];
        [_topBarBoardView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.bottom.right.equalTo(self.topBoardView);
            make.top.equalTo(self.mainView);
        }];
        
        __weak typeof(self) wSelf = self;
        _topBar = [[UIZWTopNavBar alloc] init];
        [_topBar setBackgroundColor:[UIColor clearColor]];
        _topBar.leftButtonHandler = ^(UIZWTopNavBar* view, UIButton* button){
            [wSelf backToPreViewCtrl:YES];
        };
        [_topBarBoardView addSubview:_topBar];
        [_topBar mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.topBarBoardView);
            make.top.equalTo(self.topBarBoardView).offset(self.showTopBar?0:-MyMacro_NavigationBarViewHeight);
            make.height.mas_equalTo(MyMacro_NavigationBarViewHeight);
            make.bottom.equalTo(self.topBarBoardView);
        }];
    }
    return self;
}

- (void)setTitle:(NSString *)title;
{
    [super setTitle:title];
    _topBar.titleLabel.text = self.title;
}

- (void)setShowTopBar:(BOOL)showTopBar withAnimation:(BOOL)animation;
{
    if (_showTopBar != showTopBar)
    {
        _showTopBar = showTopBar;
        
        if (animation)
        {
            self.topBoardView.hidden = NO;//为了能看到动画效果，需要先取消隐藏
            __weak typeof(self) wSelf = self;
            [UIView animateWithDuration:0.2
                             animations:^
            {
                [wSelf.view setNeedsUpdateConstraints];
                [wSelf.view updateConstraintsIfNeeded];
                [wSelf.view setNeedsLayout];
                [wSelf.view layoutIfNeeded];
            }
                             completion:^(BOOL finished)
            {
                wSelf.topBoardView.hidden = !(wSelf.showTopBar);
            }];
        }
        else
        {
            self.topBoardView.hidden = !_showTopBar;
        }
    }
}

- (void)updateViewConstraints;
{
    [_topBar mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.topBarBoardView).offset(self.showTopBar?0:-MyMacro_NavigationBarViewHeight);
    }];
    [super updateViewConstraints];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id <UIViewControllerTransitionCoordinator>)coordinator;
{
    __weak typeof(self) wSelf = self;
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context)
     {
         ;
     }
                                 completion:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context)
     {
         [wSelf.view setNeedsUpdateConstraints];
         [wSelf.view updateConstraintsIfNeeded];
         [wSelf.view setNeedsLayout];
         [wSelf.view layoutIfNeeded];
     }];
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
}

@end
