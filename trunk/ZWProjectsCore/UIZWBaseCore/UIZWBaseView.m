//
//  UIZWBaseView.m
//  ZWProjectsCore
//
//  Created by ZhangWei on 16/1/6.
//  Copyright © 2016年 ZhangWei. All rights reserved.
//

#import "UIZWBaseView.h"

@implementation UIZWBaseView

#pragma mark - 布局方法调用过程
#pragma mark【init -> updateConstraints -> layoutSubViews -> drawRect】
#pragma mark 在view的layoutSubViews方法以后的方法可以拿到view的实际frame，所以当我们真的需要frame的时候需要在这个时间点以后才能拿到

- (instancetype)initWithFrame:(CGRect)frame;
{
    if (self = [super initWithFrame:frame])
    {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

//在这里为你的view重新添加约束
//然后调用父类的updateViewConstraints方法
//- (void)updateConstraints;

//改变frame.origin不会掉用layoutSubviews
//改变frame.size会调用superView的layoutSubviews方法
//改变bounds.origin和bounds.size都会调用superView和自己view的layoutSubviews方法
//UIView启动时，此处日志self.frame值有效，但子视图的值无效，可放开以下日志查看
//最好该方法后获取frame值，即先调用[self setNeedsLayout]和[self layoutIfNeeded]两个方法
//- (void)layoutSubviews;

@end
