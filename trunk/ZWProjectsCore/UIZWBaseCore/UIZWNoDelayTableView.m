//
//  UIZWNoDelayTableView.m
//  ZWProjectsCore
//
//  Created by ZhangWei on 16/8/7.
//  Copyright © 2016年 ZhangWei. All rights reserved.
//

#import "UIZWNoDelayTableView.h"

@implementation UIZWNoDelayTableView

- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style
{
    if (self = [super initWithFrame:frame style:style])
    {
        [self setDelaysContentTouches:NO];
        for (id view in self.subviews) {
            if ([NSStringFromClass([view class]) isEqualToString:@"UITableViewWrapperView"]) {
                if ([view isKindOfClass:[UIScrollView class]]) {
                    UIScrollView *scrollView = (UIScrollView *) view;
                    scrollView.delaysContentTouches = NO;
                }
                break;
            }
        }
    }
    return self;
}

- (BOOL)touchesShouldCancelInContentView:(UIView *)view;
{
    if ([view isKindOfClass:[UIButton class]])
    {
        return YES;
    }
    return [super touchesShouldCancelInContentView:view];
}

@end
