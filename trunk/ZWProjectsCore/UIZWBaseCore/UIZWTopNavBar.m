//
//  UIZWTopNavBar.m
//  ZWProjectsCore
//
//  Created by ZhangWei on 16/5/1.
//  Copyright © 2016年 ZhangWei. All rights reserved.
//

#import "UIZWTopNavBar.h"
#import <Masonry/Masonry.h>
#import "ZWProjectsCoreMacros.h"
#import "UIFont+ZhangWei.h"
#import "UIButton+ZhangWei.h"
#import "UIImage+ZhangWei.h"
#import <KVOController/KVOController.h>

@interface UIZWTopNavBar ()

@property (nonatomic, strong) UIButton *leftButton;
@property (nonatomic, strong) UIButton *rightButton;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIImageView *separateLine;

@property (nonatomic, strong) FBKVOController *leftButtonKVOManager;
@property (nonatomic, strong) FBKVOController *rightButtonKVOManager;

@end

@implementation UIZWTopNavBar

- (void)dealloc;
{
    [self.leftButtonKVOManager unobserveAll];
    [self.rightButtonKVOManager unobserveAll];
}

- (instancetype)initWithFrame:(CGRect)frame;
{
    if (self = [super initWithFrame:frame])
    {
        __weak typeof(self) wSelf = self;
        
        self.backgroundColor = [UIColor clearColor];
        
        _separateLineHeight = MyMacro_SeperateLine_Height;
        _leftButtonSize = CGSizeZero;
        _leftButtonOffset = CGPointZero;
        _rightButtonSize = CGSizeZero;
        _rightButtonOffset = CGPointZero;
        
        _leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_leftButton zw_expandEventSize:CGSizeMake(10, 10)];
        [_leftButton setBackgroundColor:[UIColor clearColor]];
        [_leftButton setTitle:@"返回" forState:UIControlStateNormal];
        [_leftButton setTitleColor:MyMacro_Color_SystemBlue forState:UIControlStateNormal];
        [_leftButton setTitleColor:MyMacro_Color_SystemLine forState:UIControlStateHighlighted];
        [_leftButton setTitleColor:MyMacro_Color_SystemLine forState:UIControlStateDisabled];
        [_leftButton setTitleColor:MyMacro_Color_SystemLine forState:UIControlStateSelected];
        [_leftButton.titleLabel setFont:[UIFont boldSystemFontOfSize:15]];
        [_leftButton addTarget:self action:@selector(leftButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_leftButton];
        [_leftButton mas_makeConstraints:^(MASConstraintMaker *make) {
            if (@available(iOS 11.0, *))
            {
                make.left.equalTo(self.mas_safeAreaLayoutGuideLeft).offset(AdaptWidth(10)+self.leftButtonOffset.x);
            }
            else
            {
                make.left.equalTo(self).offset(AdaptWidth(10)+self.leftButtonOffset.x);
            }
            make.centerY.equalTo(self.mas_centerY).offset(self.leftButtonOffset.y);
        }];
        
        _rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_rightButton zw_expandEventSize:CGSizeMake(10, 10)];
        [_rightButton setHidden:YES];
        [_rightButton setBackgroundColor:[UIColor clearColor]];
        [_rightButton setTitleColor:MyMacro_Color_SystemBlue forState:UIControlStateNormal];
        [_rightButton setTitleColor:MyMacro_Color_SystemLine forState:UIControlStateHighlighted];
        [_rightButton setTitleColor:MyMacro_Color_SystemLine forState:UIControlStateDisabled];
        [_rightButton setTitleColor:MyMacro_Color_SystemLine forState:UIControlStateSelected];
        [_rightButton.titleLabel setFont:[UIFont boldSystemFontOfSize:15]];
        [_rightButton addTarget:self action:@selector(rightButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_rightButton];
        [_rightButton mas_makeConstraints:^(MASConstraintMaker *make) {
            if (@available(iOS 11.0, *))
            {
                make.right.equalTo(self.mas_safeAreaLayoutGuideRight).offset(-AdaptWidth(10)+self.rightButtonOffset.x);
            }
            else
            {
                make.right.equalTo(self).offset(-AdaptWidth(10)+self.rightButtonOffset.x);
            }
            make.centerY.equalTo(self.mas_centerY).offset(self.rightButtonOffset.y);
        }];
        
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont boldSystemFontOfSize:17];
        _titleLabel.textColor = MyMacro_Color_Black;
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        [self insertSubview:_titleLabel belowSubview:_leftButton];
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.greaterThanOrEqualTo(self.leftButton.mas_right).offset(AdaptWidth(5));
            make.right.lessThanOrEqualTo(self.rightButton.mas_right).offset(-AdaptWidth(5));
            make.centerX.equalTo(self).priorityLow();
            make.top.bottom.equalTo(self);
        }];
        
        _separateLine = [[UIImageView alloc] init];
        [_separateLine setImage:[UIImage zw_createImageWithColor:MyMacro_Color_SystemLine]];
        [self addSubview:_separateLine];
        [_separateLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self);
            make.height.mas_equalTo(self.separateLineHeight);
        }];
        
        [self.leftButton setContentCompressionResistancePriority:UILayoutPriorityRequired
                                                         forAxis:UILayoutConstraintAxisHorizontal];
        [self.rightButton setContentCompressionResistancePriority:UILayoutPriorityRequired
                                                          forAxis:UILayoutConstraintAxisHorizontal];
        [self.titleLabel setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh
                                                         forAxis:UILayoutConstraintAxisHorizontal];
        [self.leftButton setContentHuggingPriority:UILayoutPriorityRequired
                                           forAxis:UILayoutConstraintAxisHorizontal];
        [self.rightButton setContentHuggingPriority:UILayoutPriorityRequired
                                            forAxis:UILayoutConstraintAxisHorizontal];
        [self.titleLabel setContentHuggingPriority:UILayoutPriorityDefaultHigh
                                           forAxis:UILayoutConstraintAxisHorizontal];
        
        _leftButtonKVOManager = [[FBKVOController alloc] initWithObserver:self];
        [_leftButtonKVOManager observe:self.leftButton
                              keyPaths:@[@"hidden"]
                               options:NSKeyValueObservingOptionNew
                                 block:^(id  _Nullable observer, id  _Nonnull object, NSDictionary<NSString *,id> * _Nonnull change)
         {
             if (wSelf.leftButton.hidden)
             {
                 [wSelf.titleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
                     make.left.greaterThanOrEqualTo(wSelf.leftButton.mas_left).offset(AdaptWidth(5));
                 }];
             }
             else
             {
                 [wSelf.titleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
                     make.left.greaterThanOrEqualTo(wSelf.leftButton.mas_right).offset(AdaptWidth(5));
                 }];
             }
         }];
        
        _rightButtonKVOManager = [[FBKVOController alloc] initWithObserver:self];
        [_rightButtonKVOManager observe:self.rightButton
                              keyPaths:@[@"hidden"]
                               options:NSKeyValueObservingOptionNew
                                 block:^(id  _Nullable observer, id  _Nonnull object, NSDictionary<NSString *,id> * _Nonnull change)
         {
             if (wSelf.rightButton.hidden)
             {
                 [wSelf.titleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
                     make.right.lessThanOrEqualTo(wSelf.rightButton.mas_right).offset(-AdaptWidth(5));
                 }];
             }
             else
             {
                 [wSelf.titleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
                     make.right.lessThanOrEqualTo(wSelf.rightButton.mas_left).offset(-AdaptWidth(5));
                 }];
             }
         }];
    }
    return self;
}

- (void)setLeftButtonSize:(CGSize)leftButtonSize;
{
    if (!CGSizeEqualToSize(_leftButtonSize, leftButtonSize))
    {
        _leftButtonSize = leftButtonSize;
        
        [_leftButton mas_updateConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(leftButtonSize);
        }];
    }
}

- (void)setLeftButtonOffset:(CGPoint)leftButtonOffset;
{
    if (!CGPointEqualToPoint(_leftButtonOffset, leftButtonOffset))
    {
        _leftButtonOffset = leftButtonOffset;
        
        [_leftButton mas_updateConstraints:^(MASConstraintMaker *make) {
            if (@available(iOS 11.0, *))
            {
                make.left.equalTo(self.mas_safeAreaLayoutGuideLeft).offset(AdaptWidth(10)+self.leftButtonOffset.x);
            }
            else
            {
                make.left.equalTo(self).offset(AdaptWidth(10)+self.leftButtonOffset.x);
            }
            make.centerY.equalTo(self.mas_centerY).offset(self.leftButtonOffset.y);
        }];
    }
}

- (void)setRightButtonSize:(CGSize)rightButtonSize
{
    if (!CGSizeEqualToSize(_rightButtonSize, rightButtonSize))
    {
        _rightButtonSize = rightButtonSize;
        
        [_rightButton mas_updateConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(rightButtonSize);
        }];
    }
}

- (void)setRightButtonOffset:(CGPoint)rightButtonOffset;
{
    if (!CGPointEqualToPoint(_rightButtonOffset, rightButtonOffset))
    {
        _rightButtonOffset = rightButtonOffset;
        
        [_rightButton mas_updateConstraints:^(MASConstraintMaker *make) {
            if (@available(iOS 11.0, *))
            {
                make.right.equalTo(self.mas_safeAreaLayoutGuideRight).offset(-AdaptWidth(10)+self.rightButtonOffset.x);
            }
            else
            {
                make.right.equalTo(self).offset(-AdaptWidth(10)+self.rightButtonOffset.x);
            }
            make.centerY.equalTo(self.mas_centerY).offset(self.rightButtonOffset.y);
        }];
    }
}

- (void)setSeparateLineHeight:(NSInteger)separateLineHeight;
{
    if (_separateLineHeight != separateLineHeight)
    {
        _separateLineHeight = separateLineHeight;
        
        [_separateLine mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(self.separateLineHeight);
        }];
    }
}

#pragma mark - PrivateFunctions
- (void)leftButtonAction:(UIButton*)sender;
{
    if (self.leftButtonHandler)
    {
        self.leftButtonHandler(self, sender);
    }
}

- (void)rightButtonAction:(UIButton*)sender;
{
    if (self.rightButtonHandler)
    {
        self.rightButtonHandler(self, sender);
    }
}

@end
