//
//  UIZWBaseCollectionReusableView.h
//
//  Created by ZhangWei on 2020/10/16.
//  Copyright © 2020 ZhangWei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIZWBaseCollectionReusableView : UICollectionReusableView

//MARK: 自定义的可缩进内容视图
//自定义的内容视图，可配合customInsets属性使用。
@property (nonatomic, strong, readonly) UIView *customContentView;
//控制自定义内容视图customContentView上子控件的缩进值。 默认为UIEdgeInsetsZero。
@property (nonatomic, assign) UIEdgeInsets customInsets;

//横向滚动时height值无效
//纵向滚动时height值无效
+ (CGSize)perfectSize;

@end

/*
 该控件的的设计目的：
 1.UICollectionView上自定义的头尾视图，相同控件在不同的页面可能需要缩进等操作
 2.为了UICollectionView快速流畅的滚动，提供了默认完美高度的类方法，也可以继承后重写此方法
 */
