//
//  UIZWUniversalCell.m
//  ZWProjectTemplate
//
//  Created by ZhangWei on 2018/8/14.
//  Copyright © 2018年 ZhangWei. All rights reserved.
//

#import "UIZWUniversalCell.h"
#import <Masonry/Masonry.h>

CGFloat const UIZWUniversalCell_ImageSideLength = 60;
NSString * const UIZWUniversalCellID = @"UIZWUniversalCellID";

@interface UIZWUniversalCell ()

@property (nonatomic, strong) UIView *imageBoardView;
@property (nonatomic, strong) UIImageView *topLeftImageView;
@property (nonatomic, assign) CGSize imageBoardSize;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *subTitleLabel;

@end

@implementation UIZWUniversalCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        _isImageExist = NO;
        _imageBoardSize = CGSizeZero;
        
        _imageBoardView = [[UIView alloc] init];
        _imageBoardView.clipsToBounds = YES;
        [_imageBoardView setBackgroundColor:[UIColor clearColor]];
        [self.customContentView addSubview:_imageBoardView];
        [self.imageBoardView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.customContentView).offset(10);
            make.left.equalTo(self.customContentView).offset(20);
            make.size.mas_equalTo(self.imageBoardSize);
        }];
        
        _topLeftImageView = [[UIImageView alloc] init];
        [_topLeftImageView setBackgroundColor:[UIColor clearColor]];
        [_topLeftImageView setContentMode:UIViewContentModeScaleAspectFit];
        [_imageBoardView addSubview:_topLeftImageView];
        [_topLeftImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.imageBoardView);
            make.left.equalTo(self.imageBoardView);
            make.size.mas_equalTo(CGSizeMake(UIZWUniversalCell_ImageSideLength, UIZWUniversalCell_ImageSideLength));
        }];
        
        
        _titleLabel = [[UILabel alloc] init];
        [_titleLabel setNumberOfLines:0];
        [_titleLabel setBackgroundColor:[UIColor clearColor]];
        [_titleLabel setFont:[UIFont boldSystemFontOfSize:14]];
        [_titleLabel setTextColor:[UIColor brownColor]];
        [self.customContentView addSubview:_titleLabel];
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.customContentView).offset(10);
            make.left.equalTo(self.imageBoardView.mas_right);
            make.right.equalTo(self.customContentView).offset(-10);
            make.height.mas_greaterThanOrEqualTo(24);
        }];
        
        
        _subTitleLabel = [[UILabel alloc] init];
        [_subTitleLabel setNumberOfLines:0];
        [_subTitleLabel setBackgroundColor:[UIColor clearColor]];
        [_subTitleLabel setFont:[UIFont italicSystemFontOfSize:10]];
        [_subTitleLabel setTextColor:[UIColor darkGrayColor]];
        [self.customContentView addSubview:_subTitleLabel];
        [_subTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.titleLabel.mas_bottom);
            make.left.right.equalTo(self.titleLabel);
            make.bottom.greaterThanOrEqualTo(self.imageBoardView);
            make.bottom.equalTo(self.customContentView).offset(-10).priorityLow();
        }];
        
        [_titleLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
        [_subTitleLabel setContentHuggingPriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisVertical];
        [_titleLabel setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
        [_subTitleLabel setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisVertical];
    }
    return self;
}

- (void)setIsImageExist:(BOOL)isImageExist;
{
    if (_isImageExist != isImageExist)
    {
        _isImageExist = isImageExist;
        
        [self checkImageBoardSize];
    }
}

- (void)setImage:(UIImage *)image;
{
    _image = image;
    [_topLeftImageView setImage:_image];
    
    [self checkImageBoardSize];
}

- (void)setTitle:(NSString *)title;
{
    _title = [title copy];
    [_titleLabel setText:_title];
}

- (void)setSubTitle:(NSString *)subTitle;
{
    _subTitle = [subTitle copy];
    [_subTitleLabel setText:_subTitle];
}

- (void)checkImageBoardSize;
{
    if (self.image || self.isImageExist)
    {
        self.imageBoardSize = CGSizeMake(UIZWUniversalCell_ImageSideLength+10, UIZWUniversalCell_ImageSideLength);
    }
    else
    {
        self.imageBoardSize = CGSizeZero;
    }
}

- (void)setImageBoardSize:(CGSize)imageBoardSize;
{
    if (!CGSizeEqualToSize(_imageBoardSize, imageBoardSize))
    {
        _imageBoardSize = imageBoardSize;
        
        [self.imageBoardView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(self.imageBoardSize);
        }];
    }
}

@end
