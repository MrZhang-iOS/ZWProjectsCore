//
//  UIZWTextLimitField.h
//  ZWProjectsCore
//
//  Created by ZhangWei on 2017/5/15.
//  Copyright © 2017年 ZhangWei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIZWTextLimitField : UITextField

/**
 *  用来限制文字输入框的文字总数
 *  默认为NSIntegerMax，即无上限
 */
@property (nonatomic, assign) NSUInteger maxLength;

/**
 *  输入框内的文字发生变动后的回调
 *  包括调用【setText:】方法对text的变更。
 */
@property (nonatomic, copy) void(^textModifiedHandler)(UIZWTextLimitField *view);

/**
 *  虚拟键盘上的【Return】键被点击后是否结束响应的回调
 *  返回值YES时，关闭键盘（即，取消第一响应）
 *  默认YES
 *  ⚠️该block块受delegate重置的影响
 */
@property (nonatomic, copy) BOOL(^textFieldShouldResignDidReturnBlock)(UIZWTextLimitField *view);

/**
 *  是否允许开始文本编辑
 *  默认YES
 *  ⚠️该block块受delegate重置的影响
 */
@property (nonatomic, copy) BOOL(^textFieldShouldBeginEditingBlock)(UIZWTextLimitField *view);

/**
 *  是否允许结束文本编辑
 *  默认YES
 *  ⚠️该block块受delegate重置的影响
 */
@property (nonatomic, copy) BOOL(^textFieldShouldEndEditingBlock)(UIZWTextLimitField *view);

/**
 *  是否允许清空文本 如clearButtonMode为UITextFieldViewModeAlways时，用户可以看见'⨂'按钮并点击查看效果
 *  默认YES
 *  ⚠️该block块受delegate重置的影响
 */
@property (nonatomic, copy) BOOL(^textFieldShouldClearBlock)(UIZWTextLimitField *view);

/**
 *  文字开始编辑时的回调
 */
@property (nonatomic, copy) void(^textFieldDidBeginEditingBlock)(UIZWTextLimitField *view);

/**
 *  文字结束编辑时的回调
 */
@property (nonatomic, copy) void(^textFieldDidEndEditingBlock)(UIZWTextLimitField *view);

/**
 @abstract 因项目开发人员流动性大，为了便于维护代码，不建议使用Xib和Storyboard创建UI，统一使用源码管理UI。
 @warning This method is unavaialble. Please use【-initWithStyle: reuseIdentifier:】OR【-initWithFrame:】OR【-init:】instead.
 */
- (instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE;

/**
 @abstract 该方法自带了指定构造器宏 NS_DESIGNATED_INITIALIZER
 
 什么是指定构造器宏？
 http://sejasonwang.github.io/2016/01/27/designated-initalizer/
 http://www.swiftyper.com/2016/08/02/understanding-designated-initializer-in-objective-c/
 http://www.jianshu.com/p/b142caa44382
 */
- (instancetype)initWithFrame:(CGRect)frame NS_DESIGNATED_INITIALIZER;

@end

/**
 *该类为UITextField控件设置了可输入文字字数上限的属性
 *⚠️注意内部已默认配置了delegate属性，如果修改了该属性，会导致部分block受影响。
 */
