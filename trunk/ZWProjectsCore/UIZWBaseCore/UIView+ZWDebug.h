//
//  UIView+ZWDebug.h
//
//  Created by ZhangWei on 2020/11/5.
//  Copyright © 2020 ZhangWei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (ZWDebug)

#pragma mark - 该属性仅在DEBUG模式有效
@property(nonatomic,copy)NSString* debugText;

@end

/*
 该分类的设计目的：
 方便开发阶段对UI布局的调控
 */
