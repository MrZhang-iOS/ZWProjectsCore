//
//  UIZWBaseNavigationController.m
//  ZWProjectsCore
//
//  Created by ZhangWei on 16/5/30.
//  Copyright © 2016年 ZhangWei. All rights reserved.
//

#import "UIZWBaseNavigationController.h"

@interface UIZWBaseNavigationController ()
<UINavigationControllerDelegate
,UIGestureRecognizerDelegate>

@end

@implementation UIZWBaseNavigationController

/*
 💡代码初始化加载顺序
 -initWithRootViewController:
 -loadView
 -viewDidLoad
 
 💡xib初始化加载顺序
 -initWithNibName:bundle:
 -loadView
 -viewDidLoad
 
 💡storyboard初始化加载顺序
 -initWithCoder:
 -awakeFromNib:
 -loadView
 -viewDidLoad
 */
- (instancetype)initWithNavigationBarClass:(nullable Class)navigationBarClass toolbarClass:(nullable Class)toolbarClass;
{
    if (self = [super initWithNavigationBarClass:navigationBarClass toolbarClass:toolbarClass])
    {
        [self baseNavigationControllerInit];
    }
    return self;
}

- (instancetype)initWithRootViewController:(UIViewController *)rootViewController;
{
    if (rootViewController == nil)
    {
        rootViewController = [[UIViewController alloc] init];
    }
    if (self = [super initWithRootViewController:rootViewController])
    {
        [self baseNavigationControllerInit];
    }
    return self;
}

- (instancetype)initWithNibName:(nullable NSString *)nibNameOrNil bundle:(nullable NSBundle *)nibBundleOrNil;
{
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])
    {
        [self baseNavigationControllerInit];
    }
    return self;
}

- (nullable instancetype)initWithCoder:(NSCoder *)aDecoder;
{
    if (self = [super initWithCoder:aDecoder])
    {
        [self baseNavigationControllerInit];
    }
    return self;
}


- (void)baseNavigationControllerInit;
{
    _slipRightToPopEnable = YES;
    
    self.navigationBarHidden = YES;
    self.toolbarHidden = YES;
}

- (void)viewDidLoad;
{
    [super viewDidLoad];
    if ([self respondsToSelector:@selector(interactivePopGestureRecognizer)])
    {
        self.interactivePopGestureRecognizer.enabled = YES;
        self.interactivePopGestureRecognizer.delegate = self;
        self.delegate = self;
    }
}

- (BOOL)prefersStatusBarHidden;
{
    return [[self topViewController] prefersStatusBarHidden];;
}

- (UIStatusBarStyle)preferredStatusBarStyle;
{
    return [[self topViewController] preferredStatusBarStyle];
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation
{
    return [[self topViewController] preferredStatusBarUpdateAnimation];
}

//控制器切换动画打开手势容易导致Crash
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if ([self respondsToSelector:@selector(interactivePopGestureRecognizer)] && animated == YES)
    {
        self.interactivePopGestureRecognizer.enabled = NO;
    }
    
    [super pushViewController:viewController animated:animated];
}

//控制器切换动画打开手势容易导致Crash
- (NSArray *)popToRootViewControllerAnimated:(BOOL)animated
{
    if ([self respondsToSelector:@selector(interactivePopGestureRecognizer)] && animated == YES)
    {
        self.interactivePopGestureRecognizer.enabled = NO;
    }
    
    return  [super popToRootViewControllerAnimated:animated];
}

//控制器切换动画打开手势容易导致Crash
- (NSArray *)popToViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if([self respondsToSelector:@selector(interactivePopGestureRecognizer)])
    {
        self.interactivePopGestureRecognizer.enabled = NO;
    }
    
    return [super popToViewController:viewController animated:animated];
}

- (BOOL)shouldAutorotate;
{
    return [[self topViewController] shouldAutorotate];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations;
{
    return [[self topViewController] supportedInterfaceOrientations];
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation;
{
    return [[self topViewController] preferredInterfaceOrientationForPresentation];
}

#pragma mark - UINavigationControllerDelegate
- (void)navigationController:(UINavigationController *)navigationController
       didShowViewController:(UIViewController *)viewController
                    animated:(BOOL)animated;
{
    if ([self respondsToSelector:@selector(interactivePopGestureRecognizer)])
    {
        self.interactivePopGestureRecognizer.enabled = YES;
    }
}

#pragma mark - UIGestureRecognizerDelegate
-(BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    if ( gestureRecognizer == self.interactivePopGestureRecognizer )
    {
        if (_slipRightToPopEnable)
        {
            //根控制器如果继续响应易导致UI异常
            if (self.viewControllers.count < 2 || self.visibleViewController == self.viewControllers.firstObject)
            {
                return NO;
            }
            else
            {
                return YES;
            }
        }
        else
        {
            return NO;
        }
    }
    
    return YES;
}
@end
