//
//  UIZWBaseButton.m
//
//  Created by ZhangWei on 2020/11/5.
//  Copyright © 2020 ZhangWei. All rights reserved.
//

#import "UIZWBaseButton.h"
#import "NSMutableAttributedString+ZhangWei.h"

@implementation UIZWBaseButton

- (UIColor *)highlightedKeywordsColor;
{
    if (nil == _highlightedKeywordsColor)
    {
        return [UIColor lightTextColor];
    }
    return _highlightedKeywordsColor;
}

- (UIFont *)highlightedKeywordsFont;
{
    if (nil == _highlightedKeywordsFont)
    {
        //⚠️没有赋值是为了应对字体的动态变更
        return self.titleLabel.font;
    }
    return _highlightedKeywordsFont;
}

- (void)setHighlightedKeywords:(NSString *)highlightedKeywords;
{
    _highlightedKeywords = [highlightedKeywords copy];
    
    [self titleFontForNormal];
    NSString *titleForCurrentState = [self titleForState:self.state];
    [self setTitle:titleForCurrentState forState:self.state];
}

- (void)setTitle:(NSString *)title forState:(UIControlState)state;
{
    if (title.length > 0 && self.highlightedKeywords && [self.highlightedKeywords isKindOfClass:[NSString class]] && self.highlightedKeywords.length > 0)
    {
        [super setTitle:nil forState:state];
        
        UIColor *colorForState = [self titleColorForState:state];
        if (nil == colorForState)
        {
            //⚠️使用self.titleLabel.textColor获取当前文字颜色，在当前显示的文字已经是富文本时获取的颜色可能不稳定
            colorForState = self.titleLabel.textColor?:[UIColor darkTextColor];
        }
        NSMutableAttributedString* tmpText = [NSMutableAttributedString zw_createWithString:title
                                                                                       font:self.titleFontForNormal
                                                                                      color:colorForState];
        [tmpText zw_addFont:self.highlightedKeywordsFont substring:self.highlightedKeywords];
        [tmpText zw_addColor:self.highlightedKeywordsColor substring:self.highlightedKeywords];
        
        [self setAttributedTitle:tmpText forState:state];
    }
    else
    {
        [self setAttributedTitle:nil forState:state];
        [super setTitle:title forState:state];
    }
}

- (UIFont *)titleFontForNormal;
{
    if (nil == _titleFontForNormal)
    {
        _titleFontForNormal = self.titleLabel.font?:[UIFont systemFontOfSize:[UIFont buttonFontSize]];
    }
    return _titleFontForNormal;
}

- (void)setHighlighted:(BOOL)highlighted;
{
    [super setHighlighted:highlighted];
    
    if (self.titleFontForHighlighted && self.titleFontForNormal)
    {
        if (self.isHighlighted)
        {
            self.titleLabel.font = self.titleFontForHighlighted;
        }
        else
        {
            self.titleLabel.font = self.titleFontForNormal;
        }
    }
}

- (void)setSelected:(BOOL)selected;
{
    [super setSelected:selected];
    
    if (self.titleFontForSelected && self.titleFontForNormal)
    {
        if (self.isSelected)
        {
            self.titleLabel.font = self.titleFontForSelected;
        }
        else
        {
            self.titleLabel.font = self.titleFontForNormal;
        }
    }
}

- (void)setEnabled:(BOOL)enabled;
{
    [super setEnabled:enabled];
    
    if (self.titleFontForDisabled && self.titleFontForNormal)
    {
        if (self.enabled)
        {
            self.titleLabel.font = self.titleFontForNormal;
        }
        else
        {
            self.titleLabel.font = self.titleFontForDisabled;
        }
    }
}

@end
