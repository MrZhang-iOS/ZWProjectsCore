//
//  UIZWBaseTableViewHeaderFooterView.m
//
//  Created by ZhangWei on 2020/7/16.
//  Copyright © 2020 ZhangWei. All rights reserved.
//

#import "UIZWBaseTableViewHeaderFooterView.h"
#import <Masonry/Masonry.h>

@interface UIZWBaseTableViewHeaderFooterView ()

@property (nonatomic, strong) UIView *customContentView;

@end

@implementation UIZWBaseTableViewHeaderFooterView

+ (CGFloat)perfectHeight;
{
    return 40;
}

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithReuseIdentifier:reuseIdentifier])
    {
        self.clipsToBounds = YES;
        self.backgroundView = [[UIView alloc] init];
        self.backgroundView.backgroundColor = [UIColor clearColor];
        
        _customInsets = UIEdgeInsetsZero;
        
        _customContentView = [[UIView alloc] init];
        [self.contentView addSubview:_customContentView];
        [_customContentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.contentView).insets(self.customInsets);
        }];
    }
    return self;
}

- (void)setCustomInsets:(UIEdgeInsets)customInsets;
{
    if (!UIEdgeInsetsEqualToEdgeInsets(_customInsets, customInsets))
    {
        _customInsets = customInsets;
        [_customContentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.contentView).insets(self.customInsets);
        }];
    }
}

@end
