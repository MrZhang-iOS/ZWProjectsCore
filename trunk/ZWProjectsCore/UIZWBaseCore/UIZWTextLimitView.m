//
//  UIZWTextLimitView.m
//  ZWProjectsCore
//
//  Created by ZhangWei on 15/7/31.
//  Copyright (c) 2015年 ZhangWei. All rights reserved.
//

#import "UIZWTextLimitView.h"

@interface UIZWTextLimitView ()
<UITextViewDelegate>

@property (nonatomic, assign) BOOL delegateWorking;

@end

@implementation UIZWTextLimitView

- (void)dealloc;
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)initWithFrame:(CGRect)frame;
{
    if (self = [super initWithFrame:frame textContainer:nil])
    {
        _maxLength = NSIntegerMax;
        _delegateWorking = YES;
        self.delegate = self;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(observeUITextViewTextDidBeginEditingNotification:) name:UITextViewTextDidBeginEditingNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(observeUITextViewTextDidChangeNotification:) name:UITextViewTextDidChangeNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(observeUITextViewTextDidEndEditingNotification:) name:UITextViewTextDidEndEditingNotification object:nil];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame textContainer:(nullable NSTextContainer *)textContainer
{
    return [self initWithFrame:frame];
}

- (void)setDelegate:(id<UITextViewDelegate>)delegate;
{
    [super setDelegate:delegate];
    if (self == self.delegate)
    {
        self.delegateWorking = YES;
    }
    else
    {
        self.delegateWorking = NO;
    }
}

//重写此方法的作用是，防止控件字符超过上限
- (void)setText:(NSString *)text
{
    void (^task)(void) = ^{
        NSString* copyText = text.copy;
        //如果没有self.markedTextRange == nil限制，在汉字输入联想时程序会挂掉
        if (self.markedTextRange == nil)//markedTextRange方法必须在主线程执行，否则可能导致crash
        {
            if (self.maxLength < copyText.length)
            {
                copyText = [copyText substringToIndex:self.maxLength];
            }
        }
        BOOL isModified = ![self.text isEqualToString:copyText];
        [super setText:copyText];
        if (isModified)
        {
            if (self.textModifiedHandler)
            {
                self.textModifiedHandler(self);
            }
        }
    };
    
    if (strcmp(dispatch_queue_get_label(DISPATCH_CURRENT_QUEUE_LABEL), dispatch_queue_get_label(dispatch_get_main_queue())) == 0)
    {
        // 主队列
        task();
    }
    else
    {
        // 非主队列
        dispatch_async(dispatch_get_main_queue(), task);
    }
}

- (void)setMaxLength:(NSUInteger)maxLength;
{
    if (_maxLength != maxLength)
    {
        _maxLength = maxLength;
        
        if (maxLength < self.text.length)
        {
            self.text = self.text;
        }        
    }
}

- (void)observeUITextViewTextDidChangeNotification:(NSNotification*)info;
{
    UIZWTextLimitView* pTextView = info.object;
    if (pTextView != self)
    {
        return;
    }
    
    // UITextView 的内容
    NSString *contentText = pTextView.text;
    // 获取高亮内容的范围
    UITextRange *selectedRange = [pTextView markedTextRange];
    // 这行代码 可以认为是 获取高亮内容的长度
    NSInteger markedTextLength = [pTextView offsetFromPosition:selectedRange.start toPosition:selectedRange.end];
    // 没有高亮内容时,对已输入的文字进行操作
    if (markedTextLength == 0)
    {
        // 如果 text field 的内容长度大于我们限制的内容长度
        if (contentText.length > self.maxLength)
        {
            NSRange rangeRange = [contentText rangeOfComposedCharacterSequencesForRange:NSMakeRange(0, self.maxLength)];
            pTextView.text = [contentText substringWithRange:rangeRange];
        }
        else
        {
            if (self.textModifiedHandler)
            {
                self.textModifiedHandler(self);
            }
        }
    }
}

//通知在代理之后执行
- (void)observeUITextViewTextDidBeginEditingNotification:(NSNotification *)info;
{
    UIZWTextLimitView* pTextView = info.object;
    if (pTextView != self)
    {
        return;
    }
    
    if (!self.delegateWorking && self.textViewDidBeginEditingBlock)
    {
        self.textViewDidBeginEditingBlock(self);
    }
}

//通知在代理之后执行
- (void)observeUITextViewTextDidEndEditingNotification:(NSNotification *)info;
{
    UIZWTextLimitView* pTextView = info.object;
    if (pTextView != self)
    {
        return;
    }
    
    if (!self.delegateWorking && self.textViewDidEndEditingBlock)
    {
        self.textViewDidEndEditingBlock(self);
    }
}

#pragma mark - UITextViewDelegate
//将要开始编辑
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView;
{
    if (self.textViewShouldBeginEditingBlock)
    {
        return self.textViewShouldBeginEditingBlock(self);
    }
    else
    {
        return YES;
    }
}

//将要结束编辑
- (BOOL)textViewShouldEndEditing:(UITextView *)textView;
{
    if (self.textViewShouldEndEditingBlock)
    {
        return self.textViewShouldEndEditingBlock(self);
    }
    else
    {
        return YES;
    }
}

//开始编辑
- (void)textViewDidBeginEditing:(UITextView *)textView;
{
    if (self.textViewDidBeginEditingBlock)
    {
        self.textViewDidBeginEditingBlock(self);
    }
}

//结束编辑
- (void)textViewDidEndEditing:(UITextView *)textView;
{
    if (self.textViewDidEndEditingBlock)
    {
        self.textViewDidEndEditingBlock(self);
    }
}

//内容将要发生改变编辑
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
{
    //如果有汉字，则联想字词不计数输入限制
    if (text.length == 0 || textView.markedTextRange != nil)
    {
        return YES;
    }
    
    if ([text isEqualToString:@"\n"])
    {
        if (self.textViewShouldResignDidReturnBlock)
        {
            BOOL shouldReturn = self.textViewShouldResignDidReturnBlock(self);
            if (shouldReturn)
            {
                [self resignFirstResponder];
            }
            return !shouldReturn;
        }
    }
    
    //UIZWTextLimitView类虽然已经重构了setText方法，直接赋值就可以预防超过文字上限，但这样会导致光标永远停在最后的字符上。
    //如果用户的目的是从字符串中间输入一段新的文字则每输入一个字符时需要调整光标位置，这样用户体验就很差了
    NSString* newString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    if (self.maxLength < newString.length)
    {
        textView.text = newString;
        //返回YES，则将会把新的字符串自动拼接在旧的字符串上
        //返回NO，则会舍弃新的字符串
        return NO;
    }
    //返回YES，则将会把新的字符串自动拼接在旧的字符串上
    //返回NO，则会舍弃新的字符串
    return YES;
}

//焦点发生改变
- (void)textViewDidChangeSelection:(UITextView *)textView;
{
    if (self.textViewDidChangeSelectionBlock)
    {
        self.textViewDidChangeSelectionBlock(self);
    }
}

@end
