//
//  UIZWBaseViewController.h
//  ZWProjectsCore
//
//  Created by ZhangWei on 15/5/3.
//  Copyright (c) 2015年 ZhangWei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>
#import "UIZWCloseBoardBar.h"

@interface UIZWBaseViewController : UIViewController

/// 状态栏是否设置隐藏。默认NO，即显示状态栏。
/// ⚠️注：在横屏模式，有时系统会强制隐藏状态栏，修改该属性值也无法解决，如iOS11及以上系统的iPhoneX或者iOS13及以上系统的所有设备，状态栏被强制隐藏。
@property(nonatomic, assign) BOOL statusBarHidden;

/// 当前控制器是否可见的（仅作参考使用）。（通过viewWillAppear和viewWillDisappear接口的调用来判断）
@property(nonatomic, assign, readonly) BOOL isVisible;

#pragma mark - 当前控制器的【-loadView】方法被成功调用后以下视图才会显示在控制器上，如果子类重构了【-loadView】方法且没有调用【super loadView】方法时，以下视图则不会显示在控制器上
/**
 * 全屏含状态栏，所处最高层，也是self.view。
 * 系统UIWindow的层级关系，可作为以下层级关系的理解参考
 * UIWindowLevelNormal < UIWindowLevelStatusBar < UIWindowLevelAlert
 *
 * UIWindowLevelNormal : 默认窗口的层级
 * UIWindowLevelStatusBar : 状态栏、键盘
 * UIWindowLevelAlert :UIActionSheet、UIAlearView（ActionSheet比Alert高）
 */
@property(nonatomic, strong, readonly) UIImageView *levelHighestView;

/// 全屏含状态栏，层级关系仅次于levelHighestView，开发中可在该视图上布局控件。
@property(nonatomic, strong, readonly) UIView *fullCtrlView;

/// 状态栏下方的视图，层级关系仅次于fullCtrlView，开发中可在该视图上布局控件。
@property(nonatomic, strong, readonly) UIView *mainView;

#pragma mark - 处理虚拟键盘与当前UI的布局
/// 当虚拟键盘显示或隐藏时自动处理UI的布局【💡受isFirstResponder方法影响】，默认是NO
@property(nonatomic, assign) BOOL autoLayoutViewForVirtualKeyboard;

/// 辅助关闭虚拟键盘的作用
@property(nonatomic, strong, readonly) UIZWCloseBoardBar *virtualKeyboardCloseBar;

/// 显示虚拟键盘的关闭栏，仅当autoLayoutViewForVirtualKeyboard为YES时有效，默认是YES
@property(nonatomic, assign) BOOL showVirtualKeyboardCloseBar;

/**
 * 默认YES
 * 定义该方法的原因如下：
 * 当前App的虚拟键盘没有显示，而外界App显示有虚拟键盘，因为虚拟键盘的监听事件会导致UI在APP切换时变更，故而实现此方法可以避免此问题出现
 * 虚拟键盘对UI的变更影响加入了isFirstResponder的验证，如果不在第一响应则无需变更UI
 *
 * @return 是否在第一响应
 */
- (BOOL)isFirstResponder;

/**
 * 应用程序发生了重大事件改变，如凌晨【可重构该API，但一般不主动调用】
 * 💡无论当前控制器是否为显示时，此方法均有效
 */
- (void)applicationSignificantTimeChange:(NSNotification *)aNotification NS_REQUIRES_SUPER;

/**
 * 应用程序即将退至后台【可重构该API，但一般不主动调用】
 * 💡无论当前控制器是否为显示时，此方法均有效
 */
- (void)applicationResignActive:(NSNotification *)aNotification;

/**
 * 应用程序已经退至后台
 * 💡无论当前控制器是否为显示时，此方法均有效
 */
- (void)applicationDidEnterBackground:(NSNotification *)aNotification;

/**
 * 应用程序进入前台
 * 💡无论当前控制器是否为显示时，此方法均有效
 */
- (void)applicationWillEnterForeground:(NSNotification *)aNotification;

/**
 * 应用程序已经被激活（可能是从后台到前台的动作）【可重构该API，但一般不主动调用】
 * 💡无论当前控制器是否为显示时，此方法均有效
 */
- (void)applicationBecomeActive:(NSNotification *)aNotification;

/**
 * 用户执行了截屏动作
 * ⚠️仅当前控制器显示时，此方法均有效
 */
- (void)applicationUserDidTakeScreenShot:(NSNotification *)aNotification;

/**
 * 应用程序即将打开虚拟键盘【可重构该API，但一般不主动调用】
 * ⚠️仅当前控制器显示时，此方法均有效
 */
- (void)applicationKeyboardWillShow:(NSNotification *)aNotification NS_REQUIRES_SUPER;

/**
 * 应用程序打开了虚拟键盘【可重构该API，但一般不主动调用】
 * ⚠️仅当前控制器显示时，此方法均有效
 */
- (void)applicationKeyboardShow:(NSNotification *)aNotification NS_REQUIRES_SUPER;

/**
 * 应用程序即将改变了虚拟键盘高度，如切换输入法【可重构该API，但一般不主动调用】
 * ⚠️仅当前控制器显示时，此方法均有效
 */
- (void)applicationKeyboardWillChangeFrame:(NSNotification *)aNotification NS_REQUIRES_SUPER;

/**
 * 应用程序改变了虚拟键盘高度，如切换输入法【可重构该API，但一般不主动调用】
 * ⚠️仅当前控制器显示时，此方法均有效
 */
- (void)applicationKeyboardChangeFrame:(NSNotification *)aNotification NS_REQUIRES_SUPER;

/**
 * 应用程序即将关闭了虚拟键盘【可重构该API，但一般不主动调用】
 * ⚠️仅当前控制器显示时，此方法均有效
 */
- (void)applicationKeyboardWillHide:(NSNotification *)aNotification NS_REQUIRES_SUPER;

/**
 * 应用程序关闭了虚拟键盘【可重构该API，但一般不主动调用】
 * ⚠️仅当前控制器显示时，此方法均有效
 */
- (void)applicationKeyboardHide:(NSNotification *)aNotification NS_REQUIRES_SUPER;

/**
 * @abstract 返回上层控制器【确保主线程调用】
 * 该方法自带了宏 NS_REQUIRES_SUPER 表示子类override父类的方法时，必须在方法内部调用super的这个方法。
 */
- (void)backToPreViewCtrl:(BOOL)animation;

/**
 * 强制旋转屏幕
 * @param orientation 目标UI样式
 */
- (void)orientationToPortrait:(UIInterfaceOrientation)orientation DEPRECATED_MSG_ATTRIBUTE("Use zw_changeDeviceOrientation() method instead.");

/**
 * 请子类根据需要自行设置是否支持旋转，默认已支持旋转。
 * 决定当前界面是否开启自动转屏，如果返回NO，【- supportedInterfaceOrientations】和【- preferredInterfaceOrientationForPresentation】两个方法也不会被调用，只会支持默认的UIInterfaceOrientationMaskPortrait方向。
 * @return 是否支持旋转。
 */
- (BOOL)shouldAutorotate;

/**
 * 请子类根据项目需要自行设置支持的模式类型，默认仅支持肖像模式。返回支持的旋转方向。
 * @return 支持的旋转方向。
 */
- (UIInterfaceOrientationMask)supportedInterfaceOrientations;

/// 返回最优先显示的屏幕方向，比如同时支持Portrait和Landscape方向，但想优先显示Landscape方向，那软件启动的时候就会先显示Landscape，在手机切换旋转方向的时候仍然可以在Portrait和Landscape之间切换；
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation;

/**
 * 控制器发生了旋转，UI需要重新布局（系统接口）
 * @param size 尺寸
 * @param coordinator 回调
 */
- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id <UIViewControllerTransitionCoordinator>)coordinator;

@end

/*
 🌟🌟🌟
 方法后带了 NS_DESIGNATED_INITIALIZER 表示指定构造器宏。
 什么是指定构造器宏？
 http://sejasonwang.github.io/2016/01/27/designated-initalizer/
 http://www.swiftyper.com/2016/08/02/understanding-designated-initializer-in-objective-c/
 http://www.jianshu.com/p/b142caa44382
 
 
 UIViewController启动方法调用顺序
 ①开始-> init -> initWithNibName:bundle: -> ▶️
 ②开始-> initWithNibName:bundle: -> ▶️
 ③storyboard开始-> initWithCoder: -> awakeFromNib: -> ▶️
 ▶️ -> loadView -> viewDidLoad -> viewWillAppear -> viewWillLayoutSubviews -> updateViewConstraints -> viewDidLayoutSubviews
 -> viewDidAppear
 消失流程 viewWillDisAppear -> updateViewConstraints -> viewDidDisAppear
 在ViewController的viewDidLayoutSubviews以后的方法可以拿到view的实际frame，所以当我们真的需要frame的时候需要在这个时间点以后才能拿到
 viewWillLayoutSubviews/viewDidLayoutSubviews這兩個function是只要有畫面的變動時，就會被觸發，被觸發的頻率比viewWillAppear/viewDidAppear高很多。
 
 🌟🌟🌟
 每个控制器只能拥有一个presentedController，也就是每次只能present一个别的控制器，强行present新的会出现以下提示：
 Warning: Attempt to present <...>  on <...> which is already presenting <...>
 如A-模态->弹出B, 则
 A.presentedViewController = B
 B.presentingViewController = A
 
 可以通过如下方式获取到显示出来的控制器
 while (showInViewCtrl.presentedViewController)
 {
     //如果一个页面有多次UIAlertController弹框，此段代码是为了让后来的UIAlertController弹框页显示出来
     showInViewCtrl = showInViewCtrl.presentedViewController;
 }
 
 
 🌟🌟🌟
 AutoLayout的冲突，可以使用"约束优先级"解决，如果不影响UI的布局也可不用处理冲突，只是Console会打印一些警告日志。
 当出现约束冲突时，主动放弃低优先级的约束。
 
*/
