//
//  UIZWBaseTableViewCell.m
//
//  Created by ZhangWei on 2020/7/16.
//  Copyright © 2020 ZhangWei. All rights reserved.
//

#import "UIZWBaseTableViewCell.h"
#import <Masonry/Masonry.h>

@interface UIZWBaseTableViewCell ()

@property (nonatomic, strong) UIView *customContentView;

@end

@implementation UIZWBaseTableViewCell

+ (CGFloat)perfectHeight;
{
    return 44;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        self.clipsToBounds = YES;
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _customInsets = UIEdgeInsetsZero;
        
        _customContentView = [[UIView alloc] init];
        [self.contentView addSubview:_customContentView];
        [_customContentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.contentView).insets(self.customInsets);
        }];
    }
    return self;
}

- (void)setCustomInsets:(UIEdgeInsets)customInsets;
{
    if (!UIEdgeInsetsEqualToEdgeInsets(_customInsets, customInsets))
    {
        _customInsets = customInsets;
        [_customContentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.contentView).insets(self.customInsets);
        }];
    }
}

@end
