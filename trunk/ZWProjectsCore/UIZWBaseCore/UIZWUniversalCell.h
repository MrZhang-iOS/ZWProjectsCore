//
//  UIZWUniversalCell.h
//  ZWProjectTemplate
//
//  Created by ZhangWei on 2018/8/14.
//  Copyright © 2018年 ZhangWei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIZWBaseTableViewCell.h"

UIKIT_EXTERN NSString * const UIZWUniversalCellID;

@interface UIZWUniversalCell : UIZWBaseTableViewCell

/**
 * 如果为YES，表示图片一定存在，无论image属性是否为nil，UI布局时都会为图片预留空间。默认NO。
 * 一般用在图片加载很慢时使用。
 */
@property (nonatomic, assign) BOOL isImageExist;

/**
 * 此属性会影响Cell的布局和行高。
 * 如果图片在UI布局完毕后赋值，cell的行高可能不会根据Image的变化而调整。通过将isImageExist属性值设置为YES解决。
 */
@property (nonatomic, strong) UIImage *image;

@property (nonatomic, strong, readonly) UILabel *titleLabel;
@property (nonatomic, copy) NSString *title;

@property (nonatomic, strong, readonly)UILabel *subTitleLabel;
@property (nonatomic, copy) NSString *subTitle;

@end
