//
//  UIZWAnimationPopView.m
//  ZWProjectsCore
//
//  Created by ZhangWei on 15/5/15.
//  Copyright (c) 2015年 ZhangWei. All rights reserved.
//

#import "UIZWAnimationPopView.h"
#import <Masonry/Masonry.h>
#import "ZWProjectsCoreMacros.h"
#import "ZWProjectsCoreFunctions.h"

#if !__has_feature(objc_arc)
#error This class requires automatic reference counting
#endif

@interface UIZWAnimationPopView ()

@property (nonatomic, strong) UIView *popBoardView;

@end

@implementation UIZWAnimationPopView

- (instancetype)initWithFrame:(CGRect)frame;
{
    if (self = [super initWithFrame:frame])
    {
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.7];
        
        _popBoardView = [[UIView alloc] init];
        _popBoardView.backgroundColor = [UIColor clearColor];
        [self addSubview:_popBoardView];
        [_popBoardView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
        
        UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToResignFirstResponse:)];
        [_popBoardView addGestureRecognizer:tapGesture];
    }
    return self;
}

- (void)tapToResignFirstResponse:(UITapGestureRecognizer*)gesture;
{
    zw_closeVirtualKeyboard();
}

- (void)showInView:(UIView *)view withAnimation:(BOOL)bAnimation;
{
    void (^task)(void) = ^{
        zw_closeVirtualKeyboard();
        UIView *showInTargetView = view;
        if (!showInTargetView) {
            showInTargetView = (UIView *)zw_sharedApplicationWindow();
        }
        if (view && self.superview != view)
        {
            [view addSubview:self];
            [self mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(view);
            }];
            
            if (bAnimation)
            {
                self.popBoardView.transform = CGAffineTransformMakeScale(0, 0);
                
                __weak typeof(self) wSelf = self;
                [UIView animateWithDuration:0.2
                                      delay:0
                     usingSpringWithDamping:0.5
                      initialSpringVelocity:0
                                    options:UIViewAnimationOptionCurveLinear|UIViewAnimationOptionBeginFromCurrentState
                                 animations:^
                {
                    wSelf.popBoardView.transform = CGAffineTransformIdentity;
                }
                                 completion:^(BOOL finished)
                {
                    
                }];
            }
            else
            {
                self.popBoardView.transform = CGAffineTransformIdentity;
            }
        }
    };
    
    if (strcmp(dispatch_queue_get_label(DISPATCH_CURRENT_QUEUE_LABEL), dispatch_queue_get_label(dispatch_get_main_queue())) == 0) {
        // 主队列
        task();
    } else {
        // 非主队列
        dispatch_async(dispatch_get_main_queue(), task);
    }
}

- (void)showWithAnimation:(BOOL)bAnimation;
{
    [self showInView:nil withAnimation:bAnimation];
}

- (void)dismissWithAnimation:(BOOL)bAnimation;
{
    void (^task)(void) = ^{
        if (bAnimation)
        {
            __weak typeof(self) wSelf = self;
            [UIView animateWithDuration:0.1
                                  delay:0
                                options:UIViewAnimationOptionCurveLinear|UIViewAnimationOptionBeginFromCurrentState
                             animations:^
            {
                wSelf.popBoardView.transform = CGAffineTransformMakeScale(0, 0);
            }
                             completion:^(BOOL finished)
            {
                UIView* strongSelf = wSelf;
                @try
                {
                    if ([strongSelf respondsToSelector:@selector(removeFromSuperview)])
                    {
                        [strongSelf removeFromSuperview];
                    }
                }
                @catch (NSException *exception)
                {
                    NSLog(@"%@", exception.reason);
                }
                @finally
                {
                }
            }];
        }
        else
        {
            @try
            {
                if ([self respondsToSelector:@selector(removeFromSuperview)])
                {
                    [self removeFromSuperview];
                }
            }
            @catch (NSException *exception)
            {
                NSLog(@"%@", exception.reason);
            }
            @finally
            {
            }
        }
    };
    
    if (strcmp(dispatch_queue_get_label(DISPATCH_CURRENT_QUEUE_LABEL), dispatch_queue_get_label(dispatch_get_main_queue())) == 0) {
        // 主队列
        task();
    } else {
        // 非主队列
        dispatch_async(dispatch_get_main_queue(), task);
    }
}

@end
