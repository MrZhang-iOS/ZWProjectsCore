//
//  UIZWZoomAspectImageView.m
//  ZWProjectsCore
//
//  Created by ZhangWei on 16/3/23.
//  Copyright © 2016年 ZhangWei. All rights reserved.
//

#import "UIZWZoomAspectImageView.h"
#import <Masonry/Masonry.h>
#import "UIView+ZhangWei.h"
#import "UIImage+ZhangWei.h"

#define UIZWZoomAspectImageViewMacro_MinZoomScale 1.0
#define UIZWZoomAspectImageViewMacro_MaxZoomScale 5.0

@interface UIZWZoomAspectImageView ()
<UIScrollViewDelegate>

@property(nonatomic,strong)UIImageView* imageView;

- (void)doubleTap:(UITapGestureRecognizer*)gesture;
- (CGRect)zoomRectForScale:(float)scale withCenter:(CGPoint)center;

@end

@implementation UIZWZoomAspectImageView

- (instancetype)initWithFrame:(CGRect)frame;
{
    if (self = [super initWithFrame:frame])
    {
        self.showsVerticalScrollIndicator = NO;
        self.showsHorizontalScrollIndicator = NO;
        self.decelerationRate = UIScrollViewDecelerationRateFast;
        self.minimumZoomScale = UIZWZoomAspectImageViewMacro_MinZoomScale;
        self.maximumZoomScale = UIZWZoomAspectImageViewMacro_MaxZoomScale;
        self.delegate = self;
        
        _imageView = [[UIImageView alloc] init];
        [_imageView setUserInteractionEnabled:YES];
        [_imageView setContentMode:UIViewContentModeScaleAspectFill];
        [self addSubview:_imageView];
        
        UITapGestureRecognizer* doubleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTap:)];
        [doubleTapGesture setNumberOfTapsRequired:2];
        [_imageView addGestureRecognizer:doubleTapGesture];
    }
    return self;
}

- (void)setImage:(UIImage *)image
{
    _image = image;
    __weak typeof(self) wSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^ {
        if (image)
        {
            [wSelf.imageView setImage:image];
            
            [wSelf setNeedsUpdateConstraints];
            [wSelf updateConstraintsIfNeeded];
            [wSelf setNeedsLayout];
            [wSelf layoutIfNeeded];
            
            if (image)
            {
                [wSelf setContentOffset:CGPointMake(wSelf.imageView.center.x-wSelf.width/2.0, wSelf.imageView.center.y-wSelf.height/2.0)];
            }
            else
            {
                [wSelf setContentOffset:CGPointZero];
            }
        }
    });
}

- (UIImage*)getVisibleImage;
{
    CGRect visibleRect;
    visibleRect.origin = self.contentOffset;
    visibleRect.size = self.bounds.size;
    
    float theScale = self.image.size.width/self.contentSize.width;
    visibleRect.origin.x *= theScale;
    visibleRect.origin.y *= theScale;
    visibleRect.size.width *= theScale;
    visibleRect.size.height *= theScale;
    
    return [self.image zw_screenShotWithRect:visibleRect];
}

- (void)updateConstraints;
{
    __weak typeof(self) wSelf = self;
    
    CGFloat fImageWidth = self.image.size.width;
    CGFloat fImageHeight = self.image.size.height;
    if (self.image && fImageWidth > 0.01 && fImageHeight > 0.01)
    {
        CGFloat fImageScale = fImageWidth/fImageHeight;
        
        CGFloat fFrameWidth = self.width;
        CGFloat fFrameHeight = self.height;
        if (fFrameWidth > 0.001 && fFrameHeight > 0.001)
        {
            CGFloat fFrameScale = fFrameWidth/fFrameHeight;
            
            if (fImageScale > fFrameScale)
            {
                [_imageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.edges.equalTo(wSelf);
                    make.height.equalTo(wSelf).offset(1);
                    make.width.equalTo(wSelf.imageView.mas_height).multipliedBy(fImageScale);
                }];
            }
            else if (fImageScale < fFrameScale)
            {
                [_imageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.edges.equalTo(wSelf);
                    make.width.equalTo(wSelf).offset(1);
                    make.height.equalTo(wSelf.imageView.mas_width).multipliedBy(1/fImageScale);
                }];
            }
            else
            {
                [_imageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.edges.equalTo(wSelf);
                    make.width.height.equalTo(wSelf).offset(1);
                }];
            }
        }
        else
        {
            [_imageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(wSelf);
                make.width.equalTo(wSelf.imageView.mas_height).multipliedBy(fImageScale);
                make.width.greaterThanOrEqualTo(wSelf).offset(1);
                make.height.greaterThanOrEqualTo(wSelf).offset(1);
            }];
        }
    }
    
    [super updateConstraints];
}

#pragma mark - UIScrollViewDelegate
- (nullable UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView;
{
    return self.imageView;
}

#pragma mark - PrivateFunctions
- (void)doubleTap:(UITapGestureRecognizer*)gesture;
{
    if (self.maximumZoomScale > self.minimumZoomScale)
    {
        CGFloat newScale = self.zoomScale;
        CGFloat zoomScaleAvg = (self.maximumZoomScale + self.minimumZoomScale)/2.0;
        
        if (newScale < zoomScaleAvg)
        {
            //执行放大动作
            newScale = self.maximumZoomScale;
        }
        else
        {
            //执行缩小动作
            newScale = self.minimumZoomScale;
        }
        CGRect zoomRect = [self zoomRectForScale:newScale withCenter:[gesture locationInView:gesture.view]];
        [self zoomToRect:zoomRect animated:YES];
    }
}

- (CGRect)zoomRectForScale:(float)scale withCenter:(CGPoint)center
{
    CGRect zoomRect;
    zoomRect.size.height =self.frame.size.height / scale;
    zoomRect.size.width  =self.frame.size.width  / scale;
    zoomRect.origin.x = center.x - (zoomRect.size.width  /2.0);
    zoomRect.origin.y = center.y - (zoomRect.size.height /2.0);
    return zoomRect;
}

@end
