//
//  UIZWCloseBoardBar.h
//  ZWProjectsCore
//
//  Created by ZhangWei on 15/5/31.
//  Copyright (c) 2015年 ZhangWei. All rights reserved.
//

#import "UIZWBaseView.h"

static CGFloat const UIVirtualKeyboardCloseBarDefaultHeight = 40;

@interface UIZWCloseBoardBar : UIImageView

@property (nonatomic, strong, readonly) UIButton *rightButton;
@property (nonatomic, copy) void (^rightButtonBlock)(UIZWCloseBoardBar *view, UIButton *button);

@end
