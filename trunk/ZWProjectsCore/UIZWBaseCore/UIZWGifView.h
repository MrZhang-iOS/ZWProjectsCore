//
//  UIZWGifView.h
//  ZWProjectsCore
//
//  Created by ZhangWei on 16/1/16.
//  Copyright © 2016年 ZhangWei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIZWGifView : UIView

@property (nonatomic, strong) NSData* gifData;//Gif数据
@property (nonatomic, assign) NSInteger loopCount;//Gif循环播放的次数 小于等于-1播放一次，0无限循环，默认0
@property (nonatomic, assign) CGFloat delayInterval;//Gif动画图片之间的间隔，如果用户有设置播放间隔，以设置为准，否则从gifData中读出
@property (nonatomic, assign) CGFloat durationInterval;//Gif播放一轮的时间，如果用户有设置播放间隔，以设置为准，否则从gifData中读出
@property (nonatomic, assign, readonly) BOOL isPlaying;//Gif是否正在播放
@property (nonatomic, assign, readonly) CGSize gifSize;//Gif的长宽 从gifData中读出

/**
 @abstract 因项目开发人员流动性大，为了便于维护代码，不建议使用Xib和Storyboard创建UI，统一使用源码管理UI。
 
 @warning This method is unavaialble. Please use【-initWithStyle: reuseIdentifier:】OR【-initWithFrame:】OR【-init:】instead.
 */
- (instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE;

/**
 @abstract 该方法自带了指定构造器宏 NS_DESIGNATED_INITIALIZER
 
 什么是指定构造器宏？
 http://sejasonwang.github.io/2016/01/27/designated-initalizer/
 http://www.swiftyper.com/2016/08/02/understanding-designated-initializer-in-objective-c/
 http://www.jianshu.com/p/b142caa44382
 */
- (instancetype)initWithFrame:(CGRect)frame NS_DESIGNATED_INITIALIZER;

/**
 *  确保主线程调用
 */
- (void)playGif;
- (void)stopGif;
- (void)pauseGif;
- (void)resumeGif;

@end
