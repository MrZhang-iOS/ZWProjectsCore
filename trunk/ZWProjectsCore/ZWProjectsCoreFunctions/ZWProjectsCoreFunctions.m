//
//  ZWProjectsCoreFunctions.m
//  ZWProjectsCore
//
//  Created by ZhangWei on 15/5/5.
//  Copyright (c) 2015年 ZhangWei. All rights reserved.
//

#import "ZWProjectsCoreFunctions.h"
#import <sys/utsname.h> //手机型号
#import <SystemConfiguration/CaptiveNetwork.h> //WiFi
#import <ifaddrs.h>  //ip
#import <arpa/inet.h> //ip
#import <net/if.h> //ip
#import <sys/sysctl.h>//内存
#import <mach/mach.h>//内存
#import <sys/mount.h> //硬盘

#import <AudioToolbox/AudioToolbox.h>
#import <LocalAuthentication/LocalAuthentication.h>//指纹识别使用的头文件
#import <AVFoundation/AVFoundation.h>
#import <Security/Security.h>//利用设备间不开启同步功能钥匙串的数据制作设备唯一ID，即使APP被删除，也不会丢失，但系统抹掉重置则此ID丢失
#import <ZWRecordTextTool/ZWRecordTextTool.h>
#import "ZWProjectsCoreMacros.h"
#import "NSString+ZhangWei.h"
#import "NSData+ZhangWei.h"
#import "NSArray+ZhangWei.h"
#import "NSDictionary+ZhangWei.h"
#import "UIView+ZhangWei.h"
#import "UIViewController+ZWPresentQueue.h"
#import "NSArray+ZhangWei.h"

NSInteger const ErrorCode_ZhangWei_NotRespondsToSelector = 86001;
NSInteger const ErrorCode_ZhangWei_MethodSignatureFailed = 86002;
NSInteger const ErrorCode_ZhangWei_Parameters = 1003;
static NSString * const g_pAppLogFileName = @"ApplicationLog.txt";
static NSString * const g_pAppLogTag = @"AppLog";

#if !__has_feature(objc_arc)
#error This class requires automatic reference counting
#endif

void zw_logInConsoleOnly(NSString *log)
{
    zw_log(log, nil, YES, NO, YES);
}

void zw_logInSandboxOnly(NSString *log)
{
    zw_log(log, nil, NO, YES, YES);
}

void zw_logInSandboxAndConsole(NSString *log)
{
    zw_log(log, nil, YES, YES, YES);
}

void zw_log(NSString *log, NSString *tag, BOOL inConsole, BOOL inSandbox, BOOL showTime)
{
    if (inConsole || inSandbox)
    {
        // 是否在日志前显示时间
        if (showTime)
        {
            struct timeval currentTime;
            /// C函数time()获取当前时间的时间戳（从1970年1月1日期至现在的秒数）。如果参数不为NULL，则函数将计算出来的时间戳写入参数指针所指的地址，同时也返回这个时间戳。
            /// C函数gettimeofday()获取当前时间戳和微秒数。它的调用必须引进【#include <sys/time.h>】。
            gettimeofday(&currentTime, NULL);
            /// C函数tv_sec()获取单位为秒的当前时间戳。
            /// C函数localtime()将时间戳参数转换成对应的struct tm类型的本地时间。
            struct tm *localTime = localtime(&currentTime.tv_sec);
            char buffer[22];
            /// C函数mktime()可以将struct tm类型参数的时间转换成时间戳。
            /// C函数strftime()将struct tm类型参数的时间转换为指定格式的字符串。
            strftime(buffer, 22, "%y%m%d %z %H:%M:%S", localTime);
            //在控制台上打印日志
            if (inConsole)
            {
                /// C函数tv_usec()获取当前的微秒数，具体表示当前时间从上一秒开始已经经过了多少微秒，注意不包含秒的数据。
                /// 因为一秒等于1000000微秒，所以 `tv_usec` 取值范围是从 0 到 999999。如果值为 0，则表明当前时间为整秒。
                printf("[%s.%06d][%s] %s", buffer, currentTime.tv_usec, [tag?:g_pAppLogTag UTF8String], [log UTF8String]);
            }
            if (inSandbox)
            {
                /// C函数tv_usec()获取当前的微秒数，具体表示当前时间从上一秒开始已经经过了多少微秒，注意不包含秒的数据。
                /// 因为一秒等于1000000微秒，所以 `tv_usec` 取值范围是从 0 到 999999。如果值为 0，则表明当前时间为整秒。
                NSString *recordStr = [NSString stringWithFormat:@"[%s.%06d][%@] %@", buffer, currentTime.tv_usec, tag?:g_pAppLogTag, log];
                //在沙盒Library路径下存储日志，方便随时翻阅和调试
                [ZWRecordTextTool recordText:recordStr fileName:g_pAppLogFileName completedHanle:nil];
            }
        }
        else
        {
            if (inConsole)
            {
                //在控制台上打印日志
                printf("[%s] %s", [tag?:g_pAppLogTag UTF8String], [log UTF8String]);
            }
            if (inSandbox)
            {
                NSString *recordStr = [NSString stringWithFormat:@"[%@] %@", tag?:g_pAppLogTag, log];
                //在沙盒Library路径下存储日志，方便随时翻阅和调试
                [ZWRecordTextTool recordText:recordStr fileName:g_pAppLogFileName completedHanle:nil];
            }
        }
    }
}

NSString* zw_formatString(NSString *format, ...)
{
    va_list args;
    va_start(args, format);
    NSString *formatString = [[NSString alloc] initWithFormat:format arguments:args];
    va_end(args);
    
    return formatString;
}

UIApplication* zw_sharedApplication(void)
{
    Class UIApplicationClass = NSClassFromString(@"UIApplication");
    if(!UIApplicationClass || ![UIApplicationClass respondsToSelector:@selector(sharedApplication)])
    {
        return nil;
    }
    
    UIApplication * app = [UIApplication performSelector:@selector(sharedApplication)];
    return app;
}

UIWindow* zw_sharedApplicationWindow(void)
{
    UIWindow *keyWindow = nil;
    UIApplication *application = zw_sharedApplication();
    if ([application respondsToSelector:@selector(keyWindow)]) {
        keyWindow = zw_sharedApplication().keyWindow;
        if (keyWindow && [keyWindow isKindOfClass:[UIWindow class]]) {
            return keyWindow;
        }
        keyWindow = nil;
    }
    
    if (@available(iOS 13.0, *)) {
        if ([application respondsToSelector:@selector(connectedScenes)]) {
            NSArray<UIScene *> *allScenes = zw_sharedApplication().connectedScenes.allObjects;
            for (UIScene *scene in allScenes) {
                if (scene.activationState == UISceneActivationStateForegroundActive) {
                    if ([scene isKindOfClass:[UIWindowScene class]]) {
                        UIWindowScene *windowScene = (UIWindowScene *)scene;
                        keyWindow = windowScene.windows.firstObject;
                        if (keyWindow && [keyWindow isKindOfClass:[UIWindow class]]) {
                            return keyWindow;
                        }
                        keyWindow = nil;
                    }
                }
            }
            
            id<UISceneDelegate> sceneDelegate = allScenes.firstObject.delegate;
            if (!keyWindow && [sceneDelegate respondsToSelector:@selector(window)]) {
                keyWindow = [sceneDelegate performSelector:@selector(window)];
                if (keyWindow && [keyWindow isKindOfClass:[UIWindow class]]) {
                    return keyWindow;
                }
                keyWindow = nil;
            }
        }
    }
    
    if ([application respondsToSelector:@selector(windows)]) {
        keyWindow = zw_sharedApplication().windows.firstObject;
        if (keyWindow && [keyWindow isKindOfClass:[UIWindow class]]) {
            return keyWindow;
        }
        keyWindow = nil;
    }
    NSLog(@"❌获取应用主窗口keywindow失败【%@】", keyWindow);
    return keyWindow;
}

void zw_removeLaunchScreenCacheIfNeeded(void)
{
    NSString *filePath = [NSString stringWithFormat:@"%@/Library/SplashBoard", NSHomeDirectory()];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath])
    {
        NSError *error = nil;
        [[NSFileManager defaultManager] removeItemAtPath:filePath error:&error];
        
        if (error)
        {
            NSLog(@"清除LaunchScreen缓存失败");
        }
        else
        {
            NSLog(@"清除LaunchScreen缓存成功");
        }
    }
}

void zw_applicationBeginIgnoringInteractionEvents(void)
{
    UIApplication* app = zw_sharedApplication();
    if(!app || ![app respondsToSelector:@selector(beginIgnoringInteractionEvents)])
    {
        return;
    }
    
    [app performSelector:@selector(beginIgnoringInteractionEvents)];
}

void zw_applicationEndIgnoringInteractionEvents(void)
{
    UIApplication* app = zw_sharedApplication();
    if(!app || ![app respondsToSelector:@selector(endIgnoringInteractionEvents)])
    {
        return;
    }
    
    [app performSelector:@selector(endIgnoringInteractionEvents)];
}

/*
 * https://www.innerfence.com/howto/apple-ios-devices-dates-versions-instruction-sets
 * https://www.theiphonewiki.com/wiki/Models
 * desc: get ios device type
 */
NSString* zw_systemDeviceString(void)
{
    struct utsname systemInfo;
    
    uname(&systemInfo);
    
    NSString *platform = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    
    
    if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone (1st generation)";
    
    if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    
    if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    
    if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4 (GSM)";
    
    if ([platform isEqualToString:@"iPhone3,2"])    return @"iPhone 4 (GSM Rev A)";
    
    if ([platform isEqualToString:@"iPhone3,3"])    return @"iPhone 4 (CDMA)";
    
    if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
    
    if ([platform isEqualToString:@"iPhone5,1"])    return @"iPhone 5 (GSM/LTE)";
    
    if ([platform isEqualToString:@"iPhone5,2"])    return @"iPhone 5 (CDMA/LTE)";
    
    if ([platform isEqualToString:@"iPhone5,3"])    return @"iPhone 5c (GSM/LTE)";
    
    if ([platform isEqualToString:@"iPhone5,4"])    return @"iPhone 5c (CDMA/LTE)";
    
    if ([platform isEqualToString:@"iPhone6,1"])    return @"iPhone 5s (GSM/LTE)";
    
    if ([platform isEqualToString:@"iPhone6,2"])    return @"iPhone 5s (CDMA/LTE)";
    
    if ([platform isEqualToString:@"iPhone7,1"])    return @"iPhone 6 Plus";
    
    if ([platform isEqualToString:@"iPhone7,2"])    return @"iPhone 6";
    
    if ([platform isEqualToString:@"iPhone8,1"])    return @"iPhone 6s";
    
    if ([platform isEqualToString:@"iPhone8,2"])    return @"iPhone 6s Plus";
    
    if ([platform isEqualToString:@"iPhone8,4"])    return @"iPhone SE";
    
    if ([platform isEqualToString:@"iPhone9,1"])    return @"iPhone 7 (CDMA+GSM/LTE)";
    
    if ([platform isEqualToString:@"iPhone9,2"])    return @"iPhone 7 Plus (CDMA+GSM/LTE)";
    
    if ([platform isEqualToString:@"iPhone9,3"])    return @"iPhone 7 (GSM/LTE)";
    
    if ([platform isEqualToString:@"iPhone9,4"])    return @"iPhone 7 Plus (GSM/LTE)";
    
    if ([platform isEqualToString:@"iPhone10,1"])    return @"iPhone 8 (CDMA+GSM/LTE)";
    
    if ([platform isEqualToString:@"iPhone10,2"])    return @"iPhone 8 Plus (CDMA+GSM/LTE)";
    
    if ([platform isEqualToString:@"iPhone10,3"])    return @"iPhone X (CDMA+GSM/LTE)";
    
    if ([platform isEqualToString:@"iPhone10,4"])    return @"iPhone 8 (GSM/LTE)";
    
    if ([platform isEqualToString:@"iPhone10,5"])    return @"iPhone 8 Plus (GSM/LTE)";
    
    if ([platform isEqualToString:@"iPhone10,6"])    return @"iPhone X (GSM/LTE)";
    
    if ([platform isEqualToString:@"iPhone11,2"])    return @"iPhone XS";
    
    if ([platform isEqualToString:@"iPhone11,4"])    return @"iPhone XS Max";
    
    if ([platform isEqualToString:@"iPhone11,6"])    return @"iPhone XS Max Global";
    
    if ([platform isEqualToString:@"iPhone11,8"])    return @"iPhone XR";
    
    if ([platform isEqualToString:@"iPhone12,1"])    return @"iPhone 11";
    
    if ([platform isEqualToString:@"iPhone12,3"])    return @"iPhone 11 Pro";
    
    if ([platform isEqualToString:@"iPhone12,5"])    return @"iPhone 11 Pro Max";
    
    if ([platform isEqualToString:@"iPhone12,8"])    return @"iPhone SE (2nd generation)";
    
    if ([platform isEqualToString:@"iPhone13,1"])    return @"iPhone 12 mini";
    
    if ([platform isEqualToString:@"iPhone13,2"])    return @"iPhone 12";
    
    if ([platform isEqualToString:@"iPhone13,3"])    return @"iPhone 12 Pro";
    
    if ([platform isEqualToString:@"iPhone13,4"])    return @"iPhone 12 Pro Max";
    
    if ([platform isEqualToString:@"iPhone14,4"])    return @"iPhone 13 mini";
    
    if ([platform isEqualToString:@"iPhone14,5"])    return @"iPhone 13";
    
    if ([platform isEqualToString:@"iPhone14,2"])    return @"iPhone 13 Pro";
    
    if ([platform isEqualToString:@"iPhone14,3"])    return @"iPhone 13 Pro Max";
    
    if ([platform isEqualToString:@"iPhone14,6"])    return @"iPhone SE (3rd generation)";
    
    if ([platform isEqualToString:@"iPhone14,7"])    return @"iPhone 14";
    
    if ([platform isEqualToString:@"iPhone14,8"])    return @"iPhone 14 Plus";
    
    if ([platform isEqualToString:@"iPhone15,2"])    return @"iPhone 14 Pro";
    
    if ([platform isEqualToString:@"iPhone15,3"])    return @"iPhone 14 Pro Max";
    
    if ([platform isEqualToString:@"iPhone15,4"])    return @"iPhone 15";
    
    if ([platform isEqualToString:@"iPhone15,5"])    return @"iPhone 15 Plus";
    
    if ([platform isEqualToString:@"iPhone16,1"])    return @"iPhone 15 Pro";
    
    if ([platform isEqualToString:@"iPhone16,2"])    return @"iPhone 15 Pro Max";
    
    if ([platform isEqualToString:@"iPhone17,3"])    return @"iPhone 16";
    
    if ([platform isEqualToString:@"iPhone17,4"])    return @"iPhone 16 Plus";
    
    if ([platform isEqualToString:@"iPhone17,1"])    return @"iPhone 16 Pro";
    
    if ([platform isEqualToString:@"iPhone17,2"])    return @"iPhone 16 Pro Max";
    
    
    
    
    if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
    
    if ([platform isEqualToString:@"iPad1,2"])      return @"iPad 3G";
    
    if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
    
    if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2 (GSM)";
    
    if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
    
    if ([platform isEqualToString:@"iPad2,4"])      return @"iPad 2 (Wi‑Fi, A5R)";
    
    if ([platform isEqualToString:@"iPad2,5"])      return @"iPad Mini (WiFi)";
    
    if ([platform isEqualToString:@"iPad2,6"])      return @"iPad mini (GSM/LTE)";
    
    if ([platform isEqualToString:@"iPad2,7"])      return @"iPad mini (CDMA/LTE)";
    
    if ([platform isEqualToString:@"iPad3,1"])      return @"iPad 3 (WiFi)";
    
    if ([platform isEqualToString:@"iPad3,2"])      return @"iPad 3 (GSM/LTE)";
    
    if ([platform isEqualToString:@"iPad3,3"])      return @"iPad 3 (CDMA/LTE)";
    
    if ([platform isEqualToString:@"iPad3,4"])      return @"iPad 4 (WiFi)";
    
    if ([platform isEqualToString:@"iPad3,5"])      return @"iPad 4 (GSM/LTE)";
    
    if ([platform isEqualToString:@"iPad3,6"])      return @"iPad 4 (CDMA/LTE)";
    
    if ([platform isEqualToString:@"iPad4,1"])      return @"iPad Air (Wi‑Fi)";
    
    if ([platform isEqualToString:@"iPad4,2"])      return @"iPad Air (LTE)";
    
    if ([platform isEqualToString:@"iPad4,3"])      return @"iPad Air (China)";
    
    if ([platform isEqualToString:@"iPad4,4"])      return @"iPad mini 2 (Wi‑Fi)";
    
    if ([platform isEqualToString:@"iPad4,5"])      return @"iPad mini 2 (LTE)";
    
    if ([platform isEqualToString:@"iPad4,6"])      return @"iPad mini 2 (China)";
    
    if ([platform isEqualToString:@"iPad4,7"])      return @"iPad mini 3 (Wi‑Fi)";
    
    if ([platform isEqualToString:@"iPad4,8"])      return @"iPad mini 3 (LTE)";
    
    if ([platform isEqualToString:@"iPad4,9"])      return @"iPad mini 3 (China)";
    
    if ([platform isEqualToString:@"iPad5,1"])      return @"iPad mini 4 (Wi‑Fi)";
    
    if ([platform isEqualToString:@"iPad5,2"])      return @"iPad mini 4 (LTE)";
    
    if ([platform isEqualToString:@"iPad5,3"])      return @"iPad Air 2 (Wi‑Fi)";
    
    if ([platform isEqualToString:@"iPad5,4"])      return @"iPad Air 2 (LTE)";
    
    if ([platform isEqualToString:@"iPad6,3"])      return @"iPad Pro (9.7\" Wi‑Fi)";
    
    if ([platform isEqualToString:@"iPad6,4"])      return @"iPad Pro (9.7\" LTE)";
    
    if ([platform isEqualToString:@"iPad6,7"])      return @"iPad Pro (12.9\" Wi‑Fi)";
    
    if ([platform isEqualToString:@"iPad6,8"])      return @"iPad Pro (12.9\" LTE)";
    
    if ([platform isEqualToString:@"iPad6,11"])      return @"iPad 5 (Wi‑Fi)";
    
    if ([platform isEqualToString:@"iPad6,12"])      return @"iPad 5 (LTE)";
    
    if ([platform isEqualToString:@"iPad7,1"])      return @"iPad Pro 2 (12.9\" Wi‑Fi)";
    
    if ([platform isEqualToString:@"iPad7,2"])      return @"iPad Pro 2 (12.9\" LTE)";
    
    if ([platform isEqualToString:@"iPad7,3"])      return @"iPad Pro (10.5\" Wi‑Fi)";
    
    if ([platform isEqualToString:@"iPad7,4"])      return @"iPad Pro (10.5\" LTE)";
    
    if ([platform isEqualToString:@"iPad7,5"])      return @"iPad 6th Gen (WiFi)";
    
    if ([platform isEqualToString:@"iPad7,6"])      return @"iPad 6th Gen (WiFi+Cellular)";
    
    if ([platform isEqualToString:@"iPad7,11"])      return @"iPad 7th Gen 10.2-inch (WiFi)";
    
    if ([platform isEqualToString:@"iPad7,12"])      return @"iPad 7th Gen 10.2-inch (WiFi+Cellular)";
    
    if ([platform isEqualToString:@"iPad8,1"])      return @"iPad Pro (11 inch, WiFi)";
    
    if ([platform isEqualToString:@"iPad8,2"])      return @"iPad Pro (11 inch, 1TB, WiFi)";
    
    if ([platform isEqualToString:@"iPad8,3"])      return @"iPad Pro (11 inch, WiFi+Cellular)";
    
    if ([platform isEqualToString:@"iPad8,4"])      return @"iPad Pro (11 inch, 1TB, WiFi+Cellular)";
    
    if ([platform isEqualToString:@"iPad8,5"])      return @"iPad Pro 3rd Gen (12.9 inch, WiFi)";
    
    if ([platform isEqualToString:@"iPad8,6"])      return @"iPad Pro 3rd Gen (12.9 inch, 1TB, WiFi)";
    
    if ([platform isEqualToString:@"iPad8,7"])      return @"iPad Pro 3rd Gen (12.9 inch, WiFi+Cellular)";
    
    if ([platform isEqualToString:@"iPad8,8"])      return @"iPad Pro 3rd Gen (12.9 inch, 1TB, WiFi+Cellular)";
    
    if ([platform isEqualToString:@"iPad8,9"])      return @"iPad Pro 2nd Gen (11 inch, WiFi)";
    
    if ([platform isEqualToString:@"iPad8,10"])      return @"iPad Pro 2nd Gen (11 inch, WiFi+Cellular)";
    
    if ([platform isEqualToString:@"iPad8,11"])      return @"iPad Pro 4rd Gen  (12.9 inch, WiFi)";
    
    if ([platform isEqualToString:@"iPad8,12"])      return @"iPad Pro 4rd Gen  (12.9 inch, WiFi+Cellular)";
    
    if ([platform isEqualToString:@"iPad11,1"])      return @"iPad mini 5th Gen (WiFi)";
    
    if ([platform isEqualToString:@"iPad11,2"])      return @"iPad mini 5th Gen";
    
    if ([platform isEqualToString:@"iPad11,3"])      return @"iPad Air 3rd Gen (WiFi)";
    
    if ([platform isEqualToString:@"iPad11,4"])      return @"iPad Air 3rd Gen (LTE)";
    
    if ([platform isEqualToString:@"iPad11,6"])      return @"iPad 8th Gen (WiFi)";
    
    if ([platform isEqualToString:@"iPad11,7"])      return @"iPad 8th Gen (WiFi+Cellular)";
    
    if ([platform isEqualToString:@"iPad12,1"])      return @"iPad 9th Gen (WiFi)";
    
    if ([platform isEqualToString:@"iPad12,2"])      return @"iPad 9th Gen (WiFi+Cellular)";
    
    if ([platform isEqualToString:@"iPad13,1"])      return @"iPad Air 4th Gen (WiFi)";
    
    if ([platform isEqualToString:@"iPad13,2"])      return @"iPad Air 4th Gen (LTE)";
    
    if ([platform isEqualToString:@"iPad13,4"])      return @"iPad Pro 3rd Gen (11 inch, WiFi)";
    
    if ([platform isEqualToString:@"iPad13,5"])      return @"iPad Pro 3rd Gen (11 inch, WiFi)";
    
    if ([platform isEqualToString:@"iPad13,6"])      return @"iPad Pro 3rd Gen (11 inch, WiFi+Cellular)";
    
    if ([platform isEqualToString:@"iPad13,7"])      return @"iPad Pro 3rd Gen (11 inch, WiFi+Cellular)";
    
    if ([platform isEqualToString:@"iPad13,8"])      return @"iPad Pro 5th Gen  (12.9 inch, WiFi)";
    
    if ([platform isEqualToString:@"iPad13,9"])      return @"iPad Pro 5th Gen (12.9 inch, WiFi)";
    
    if ([platform isEqualToString:@"iPad13,10"])      return @"iPad Pro 5th Gen (12.9 inch, WiFi+Cellular)";
    
    if ([platform isEqualToString:@"iPad13,11"])      return @"iPad Pro 5th Gen (12.9 inch, WiFi+Cellular)";
    
    if ([platform isEqualToString:@"iPad14,1"])      return @"iPad mini 6th Gen (Wi-Fi Only)";
    
    if ([platform isEqualToString:@"iPad14,2"])      return @"iPad mini 6th Gen (Wi-Fi+Cell)";
    
    if ([platform isEqualToString:@"iPad14,3"])      return @"iPad Pro (11-inch) (WiFi Only 4th generation)";
    
    if ([platform isEqualToString:@"iPad14,4"])      return @"iPad Pro (11-inch) (WiFi/Cell 4th generation)";
    
    if ([platform isEqualToString:@"iPad14,5"])      return @"iPad Pro (12.9-inch) (WiFi Only 6th generation)";
    
    if ([platform isEqualToString:@"iPad14,6"])      return @"iPad Pro (12.9-inch) (WiFi/Cell 6th generation)";
    
    if ([platform isEqualToString:@"iPad16,3"])      return @"iPad Pro M4 (11-inch) (Wi-Fi Only)";
    
    if ([platform isEqualToString:@"iPad16,4"])      return @"iPad Pro M4 (11-inch) (Wi-Fi+Cell)";
    
    if ([platform isEqualToString:@"iPad16,5"])      return @"iPad Pro M4 (13-inch) (Wi-Fi Only)";
    
    if ([platform isEqualToString:@"iPad16,6"])      return @"iPad Pro M4 (13-inch) (Wi-Fi+Cell)";
    
    
    
    
    if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch (1 Gen)";
    
    if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch (2 Gen)";
    
    if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch (3 Gen)";
    
    if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch (4 Gen)";
    
    if ([platform isEqualToString:@"iPod5,1"])      return @"iPod Touch (5 Gen)";
    
    if ([platform isEqualToString:@"iPod7,1"])      return @"iPod Touch (6 Gen)";
    
    if ([platform isEqualToString:@"iPod9,1"])      return @"iPod Touch (7 Gen)";
    
    
    
    
    if ([platform isEqualToString:@"i386"])         return @"Simulator（32位）";
    
    if ([platform isEqualToString:@"x86_64"])       return @"Simulator（64位）";
    
    return platform;
}

NSString* zw_wifiName(void)
{
    NSString *wifiName = nil;
    
    CFArrayRef wifiInterfaces = CNCopySupportedInterfaces();
    if (!wifiInterfaces) {
        return nil;
    }
    
    NSArray *interfaces = (__bridge NSArray *)wifiInterfaces;
    
    for (NSString *interfaceName in interfaces)
    {
        CFDictionaryRef dictRef = CNCopyCurrentNetworkInfo((__bridge CFStringRef)(interfaceName));
        
        if (dictRef)
        {
            NSDictionary *networkInfo = (__bridge NSDictionary *)dictRef;
            
            wifiName = [networkInfo objectForKey:(__bridge NSString *)kCNNetworkInfoKeySSID];
            
            CFRelease(dictRef);
        }
    }
    
    CFRelease(wifiInterfaces);
    return wifiName;
}

#define IOS_CELLULAR    @"pdp_ip0"
#define IOS_WIFI        @"en0"
#define IOS_VPN         @"utun0"
#define IP_ADDR_IPv4    @"ipv4"
#define IP_ADDR_IPv6    @"ipv6"

NSString* zw_getIpAddress(BOOL isIPv4)
{
    NSArray *searchArray = isIPv4 ?
    @[ IOS_VPN @"/" IP_ADDR_IPv4, IOS_VPN @"/" IP_ADDR_IPv6, IOS_WIFI @"/" IP_ADDR_IPv4, IOS_WIFI @"/" IP_ADDR_IPv6, IOS_CELLULAR @"/" IP_ADDR_IPv4, IOS_CELLULAR @"/" IP_ADDR_IPv6 ] :
    @[ IOS_VPN @"/" IP_ADDR_IPv6, IOS_VPN @"/" IP_ADDR_IPv4, IOS_WIFI @"/" IP_ADDR_IPv6, IOS_WIFI @"/" IP_ADDR_IPv4, IOS_CELLULAR @"/" IP_ADDR_IPv6, IOS_CELLULAR @"/" IP_ADDR_IPv4 ] ;
    
    NSMutableDictionary *addresses = [NSMutableDictionary dictionaryWithCapacity:8];
    
    // retrieve the current interfaces - returns 0 on success
    struct ifaddrs *interfaces;
    if(!getifaddrs(&interfaces)) {
        // Loop through linked list of interfaces
        struct ifaddrs *interface;
        for(interface=interfaces; interface; interface=interface->ifa_next) {
            if(!(interface->ifa_flags & IFF_UP) /* || (interface->ifa_flags & IFF_LOOPBACK) */ ) {
                continue; // deeply nested code harder to read
            }
            const struct sockaddr_in *addr = (const struct sockaddr_in*)interface->ifa_addr;
            char addrBuf[ MAX(INET_ADDRSTRLEN, INET6_ADDRSTRLEN) ];
            if(addr && (addr->sin_family==AF_INET || addr->sin_family==AF_INET6)) {
                NSString *name = [NSString stringWithUTF8String:interface->ifa_name];
                NSString *type;
                if(addr->sin_family == AF_INET) {
                    if(inet_ntop(AF_INET, &addr->sin_addr, addrBuf, INET_ADDRSTRLEN)) {
                        type = IP_ADDR_IPv4;
                    }
                } else {
                    const struct sockaddr_in6 *addr6 = (const struct sockaddr_in6*)interface->ifa_addr;
                    if(inet_ntop(AF_INET6, &addr6->sin6_addr, addrBuf, INET6_ADDRSTRLEN)) {
                        type = IP_ADDR_IPv6;
                    }
                }
                if(type) {
                    NSString *key = [NSString stringWithFormat:@"%@/%@", name, type];
                    addresses[key] = [NSString stringWithUTF8String:addrBuf];
                }
            }
        }
        // Free memory
        freeifaddrs(interfaces);
    }
    
    if ([addresses count] <= 0)
    {
        addresses = nil;
    }
    
    NSLog(@"IP Address Info: %@", addresses);
    
    __block NSString *address;
    [searchArray enumerateObjectsUsingBlock:^(NSString *key, NSUInteger idx, BOOL *stop)
     {
         address = addresses[key];
         //筛选出合法的IP地址格式
         if([address zw_isValidIpAddress])
         {
             *stop = YES;
         }
     } ];
    return address ? address : @"0.0.0.0";
}

NSString* zw_fileSizeToString(unsigned long long fileSize)
{
    unsigned long long KB = 1024;
    unsigned long long MB = KB*KB;
    unsigned long long GB = MB*KB;
    
    if (fileSize < KB)
    {
        return [NSString stringWithFormat:@"%llu B",fileSize];
    }
    else if (fileSize < MB)
    {
        return [NSString stringWithFormat:@"%.1f KB",((CGFloat)fileSize)/KB];
    }
    else if (fileSize < GB)
    {
        return [NSString stringWithFormat:@"%.1f MB",((CGFloat)fileSize)/MB];
    }
    else
    {
        return [NSString stringWithFormat:@"%.1f GB",((CGFloat)fileSize)/GB];
    }
}

NSString* zw_fileSizeToAppleString(unsigned long long fileSize)
{
    unsigned long long KB = 1000;
    unsigned long long MB = KB*KB;
    unsigned long long GB = MB*KB;
    
    if (fileSize < KB)
    {
        return [NSString stringWithFormat:@"%llu B",fileSize];
    }
    else if (fileSize < MB)
    {
        return [NSString stringWithFormat:@"%.1f KB",((CGFloat)fileSize)/KB];
    }
    else if (fileSize < GB)
    {
        return [NSString stringWithFormat:@"%.1f MB",((CGFloat)fileSize)/MB];
    }
    else
    {
        return [NSString stringWithFormat:@"%.1f GB",((CGFloat)fileSize)/GB];
    }
}

unsigned long long zw_getTotalMemorySize(void)
{
    return [NSProcessInfo processInfo].physicalMemory;
}

unsigned long long zw_getAvailableMemorySize(void)
{
    vm_statistics_data_t vmStats;
    mach_msg_type_number_t infoCount = HOST_VM_INFO_COUNT;
    kern_return_t kernReturn = host_statistics(mach_host_self(), HOST_VM_INFO, (host_info_t)&vmStats, &infoCount);
    if (kernReturn != KERN_SUCCESS)
    {
        return NSNotFound;
    }
    return ((vm_page_size * vmStats.free_count + vm_page_size * vmStats.inactive_count));
}

unsigned long long zw_getTotalDiskSize(void)
{
    struct statfs buf;
    unsigned long long freeSpace = -1;
    if (statfs("/var", &buf) >= 0)
    {
        freeSpace = (unsigned long long)(buf.f_bsize * buf.f_blocks);
    }
    return freeSpace;
}

unsigned long long zw_getAvailabelDiskSize(void)
{
    struct statfs buf;
    unsigned long long freeSpace = -1;
    if (statfs("/var", &buf) >= 0)
    {
        freeSpace = (unsigned long long)(buf.f_bsize * buf.f_bavail);
    }
    return freeSpace;
}

unsigned long long zw_getFileSizeWithoutSubfoldersAtPath(NSString *filePath)
{
    unsigned long long fileSize = 0;
    if(zw_isFileExistsAtPath(filePath))
    {
        NSDictionary* attributesOfItem = zw_getFileAttributesAtPath(filePath);
        fileSize = attributesOfItem.fileSize;
    }
    return fileSize; 
}

unsigned long long zw_getFileSizeAtPath(NSString* filePath)
{
    unsigned long long folderSize = zw_getFileSizeWithoutSubfoldersAtPath(filePath);
    if (zw_isFileExistsAtPath(filePath))
    {
        //如果是文件夹，还要计算其所有子目录的文件大小
        if (zw_isFolderPath(filePath))
        {
            // subpathsAtPath 会递归获取指定路径下所有子目录的文件和文件夹（直到某个子目录被定位到文件类型为止）
            // contentsOfDirectoryAtPath 只是获取指定路径下一级目录的文件或者文件夹
            NSArray *allChildrenFiles =[[NSFileManager defaultManager] subpathsAtPath:filePath];
            for (NSString *tmpPath in allChildrenFiles)
            {
                NSString *absolutePath=[filePath stringByAppendingPathComponent:tmpPath];
                folderSize += zw_getFileSizeWithoutSubfoldersAtPath(absolutePath);
            }
        }
    }
    return folderSize;
}

NSString* zw_createUUID(void)
{
    //NSUUID在iOS6中才出现，这跟CFUUID几乎完全一样，只不过它是Objective-C接口。
    //读取NSUUID时，注意到获取到的这个值跟CFUUID的值几乎不可能一样，但也有170亿分之1的可能一样。
    NSString* iOS6UUID = [[NSUUID UUID] UUIDString];
    iOS6UUID = [iOS6UUID stringByAppendingString:@"_iOS"];
    return iOS6UUID;
}

NSString* zw_createUUID_Ex(NSString* prefix, NSString* suffix)
{
    NSMutableString* resultString =[NSMutableString string];
    if (prefix && [prefix isKindOfClass:[NSString class]] && prefix.length>0)
    {
        [resultString appendString:prefix];
    }
    [resultString appendString:zw_createUUID()];
    if (suffix && [suffix isKindOfClass:[NSString class]] && suffix.length>0)
    {
        [resultString appendString:suffix];
    }
    return resultString;
}

NSString* zw_createRandDigitalAlphabetStringWithLenth(NSInteger stringLength)
{
    if (stringLength <= 0)
    {
        return nil;
    }
    
    NSString *sourceStr = @"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    NSMutableString *resultStr = [[NSMutableString alloc] init];
    srand((unsigned)time(0));
    for (int i = 0; i < stringLength; i++)
    {
        unsigned index = rand() % [sourceStr length];
        NSString *oneStr = [sourceStr substringWithRange:NSMakeRange(index, 1)];
        [resultStr appendString:oneStr];
    }
    return resultStr;
}

BOOL zw_openURLString(NSString* url)
{
    BOOL bReturn = NO;
    if (url && [url isKindOfClass:[NSString class]] && url.length > 0)
    {
        NSURL *myURL = [NSURL URLWithString:url];
        UIApplication* app = zw_sharedApplication();
        if (app && [app respondsToSelector:@selector(openURL:)])
        {
            NSNumber* resultValue = zw_getResultValueAfterTargetPerformSelector(app, @selector(openURL:), [NSArray arrayWithObjects:myURL, nil]);
            if ([resultValue isKindOfClass:[NSNumber class]])
            {
                bReturn = [resultValue boolValue];
            }
        }
    }
    return bReturn;
}

BOOL zw_openURLStringWithCallbackHandler(NSString* url, NSDictionary* options, void (^handler)(BOOL success))
{
    BOOL bReturn = NO;
    if (url && [url isKindOfClass:[NSString class]] && url.length > 0)
    {
        NSURL *myURL = [NSURL URLWithString:url];
        NSMutableArray* tmpParameters = [[NSMutableArray alloc] init];
        [tmpParameters addObject:myURL];
        if (options)
        {
            [tmpParameters addObject:options];
        }
        else
        {
            [tmpParameters addObject:[NSNull null]];
        }
        if (handler)
        {
            [tmpParameters addObject:handler];
        }
        else
        {
            [tmpParameters addObject:[NSNull null]];
        }
        UIApplication* app = zw_sharedApplication();
        if (app && [app respondsToSelector:@selector(openURL:options:completionHandler:)])
        {
            NSNumber* resultValue = zw_getResultValueAfterTargetPerformSelector(app, @selector(openURL:options:completionHandler:), tmpParameters);
            if ([resultValue isKindOfClass:[NSNumber class]])
            {
                bReturn = [resultValue boolValue];
            }
        }
    }
    return bReturn;
}

BOOL zw_canOpenURLString(NSString* url)
{
    BOOL bReturn = NO;
    if (url && [url isKindOfClass:[NSString class]] && url.length > 0)
    {
        NSURL *myURL = [NSURL URLWithString:url];
        UIApplication* app = zw_sharedApplication();
        if (app && [app respondsToSelector:@selector(canOpenURL:)])
        {
            NSNumber* resultValue = zw_getResultValueAfterTargetPerformSelector(app, @selector(canOpenURL:), [NSArray arrayWithObjects:myURL, nil]);
            if ([resultValue isKindOfClass:[NSNumber class]])
            {
                bReturn = [resultValue boolValue];
            }
        }
    }
    return bReturn;
}

BOOL zw_openOtherApp(NSString* schemesString)
{
    return zw_openURLString([schemesString stringByAppendingString:@"://"]);
}

BOOL zw_openOtherAppWithCallbackHandler(NSString* schemesString, NSDictionary* options, void (^handler)(BOOL success))
{
    return zw_openURLStringWithCallbackHandler([schemesString stringByAppendingString:@"://"], options, handler);
}

BOOL zw_canOpenOtherApp(NSString* schemesString)
{
    return zw_canOpenURLString([schemesString stringByAppendingString:@"://"]);
}

void zw_openAppSetting_iOS8OrLater(void)
{
    if (zw_ios8OrLater())
    {
        zw_openURLString([NSURL URLWithString:UIApplicationOpenSettingsURLString].absoluteString);
    }
}

void zw_callWithPhone(NSString* phone)
{
    NSString *cleanedString = [[phone componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
    NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", cleanedString]];
    zw_openURLString(telURL.absoluteString);
}

NSString* zw_deviceOwnerAuthenticatedErrorReferenceDescription(NSError* error)
{
    NSString* errorReferenceDescription = nil;
    if (error)
    {
        switch (error.code)
        {
            case LAErrorAuthenticationFailed://LAPolicyDeviceOwnerAuthentication方式不会返回此错误码，因为连续3次生物识别技术验证身份失败后系统会自动调起密码验证功能
            {
                errorReferenceDescription = [NSString stringWithFormat:@"连续3次生物识别技术验证身份失败——LAErrorAuthenticationFailed【%@】", error];
            }
                break;
            case LAErrorUserCancel:
            {
                errorReferenceDescription = [NSString stringWithFormat:@"用生物识别技术验证身份时用户点击了取消按钮(也可能是Home按钮)，或者连续3次生物识别技术验证身份失败后用户点击了密码验证框上的取消按钮——LAErrorUserCancel【%@】", error];
            }
                break;
            case LAErrorUserFallback://LAPolicyDeviceOwnerAuthentication方式不会返回此错误码，因为用户点击localizedFallbackTitle按钮会自动调起密码验证功能
            {
                errorReferenceDescription = [NSString stringWithFormat:@"用户点击了Fallback按钮，在这里可以做一些自定义的操作——LAErrorUserFallback【%@】", error];
            }
                break;
            case LAErrorSystemCancel:
            {
                errorReferenceDescription = [NSString stringWithFormat:@"用户主动切入其他App，不在刚刚那个页面，所以没法进行生物识别技术身份验证，被系统取消——LAErrorSystemCancel【%@】", error];
            }
                break;
            case LAErrorPasscodeNotSet:
            {
                errorReferenceDescription = [NSString stringWithFormat:@"设备没有设置密码——LAErrorPasscodeNotSet【%@】", error];
            }
                break;
            case LAErrorBiometryNotAvailable:
            {
                errorReferenceDescription = [NSString stringWithFormat:@"设备不支持生物识别技术——LAErrorBiometryNotAvailable【%@】", error];
            }
                break;
            case LAErrorBiometryNotEnrolled:
            {
                errorReferenceDescription = [NSString stringWithFormat:@"设备没有录入生物识别技术数据(如指纹数据，或者人脸数据)——LAErrorBiometryNotEnrolled【%@】", error];
            }
                break;
            case LAErrorBiometryLockout://LAPolicyDeviceOwnerAuthentication方式不会返回此错误码，因为连续3次身份验证失败后系统会自动调起密码验证功能，密码验证成功即抛出成功，否则一直在密码验证页永远不会达到连续5次生物识别技术验证身份失败
            {
                errorReferenceDescription = [NSString stringWithFormat:@"连续5次生物识别技术验证身份失败，设备被锁死(可能会被锁一分钟或者更长时间)，输入数字密码后才能再次进行生物识别技术身份验证——LAErrorBiometryLockout【%@】", error];
            }
                break;
            case LAErrorAppCancel:
            {
                errorReferenceDescription = [NSString stringWithFormat:@"在用户不能控制情况下APP被挂起，生物识别技术验证身份被其他App中断——LAErrorAppCancel【%@】", error];
            }
                break;
            case LAErrorInvalidContext:
            {
                errorReferenceDescription = [NSString stringWithFormat:@"请求生物识别技术验证身份出错，可能是LAContext已经失效——LAErrorInvalidContext【%@】", error];
            }
                break;
            case LAErrorNotInteractive:
            {
                errorReferenceDescription = [NSString stringWithFormat:@"生物识别技术验证身份失败，因为它需要显示已被禁止的UI——LAErrorNotInteractive【%@】", error];
            }
                break;
            default:
            {
                errorReferenceDescription = [NSString stringWithFormat:@"其他进行生物识别技术验证身份的错误(如模拟器不支持生物识别技术)【%@】", error];
            }
                break;
        }
    }
    return errorReferenceDescription;
}

void zw_canUseDeviceOwnerAuthentication(void(^checkHandler)(LAContext* pLAContext, BOOL canUse, NSError* error, NSString* errorReferenceDescription))
{
    if (zw_ios9OrLater())
    {
        /**
         *  本地认证上下文联系对象，每次使用设备所有者授权都要重新初始化，否则会一直显示验证成功。
         *  ⭐️LAContext实例对象的biometryType属性仅在调用了canEvaluatePolicy方法之后且返回YES时才会被赋值，否则可能一直返回LABiometryNone
         */
        __block LAContext* pLAContext = [[LAContext alloc] init];
        NSError * error = nil;
        if ([pLAContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthentication error:&error])
        {
            if (checkHandler)
            {
                checkHandler(pLAContext, YES, error, nil);
            }
        }
        else
        {
            if (checkHandler)
            {
                checkHandler(pLAContext, NO, error, zw_deviceOwnerAuthenticatedErrorReferenceDescription(error));
            }
        }
    }
    else
    {
        if (checkHandler)
        {
            checkHandler(nil, NO, nil, @"系统版本小iOS9.0");
        }
    }
}

void zw_useDeviceOwnerAuthentication(void(^checkHandler)(LAContext* pLAContext, BOOL canUse, NSError* error, NSString* errorReferenceDescription),
                                     NSString* resonInSystemDialog,
                                     NSString*fallbackTitle,
                                     void(^reply)(LAContext* pLAContext, BOOL success, NSError* error, NSString* errorReferenceDescription))
{
    /**
     *  生物识别技术验证框或密码框上的副标题（主标题有系统提供）
     *  localizedReason的参数不能为nil或者@""字符串，即字符长度不能为0，否则将导致Crash（Non-empty localizedReason must be provided.）
     */
    if (resonInSystemDialog.length <= 0)
    {
        resonInSystemDialog = @" ";
    }
    
    zw_canUseDeviceOwnerAuthentication(^(LAContext* pLAContext, BOOL canUse, NSError *error, NSString *errorReferenceDescription) {
        if (checkHandler)
        {
            checkHandler(pLAContext, canUse, error, errorReferenceDescription);
        }
        
        if (canUse)
        {
            /**
             *  localizedFallbackTitle属性是设备所有者授权验证失败后第二个按钮的文字；如果不设置或者设置为nil，第二个按钮默认标题为<输入密码>；如果不想有第二个按钮的话，可以设置为空字符串<@"">。
             */
            pLAContext.localizedFallbackTitle = fallbackTitle;
            /**
             *  生物识别技术验证框或密码框上的副标题（主标题有系统提供）
             *  localizedReason的参数不能为nil或者@""字符串，即字符长度不能为0，否则将导致Crash（Non-empty localizedReason must be provided.）
             *  reply是回调处理。内含两个回调参数success和error。success是BOOL值，YES表示验证成功，NO表示验证失败。
             */
            [pLAContext evaluatePolicy:LAPolicyDeviceOwnerAuthentication
                       localizedReason:resonInSystemDialog
                                 reply:^(BOOL success, NSError * _Nullable error)
             {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (reply)
                    {
                        reply(pLAContext, success, error, zw_deviceOwnerAuthenticatedErrorReferenceDescription(error));
                    }
                });
            }];
        }
        else
        {
            if (reply)
            {
                reply(pLAContext, NO, error, errorReferenceDescription);
            }
        }
    });
}

PHAuthorizationStatus zw_privacyAuthorizationStatusOfPhotoLibrary(void)
{
    return [PHPhotoLibrary authorizationStatus];
}

void zw_requestPrivacyAccessAboutPhotoLibrary(void(^completionHandler)(PHAuthorizationStatus status))
{
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             if (completionHandler)
             {
                 completionHandler(status);
             }
         });
     }];
}

BOOL zw_isCameraAvailable(void)
{
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        return YES;
    }
    NSLog(@"您的设备没有摄像头或者相关的驱动");
    return NO;
}

AVAuthorizationStatus zw_privacyAuthorizationStatusOfCamera(void)
{
    return [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
}

AVAuthorizationStatus zw_privacyAuthorizationStatusOfMicrophone(void)
{
    return [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeAudio];
}

void zw_requestPrivacyAccessAboutDeviceCaptureWithMediaType(NSString* mediaType, void(^completionHandler)(BOOL granted))
{
    [AVCaptureDevice requestAccessForMediaType:mediaType completionHandler:^(BOOL granted)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             if (completionHandler)
             {
                 completionHandler(granted);
             }
         });
     }];
}

NSString* zw_getAppDisplayVersion(void)
{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
}

NSString* zw_getAppBuildVersion(void)
{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
}

NSString* zw_getSendToServerAppVersion(NSUInteger nDotCount)
{
    NSString* pCurentVersion = zw_getAppDisplayVersion();
    NSMutableArray *pVersions = [NSMutableArray arrayWithArray:[pCurentVersion componentsSeparatedByString:@"."]];
    if ([pVersions count] > nDotCount+1)
    {
        [pVersions removeObjectsInRange:NSMakeRange(nDotCount+1, [pVersions count]-(nDotCount+1))];
        pCurentVersion = [pVersions componentsJoinedByString:@"."];
    }
    else
    {
        while ([pVersions count] <= nDotCount)
        {
            [pVersions addObject:@"0"];
        }
        pCurentVersion = [pVersions componentsJoinedByString:@"."];
    }
    return pCurentVersion;
}

NSInteger zw_getAppVersionNumber(NSUInteger nDotCount)
{
    NSString* appversion = zw_getSendToServerAppVersion(nDotCount);
    NSArray* appversionArray = [appversion componentsSeparatedByString:@"."];
    NSMutableString* newversionNumberString = [[NSMutableString alloc] init];
    for (NSString* version in appversionArray)
    {
        [newversionNumberString appendFormat:@"%02ld", (long)[version integerValue]];
    }
    NSInteger resultValue = [newversionNumberString integerValue];
    return resultValue;
}

NSString* zw_getSendToServerDeviceType(void)
{
    NSString* pSystemDeviceString = [zw_systemDeviceString() lowercaseString];
    
    if ([pSystemDeviceString rangeOfString:@"ipod"].length > 0)
    {
        return @"iPod";
    }
    
    if ([pSystemDeviceString rangeOfString:@"ipad"].length > 0)
    {
        return @"iPad";
    }
    
    return @"iPhone";
}

NSString* zw_getAppNameDisplayOnDevice(void)
{
    NSString *pStrAppName =    [[[NSBundle mainBundle] localizedInfoDictionary] objectForKey:@"CFBundleDisplayName"]?: ([[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"]?: [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"]);
    return pStrAppName;
}

NSString* zw_getAppBundleIdentifier(void)
{
    NSString *pStrAppBundleIdentifier =    [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"];
    return pStrAppBundleIdentifier;
}

NSArray<NSString*>* zw_getAppIconFileNames(void)
{
    NSDictionary* infoPlist = [[NSBundle mainBundle] infoDictionary];
    return [infoPlist valueForKeyPath:@"CFBundleIcons.CFBundlePrimaryIcon.CFBundleIconFiles"];
}

NSArray<NSDictionary*>* zw_getLaunchImageFileNames(void)
{
    return [[[NSBundle mainBundle] infoDictionary] valueForKey:@"UILaunchImages"];
}

void zw_makeDeviceToShake(void)
{
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
}

NSString* zw_systemVersionString(void)
{
    return [[UIDevice currentDevice] systemVersion];
}

NSString* zw_systemTimeZoneString(void)
{
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    NSString *tzName = [timeZone name];
    return tzName;
}

NSString* zw_uniqueDeviceIdentifier(void)
{
    //该类方法没有线程保护，所以可能因异步而导致创建出不同的设备唯一ID，故而增加此线程锁！
    @synchronized ([NSNotificationCenter defaultCenter])
    {
        NSString* service = @"ZW_CreateDeviceIdentifierByKeychain";
        NSString* account = @"ZW_VirtualDeviceIdentifier";
        //获取iOS系统推荐的获取设备唯一ID
        NSString* recommendDeviceIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        if (recommendDeviceIdentifier.length <= 0)
        {
            //增加一个保险，如果获取失败就自动创建一个随机的唯一ID
            recommendDeviceIdentifier = zw_createUUID();
        }
        recommendDeviceIdentifier = [recommendDeviceIdentifier stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSMutableDictionary* queryDic = [NSMutableDictionary dictionary];
        [queryDic setObject:(__bridge id)kSecClassGenericPassword forKey:(__bridge id)kSecClass];
        [queryDic setObject:service forKey:(__bridge id)kSecAttrService];
        [queryDic setObject:(__bridge id)kCFBooleanFalse forKey:(__bridge id)kSecAttrSynchronizable];
        [queryDic setObject:account forKey:(__bridge id)kSecAttrAccount];
        [queryDic setObject:(__bridge id)kSecMatchLimitOne forKey:(__bridge id)kSecMatchLimit];//默认值为kSecMatchLimitOne，表示返回结果集的第一个
        [queryDic setObject:(__bridge id)kCFBooleanTrue forKey:(__bridge id)kSecReturnData];
        CFTypeRef keychainPassword = NULL;
        //首先查询钥匙串是否存在对应的值，如果存在则直接返回钥匙串中的值
        OSStatus queryResult = SecItemCopyMatching((__bridge CFDictionaryRef)queryDic, &keychainPassword);
        if (queryResult == errSecSuccess)
        {
            NSString *pwd = [[NSString alloc] initWithData:(__bridge NSData * _Nonnull)(keychainPassword) encoding:NSUTF8StringEncoding];
            if (keychainPassword) {
                CFRelease(keychainPassword);
            }
            if ([pwd isKindOfClass:[NSString class]] && pwd.length > 0)
            {
                return pwd;
            }
            else
            {
                //如果钥匙串中的相关数据不合法，则删除对应的数据重新创建
                NSMutableDictionary* deleteDic = [NSMutableDictionary dictionary];
                [deleteDic setObject:(__bridge id)kSecClassGenericPassword forKey:(__bridge id)kSecClass];
                [deleteDic setObject:service forKey:(__bridge id)kSecAttrService];
                [deleteDic setObject:(__bridge id)kCFBooleanFalse forKey:(__bridge id)kSecAttrSynchronizable];
                [deleteDic setObject:account forKey:(__bridge id)kSecAttrAccount];
                OSStatus status = SecItemDelete((__bridge CFDictionaryRef)deleteDic);
                if (status != errSecSuccess)
                {
                    return recommendDeviceIdentifier;
                }
            }
        } else {
            if (keychainPassword) {
                CFRelease(keychainPassword);
            }
        }
        if (recommendDeviceIdentifier.length > 0)
        {
            //创建数据到钥匙串，达到APP即使被删除也不会变更的设备唯一ID，除非系统抹除数据，否则该数据将存储在钥匙串中
            NSMutableDictionary* createDic = [NSMutableDictionary dictionary];
            [createDic setObject:(__bridge id)kSecClassGenericPassword forKey:(__bridge id)kSecClass];
            [createDic setObject:service forKey:(__bridge id)kSecAttrService];
            [createDic setObject:account forKey:(__bridge id)kSecAttrAccount];
            [createDic setObject:(__bridge id)kCFBooleanFalse forKey:(__bridge id)kSecAttrSynchronizable];//不可以使用iCloud同步钥匙串数据，否则导致同一个iCloud账户的多个设备获取的唯一ID相同
            [createDic setObject:(__bridge id)kSecAttrAccessibleAfterFirstUnlockThisDeviceOnly forKey:(__bridge id)kSecAttrAccessible];//增加一道保险，防止钥匙串数据被同步到其他设备，保证设备ID绝对唯一。
            [createDic setObject:[recommendDeviceIdentifier dataUsingEncoding:NSUTF8StringEncoding] forKey:(__bridge id)kSecValueData];
            OSStatus createResult = SecItemAdd((__bridge CFDictionaryRef)createDic, nil);
            if (createResult != errSecSuccess)
            {
                NSLog(@"通过钥匙串创建设备唯一ID不成功！");
            }
        }
        return recommendDeviceIdentifier;
    }
}

NSString* zw_getAppBaseInfoForFeedBack(BOOL isHtmlFormat)
{
    if (isHtmlFormat)
    {
        return [NSString stringWithFormat:@"Device:%@<br>iOSVersion:%@<br>Timezone:%@<br>AppName:%@<br>AppVersion:%@<br>", zw_systemDeviceString(), zw_systemVersionString(), zw_systemTimeZoneString(), zw_getAppNameDisplayOnDevice(), zw_getAppDisplayVersion()];
    }
    else
    {
        return [NSString stringWithFormat:@"Device:%@#iOSVersion:%@#Timezone:%@#AppName:%@#AppVersion:%@\n", zw_systemDeviceString(), zw_systemVersionString(), zw_systemTimeZoneString(), zw_getAppNameDisplayOnDevice(), zw_getAppDisplayVersion()];
    }
}

BOOL zw_isiPad(void)
{
    static BOOL bUnknowiPad = YES;
    static BOOL bIpad = NO;
    if (bUnknowiPad)
    {
        bIpad = IS_IPAD;
        bUnknowiPad = NO;
    }
    return bIpad;
}

BOOL zw_isiPhone4(void)
{
    static BOOL bUnknowiPhone4 = YES;
    static BOOL bIphone4 = NO;
    if (bUnknowiPhone4)
    {
        bIphone4 = IS_IPHONE_4_OR_LESS;
        bUnknowiPhone4 = NO;
    }
    return bIphone4;
}

BOOL zw_isiPhone5(void)
{
    static BOOL bUnknowiPhone5 = YES;
    static BOOL bIphone5 = NO;
    if (bUnknowiPhone5)
    {
        bIphone5 = IS_IPHONE_5;
        bUnknowiPhone5 = NO;
    }
    return bIphone5;
}

BOOL zw_isiPhone6(void)
{
    static BOOL bUnknowiPhone6 = YES;
    static BOOL bIphone6 = NO;
    if (bUnknowiPhone6)
    {
        bIphone6 = IS_IPHONE_6;
        bUnknowiPhone6 = NO;
    }
    return bIphone6;
}

BOOL zw_isiPhone6Plus(void)
{
    static BOOL bUnknowiPhone6Plus = YES;
    static BOOL bIphone6Plus = NO;
    if (bUnknowiPhone6Plus)
    {
        bIphone6Plus = IS_IPHONE_6P;
        bUnknowiPhone6Plus = NO;
    }
    return bIphone6Plus;
}

BOOL zw_isiPhoneX(void)
{
    static BOOL bUnknowiPhoneX = YES;
    static BOOL bIphoneX = NO;
    if (bUnknowiPhoneX)
    {
        bIphoneX = IS_IPHONE_X;
        bUnknowiPhoneX = NO;
    }
    return bIphoneX;
}

BOOL zw_isiPhoneXR(void)
{
    static BOOL bUnknowiPhoneXR = YES;
    static BOOL bIphoneXR = NO;
    if (bUnknowiPhoneXR)
    {
        bIphoneXR = IS_IPHONE_XR;
        bUnknowiPhoneXR = NO;
    }
    return bIphoneXR;
}

BOOL zw_isiPhoneXSMax(void)
{
    static BOOL bUnknowiPhoneXSMax = YES;
    static BOOL bIphoneXSMax = NO;
    if (bUnknowiPhoneXSMax)
    {
        bIphoneXSMax = IS_IPHONE_XSMax;
        bUnknowiPhoneXSMax = NO;
    }
    return bIphoneXSMax;
}

BOOL zw_isNoHomeButtonStyleScreen(void)
{
    static BOOL bUnknowNoHomeButtonStyleScreen = YES;
    static BOOL bNoHomeButtonStyleScreen = NO;
    if (bUnknowNoHomeButtonStyleScreen)
    {
        if (@available(iOS 11.0, *))
        {
            UIWindow *mainWindow = zw_sharedApplicationWindow();
            if (mainWindow.safeAreaInsets.bottom > 0.0)
            {
                bNoHomeButtonStyleScreen = YES;
            }
        }
        bUnknowNoHomeButtonStyleScreen = NO;
    }
    return bNoHomeButtonStyleScreen;
}

BOOL zw_isSimulator(void)
{
#if TARGET_OS_SIMULATOR
    return YES;
#endif
#if TARGET_OS_IPHONE
    return NO;
#endif
    return NO;
}

BOOL zw_ios8OrLater(void)
{
    static BOOL bUnknowiOS8 = YES;
    static BOOL bIos8OrLater = NO;
    if (bUnknowiOS8)
    {
        if (@available(iOS 8.0, *))
        {
            bIos8OrLater = YES;
        }
        else
        {
            bIos8OrLater = NO;
        }
        bUnknowiOS8 = NO;
    }
    return bIos8OrLater;
}

BOOL zw_ios9OrLater(void)
{
    static BOOL bUnknowiOS9 = YES;
    static BOOL bIos9OrLater = NO;
    if (bUnknowiOS9)
    {
        if (@available(iOS 9.0, *))
        {
            bIos9OrLater = YES;
        }
        else
        {
            bIos9OrLater = NO;
        }
        bUnknowiOS9 = NO;
    }
    return bIos9OrLater;
}

BOOL zw_ios10OrLater(void)
{
    static BOOL bUnknowiOS10 = YES;
    static BOOL bIos10OrLater = NO;
    if (bUnknowiOS10)
    {
        if (@available(iOS 10.0, *))
        {
            bIos10OrLater = YES;
        }
        else
        {
            bIos10OrLater = NO;
        }
        bUnknowiOS10 = NO;
    }
    return bIos10OrLater;
}

BOOL zw_ios11OrLater(void)
{
    static BOOL bUnknowiOS11 = YES;
    static BOOL bIos11OrLater = NO;
    if (bUnknowiOS11)
    {
        if (@available(iOS 11.0, *))
        {
            bIos11OrLater = YES;
        }
        else
        {
            bIos11OrLater = NO;
        }
        bUnknowiOS11 = NO;
    }
    return bIos11OrLater;
}

NSString*  zw_getResourcePath(void)
{
    return [[NSBundle mainBundle] resourcePath];
}

NSString* zw_getTempPath(void)
{
    return NSTemporaryDirectory();
}

NSString* zw_getLibraryCachesPath(void)
{
    return [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
}

NSString* zw_getLibraryPath(void)
{
    return [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) lastObject];
}

NSString* zw_getDocumentPath(void)
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
}

NSString* zw_getAppGroupDirectoryWithAppGroupID(NSString* AppGroupID)
{
    if (AppGroupID && [AppGroupID isKindOfClass:[NSString class]] && AppGroupID.length > 0)
    {
        NSURL* appGroupURL = [[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:AppGroupID];
        return appGroupURL.path;
    }
    return nil;
}

BOOL zw_isFolderPath(NSString* folderPath)
{
    BOOL isFolder = NO;
    if (folderPath && [folderPath isKindOfClass:[NSString class]] && folderPath.length > 0)
    {
        [[NSFileManager defaultManager] fileExistsAtPath:folderPath isDirectory:&isFolder];
        return isFolder;
    }
    return isFolder;
}

BOOL zw_isFileExistsAtPath(NSString *filePath)
{
    if (filePath)
    {
        return [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    }
    return NO;
}

BOOL zw_isAppGroupDirectoryExist(NSString* AppGroupID)
{
    return (zw_getAppGroupDirectoryWithAppGroupID(AppGroupID) != nil);
}

BOOL zw_isFileReadableAtPath(NSString *filePath)
{
    if (filePath)
    {
        return [[NSFileManager defaultManager] isReadableFileAtPath:filePath];
    }
    return NO;
}

BOOL zw_isFileWritableAtPath(NSString *filePath)
{
    if (filePath)
    {
        return [[NSFileManager defaultManager] isWritableFileAtPath:filePath];
    }
    return NO;
}

BOOL zw_isFileExecutableAtPath(NSString *filePath)
{
    if (filePath)
    {
        return [[NSFileManager defaultManager] isExecutableFileAtPath:filePath];
    }
    return NO;
}

BOOL zw_isFileDeletableAtPath(NSString *filePath)
{
    if (filePath)
    {
        return [[NSFileManager defaultManager] isDeletableFileAtPath:filePath];
    }
    return NO;
}

BOOL zw_removeFileAtPath(NSString *filePath)
{
    BOOL success = NO;
    if(zw_isFileExistsAtPath(filePath))
    {
        NSError *error;
        NSFileManager *fileManager = [NSFileManager defaultManager];
        success = [fileManager removeItemAtPath:filePath error:&error];
        if (!success)
        {
            NSLog(@"failed to remove %@ - '%@'.",  filePath, [error localizedDescription]);
        }
    }
    return success;
}

BOOL zw_copyFileToDisPath(NSString *srcPath, NSString *distPath)
{
    if(srcPath == nil || distPath == nil || [srcPath isEqualToString:distPath])
    {
        return NO;
    }
    zw_removeFileAtPath(distPath);
    
    BOOL success = NO;
    NSError *error;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    success = [fileManager copyItemAtPath:srcPath toPath:distPath error:&error];
    if (!success)
    {
        NSLog(@"failed to copy %@ - '%@'.",  srcPath, [error localizedDescription]);
    }
    return success;
}

BOOL zw_createFolderInDirectory(NSString* directoryPath, NSString* folderName)
{
    if (zw_isFileExistsAtPath(directoryPath) == NO)
    {
        NSLog(@"路径不存在【%@】", directoryPath);
        return NO;
    }
    if (folderName.length <= 0)
    {
        return YES;
    }
    NSArray* pSubFolders = [folderName componentsSeparatedByString:@"/"];
    BOOL bPathValid = YES;
    if (pSubFolders && [pSubFolders count])
    {
        for (NSString* pTempPath in pSubFolders)
        {
            if (pTempPath.length <= 0)
            {
                bPathValid = NO;
                break;
            }
        }
    }
    else
    {
        bPathValid = NO;
    }
    if (!bPathValid)
    {
        NSLog(@"create folder failed, folderName is invalid, 部分子文件夹名字的长度小于或等于0,【%@】", folderName);
        return NO;
    }
    
    NSString* pFolderPath = [NSString stringWithString:directoryPath];
    NSError* pError = nil;
    BOOL bCreateResult = YES;
    for (NSString* pSubFolderName in pSubFolders)
    {
        pFolderPath = [pFolderPath stringByAppendingPathComponent:pSubFolderName];
        if (NO == [[NSFileManager defaultManager] fileExistsAtPath:pFolderPath] || NO == zw_isFolderPath(pFolderPath))
        {
            //createIntermediates是一个BOOL值，表示是否创建中间路径。YES为创建(无论是否已经存在)，NO为不创建。
            //例如：参数createIntermediates为【my_work/important_work】，即需要在【my_work】文件夹下创建一个【important_work】文件夹。多级目录也是如此。
            //当【my_work】文件夹不存在，
            //①设置createIntermediates=YES时，则【my_work】和【important_work】文件夹都会被创建，
            //②若设置createIntermediates=NO时，则【my_work】和【important_work】文件夹都不会被创建。
            //当【my_work】文件夹已经存在，createIntermediates的值为YES或者NO，都不影响【important_work】文件夹的创建。
            bCreateResult = [[NSFileManager defaultManager] createDirectoryAtPath:pFolderPath withIntermediateDirectories:YES attributes:nil error:&pError];
            if (!bCreateResult)
            {
                NSLog(@"create folder failed, Error message【%@】", [pError localizedDescription]);
                break;
            }
        }
    }
    return bCreateResult;
}

BOOL zw_createFolderInDocumentDirectory(NSString* folderName)
{
    return zw_createFolderInDirectory(zw_getDocumentPath(), folderName);
}

BOOL zw_createFolderInLibraryDirectory(NSString* folderName)
{
    return zw_createFolderInDirectory(zw_getLibraryPath(), folderName);
}

BOOL zw_createFolderInAppGroupDirectory(NSString* AppGroupID, NSString* folderName)
{
    return zw_createFolderInDirectory(zw_getAppGroupDirectoryWithAppGroupID(AppGroupID), folderName);
}

NSString* zw_getFolderPathInDirectory(NSString* directoryPath, NSString* folderName)
{    
    if (zw_isFileExistsAtPath(directoryPath) == NO)
    {
        NSLog(@"路径不存在【%@】", directoryPath);
        return nil;
    }
    NSString* pFolderPath = [directoryPath stringByAppendingPathComponent:folderName];
    if ([[NSFileManager defaultManager] fileExistsAtPath:pFolderPath] && zw_isFolderPath(pFolderPath))
    {
        return pFolderPath;
    }
    else
    {
        if (zw_createFolderInDirectory(directoryPath, folderName))
        {
            return pFolderPath;
        }
        else
        {
            return nil;
        }
    }
}

NSString* zw_getFolderPathInDocumentDirectory(NSString* folderName)
{
    return zw_getFolderPathInDirectory(zw_getDocumentPath(), folderName);
}

NSString* zw_getFolderPathInLibraryDirectory(NSString* folderName)
{
    return zw_getFolderPathInDirectory(zw_getLibraryPath(), folderName);
}

NSString* zw_getFolderPathInAppGroupDirectory(NSString* AppGroupID, NSString* folderName)
{
    return zw_getFolderPathInDirectory(zw_getAppGroupDirectoryWithAppGroupID(AppGroupID), folderName);
}

BOOL zw_clearFolderInDirectory(NSString* directoryPath,NSString* folderName)
{
    NSString *pFolderPath = zw_getFolderPathInDirectory(directoryPath, folderName);
    if (pFolderPath)
    {
        NSError* error = nil;
        NSArray *fileList =[[NSFileManager defaultManager] contentsOfDirectoryAtPath:pFolderPath error:&error];
        if (error)
        {
            NSLog(@"%@", error);
            return NO;
        }
        else
        {
            for (NSString *file in fileList)
            {
                NSString *path =[pFolderPath stringByAppendingPathComponent:file];
                if (!zw_removeFileAtPath(path))
                {
                    return NO;
                }
            }
            return YES;
        }
    }
    else
    {
        return NO;
    }
}

BOOL zw_clearFolderInDocumentDirectory(NSString* folderName)
{
    return zw_clearFolderInDirectory(zw_getDocumentPath(), folderName);
}

BOOL zw_clearFolderInLibraryDirectory(NSString* folderName)
{
    return zw_clearFolderInDirectory(zw_getLibraryPath(), folderName);
}

BOOL zw_clearFolderInAppGroupDirectory(NSString* AppGroupID, NSString* folderName)
{
    return zw_clearFolderInDirectory(zw_getAppGroupDirectoryWithAppGroupID(AppGroupID), folderName);
}

NSDictionary<NSString*,NSString*>* zw_getFilesPathInDirectory(NSString* directoryPath, NSString* folderName, NSString* fileFormat, BOOL bDeletePathExtensionForKey)
{
    NSMutableDictionary<NSString*,NSString*>* pResultDic = [[NSMutableDictionary alloc] init];
    NSArray * imagesPath = [NSBundle pathsForResourcesOfType:fileFormat inDirectory:zw_getFolderPathInDirectory(directoryPath, folderName)];
    for (NSString* pTempPath in imagesPath)
    {
        NSString* fileName = [pTempPath lastPathComponent];
        if (bDeletePathExtensionForKey)
        {
            fileName = [fileName stringByDeletingPathExtension];
        }
        [pResultDic setValue:pTempPath forKey:fileName];
    }
    return pResultDic;
}

NSDictionary* zw_getFilesPathInDocumentDirectory(NSString* folderName, NSString* fileFormat, BOOL bDeletePathExtensionForKey)
{
    return zw_getFilesPathInDirectory(zw_getDocumentPath(), folderName, fileFormat, bDeletePathExtensionForKey);
}

NSDictionary* zw_getFilesPathInLibraryDirectory(NSString* folderName, NSString* fileFormat, BOOL bDeletePathExtensionForKey)
{
    return zw_getFilesPathInDirectory(zw_getLibraryPath(), folderName, fileFormat, bDeletePathExtensionForKey);
}

NSDictionary* zw_getFilesPathInAppGroupDirectory(NSString* AppGroupID, NSString* folderName, NSString* fileFormat, BOOL bDeletePathExtensionForKey)
{
    return zw_getFilesPathInDirectory(zw_getAppGroupDirectoryWithAppGroupID(AppGroupID), folderName, fileFormat, bDeletePathExtensionForKey);
}

NSDictionary<NSFileAttributeKey, id>* zw_getFileAttributesAtPath(NSString *filePath)
{
    if (filePath)
    {
        NSError* error = nil;
        NSDictionary<NSFileAttributeKey, id>* fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:filePath
                                                                                                               error:&error];
        if (error)
        {
            NSLog(@"%@", error);
        }
        return fileAttributes;
    }
    return nil;
}

BOOL zw_saveDataToUserDefaults(NSString* AppGroupID, NSString* dataKey, id dataValue)
{
    //此处dataValue可以为nil，表示清空key对应的值
    if (dataKey && [dataKey isKindOfClass:[NSString class]])
    {
        NSUserDefaults* userDefaults = nil;
        if (AppGroupID && [AppGroupID isKindOfClass:[NSString class]] && AppGroupID.length > 0)
        {
            userDefaults = [[NSUserDefaults alloc] initWithSuiteName:AppGroupID];
        }
        else
        {
            userDefaults = [NSUserDefaults standardUserDefaults];
        }
        @synchronized(userDefaults)
        {
            if ([dataValue isKindOfClass:[NSDictionary class]])
            {
                //⚠️⚠️⚠️NSUserDefaults存储的NSDictionary模型实例包含NSNull值将导致App崩溃
                NSDictionary* dicObj = (NSDictionary*)dataValue;
                dataValue = [dicObj zw_replaceNullsWithBlanks];
            }
            [userDefaults setObject:dataValue forKey:dataKey];
            return [userDefaults synchronize];
        }
    }
    return NO;
}

id zw_getDataFromUserDefaults(NSString* AppGroupID, NSString* dataKey)
{
    if (dataKey && [dataKey isKindOfClass:[NSString class]])
    {
        NSUserDefaults* userDefaults = nil;
        if (AppGroupID && [AppGroupID isKindOfClass:[NSString class]] && AppGroupID.length > 0)
        {
            userDefaults = [[NSUserDefaults alloc] initWithSuiteName:AppGroupID];
        }
        else
        {
            userDefaults = [NSUserDefaults standardUserDefaults];
        }
        return [userDefaults objectForKey:dataKey];
    }
    return nil;
}

BOOL zw_clearDataFromUserDefaults(NSString* AppGroupID)
{
    NSUserDefaults* userDefaults = nil;
    if (AppGroupID && [AppGroupID isKindOfClass:[NSString class]] && AppGroupID.length > 0)
    {
        userDefaults = [[NSUserDefaults alloc] initWithSuiteName:AppGroupID];
    }
    else
    {
        userDefaults = [NSUserDefaults standardUserDefaults];
    }
    @synchronized(userDefaults)
    {
        //⭐️因[NSUserDefaults resetStandardUserDefaults]达不到清除缓存的功能，故而封装此方法，也可以通过清除持久域实现【removePersistentDomainForName:】
        NSDictionary *dic = [userDefaults dictionaryRepresentation];
        for (id key in [dic allKeys])
        {
            [userDefaults removeObjectForKey:key];
        }
        
//        //第二种清除持久数据的方式
//        NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
//        [userDefaults removePersistentDomainForName:appDomain];
        return [userDefaults synchronize];
    }
}

BOOL zw_showSystemShareBox(NSArray *activityItems, UIView *sourceView, void(^completionHandler)(BOOL isSuccess, NSError *error, NSString *activityType))
{
    if (sourceView && [sourceView isKindOfClass:[UIView class]])
    {
        UIViewController *sourceViewController = [sourceView zw_getViewController];
        if (sourceViewController)
        {
            void (^task)(void) = ^{
                if (activityItems == nil || activityItems.count <= 0)
                {
                    return;
                }
                /**
                 参数一：这个数组不能为nil，至少要有一个对象，否则crash。
                 在执行activity中用到的数据对象数组。数组中的对象类型是可变的，并依赖于应用程序管理的数据。例如，数据可能是由一个或者多个字符串/图像对象，代表了当前选中的内容。
                 数组中的对象，也可以通过UIActivityItemSource协议来代替，例如UIActivityItemProvider对象。源和提供者扮演代理的角色，根据实际情况，需要相应数据时再进行提供。这里, 分享的数据包括文本, 图像, 访问网址。 当然, 这些是可选项。 比如你不想添加url，只包含文本和图像也是可以的。
                 【注：网址url中如果带有多个参数，参数的key不可以相同，否则facebook剔掉前面只保留最后一个key，如分享http://awstestjp.gigabud.com:8111/menu/user/showPick.do?uid=1234&rid=iOS_c6f619bd-925e-469e-a951-26a9fecfc5c41450409784335_id&rid=1E40CC4E644C4CFF83B08519BAA117CF_ios&lang=zh_TW将被剔成http://awstestjp.gigabud.com:8111/menu/user/showPick.do?uid=1234&rid=1E40CC4E644C4CFF83B08519BAA117CF_ios&lang=zh_TW】
                 
                 参数二：是一个UIActivity对象的数组，代表了应用程序支持的自定义服务。这个参数可以是nil。具体如何使用请参考模板相应Demo
                 */
                
                UIActivityViewController* activityCtrl = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
                
                activityCtrl.completionWithItemsHandler = ^(NSString * __nullable activityType, BOOL completed, NSArray * __nullable returnedItems, NSError * __nullable activityError) {
                    NSLog(@"%@【%@】\n【%@】\n【%@】", activityType, (completed?@"完成":@"未完成"), returnedItems, activityError);
                    if (completionHandler)
                    {
                        completionHandler(completed, activityError, activityType);
                    }
                };
                
                UIPopoverPresentationController* popover = activityCtrl.popoverPresentationController;
                if (popover)
                {
                    popover.sourceView = sourceView;
                    popover.permittedArrowDirections = UIPopoverArrowDirectionAny;
                    //以下代码为了设置iPad上显示的位置，否则可能导致iPad无法显示系统分享框
                    CGSize screenSize = [UIScreen mainScreen].bounds.size;
                    popover.sourceRect = CGRectMake(screenSize.width/2.0,screenSize.height-44,0,0);
                }
                
                [sourceViewController presentViewController:activityCtrl animated:YES completion:nil];
            };
            
            if (strcmp(dispatch_queue_get_label(DISPATCH_CURRENT_QUEUE_LABEL), dispatch_queue_get_label(dispatch_get_main_queue())) == 0)
            {
                // 主队列
                task();
            }
            else
            {
                // 非主队列
                dispatch_async(dispatch_get_main_queue(), task);
            }
            return YES;
        }
    }
    return NO;
}

BOOL zw_showSystemAlertController(id title,
                                  id message,
                                  NSString *cancelActionTitle,
                                  UIColor *cancelActionTitleColor,
                                  NSArray *otherActionTitles,
                                  UIViewController *presentingViewController,
                                  UIAlertControllerStyle alertStyle,
                                  void(^completionHandler)(NSInteger atIndex, UIAlertAction *action))
{
    NSString *strTitle = nil;
    NSString *strMessage = nil;
    NSAttributedString *attributedTitle = nil;
    NSAttributedString *attributedMessage = nil;
    
    if ([title isKindOfClass:[NSString class]])
    {
        strTitle = (NSString *)title;
    }
    else if ([title isKindOfClass:[NSAttributedString class]])
    {
        attributedTitle = (NSAttributedString *)title;
        strTitle = attributedTitle.string;
    }
    
    if ([message isKindOfClass:[NSString class]])
    {
        strMessage = (NSString *)message;
    }
    else if ([message isKindOfClass:[NSAttributedString class]])
    {
        attributedMessage = (NSAttributedString *)message;
        strMessage = attributedMessage.string;
    }
    
    BOOL isOtherActionsValid = NO;
    for (id tmpOtherActionTitleObject in otherActionTitles)
    {
        if ([tmpOtherActionTitleObject isKindOfClass:[NSString class]] && ((NSString *)tmpOtherActionTitleObject).length > 0)
        {
            isOtherActionsValid = YES;
            break;
        }
        else if ([tmpOtherActionTitleObject isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *tmpOtherActionTitleDic = (NSDictionary *)tmpOtherActionTitleObject;
            NSString *actionTitle = [tmpOtherActionTitleDic zw_getNSStringObjectForKey:@"title"];
            if (actionTitle.length > 0)
            {
                isOtherActionsValid = YES;
                break;
            }
        }
    }
    if (strTitle.length <= 0 && strMessage.length <= 0 && !isOtherActionsValid)
    {
        return NO;
    }
    
    UIViewController* showInViewCtrl = nil;
    
    BOOL isShowInAlertWindow = NO;
    static UIWindow *s_pAlerWindow = nil;
    if (presentingViewController && [presentingViewController isKindOfClass:[UIViewController class]])
    {
        showInViewCtrl = presentingViewController;
    }
    else
    {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            
            //在iOS9以后，苹果推荐每个UIWindow都必须有一个rootViewController。否则在启动过程使用了不包含rootViewController的UIWindow会导致必现的crash。
            UIViewController* rootCtrl = [[UIViewController alloc]init];
            
            s_pAlerWindow = [[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
            s_pAlerWindow.rootViewController = rootCtrl;
            s_pAlerWindow.windowLevel = UIWindowLevelAlert + 1;//💡注意：层级高于状态栏和键盘
        });
        showInViewCtrl = s_pAlerWindow.rootViewController;
        isShowInAlertWindow = YES;
    }
    
    UIAlertController* displayAlert = [UIAlertController alertControllerWithTitle:strTitle
                                                                          message:strMessage
                                                                   preferredStyle:alertStyle];
    if (attributedTitle)
    {
        [displayAlert setValue:attributedTitle forKey:@"attributedTitle"];
    }
    if (attributedMessage)
    {
        [displayAlert setValue:attributedMessage forKey:@"attributedMessage"];
    }
    
    //添加取消action
    if (cancelActionTitle == nil || ![cancelActionTitle isKindOfClass:[NSString class]] || cancelActionTitle.length <= 0)
    {
        cancelActionTitle = @"关闭";
    }
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:cancelActionTitle
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction * _Nonnull action) {
        if (isShowInAlertWindow)
        {
            [s_pAlerWindow resignKeyWindow];
        }
        if (completionHandler)
        {
            completionHandler(0, action);
        }
    }];
    if (cancelActionTitleColor && [cancelActionTitleColor isKindOfClass:[UIColor class]])
    {
        [cancelAction setValue:cancelActionTitleColor forKey:@"_titleTextColor"];
    }
    [displayAlert addAction:cancelAction];
    
    
    NSInteger nCount = [otherActionTitles count];
    for (NSInteger nIndex = 0; nIndex < nCount; ++nIndex)
    {
        id tmpOtherActionTitleObject = [otherActionTitles zw_safeObjectAtIndex:nIndex];
        NSString *actionTitle = nil;
        UIColor *actionTitleColor = nil;
        NSNumber *actionTitleStyleNumber = nil;
        if ([tmpOtherActionTitleObject isKindOfClass:[NSString class]])
        {
            actionTitle = (NSString *)tmpOtherActionTitleObject;
        }
        else if ([tmpOtherActionTitleObject isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *tmpOtherActionTitleDic = (NSDictionary *)tmpOtherActionTitleObject;
            actionTitle = [tmpOtherActionTitleDic zw_getNSStringObjectForKey:@"title"];
            actionTitleColor = [tmpOtherActionTitleDic zw_getUIColorObjectForKey:@"color"];
            actionTitleStyleNumber = [tmpOtherActionTitleDic zw_getNSNumberObjectForKey:@"style"];
        }
        
        if (actionTitle.length > 0)
        {
            //⚠️UIAlertController can only have one action with a style of UIAlertActionStyleCancel，如果多个会导致Crash！！！
            UIAlertActionStyle actionTitleStyle = UIAlertActionStyleDefault;
            if (actionTitleStyleNumber)
            {
                NSInteger styleValue = [actionTitleStyleNumber integerValue];
                if (styleValue == UIAlertActionStyleDefault || styleValue == UIAlertActionStyleDestructive)
                {
                    actionTitleStyle = styleValue;
                }
            }
            UIAlertAction* alertAction = [UIAlertAction actionWithTitle:actionTitle
                                                                  style:actionTitleStyle
                                                                handler:^(UIAlertAction * _Nonnull action) {
                if (isShowInAlertWindow)
                {
                    [s_pAlerWindow resignKeyWindow];
                }
                if (completionHandler)
                {
                    completionHandler(nIndex+1, action);
                }
            }];
            if (actionTitleColor)
            {
                [alertAction setValue:actionTitleColor forKey:@"_titleTextColor"];
            }
            [displayAlert addAction:alertAction];
        }
    }
    
    void (^task)(void) = ^{
        if (isShowInAlertWindow)
        {
            [s_pAlerWindow makeKeyAndVisible];
            [showInViewCtrl presentViewController:displayAlert
                                         animated:YES
                                       completion:nil];
        }
        else
        {
            [showInViewCtrl zw_presentViewController:displayAlert
                                   presentCompletion:nil
                                   dismissCompletion:nil];
        }
    };
    
    if (strcmp(dispatch_queue_get_label(DISPATCH_CURRENT_QUEUE_LABEL), dispatch_queue_get_label(dispatch_get_main_queue())) == 0)
    {
        // 主队列
        task();
    }
    else
    {
        // 非主队列
        dispatch_async(dispatch_get_main_queue(), task);
    }
    return YES;
}

BOOL zw_isMainThread(void)
{
    return [NSThread isMainThread];
}

void zw_verifyTextWithRegularExpressionsForPoundOrAtSignType(NSString* text, NSString* patternForAtSignType, NSString* patternForPoundSignType, void(^callBack)(enTextWithSignType signStringType, NSString* signString))
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSRegularExpression *regExForAtSignType = [NSRegularExpression regularExpressionWithPattern:patternForAtSignType options:0 error:nil];
        NSArray *arrOfAtSignType = [regExForAtSignType matchesInString:text options:0 range:NSMakeRange(0, text.length)];
        NSTextCheckingResult *lastAtMatch = [arrOfAtSignType lastObject];
        
        NSRegularExpression *regExForPoundSignType = [NSRegularExpression regularExpressionWithPattern:patternForPoundSignType options:0 error:nil];
        NSArray *arrOfPoundSignType = [regExForPoundSignType matchesInString:text options:0 range:NSMakeRange(0, text.length)];
        NSTextCheckingResult *lastPoundMatch = [arrOfPoundSignType lastObject];
        
        enTextWithSignType signStringType = enTextWithSignTypeNormal;
        NSString* signString = nil;
        if (lastAtMatch && (lastAtMatch.range.location+lastAtMatch.range.length) == text.length)
        {
            signStringType = enTextWithSignTypeAt;
            signString = [text substringWithRange:lastAtMatch.range];
            signString = [signString stringByReplacingOccurrencesOfString:@"@" withString:@""];
        }
        else if (lastPoundMatch && (lastPoundMatch.range.location+lastPoundMatch.range.length) == text.length)
        {
            signStringType = enTextWithSignTypePound;
            signString = [text substringWithRange:lastPoundMatch.range];
            signString = [signString stringByReplacingOccurrencesOfString:@"#" withString:@""];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (callBack)
            {
                callBack(signStringType, signString);
            }
        });
    });
}

NSString* zw_refreshTextWithSignString(NSString* text, enTextWithSignType signStringType, NSString* signString)
{
    NSString* resultString = text;
    switch (signStringType)
    {
        case enTextWithSignTypeAt:
        {
            NSRange atSignRange = [text rangeOfString:@"@" options:NSBackwardsSearch];
            if (atSignRange.length > 0)
            {
                NSString* preStringWithAtSign = [text substringToIndex:atSignRange.location+atSignRange.length];
                resultString = [preStringWithAtSign stringByAppendingString:[NSString stringWithFormat:@"%@ ", signString]];
            }
        }
            break;
        case enTextWithSignTypePound:
        {
            NSRange poundSignRange = [text rangeOfString:@"#" options:NSBackwardsSearch];
            if (poundSignRange.length > 0)
            {
                NSString* preStringWithPoundSign = [text substringToIndex:poundSignRange.location+poundSignRange.length];
                resultString = [preStringWithPoundSign stringByAppendingString:[NSString stringWithFormat:@"%@ ", signString]];
            }
        }
            break;
        default:
            break;
    }
    return resultString;
}

void zw_copyTextToPasteboard(NSString* text)
{
    if (text.length > 0)
    {
        [UIPasteboard generalPasteboard].string = text;
    }
}

/*
 typedef NS_ENUM(NSUInteger, NSRoundingMode)
 {
 NSRoundPlain,   // Round up on a tie(四舍五入)
 NSRoundDown,    // Always down == truncate(只舍不入)
 NSRoundUp,      // Always up(只入不舍)
 NSRoundBankers  // on a tie round so last digit is even(银行家舍入法，如果精确的那位是5，还要看精确度的前一位是偶数还是奇数，如果是奇数则入，偶数则舍。
 //例如scale=1，表示精确到小数点后一位, NSDecimalNumber 为1.25时，NSRoundPlain结果为1.3，而NSRoundBankers则是1.2)
 };
 
 -------------- 苹果官方给出的实例--------------
 Original 1.2  1.21  1.25  1.35  1.27
 Plain    1.2  1.2   1.3   1.4   1.3
 Down     1.2  1.2   1.2   1.3   1.2
 Up       1.2  1.3   1.3   1.4   1.3
 Bankers  1.2  1.2   1.2   1.4   1.3
 -------------- 苹果官方给出的实例--------------
 
 scale表示精确到小数点后几位,并且NSRoundingMode影响的也是scale位
 raiseOnExactness/raiseOnOverflow/raiseOnUnderflow raiseOnDivideByZero
 分别表示数据精确/上溢/下益/除以0时是否以异常处理,一般情况下都设置为NO
 
 参考资料https://www.jianshu.com/p/4a1477da1cb9
 */
NSDecimalNumber* zw_convertDoubleWithRoundDown(double doubleValue, NSUInteger nCountAfterPoint)
{
    NSDecimalNumber* doubleDecimalNumber = [[NSDecimalNumber alloc] initWithDouble:doubleValue];
    NSDecimalNumberHandler* roundDownBehavior = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundDown
                                                                                                       scale:nCountAfterPoint
                                                                                            raiseOnExactness:NO
                                                                                             raiseOnOverflow:NO
                                                                                            raiseOnUnderflow:NO
                                                                                         raiseOnDivideByZero:NO];
    NSDecimalNumber *roundDownDecimalNumber = [doubleDecimalNumber decimalNumberByRoundingAccordingToBehavior:roundDownBehavior];
    return roundDownDecimalNumber;
}

NSDecimalNumber* zw_convertDoubleWithRoundPlain(double doubleValue, NSUInteger nCountAfterPoint)
{
    NSDecimalNumber* doubleDecimalNumber = [[NSDecimalNumber alloc] initWithDouble:doubleValue];
    NSDecimalNumberHandler* roundPlainBehavior = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundPlain
                                                                                                        scale:nCountAfterPoint
                                                                                             raiseOnExactness:NO
                                                                                              raiseOnOverflow:NO
                                                                                             raiseOnUnderflow:NO
                                                                                          raiseOnDivideByZero:NO];
    NSDecimalNumber *roundPlainDecimalNumber = [doubleDecimalNumber decimalNumberByRoundingAccordingToBehavior:roundPlainBehavior];
    return roundPlainDecimalNumber;
}

NSString* zw_convertToJsonStringWithObject(NSObject* object)
{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:object options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = nil;
    if (!jsonData)
    {
        NSLog(@"%@",error);
    }
    else
    {
        jsonString = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    return jsonString;
}

BOOL zw_checkNSObjectInstanceSame(NSObject* pFirstObj, NSObject* pSecondObj)
{
    if (pFirstObj == nil && pSecondObj != nil)
    {
        return NO;
    }
    else if (pFirstObj != nil && pSecondObj == nil)
    {
        return NO;
    }
    else if (pFirstObj != nil && pSecondObj != nil && pFirstObj != pSecondObj)
    {
        if ([pFirstObj isKindOfClass:[NSString class]] && [pSecondObj isKindOfClass:[NSString class]])
        {
            NSString* pFirstString = (NSString*)pFirstObj;
            NSString* pSecondString = (NSString*)pSecondObj;
            return [pFirstString isEqualToString:pSecondString];
        }
        else if ([pFirstObj isKindOfClass:[NSNumber class]] && [pSecondObj isKindOfClass:[NSNumber class]])
        {
            NSNumber* pFirstNSNumber = (NSNumber*)pFirstObj;
            NSNumber* pSecondNSNumber = (NSNumber*)pSecondObj;
            return [pFirstNSNumber isEqualToNumber:pSecondNSNumber];
        }
        else if ([pFirstObj isKindOfClass:[NSArray class]] && [pSecondObj isKindOfClass:[NSArray class]])
        {
            NSArray* pFirstNSArray = (NSArray*)pFirstObj;
            NSArray* pSecondNSArray = (NSArray*)pSecondObj;
            return [pFirstNSArray isEqualToArray:pSecondNSArray];
        }
        else if ([pFirstObj isKindOfClass:[NSDictionary class]] && [pSecondObj isKindOfClass:[NSDictionary class]])
        {
            NSDictionary* pFirstNSDictionary = (NSDictionary*)pFirstObj;
            NSDictionary* pSecondNSDictionary = (NSDictionary*)pSecondObj;
            return [pFirstNSDictionary isEqualToDictionary:pSecondNSDictionary];
        }
        else if ([pFirstObj isKindOfClass:[NSSet class]] && [pSecondObj isKindOfClass:[NSSet class]])
        {
            NSSet* pFirstNSSet = (NSSet*)pFirstObj;
            NSSet* pSecondNSSet = (NSSet*)pSecondObj;
            return [pFirstNSSet isEqualToSet:pSecondNSSet];
        }
        else if ([pFirstObj isKindOfClass:[NSDate class]] && [pSecondObj isKindOfClass:[NSDate class]])
        {
            NSDate* pFirstNSDate = (NSDate*)pFirstObj;
            NSDate* pSecondNSDate = (NSDate*)pSecondObj;
            return [pFirstNSDate isEqualToDate:pSecondNSDate];
        }
        else
        {
            return [pFirstObj isEqual:pSecondObj];
        }
    }
    return YES;
}

BOOL zw_checkLeapYear(NSInteger year)
{
    return ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0));
}

void zw_makeNetworkActivityIndicatorVisible(BOOL isVisible)
{
    void (^task)(void) = ^{
        UIApplication* app = zw_sharedApplication();
        if (app)
        {
            app.networkActivityIndicatorVisible = isVisible;
        }
    };
    
    if (strcmp(dispatch_queue_get_label(DISPATCH_CURRENT_QUEUE_LABEL), dispatch_queue_get_label(dispatch_get_main_queue())) == 0)
    {
        // 主队列
        task();
    }
    else
    {
        // 非主队列
        dispatch_async(dispatch_get_main_queue(), task);
    }
}

void zw_closeVirtualKeyboard(void)
{
    void (^task)(void) = ^{
        UIApplication* app = zw_sharedApplication();
        [app sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
    };
    
    if (strcmp(dispatch_queue_get_label(DISPATCH_CURRENT_QUEUE_LABEL), dispatch_queue_get_label(dispatch_get_main_queue())) == 0)
    {
        // 主队列
        task();
    }
    else
    {
        // 非主队列
        dispatch_async(dispatch_get_main_queue(), task);
    }
}

id zw_getResultValueAfterTargetPerformSelector(id target, SEL selector, NSArray* paras)
{
    //鉴定执行方法是否被实例支持
    if (![target respondsToSelector:selector])
    {
        return [NSError errorWithDomain:@"ZWProjectsCoreFunctions"
                                   code:ErrorCode_ZhangWei_NotRespondsToSelector
                               userInfo:@{NSLocalizedDescriptionKey:@"zw_getResultValueAfterTargetPerformSelector实例不支持对应的方法"}];
    }
    // 方法签名(方法的描述)
    NSMethodSignature *signature = [[target class] instanceMethodSignatureForSelector:selector];
    if (signature == nil)
    {
        //可以抛出异常也可以不操作。
        return [NSError errorWithDomain:@"ZWProjectsCoreFunctions"
                                   code:ErrorCode_ZhangWei_MethodSignatureFailed
                               userInfo:@{NSLocalizedDescriptionKey:@"zw_getResultValueAfterTargetPerformSelector实例化失败"}];
    }
    
    // NSInvocation : 利用一个NSInvocation对象包装一次方法调用（方法调用者、方法名、方法参数、方法返回值）
    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:signature];
    invocation.target = target;
    invocation.selector = selector;
    
    // 设置参数
    NSInteger paramsCount = signature.numberOfArguments - 2; // 除self、_cmd以外的参数个数
    paramsCount = MIN(paramsCount, paras.count);
    for (NSInteger i = 0; i < paramsCount; i++)
    {
        id object = paras[i];
        if ([object isKindOfClass:[NSNull class]])
            continue;
        [invocation setArgument:&object atIndex:i + 2];
    }
    
    // 调用方法
    [invocation invoke];
    
    // 声明返回值变量
    id returnValue = nil;
    
    // 返回值类型
    const char *returnType = signature.methodReturnType;
    if (returnType)// 有返回值类型，才去获得返回值
    {
        //如果没有返回值，也就是消息声明为void，那么表示执行完成就好，返回结果无关紧要
        if( !strcmp(returnType, @encode(void)) )
        {
            returnValue =  [NSNumber numberWithBool:YES];
        }
        //如果返回值为对象，直接赋值给返回值变量
        else if( !strcmp(returnType, @encode(id)) )
        {
            [invocation getReturnValue:&returnValue];
        }
        else
        {
            //如果返回值为普通类型NSInteger或者BOOL
            //返回值长度
            NSUInteger length = [signature methodReturnLength];
            //根据长度申请内存
            void *buffer = (void *)malloc(length);
            //为变量赋值
            [invocation getReturnValue:buffer];
            
            if( !strcmp(returnType, @encode(BOOL)) )//表示为布尔类型
            {
                returnValue = [NSNumber numberWithBool:*((BOOL*)buffer)];
            }
            else if( !strcmp(returnType, @encode(NSInteger)) )//表示为NSInteger类型
            {
                returnValue = [NSNumber numberWithInteger:*((NSInteger*)buffer)];
            }
            else
            {
                returnValue = [NSValue valueWithBytes:buffer objCType:returnType];
            }
        }
    }
    
    return returnValue;
}

ConstellationType zw_getConstellation(NSInteger nMonth, NSInteger nDay)
{
    if (nMonth < 1 || nMonth > 12 || nDay < 1 | nDay > 31)
    {
        return ConstellationType_Unknown;
    }
    
    static NSArray* cnstellations = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cnstellations = @[@119,@218,@320,@419,@520,@621,@722,@822,@922,@1023,@1122,@1221];
    });
    
    NSInteger tmpValue = nMonth * 100 + nDay;
    
    NSInteger nResult = 10;
    for (NSInteger nIndex = 0; nIndex < [cnstellations count]; ++nIndex)
    {
        if (tmpValue <= [[cnstellations zw_getNSNumberObjectAtIndex:nIndex] integerValue])
        {
            nResult += nIndex;
            break;
        }
    }
    return nResult > 12?(nResult - 12):nResult;
}

//从URL中抓取参数值
NSString* zw_extractParameterValueFromURL(NSURL *url, NSString *parameterName)
{
    if (url && parameterName && [url isKindOfClass:[NSURL class]] && [parameterName isKindOfClass:[NSString class]])
    {
        NSMutableDictionary *allParameters = [[NSMutableDictionary alloc] init];
        NSString *urlString = url.query;
        for (NSString *qs in [urlString componentsSeparatedByString:@"&"])
        {
            NSArray* tmp = [qs componentsSeparatedByString:@"="];
            if ([tmp count] == 2)
            {
                NSString* key = [tmp firstObject];
                NSString* value = [[tmp lastObject] stringByRemovingPercentEncoding];
                [allParameters setValue:value forKey:key];
            }
        }
        return [allParameters objectForKey:parameterName];
    }
    return nil;
}

void zw_changeDeviceOrientation(UIInterfaceOrientation targetOrientation) {
    /// ⚠️不能使用zw_getResultValueAfterTargetPerformSelector，因为参数无法通过数组传递！！！
    SEL selector = NSSelectorFromString(@"setOrientation:");
    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[UIDevice instanceMethodSignatureForSelector:selector]];
    [invocation setSelector:selector];
    [invocation setTarget:[UIDevice currentDevice]];
    NSInteger val = targetOrientation;
    [invocation setArgument:&val atIndex:2];//前两个参数已被target和selector占用
    [invocation invoke];
}
