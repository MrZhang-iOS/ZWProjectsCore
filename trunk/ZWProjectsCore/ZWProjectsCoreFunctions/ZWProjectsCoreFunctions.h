//
//  ZWProjectsCoreFunctions.h
//  ZWProjectsCore
//
//  Created by ZhangWei on 15/5/5.
//  Copyright (c) 2015年 ZhangWei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <Photos/Photos.h>
#import <LocalAuthentication/LocalAuthentication.h>

UIKIT_EXTERN NSInteger const ErrorCode_ZhangWei_NotRespondsToSelector;
UIKIT_EXTERN NSInteger const ErrorCode_ZhangWei_MethodSignatureFailed;
UIKIT_EXTERN NSInteger const ErrorCode_ZhangWei_Parameters;

// 如果项目中有.mm文件__cplusplus就为真，就把包括的代码部分按照C编译
#ifdef __cplusplus
extern "C" {
#endif

/**
 * 将日志保存到沙盒Library路径下或者打印到控制台
 * @param log 日志内容字符串
 */
void zw_logInConsoleOnly(NSString *log);
void zw_logInSandboxOnly(NSString *log);
void zw_logInSandboxAndConsole(NSString *log);

/// 对日志的操作。
/// - Parameters:
///   - log: 日志内容
///   - tag: 该日志归属的tag，便于过滤日志使用。默认[AppLog]。
///   - inConsole: 是否显示到控制台。
///   - inSandbox: 是否存储到沙盒，路径默认在【Library/ZWRecordTextTool/ApplicationLog.txt】。
///   - showTime: 是否显示时间信息（精确到微秒）。
void zw_log(NSString *log, NSString *tag, BOOL inConsole, BOOL inSandbox, BOOL showTime);

/**
 * 快速获得一个不定参数的字符串
 */
NSString* zw_formatString(NSString *format, ...);

/**
 * 获取应用实例。
 * 应用扩展中不允许使用[UIApplication sharedApplication]方式获取应用实例，但为了保证代码一致情况下通过编译，遂使用了这个封装方法
 */
UIApplication* zw_sharedApplication(void);

/**
 * 获取Window窗口。
 * ⚠️XCode11中创建的工程增加了SceneDelegate类用来管理Window，AppDelegate只负责应用程序的生命周期管理。
 */
UIWindow* zw_sharedApplicationWindow(void);

/**
 * 移除启动屏缓存
 */
void zw_removeLaunchScreenCacheIfNeeded(void);

/**
 * 对交互的时间忽略状态控制
 * 应用扩展中不允许使用UIApplication的-beginIgnoringInteractionEvents和-endIgnoringInteractionEvents，但为了保证代码一致情况下通过编译，遂使用了这个封装方法
 */
void zw_applicationBeginIgnoringInteractionEvents(void);
void zw_applicationEndIgnoringInteractionEvents(void);

/**
 *  获取设备信息
 */
NSString* zw_systemDeviceString(void);

/**
 *  当前手机连接的WIFI名称(SSID)
 *  ⚠️Starting with iOS 13, the CNCopyCurrentNetworkInfo API will no longer return valid Wi-Fi SSID and BSSID information.
 */
NSString* zw_wifiName(void) NS_DEPRECATED_IOS(4.1, 13.0);

/**
 *  获取设备当前的网络IP地址（WiFi 流量通用）
 *  @param isIPv4 YES表示优先取IPv4；NO表示优先取IPv6
 */
NSString* zw_getIpAddress(BOOL isIPv4);

/**
 *  容量字符串转换
 *  @param fileSize 文件的data.byte值
 *  @return 经过转换后的B、KB、MB、GB的字符串，如1024 = 1024B
 */
NSString* zw_fileSizeToString(unsigned long long fileSize);

/**
 *  苹果的容量转换
 *  @param fileSize 文件的data.byte值
 *  @return 经过转换后的B、KB、MB、GB的字符串，如1000 = 1B
 */
NSString* zw_fileSizeToAppleString(unsigned long long fileSize);

/**
 *  获取内存总容量 单位byte
 */
unsigned long long zw_getTotalMemorySize(void);

/**
 *  获取可使用的内存容量 单位byte 查找失败时返回 NSNotFound
 */
unsigned long long zw_getAvailableMemorySize(void);

/**
 *  获取硬盘总容量
 */
unsigned long long zw_getTotalDiskSize(void);

/**
 *  获取可用硬盘容量
 */
unsigned long long zw_getAvailabelDiskSize(void);

/**
 *  获取指定文件容量，如果是文件夹不包括所有子目录的文件
 */
unsigned long long zw_getFileSizeWithoutSubfoldersAtPath(NSString *filePath);

/**
 *  获取指定文件容量，如果是文件夹还包括所有子目录的文件
 */
unsigned long long zw_getFileSizeAtPath(NSString* filePath);

/**
 *  创建一个唯一的字符串
 */
NSString* zw_createUUID(void);

/**
 * 创建一个唯一字符串的扩展方法
 * @param prefix 前缀
 * @param suffix 后缀
 */
NSString* zw_createUUID_Ex(NSString* prefix, NSString* suffix);

/**
 *  创建一组随机的字符串（由0~9的十个数字、A~Z的二十六个大写字母随机组合），可以用来生成指定长度的订单号，如支付宝订单号
 *  @param stringLength 字符串长度
 */
NSString* zw_createRandDigitalAlphabetStringWithLenth(NSInteger stringLength);

/**
 *  打开某个url
 *  注意参数url字符串需要前缀【http://】或者【https://】
 */
BOOL zw_openURLString(NSString* url);
BOOL zw_openURLStringWithCallbackHandler(NSString* url, NSDictionary* options, void (^handler)(BOOL success));
BOOL zw_canOpenURLString(NSString* url);

/**
 * 打开某个app
 */
BOOL zw_openOtherApp(NSString* schemesString);
BOOL zw_openOtherAppWithCallbackHandler(NSString* schemesString, NSDictionary* options, void (^handler)(BOOL success));
/**
 * 判断是否能打开某个app，AppScheme不在白名单时可能返回NO
 */
BOOL zw_canOpenOtherApp(NSString* schemesString);

/**
 *  跳转到App的系统设置页面，如各种隐私权限等
 *  注iOS8.0及以上系统才具备此功能
 */
void zw_openAppSetting_iOS8OrLater(void);

/**
 *  拨打电话
 *  @param phone 电话号码
 */
void zw_callWithPhone(NSString* phone);

/**
 *  获取设备所有者授权验证(TouchID、FaceID或者密码)相关错误码的参考描述
 */
NSString* zw_deviceOwnerAuthenticatedErrorReferenceDescription(NSError* error);

/**
 *  是否具备设备所有者授权(TouchID、FaceID或者密码)功能，并且判断可用性
 */
void zw_canUseDeviceOwnerAuthentication(void(^checkHandler)(LAContext* pLAContext, BOOL canUse, NSError* error, NSString* errorReferenceDescription));

/**
 *  使用设备所有者授权(TouchID、FaceID或者密码)验证身份(优先使用生物识别技术验证，如果没有则使用密码验证)
 *  @param checkHandler        设备是否支持所有者授权验证及其可用性的回调块
 *  @param resonInSystemDialog 生物识别技术验证框或密码框上的副标题（主标题有系统提供）
 *  @param fallbackTitle       生物识别技术验证失败后第二个按钮的文字；如果不设置或者设置为nil，第二个按钮默认标题为<输入密码>；如果不想有第二个按钮的话，可以设置为空字符串<@"">。
 *  @param reply               所有者授权验证结果
 */
void zw_useDeviceOwnerAuthentication(void(^checkHandler)(LAContext* pLAContext, BOOL canUse, NSError* error, NSString* errorReferenceDescription),
                                     NSString* resonInSystemDialog,
                                     NSString*fallbackTitle,
                                     void(^reply)(LAContext* pLAContext, BOOL success, NSError* error, NSString* errorReferenceDescription));

//相册的隐私授权状态
//  PHAuthorizationStatusNotDetermined = 0, //用户尚未对隐私授权做出选择 User has not yet made a choice with regards to this application
//  PHAuthorizationStatusRestricted,        //此应用程序没有被授权访问相册数据。可能是家长控制权限 This application is not authorized to access photo data.
//  PHAuthorizationStatusDenied,            //用户已经明确否认了此应用程序请求相册数据的访问 User has explicitly denied this application access to photos data.
//  PHAuthorizationStatusAuthorized         //用户已经授权此应用程序访问相册数据 User has authorized this application to access photos data.
PHAuthorizationStatus zw_privacyAuthorizationStatusOfPhotoLibrary(void);
void zw_requestPrivacyAccessAboutPhotoLibrary(void(^completionHandler)(PHAuthorizationStatus status));

// 判断是否有摄像头可用，如果设备根本就没有摄像头，则授权相机权限就更谈不上了。
BOOL zw_isCameraAvailable(void);
//相机、麦克风的隐私授权状态
//  AVAuthorizationStatusNotDetermined = 0, //用户尚未对隐私授权做出选择。
//  AVAuthorizationStatusRestricted,        //此应用程序没有被授权访问相机或者麦克风。可能是家长控制权限。
//  AVAuthorizationStatusDenied,            //用户已经明确否认了此应用程序请求相机或者麦克风的访问。
//  AVAuthorizationStatusAuthorized         //用户已经授权此应用程序访问相机或者麦克风。
AVAuthorizationStatus zw_privacyAuthorizationStatusOfCamera(void);
AVAuthorizationStatus zw_privacyAuthorizationStatusOfMicrophone(void);
//AVMediaTypeVideo请求相机拍照权限；
//AVMediaTypeAudio请求麦克风的权限；
//拍照时只用请求AVMediaTypeVideo权限，录视频时需要请求这AVMediaTypeVideo和AVMediaTypeAudio两个权限
void zw_requestPrivacyAccessAboutDeviceCaptureWithMediaType(NSString* mediaType, void(^completionHandler)(BOOL granted));

/**
 *  获取plist中配置的版本号, 这是用来显示给用户的看的版本号，也是系统设置中读取的app版本号
 */
NSString* zw_getAppDisplayVersion(void);

/**
 *  获取plist中配置的Build号, 每一个版本用来上传到iTunesConnect的递增号码
 */
NSString* zw_getAppBuildVersion(void);

/**
 *  获取指定位数的App版本号
 *  @param nDotCount 版本号中小数点的个数
 *  @return 如 plist中版本号为 3.2.5.01，参数nDotCount为2时则只返回3.2.5。
 */
NSString* zw_getSendToServerAppVersion(NSUInteger nDotCount);

/**
 *  将发送给系统的版本号转换成数字 *
 *  @param nDotCount 版本号中小数点的个数 *
 *  @return 相邻小数点之间的版本号转换成两位数的数字，不足两位的补零，合成完成的数字去掉，如3.2.5.01，参数2 结果为30205。
 */
NSInteger zw_getAppVersionNumber(NSUInteger nDotCount);

/**
 *  获取发送给服务器的设备类型
 *  @return 三种值 iPod、iPad、iPhone
 */
NSString* zw_getSendToServerDeviceType(void);

/**
 *  获取应用程序在设备上显示的名字
 */
NSString* zw_getAppNameDisplayOnDevice(void);

/**
 *  获取app在itunesconnect中定义BundleID
 */
NSString* zw_getAppBundleIdentifier(void);

/**
 * 获取应用程序包含的图标文件名
 */
NSArray<NSString*>* zw_getAppIconFileNames(void);

/**
 * 获取App包含的启动页图片文件名
 * UILaunchImageSize：启动页图片大小
 * UILaunchImageOrientation：启动页图片支持的屏幕方向（Portrait：竖屏；Landscape：横屏）
 * UILaunchImageName：图片名字
 */
NSArray<NSDictionary*>* zw_getLaunchImageFileNames(void);

/**
 *  使设备震动，iPod等没有震动功能时无响应。
 */
void zw_makeDeviceToShake(void);

/**
 *  设备iOS系统的版本号
 */
NSString* zw_systemVersionString(void);

/**
 *  设备iOS系统的时区
 */
NSString* zw_systemTimeZoneString(void);

/**
 *  获取每一个物理设备的唯一字符串，只有在特殊情况才会发生变更（如系统重置，系统升级或者App重新安装都不会发生变更）
 */
NSString* zw_uniqueDeviceIdentifier(void);

/**
 *  获取App相关的基础信息
 *  @param isHtmlFormat html格式字符串
 */
NSString* zw_getAppBaseInfoForFeedBack(BOOL isHtmlFormat);

/**
 *  是否是iPad设备尺寸【注：并非判断硬件设备类型】
 */
BOOL zw_isiPad(void);
BOOL zw_isiPhone4(void);//3.5寸
/**
 *  是否iPhone5设备尺寸，注，当没有适配iphone6及iphone6+时，即使运行在iphone6或iphone6+设备上，调用此API也返回YES
 *  iphone6和6plus可以设置标准模式和放大模式。在放大模式下，6plus会退化为375x667，此时应该当成iphone6来做适配。而iphone6会退化为320x568，应该当作iphone5来适配。
 */
BOOL zw_isiPhone5(void);//4.0寸
BOOL zw_isiPhone6(void);//4.7寸
BOOL zw_isiPhone6Plus(void);//5.5寸
BOOL zw_isiPhoneX(void);//5.8寸
BOOL zw_isiPhoneXR(void);//6.1寸
BOOL zw_isiPhoneXSMax(void);//6.5寸
BOOL zw_isNoHomeButtonStyleScreen(void);//无Home按钮样式的屏幕，如iPhoneX、iPhoneXR、iPhoneXSMax、iPadPro12.9三代、iPadPro11都属于此类。

/**
 *  判断是否模拟器【也有可能是mac、windows、watchos等系统】
 */
BOOL zw_isSimulator(void);

/**
 *  鉴别iOS系统版本号
 */
BOOL zw_ios8OrLater(void);
BOOL zw_ios9OrLater(void);
BOOL zw_ios10OrLater(void);
BOOL zw_ios11OrLater(void);

/**
 * 获取App的资源路径
 */
NSString* zw_getResourcePath(void);

/**
 * 获取App沙盒下的临时文件存储路径
 * ⚠️如果将一个文件存储在此沙盒目录下，这个文件何时会被删除是不可预知的，也就是说，随时会被删除。
 */
NSString* zw_getTempPath(void);

/**
 * 获取App沙盒下的Library->Caches文件存储路径
 * ⚠️不会被同步到iCloud，同时在不主动删除的情况下可以长时间存在，当硬盘告急可能会被系统删除
 */
NSString* zw_getLibraryCachesPath(void);

/**
 * 获取App沙盒下的Library文件存储路径
 * ⚠️iTunes会备份此目录，但不包含其子目录Caches
 */
NSString* zw_getLibraryPath(void);

/**
 * 获取App沙盒下的Document文件存储路径
 * ⚠️iTunes会备份此目录，还可以通过配置实现iTunes共享文件
 */
NSString* zw_getDocumentPath(void);

/**
 * 获取App关联的AppGroup共享域文件存储路径，同一开发者账号下App之间可以共享该域的数据
 * @param AppGroupID AppleDeveloper中申请的AppGroups Identifier，且App已关联此ID
 * @return 参数无效或App未关联此ID时返回nil，模拟器在AppGroupID无效或未关联时也会正常返回值，但是以真机为准！
 */
NSString* zw_getAppGroupDirectoryWithAppGroupID(NSString* AppGroupID);

/**
 * 判断路径是否是文件夹
 * @param folderPath 目录的完整路径
 * @return 路径不存在或者路径非文件夹时值为NO
 */
BOOL zw_isFolderPath(NSString* folderPath);

/**
 * 指定路径或文件是否存在
 * ⚠️尝试判断，结果不一定准确，苹果建议实际操作。（It's far better to attempt an operation (like loading a file or creating a directory) and handle the error gracefully than it is to try to figure out ahead of time whether the operation will succeed.）
 */
BOOL zw_isFileExistsAtPath(NSString *filePath);

/**
 * 指定的共享域文件存储路径是否存在路径
 */
BOOL zw_isAppGroupDirectoryExist(NSString* AppGroupID);

/**
 * 指定路径或文件是否可读
 * ⚠️尝试判断，结果不一定准确，苹果建议实际操作。（It's far better to attempt an operation (like loading a file or creating a directory) and handle the error gracefully than it is to try to figure out ahead of time whether the operation will succeed.）
 */
BOOL zw_isFileReadableAtPath(NSString *filePath);

/**
 * 指定路径或文件是否可写
 * ⚠️尝试判断，结果不一定准确，苹果建议实际操作。（It's far better to attempt an operation (like loading a file or creating a directory) and handle the error gracefully than it is to try to figure out ahead of time whether the operation will succeed.）
 */
BOOL zw_isFileWritableAtPath(NSString *filePath);

/**
 * 指定路径或文件是否可执行
 * ⚠️尝试判断，结果不一定准确，苹果建议实际操作。（It's far better to attempt an operation (like loading a file or creating a directory) and handle the error gracefully than it is to try to figure out ahead of time whether the operation will succeed.）
 */
BOOL zw_isFileExecutableAtPath(NSString *filePath);

/**
 * 指定路径或文件是否可删除
 * ⚠️尝试判断，结果不一定准确，苹果建议实际操作。（It's far better to attempt an operation (like loading a file or creating a directory) and handle the error gracefully than it is to try to figure out ahead of time whether the operation will succeed.）
 */
BOOL zw_isFileDeletableAtPath(NSString *filePath);

/**
 * 移除指定路径或文件
 */
BOOL zw_removeFileAtPath(NSString *filePath);

/**
 * 拷贝指定源路径或文件到指定目标路径或文件
 */
BOOL zw_copyFileToDisPath(NSString *srcPath, NSString *distPath);

/**
 * 在指定路径、Document文件夹、AppGroup域下创建子文件夹。
 * @param directoryPath 已经存在的路径
 * @param folderName  子文件夹名字，支持创建多层级文件夹，文件夹名字以‘/’组合即可，如参数为“sub1/sub2”时，先在指定路径下创建sub1子文件夹，再在sub1文件夹下创建sub2子文件夹
 *                   可以为nil、空字符串【即@""】、字符串长度大于0的单层文件夹，也可以是已“/”分割的字符串表示多级文件夹，但不可含双“/”，如
 *                   @"path//subpath"，❌无效参数
 *                   @"abc"，单个文件夹
 *                   @"abc/subabc"，多层级文件夹
 * @return 参数指定的文件夹路径如果合法，无论是临时创建成功的还是已经存在的，都返回YES，否则返回NO
 */
BOOL zw_createFolderInDirectory(NSString* directoryPath, NSString* folderName);
BOOL zw_createFolderInDocumentDirectory(NSString* folderName);
BOOL zw_createFolderInLibraryDirectory(NSString* folderName);
BOOL zw_createFolderInAppGroupDirectory(NSString* AppGroupID, NSString* folderName);

/**
 * 获取指定路径、Document文件夹、AppGroup域下子文件夹的路径。
 * @param directoryPath 已经存在的路径
 * @param folderName  子文件夹名字，文件夹名字以‘/’组合表示多层级文件夹，如参数为“sub1/sub2”时，表示指定路径下sub1子文件夹中的sub2子文件夹，文件夹不存在时自动创建。
 *                   可以为nil、空字符串【即@""】、字符串长度大于0的单层文件夹，也可以是已“/”分割的字符串表示多级文件夹，但不可含双“/”，如
 *                   @"path//subpath"，❌无效参数
 *                   @"abc"，单个文件夹
 *                   @"abc/subabc"，多层级文件夹
 * @return 如果指定的folderName文件不存在，则自动创建，参数合法时返回指定的文件夹路径，否则返回nil
 */
NSString* zw_getFolderPathInDirectory(NSString* directoryPath, NSString* folderName);
NSString* zw_getFolderPathInDocumentDirectory(NSString* folderName);
NSString* zw_getFolderPathInLibraryDirectory(NSString* folderName);
NSString* zw_getFolderPathInAppGroupDirectory(NSString* AppGroupID, NSString* folderName);

/**
 * 💡建议开线程使用，因为可能比较耗时。
 * 清除指定路径、Document文件夹、AppGroup域下指定子文件夹中的文件，如果多层级时只删除最后一个路径下的文件；如果清除的文件夹不存在，自动创建；如果folderName为nil或空字符即【@""】表示删除指定路径下所有的文件，包括子文件夹也会被删除。
 * @param directoryPath 已经存在的路径
 * @param folderName  子文件夹名字，文件夹名字以‘/’组合表示多层级文件夹，如参数为“sub1/sub2”时，表示指定路径下sub1子文件夹中的sub2子文件夹，文件夹不存在时自动创建。
 *                   可以为nil、空字符串【即@""】、字符串长度大于0的单层文件夹，也可以是已“/”分割的字符串表示多级文件夹，但不可含双“/”，如
 *                   @"path//subpath"，❌无效参数
 *                   @"abc"，单个文件夹
 *                   @"abc/subabc"，多层级文件夹
 * @return 参数指定的文件夹路径如果合法，无论是删除成功的还是为创建的路径都返回YES，否则返回NO。
 */
BOOL zw_clearFolderInDirectory(NSString* directoryPath,NSString* folderName);
BOOL zw_clearFolderInDocumentDirectory(NSString* folderName);
BOOL zw_clearFolderInLibraryDirectory(NSString* folderName);
BOOL zw_clearFolderInAppGroupDirectory(NSString* AppGroupID, NSString* folderName);

/**
 * 获取指定路径、Document文件夹、AppGroup域中及其子文件夹中后缀名为指定字符格式的所有文件路径的集合。
 * @param directoryPath 已经存在的路径
 * @param folderName  子文件夹名字，文件夹名字以‘/’组合表示多层级文件夹，如参数为“sub1/sub2”时，表示指定路径下sub1子文件夹中的sub2子文件夹，文件夹不存在时自动创建。
 *                   可以为nil、空字符串【即@""】、字符串长度大于0的单层文件夹，也可以是已“/”分割的字符串表示多级文件夹，但不可含双“/”，如
 *                   @"path//subpath"，❌无效参数
 *                   @"abc"，单个文件夹
 *                   @"abc/subabc"，多层级文件夹
 * @param fileFormat 需要查找的文件格式的后缀名，如jpg
 * @param bDeletePathExtensionForKey 返回值的键中文件名是否包含后缀
 * @return 所有文件名与匹配的文件路径键值对（文件名是否包含后缀由参数bDeletePathExtensionForKey决定）。
 */
NSDictionary<NSString*,NSString*>* zw_getFilesPathInDirectory(NSString* directoryPath, NSString* folderName, NSString* fileFormat, BOOL bDeletePathExtensionForKey);
NSDictionary<NSString*,NSString*>* zw_getFilesPathInDocumentDirectory(NSString* folderName, NSString* fileFormat, BOOL bDeletePathExtensionForKey);
NSDictionary<NSString*,NSString*>* zw_getFilesPathInLibraryDirectory(NSString* folderName, NSString* fileFormat, BOOL bDeletePathExtensionForKey);
NSDictionary<NSString*,NSString*>* zw_getFilesPathInAppGroupDirectory(NSString* AppGroupID, NSString* folderName, NSString* fileFormat, BOOL bDeletePathExtensionForKey);

/**
 * 获取文件的属性
 * @return 文件属性的集合
 * NSFileSize（不可更改）
 * 文件或者文件夹的大小，注意单位是byte
 *
 * NSFileAppendOnly
 * 这个键的值需要设置为一个表示布尔值的NSNumber对象，表示创建的目录是否是只读的。
 *
 * NSFileCreationDate（可更改时间）
 * 这个键的值需要设置为一个NSDate对象，表示目录的创建时间。
 *
 * NSFileOwnerAccountName
 * 这个键的值需要设置为一个NSString对象，表示这个目录的所有者的名字。
 *
 * NSFileGroupOwnerAccountName
 * 这个键的值需要设置为一个NSString对象，表示这个目录的用户组的名字。
 *
 * NSFileGroupOwnerAccountID
 * 这个键的值需要设置为一个表示unsigned int的NSNumber对象，表示目录的组ID。
 *
 * NSFileModificationDate（可更改时间）
 * 这个键的值需要设置一个NSDate对象，表示目录的修改时间。
 *
 * NSFileOwnerAccountID
 * 这个键的值需要设置为一个表示unsigned int的NSNumber对象，表示目录的所有者ID。
 *
 * NSFilePosixPermissions
 * 这个键的值需要设置为一个表示short int的NSNumber对象，表示目录的访问权限。
 *
 * NSFileReferenceCount
 * 这个键的值需要设置为一个表示unsigned long的NSNumber对象，表示目录的引用计数，即这个目录的硬链接数。
 */
NSDictionary<NSFileAttributeKey, id>* zw_getFileAttributesAtPath(NSString *filePath);

/**
 * 对NSUserDefaults进行保存、读取、清空数据
 * @param AppGroupID 共享域的Key，如果为nil，则直接使用standardUserDefaults
 * @param dataKey 不可以为nil
 * @param dataValue 为nil，表示删除对应dataKey的值，和removeObjectForKey效果一样
 */
BOOL zw_saveDataToUserDefaults(NSString* AppGroupID, NSString* dataKey, id dataValue);
id zw_getDataFromUserDefaults(NSString* AppGroupID, NSString* dataKey);
BOOL zw_clearDataFromUserDefaults(NSString* AppGroupID);

/**
 * 显示系统分享框
 * @param activityItems 待分享的内容
 * @param sourceView 分享源视图，该值不可为空，⚠️系统分享框依赖的presentingCtrl为该视图所在的视图控制器。
 * @param completionHandler 系统分享框的回调
 * @return 返回值为YES时表示系统分享框可显示，NO表示无法显示，请检查参数sourceView是否有值且存在所属视图控制器。
 */
BOOL zw_showSystemShareBox(NSArray *activityItems,
                           UIView *sourceView,
                           void(^completionHandler)(BOOL isSuccess, NSError *error, NSString *activityType));

/**
 * LIFO模式显示系统弹框
 * @param title 标题，可以为nil
 * @param message 内容，可以为nil
 * -- 标题和内容可以为NSString对象
 * -- 标题和内容也可以为NSAttributedString对象
 * @param cancelActionTitle 取消操作的标题，该值为nil时默认操作标题为“关闭”
 * @param cancelActionTitleColor 取消操作的标题颜色，该值可以为nil
 * @param otherActionTitles 其他操作的标题，该值可以为nil。⚠️当标题、内容和其他操作标题数组这三个属性都为空时，不显示系统弹框。
 * -- 数组内为NSString对象时，表示其他操作的标题
 * -- 数组内为NSDictionary对象时，表示其他操作的信息，需组装成title、color、style关键字属性信息，至少需要title，否则操作无效
 * @param presentingViewController 模态系统弹框的根视图控制器。该参数为nil时可能会导致模态沙盒路径失败。
 * @param alertStyle 系统弹框样式
 * @param completionHandler 系统弹框操作后的回调。
 * -- cancelAction对应的索引为0
 * -- otherActionTitles对应的索引从1开始递增，即使数组中包含无效元素，不影响索引的递增值
 * @return 返回值为YES时表示系统弹框可显示，NO表示无法显示，请检查参数title、message、otherActionTitles、presentingViewController等参数是否有效。
 */
BOOL zw_showSystemAlertController(id title,
                                  id message,
                                  NSString *cancelActionTitle,
                                  UIColor *cancelActionTitleColor,
                                  NSArray *otherActionTitles,
                                  UIViewController *presentingViewController,
                                  UIAlertControllerStyle alertStyle,
                                  void(^completionHandler)(NSInteger atIndex, UIAlertAction *action));

/**
 *  检测是否在主线程
 */
BOOL zw_isMainThread(void);

typedef NS_ENUM(NSInteger, enTextWithSignType)
{
    enTextWithSignTypeNormal = 0,//正常文字
    enTextWithSignTypeAt,//@符号的连续字符串
    enTextWithSignTypePound,//#符号的连续字符串
    enTextWithSignTypeCount,
};//特殊符号字符串的类型

/**
 *  验证一段文字结尾是@或者#的连续字符串,如微信群，给指定的人发消息@somebody
 *  @param text 一段正在录入的文字
 *  @param patternForAtSignType @开头关键字的正则表达式
 *  @param patternForPoundSignType #开头关键字的正则表达式
 *  @param callBack 回调块
 *  文字为【789@123】时，      回调块中signStringType值enTextWithSignTypeAt，signString值为123；
 *  文字为【789@123  96】时，回调块中signStringType值enTextWithSignTypeNormal，signString值nil；
 *  文字为【789#】时，              回调块中signStringType值enTextWithSignTypePound，signString为@""；
 */
void zw_verifyTextWithRegularExpressionsForPoundOrAtSignType(NSString* text, NSString* patternForAtSignType, NSString* patternForPoundSignType, void(^callBack)(enTextWithSignType signStringType, NSString* signString));

/**
 * 用指定的文字来替换源文字中@或#符号后的字符串返回新的字符串，如微信中@给某些用户时根据模糊字符查询出的用户，经用户选择确定后，替换掉模糊字符串
 * @param text 源文字（包含@或者#）
 * @param signStringType 特殊字符类型，值为enTextWithSignTypeNormal时字符串不做处理，否则处理后,还需以空格结尾。
 * @param signString 指定的字符串
 * @return 新的字符串
 */
NSString* zw_refreshTextWithSignString(NSString* text, enTextWithSignType signStringType, NSString* signString);

/**
 *  拷贝文字到系统粘贴板
 */
void zw_copyTextToPasteboard(NSString* text);

/**
 * 截取精度很高的小数其小数点后指定的位数，如经纬度截取小数点后6位可用此方法（只舍不入）
 * @param doubleValue 高精度小数
 * @param nCountAfterPoint 小数点后指定保留的位数
 * @return 对涉及商品价格等对浮点数精度要求较高的算术运算时尽量使用NSDecimalNumber模型代替原始C的算术运算，防止计算机精度误差造成的失误
 */
NSDecimalNumber* zw_convertDoubleWithRoundDown(double doubleValue, NSUInteger nCountAfterPoint);

/**
 * 四舍五入法截取精度很高的小数其小数点后指定的位数
 * @param doubleValue 高精度小数
 * @param nCountAfterPoint 小数点后指定保留的位数
 * @return 对涉及商品价格等对浮点数精度要求较高的算术运算时尽量使用NSDecimalNumber模型代替原始C的算术运算，防止计算机精度误差造成的失误
 */
NSDecimalNumber* zw_convertDoubleWithRoundPlain(double doubleValue, NSUInteger nCountAfterPoint);

/**
 * 转换NSObject对象成Json字符串
 * @param object NSObject的对象
 * @return 换换后的Json字符串
 */
NSString* zw_convertToJsonStringWithObject(NSObject* object);

/**
 * 比较两个NSObject实例的值是否一样（两个实例的指针可以不同），包括是否同为nil。
 * 💡 【isEqualToString:】该方法仅在字符串不为nil时比较才有效，如果两个同为nil的字符串使用【isEqualToString:】，返回值为NO
 */
BOOL zw_checkNSObjectInstanceSame(NSObject* pFirstObj, NSObject* pSecondObj);

/**
 * 判断公历年是否是闰年
 * @param year 年份
 */
BOOL zw_checkLeapYear(NSInteger year);

/**
 * 使系统状态栏上的网络活跃图标显示或者隐藏
 * @param isVisible 是否显示
 */
void zw_makeNetworkActivityIndicatorVisible(BOOL isVisible);

/**
 * 关闭系统的虚拟键盘，用在视图上的按钮被虚拟键盘遮挡时。
 */
void zw_closeVirtualKeyboard(void);

/**
 * 使实例执行指定方法，可设定参数
 * @param target 实例
 * @param selector 方法名
 * @param paras 参数，如果多于实际使用的参数将被忽略。如果中间部分参数为nil，可以使用[NSNull null]对象代替对应位置的参数值。
 * @return 实例执行指定方法后的返回值
 */
id zw_getResultValueAfterTargetPerformSelector(id target, SEL selector, NSArray* paras);

typedef NS_ENUM(NSInteger, ConstellationType)
{
    ConstellationType_Unknown = 0,//星座未知
    ConstellationType_Aries = 1,//白羊座
    ConstellationType_Taurus = 2,//金牛座
    ConstellationType_Gemini = 3,//双子座
    ConstellationType_Cancer = 4,//巨蟹座
    ConstellationType_Leo = 5,//狮子座
    ConstellationType_Virgo = 6,//处女座
    ConstellationType_Libra = 7,//天秤座
    ConstellationType_Scorpio = 8,//天蝎座
    ConstellationType_Sagittarius = 9,//射手座
    ConstellationType_Capricorn = 10,//摩羯座
    ConstellationType_Aquarius = 11,//水瓶座
    ConstellationType_Pisces = 12,//双鱼座
};//星座类型
/**
 * 根据月份和日计算星座
 * @param nMonth 月份
 * @param nDay 日期
 * @return 星座的枚举值
 */
ConstellationType zw_getConstellation(NSInteger nMonth, NSInteger nDay);

/**
 * 从URL中提取参数值
 * @param url 网络链接的字符串
 * @param parameterName 参数名
 * @return url中参数名对应的值
 */
NSString* zw_extractParameterValueFromURL(NSURL *url, NSString *parameterName);

/// 修改设备的朝向。
/// @param targetOrientation 设备的目标朝向。
void zw_changeDeviceOrientation(UIInterfaceOrientation targetOrientation);

#ifdef __cplusplus
}
#endif
