//
//  ZWSendSMSManager.h
//  ZWProjectsCore
//
//  Created by ZhangWei on 16/1/15.
//  Copyright © 2016年 ZhangWei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface ZWSendSMSManager : NSObject

+ (BOOL)canSendSMS;

+ (void)sendSMSWithBody:(NSString*)smsBody
             recipients:(NSArray <NSString*>*)recipients
            popFromCtrl:(__weak UIViewController*)actionController
               finished:(void(^)(void))completedHandle
           resultHandle:(void(^)(BOOL canSend, MessageComposeResult result))resultBlock;

+ (void)dismissIfSMSCtrlDisplaying;

@end
