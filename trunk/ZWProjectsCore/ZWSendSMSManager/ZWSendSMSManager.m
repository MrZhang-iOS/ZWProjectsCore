//
//  ZWSendSMSManager.m
//  ZWProjectsCore
//
//  Created by ZhangWei on 16/1/15.
//  Copyright © 2016年 ZhangWei. All rights reserved.
//

#import "ZWSendSMSManager.h"

static ZWSendSMSManager* g_pZWSendSMSManager = nil;

@interface ZWSendSMSManager ()
<MFMessageComposeViewControllerDelegate>
@property(nonatomic,weak)UIViewController* actionController;
@property(nonatomic,copy)void(^resultBlock)(BOOL canSend, MessageComposeResult status);

+ (ZWSendSMSManager*)sharedZWSendSMSManager;

- (void)sendSMSWithBody:(NSString*)smsBody
             recipients:(NSArray*)recipients
            popFromCtrl:(__weak UIViewController*)actionController
               finished:(void(^)(void))completedHandle
           resultHandle:(void(^)(BOOL canSend, MessageComposeResult result))resultBlock;

@end
@implementation ZWSendSMSManager

+ (BOOL)canSendSMS;
{
    Class messageClass = (NSClassFromString(@"MFMessageComposeViewController"));
    if (messageClass != nil && [messageClass canSendText])
    {
        return YES;
    }
    return NO;
}

+ (void)sendSMSWithBody:(NSString*)smsBody
             recipients:(NSArray*)recipients
            popFromCtrl:(__weak UIViewController*)actionController
               finished:(void(^)(void))completedHandle
           resultHandle:(void(^)(BOOL canSend, MessageComposeResult result))resultBlock;
{
    void (^task)(void) = ^{
        if ([ZWSendSMSManager canSendSMS])
        {
            [[ZWSendSMSManager sharedZWSendSMSManager] sendSMSWithBody:smsBody
                                                        recipients:recipients
                                                       popFromCtrl:actionController
                                                          finished:completedHandle
                                                      resultHandle:resultBlock];
        }
        else
        {
            if (resultBlock)
            {
                resultBlock(NO, MessageComposeResultFailed);
            }
        }
    };
    
    if (strcmp(dispatch_queue_get_label(DISPATCH_CURRENT_QUEUE_LABEL), dispatch_queue_get_label(dispatch_get_main_queue())) == 0)
    {
        // 主队列
        task();
    }
    else
    {
        // 非主队列
        dispatch_async(dispatch_get_main_queue(), task);
    }
}

+ (void)dismissIfSMSCtrlDisplaying;
{
    if ([ZWSendSMSManager sharedZWSendSMSManager].actionController)
    {
        [[ZWSendSMSManager sharedZWSendSMSManager].actionController dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark - PrivateFunctions
+ (ZWSendSMSManager*)sharedZWSendSMSManager;
{
    static dispatch_once_t pred;
    dispatch_once(&pred, ^ {
        g_pZWSendSMSManager = [[ZWSendSMSManager alloc] init];
    });
    return g_pZWSendSMSManager;
}

- (void)sendSMSWithBody:(NSString*)smsBody
             recipients:(NSArray*)recipients
            popFromCtrl:(__weak UIViewController*)actionController
               finished:(void(^)(void))completedHandle
           resultHandle:(void(^)(BOOL canSend, MessageComposeResult result))resultBlock;
{
    self.actionController = actionController;
    self.resultBlock = resultBlock;
    
    MFMessageComposeViewController* tempCtrl = [[MFMessageComposeViewController alloc] init];
    tempCtrl.messageComposeDelegate = self;
    tempCtrl.body = smsBody;
    tempCtrl.recipients = recipients;
    
    //添加附件
    if ([MFMessageComposeViewController canSendAttachments])
    {
        /*
         //第一种方法
        //messageController.attachments=...;
        
        //第二种方法
        NSArray *attachments= [self.attachments.text componentsSeparatedByString:@","];
        if (attachments.count>0) {
            [attachments enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                NSString *path=[[NSBundle mainBundle]pathForResource:obj ofType:nil];
                NSURL *url=[NSURL fileURLWithPath:path];
                [messageController addAttachmentURL:url withAlternateFilename:obj];
            }];
        }
         
        //第三种方法
        //            NSString *path=[[NSBundle mainBundle]pathForResource:@"photo.jpg" ofType:nil];
        //            NSURL *url=[NSURL fileURLWithPath:path];
        //            NSData *data=[NSData dataWithContentsOfURL:url];
        //
        //attatchData:文件数据
        //uti:统一类型标识，标识具体文件类型，详情查看：帮助文档中System-Declared Uniform Type Identifiers
        //fileName:展现给用户看的文件名称
        //[messageController addAttachmentData:data typeIdentifier:@"public.image"  filename:@"photo.jpg"];
         */
    }
    if (actionController)
    {
        [actionController presentViewController:tempCtrl animated:YES completion:completedHandle];
    }
}
#pragma mark - MFMessageComposeViewControllerDelegate
//发送完成，不管成功与否
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result;
{
    __weak typeof(self) wSelf = self;
    
    if (self.actionController)
    {
        [self.actionController dismissViewControllerAnimated:YES completion:^{
            if (wSelf.resultBlock)
            {
                wSelf.resultBlock(YES, result);
            }
        }];
    }
    else
    {
        if (wSelf.resultBlock)
        {
            wSelf.resultBlock(YES, result);
        }
    }
}
@end
