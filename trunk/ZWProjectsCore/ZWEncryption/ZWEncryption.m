//
//  ZWEncryption.m
//  ZWProjectsCore
//
//  Created by ZhangWei on 14-6-27.
//  Copyright (c) 2014年 ZhangWei. All rights reserved.
//

#import "ZWEncryption.h"
#import "GTMBase64.h"
#import "NSData+AES.h"
#import "NSString+MD5.h"
#import "NSString+ZhangWei.h"
#import "NSData+ZhangWei.h"
#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonHMAC.h>

@implementation ZWEncryption
#pragma mark - Base64
+ (NSData*)enCodeBase64ToDataWithData:(NSData*)data;
{
    return [GTMBase64 encodeData:data];
}

+ (NSData*)deCodeBase64ToDataWithData:(NSData*)data;
{
    return [GTMBase64 decodeData:data];
}

+ (NSData*)enCodeBase64ToDataWithString:(NSString*)string;
{
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    return [GTMBase64 encodeData:data];
}

+ (NSString*)deCodeBase64ToStringWithData:(NSData*)data;
{
    NSData* deData = [GTMBase64 decodeData:data];
    return [[NSString alloc] initWithData:deData encoding:NSUTF8StringEncoding];
}

+ (NSString*)enCodeBase64ToStringWithData:(NSData*)data;
{
    NSData *deData = [GTMBase64 encodeData:data];
    return [[NSString alloc] initWithData:deData encoding:NSUTF8StringEncoding];
}

+ (NSData*)deCodeBase64ToDataWithString:(NSString*)string;
{
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    return [GTMBase64 decodeData:data];
}

+ (NSString*)enCodeBase64ToStringWithString:(NSString*)string;
{
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    data = [GTMBase64 encodeData:data];
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}

+ (NSString*)deCodeBase64ToStringWithString:(NSString*)string;
{
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    data = [GTMBase64 decodeData:data];
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}

+ (NSString*)decodeGBKBase64ToStringWithString:(NSString*)string;
{
    if (string && [string isKindOfClass:[NSString class]])
    {
        //定义GBK编码格式
        NSStringEncoding enc = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
        NSData *decodedData = [[NSData alloc]initWithBase64EncodedString:string options:0];
        return [[NSString alloc] initWithData:decodedData encoding:enc];
    }
    return nil;
}

#pragma mark - AES加密
+ (NSData*)enCodeAESToDataWithData:(NSData*)data key:(NSString*)key;
{
    return [data AES256EncryptWithKey:key];
}

+ (NSData*)deCodeAESToDataWithData:(NSData*)data key:(NSString*)key;
{
    return [data AES256DecryptWithKey:key];
}

+ (NSData*)enCodeAESToDataWithString:(NSString*)string key:(NSString*)key;
{
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    return [data AES256EncryptWithKey:key];
}

+ (NSString*)deCodeAESToStringWithData:(NSData*)data key:(NSString*)key;
{
    NSData *deData = [data AES256DecryptWithKey:key];
    return [[NSString alloc] initWithData:deData encoding:NSUTF8StringEncoding];
}

#pragma mark - MD5加密
+ (NSString*)createMD5StringWithString:(NSString*)string;
{
    return [string md5Encrypt];
}

+ (NSString*)createMD5StringWithData:(NSData*)data;
{
    NSString* pString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    return [pString md5Encrypt];
}

#pragma mark - hmacSHA512加密
+ (NSString *)hmacSHA512StringWithKey:(NSString *)key content:(NSString*)content;
{
    if (key && content && [key isKindOfClass:[NSString class]] && [content isKindOfClass:[NSString class]])
    {
        NSData *keyData = [key zw_convertToDataFromHexString];
        NSData *messageData = [content dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableData *mutableData = [NSMutableData dataWithLength:CC_SHA512_DIGEST_LENGTH];
        CCHmac(kCCHmacAlgSHA512, keyData.bytes, keyData.length, messageData.bytes, messageData.length, mutableData.mutableBytes);
        
        return [mutableData zw_convertToHexString];
    }
    return nil;
}

@end
