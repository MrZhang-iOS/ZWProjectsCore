//
//  ZWEncryption.h
//  ZWProjectsCore
//
//  Created by ZhangWei on 14-6-27.
//  Copyright (c) 2014年 ZhangWei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZWEncryption : NSObject

#pragma mark - Base64加密+解密
+ (NSData*)enCodeBase64ToDataWithData:(NSData*)data;
+ (NSData*)deCodeBase64ToDataWithData:(NSData*)data;

+ (NSData*)enCodeBase64ToDataWithString:(NSString*)string;
+ (NSString*)deCodeBase64ToStringWithData:(NSData*)data;

+ (NSString*)enCodeBase64ToStringWithData:(NSData*)data;
+ (NSData*)deCodeBase64ToDataWithString:(NSString*)string;

+ (NSString*)enCodeBase64ToStringWithString:(NSString*)string;
+ (NSString*)deCodeBase64ToStringWithString:(NSString*)string;

/*
 有些字符串通过UTF-8转换出来可能是nil，需要通过GBK格式转换，如下字符串：
 eyJlcnJvckNvZGUiOi05OSwiZXJyb3JNc2ciOiLWuMHutO3O8yIsInJldE1zZyI6ItK1zvGxqM7E0uyzoyJ9
 转换后为：
 {"errorCode":-99,"errorMsg":"指令错误","retMsg":"业务报文异常"}
 */
+ (NSString*)decodeGBKBase64ToStringWithString:(NSString*)string;

#pragma mark - AES加密+解密
+ (NSData*)enCodeAESToDataWithData:(NSData*)data key:(NSString*)key;
+ (NSData*)deCodeAESToDataWithData:(NSData*)data key:(NSString*)key;

+ (NSData*)enCodeAESToDataWithString:(NSString*)string key:(NSString*)key;
+ (NSString*)deCodeAESToStringWithData:(NSData*)data key:(NSString*)key;

#pragma mark - 创建MD5
+ (NSString*)createMD5StringWithString:(NSString*)string;
+ (NSString*)createMD5StringWithData:(NSData*)data;

#pragma mark - hmacSHA512加密
+ (NSString *)hmacSHA512StringWithKey:(NSString *)key content:(NSString*)content;

@end
