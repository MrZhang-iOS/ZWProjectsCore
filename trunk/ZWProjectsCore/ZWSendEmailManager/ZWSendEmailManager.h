//
//  ZWSendEmailManager.h
//  ZWProjectsCore
//
//  Created by ZhangWei on 15-1-4.
//  Copyright © 2015年 ZhangWei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "ZWEmailAttachedData.h"

@interface ZWSendEmailManager : NSObject

+ (BOOL)isSystemSettingsConfigEmailAccount;

/**
 *  从指定控制器弹出准备发送邮件的页面
 *
 *  @param subjectString    邮件的标题
 *  @param messageBody      邮件的内容
 *  @param isHtmlFormat     邮件的内容是否html格式
 *  @param recipients       收件人
 *  @param ccRecipients     抄送的收件人
 *  @param bccRecipients    密送的收件人
 *  @param attachmentFiles  附件的数组，可以附带多个或者不带附件
 *  @param actionController 控制邮件弹出的控制器
 *  @param resultBlock       switch (result)
                             {
                                 case MFMailComposeResultCancelled:
                                     NSLog(@"Result: canceled");
                                     break;
                                 case MFMailComposeResultSaved:
                                     NSLog(@"Result: saved");
                                     break;
                                 case MFMailComposeResultSent:
                                     NSLog(@"Result: sent");
                                     break;
                                 case MFMailComposeResultFailed:
                                     NSLog(@"Result: failed");
                                     break;
                                 default:
                                     NSLog(@"Result: not sent");
                                     break;
                             }
 ⭐️注意 Gmail附件上限为25M，超过此上限阈值，附件将会被直接剔除。
 */
+ (void)sendEmailWithSubject:(NSString*)subjectString
                 messageBody:(NSString*)messageBody
     isMessageBodyHtmlFormat:(BOOL)isHtmlFormat
                  recipients:(NSArray<NSString *> *)recipients
                ccRecipients:(NSArray<NSString *> *)ccRecipients
               bccRecipients:(NSArray<NSString *> *)bccRecipients
              attachmentData:(NSArray<ZWEmailAttachedData *> *)attachmentFiles
                 popFromCtrl:(UIViewController*)actionController
                resultHandle:(void(^)(BOOL canSend, MFMailComposeResult status))resultBlock;

/**
 *  请在依附的控制器消失之前调用以下方法关闭邮箱的页面，否则将导致邮箱页面无法关闭
 */
+ (void)dismissIfEmailCtrlDisplaying;

@end

/*
*
*  当前项目内部打开系统的邮箱页面
*
*/
