//
//  ZWSendEmailManager.m
//
//  Created by ZhangWei on 15-1-4.
//
//

#import "ZWSendEmailManager.h"
#import "ZWProjectsCoreFunctions.h"

static ZWSendEmailManager* g_pZWSendEmailManager = nil;

@interface ZWSendEmailManager ()
<MFMailComposeViewControllerDelegate>

@property(nonatomic,weak)UIViewController* actionController;

@property(nonatomic,copy)void(^resultBlock)(BOOL canSend, MFMailComposeResult status);

+ (ZWSendEmailManager*)sharedZWSendEmailManager;

- (void)sendEmailWithSubject:(NSString*)subjectString
                 messageBody:(NSString*)messageBody
     isMessageBodyHtmlFormat:(BOOL)isHtmlFormat
                  recipients:(NSArray*)recipients
                ccRecipients:(NSArray*)ccRecipients
               bccRecipients:(NSArray*)bccRecipients
              attachmentData:(NSArray*)attachmentFiles;

-(void)displayComposerSheetWithSubject:(NSString*)subjectString
                           messageBody:(NSString*)messageBody
               isMessageBodyHtmlFormat:(BOOL)isHtmlFormat
                            recipients:(NSArray*)recipients
                          ccRecipients:(NSArray*)ccRecipients
                         bccRecipients:(NSArray*)bccRecipients
                        attachmentData:(NSArray*)attachmentFiles;

/**
 *  如果系统没有配置邮箱账号，此参数均无效，仅仅是可以从当前应用跳转到邮箱应用
 *
 *  @param subjectString   标题
 *  @param messageBody     内容
 *  @param isHtmlFormat    html格式
 *  @param recipients      接收者
 *  @param ccRecipients    抄送接收者
 *  @param bccRecipients   密送接收者
 *  @param attachmentFiles 附件尚未完善，如果需要请补上，或者直接使用-displayComposerSheetWithSubject
 */
-(void)launchMailAppOnDeviceWithSubject:(NSString*)subjectString
                            messageBody:(NSString*)messageBody
                isMessageBodyHtmlFormat:(BOOL)isHtmlFormat
                             recipients:(NSArray*)recipients
                           ccRecipients:(NSArray*)ccRecipients
                          bccRecipients:(NSArray*)bccRecipients
                         attachmentData:(NSArray*)attachmentFiles;

@end

@implementation ZWSendEmailManager

+ (BOOL)isSystemSettingsConfigEmailAccount;
{
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    
    if (mailClass != nil && [mailClass canSendMail])
    {
        return YES;
    }
    
    return NO;
}

+ (void)sendEmailWithSubject:(NSString*)subjectString
                 messageBody:(NSString*)messageBody
     isMessageBodyHtmlFormat:(BOOL)isHtmlFormat
                  recipients:(NSArray<NSString *> *)recipients
                ccRecipients:(NSArray<NSString *> *)ccRecipients
               bccRecipients:(NSArray<NSString *> *)bccRecipients
              attachmentData:(NSArray<ZWEmailAttachedData *> *)attachmentFiles
                 popFromCtrl:(UIViewController*)actionController
                resultHandle:(void(^)(BOOL canSend, MFMailComposeResult status))resultBlock;
{
    if ([ZWSendEmailManager isSystemSettingsConfigEmailAccount])
    {
        [ZWSendEmailManager sharedZWSendEmailManager].actionController = actionController;
        [ZWSendEmailManager sharedZWSendEmailManager].resultBlock = resultBlock;
        [[ZWSendEmailManager sharedZWSendEmailManager] sendEmailWithSubject:subjectString
                                                            messageBody:messageBody
                                                isMessageBodyHtmlFormat:isHtmlFormat
                                                             recipients:recipients
                                                           ccRecipients:ccRecipients
                                                          bccRecipients:bccRecipients
                                                         attachmentData:attachmentFiles];
    }
    else
    {
        if (resultBlock)
        {
            resultBlock(NO, MFMailComposeResultCancelled);
        }
    }
}

+ (void)dismissIfEmailCtrlDisplaying;
{
    if ([ZWSendEmailManager sharedZWSendEmailManager].actionController)
    {
        [[ZWSendEmailManager sharedZWSendEmailManager].actionController dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark - PrivateFunctions
+ (ZWSendEmailManager*)sharedZWSendEmailManager;
{
    static dispatch_once_t pred;
    dispatch_once(&pred, ^ {
        g_pZWSendEmailManager = [[ZWSendEmailManager alloc] init];
    });
    return g_pZWSendEmailManager;
}

- (id)init
{
    if (self = [super init])
    {
    }
    return self;
}

- (void)sendEmailWithSubject:(NSString*)subjectString
                 messageBody:(NSString*)messageBody
     isMessageBodyHtmlFormat:(BOOL)isHtmlFormat
                  recipients:(NSArray*)recipients
                ccRecipients:(NSArray*)ccRecipients
               bccRecipients:(NSArray*)bccRecipients
              attachmentData:(NSArray*)attachmentFiles;
{
    void (^task)(void) = ^{
        //无需检验系统是否可否发送邮件，直接调用以下API，系统识别不能发送邮件时将自动弹框提醒用户
        [self displayComposerSheetWithSubject:subjectString
                                  messageBody:messageBody
                      isMessageBodyHtmlFormat:isHtmlFormat
                                   recipients:recipients
                                 ccRecipients:ccRecipients
                                bccRecipients:bccRecipients
                               attachmentData:attachmentFiles];
    };
    
    if (strcmp(dispatch_queue_get_label(DISPATCH_CURRENT_QUEUE_LABEL), dispatch_queue_get_label(dispatch_get_main_queue())) == 0)
    {
        // 主队列
        task();
    }
    else
    {
        // 非主队列
        dispatch_async(dispatch_get_main_queue(), task);
    }
}

-(void)displayComposerSheetWithSubject:(NSString*)subjectString
                           messageBody:(NSString*)messageBody
               isMessageBodyHtmlFormat:(BOOL)isHtmlFormat
                            recipients:(NSArray*)recipients
                          ccRecipients:(NSArray*)ccRecipients
                         bccRecipients:(NSArray*)bccRecipients
                        attachmentData:(NSArray*)attachmentFiles;
{
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    [picker setSubject:subjectString];
    [picker setMessageBody:messageBody isHTML:isHtmlFormat];
    [picker setToRecipients:recipients];
    [picker setCcRecipients:ccRecipients];
    [picker setBccRecipients:bccRecipients];
    for (ZWEmailAttachedData* tempAttachmentDataModel in attachmentFiles)
    {
        if (tempAttachmentDataModel.attachmentData && tempAttachmentDataModel.mimeType && tempAttachmentDataModel.fileName)
        {
            [picker addAttachmentData:tempAttachmentDataModel.attachmentData mimeType:tempAttachmentDataModel.mimeType fileName:tempAttachmentDataModel.fileName];
        }
    }
    
    if (self.actionController && picker)
    {
        [self.actionController presentViewController:picker animated:YES completion:nil];
    }
}

-(void)launchMailAppOnDeviceWithSubject:(NSString*)subjectString
                            messageBody:(NSString*)messageBody
                isMessageBodyHtmlFormat:(BOOL)isHtmlFormat
                             recipients:(NSArray*)recipients
                           ccRecipients:(NSArray*)ccRecipients
                          bccRecipients:(NSArray*)bccRecipients
                         attachmentData:(NSArray*)attachmentFiles;
{
    NSMutableString *mailUrl = [[NSMutableString alloc] init];
    //添加收件人
    [mailUrl appendFormat:@"mailto:%@", recipients.count>0?[recipients componentsJoinedByString:@","]:@""];
    //添加抄送
    [mailUrl appendFormat:@"?cc=%@", ccRecipients.count>0?[ccRecipients componentsJoinedByString:@","]:@""];
    //添加密送
    [mailUrl appendFormat:@"&bcc=%@", bccRecipients.count>0?[bccRecipients componentsJoinedByString:@","]:@""];
    //添加主题
    [mailUrl appendString:[NSString stringWithFormat:@"&subject=%@", subjectString]];
    //添加邮件内容
    if (isHtmlFormat)
    {
        if (messageBody)
        {
            [mailUrl appendString:[NSString stringWithFormat:@"&body=<b>%@</b>", messageBody]];
        }
    }
    else
    {
        if (messageBody)
        {
            [mailUrl appendString:[NSString stringWithFormat:@"&body=%@", messageBody]];
        }
    }
    
    //修复了⚠️。旧的方法如下
    //NSString* email = [mailUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString* email = [mailUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    zw_openURLString(email); 
    
    if (self.resultBlock)
    {
        self.resultBlock(YES, MFMailComposeResultSent);
    }
}

#pragma mark - MFMailComposeViewControllerDelegate
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    if (error)
    {
        NSLog(@"%@", error.localizedDescription);
    }
    
    __weak typeof(self) wSelf = self;
    if (self.actionController)
    {
        [self.actionController dismissViewControllerAnimated:YES completion:^
         {
             if (wSelf.resultBlock)
             {
                 wSelf.resultBlock(YES, result);
             }
         }];
    }
    else
    {
        if (wSelf.resultBlock)
        {
            wSelf.resultBlock(YES, result);
        }
    }
}

@end
