//
//  ZWEmailAttachedData.h
//  ZWProjectsCore
//
//  Created by ZhangWei on 15/11/19.
//  Copyright © 2015年 ZhangWei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZWEmailAttachedData : NSObject

/**
 *  Must not be <tt>nil</tt>.即不能为空
 */
@property(nonatomic,strong)NSData* attachmentData;

/**
 *  Must not be <tt>nil</tt>.即不能为空
 */
//如果没有注明图片或者其他类型，在邮箱里可能无法预览图片或其他类型；但如果文件名带有后缀，mimeType为空字符串，也能识别附件类型
//  png图片  image/png
//  jpeg图片  image/jpeg
//  html文件  text/html
//  pdf文件   application/pdf
//  word文档  application/msword
//  ppt文档   application/vnd.ms-powerpoint
//  zip文件   application/zip, application/octet-stream
@property(nonatomic,copy)NSString* mimeType;

/**
 *  Must not be <tt>nil</tt>.不能为空。它是附件带后缀的文件名，如*.zip *.png等
 */
//This is displayed below the attachment's icon if the attachment is not decoded when displayed.
@property(nonatomic,copy)NSString* fileName;

@end
