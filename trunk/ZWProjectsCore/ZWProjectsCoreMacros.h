//
//  ZWProjectsCoreMacros.h
//  ZWProjectsCore
//
//  Created by ZhangWei on 15/5/3.
//  Copyright (c) 2015年 ZhangWei. All rights reserved.
//

#ifndef ZWProjectsCoreMacros_h
#define ZWProjectsCoreMacros_h

#pragma mark - App通用宏

/**
 * 弱引用/强引用（两个连着的井号##在宏中是一个特殊符号，它表示将两个参数连接起来这种运算。注意函数宏必须是有意义的运算，因此你不能直接写AB来连接两个参数，而需要写成例子中的A##B。）
 */
#define __weakSelf(type)  __weak typeof(type) weak##type = type;
#define __strongSelf(type)  __strong typeof(type) strong##type = weak##type;

/**
 * 强弱引用，规避循环引用问题。源自YYKit。
 */
#ifndef weakify
    #if DEBUG
        #if __has_feature(objc_arc)
            #define weakify(object) autoreleasepool{} __weak __typeof__(object) weak##_##object = object;
        #else
            #define weakify(object) autoreleasepool{} __block __typeof__(object) block##_##object = object;
        #endif
    #else
        #if __has_feature(objc_arc)
            #define weakify(object) try{} @finally{} {} __weak __typeof__(object) weak##_##object = object;
        #else
            #define weakify(object) try{} @finally{} {} __block __typeof__(object) block##_##object = object;
        #endif
    #endif
#endif

#ifndef strongify
    #if DEBUG
        #if __has_feature(objc_arc)
            #define strongify(object) autoreleasepool{} __typeof__(object) object = weak##_##object;
        #else
            #define strongify(object) autoreleasepool{} __typeof__(object) object = block##_##object;
        #endif
    #else
        #if __has_feature(objc_arc)
            #define strongify(object) try{} @finally{} __typeof__(object) object = weak##_##object;
        #else
            #define strongify(object) try{} @finally{} __typeof__(object) object = block##_##object;
        #endif
    #endif
#endif

/**
 * 快速获得一个不定参数的字符串
 */
#define PTString(fmt, ...) zw_formatString(fmt, ##__VA_ARGS__)

/**
 * ⭐️日志的详细内容，使用以下预定义宏是为了打印日志时一并打印出具体的文件名、方法名、行号，如果直接封装在一个函数里，可能导致这些信息固定不变，对开发无法起到调试帮助的作用。
 *
 * ANSIC标准定义了以下5种可供C语言使用的预定义宏：
 * __PRETTY_FUNCTION__ 是GCC编译器特有的常量，在编译时自动生成，表示带有详细类型信息和名称修饰的函数名
 * __FUNCTION__ 是标准C++中定义的常量，返回的是当前函数的名称，但不包含函数的参数类型和修饰符
 * __LINE__ 在源代码中插入当前源代码行号
 * __FILE__ 在源代码中插入当前源代码文件名
 * __DATE__ 在源代码中插入当前编译日期〔慎用！注意和当前系统日期区别开来〕
 * __TIME__ 在源代码中插入当前编译时间〔慎用！注意和当前系统时间区别开来〕
 * NSStringFromSelector(_cmd) 获取当前函数的方法名
 * VA_ARGS  是C99编译器标准中定义的一个可变参数宏(variadic macros)。宏前面加上##的作用在于，当可变参数的个数为0时，##可以把前面多余的","去掉。如果有可变参数， 编译器会把这些可变参数放到逗号的后面。
 */
#define LogDetail(fmt, ...) PTString(@"%@\nFILE【%@(l:%d)】\nFUNC【%s】\n\n", [NSString stringWithFormat:(fmt), ##__VA_ARGS__], [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, __PRETTY_FUNCTION__)

/**
 * 覆盖系统打印日志的接口，添加了一些附加信息(如，文件名、行号、方法名等)，打印到控制台的同时还保存在本地沙盒路径中
 */
#define NSLog(fmt, ...) zw_logInSandboxAndConsole(LogDetail(fmt, ##__VA_ARGS__))

/**
 * 指定项目的AppID在AppStore中的链接
 * 也可以使用如下链接 https://itunes.apple.com/cn/app/idXXXXXX?mt=8
 */
#define AppURL_In_AppStore(AppID) PTString(@"itms-apps://itunes.apple.com/app/id%@", AppID)

/*
 * 跳转至AppStore评分页面
 */
#define AppEvaluationURL_In_AppStore(AppID) PTString(@"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%@", AppID)

/**
 * 获取运行App屏幕 宽度、高度（非设备屏幕，如iPhone版App运行在iPad上时）
 */
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)
#define IS_RETINA3 ([[UIScreen mainScreen] scale] >= 3.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))
#define AdaptWidth(x) ceilf(x * (SCREEN_WIDTH / 375.0))
#define AdaptHeight(x) ceilf(x * (SCREEN_HEIGHT / 812.0))
#define AdaptScreenMaxLength(x) ceilf(x * (SCREEN_MAX_LENGTH / 812.0))
#define AdaptScreenMinLength(x) ceilf(x * (SCREEN_MIN_LENGTH / 375.0))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
#define IS_IPHONE_X (IS_IPHONE && SCREEN_MAX_LENGTH == 812.0)
#define IS_IPHONE_XR (IS_IPHONE && SCREEN_MAX_LENGTH == 896.0 && IS_RETINA && !IS_RETINA3)
#define IS_IPHONE_XSMax (IS_IPHONE && SCREEN_MAX_LENGTH == 896.0 && IS_RETINA3)

/**
 *  获取指定字体的文字宽或者高（ios7.0及以上有效）
 *
 *  @param text    指定文字
 *  @param font    指定字体
 *  @param maxSize size区间
 *
 *  @return 实际区间
 */
#define MB_MULTILINE_TEXTSIZE(text, font, maxSize) [text length] > 0 ? [text \
boundingRectWithSize:maxSize options:(NSStringDrawingUsesLineFragmentOrigin) \
attributes:@{NSFontAttributeName:font} context:nil].size : CGSizeZero;


/**
 信号量。用于限制对于某一资源的同时访问。
 使用时请自行创建信号量实例.
 如：
 {
 @property(nonatomic,strong)dispatch_semaphore_t myLock;
 myLock = dispatch_semaphore_create(1);
 }
 
 @param lock 信号量实例
 @return
 */
#define LOCKSEMAPHORE(lock) dispatch_semaphore_wait(lock, DISPATCH_TIME_FOREVER);
#define UNLOCKSEMAPHORE(lock) dispatch_semaphore_signal(lock);

/**
 *  设置 view 圆角和边框
 */
#define LRViewBorderRadius(View, Radius, Width, Color)\
\
[View.layer setCornerRadius:(Radius)];\
[View.layer setMasksToBounds:YES];\
[View.layer setBorderWidth:(Width)];\
[View.layer setBorderColor:[Color CGColor]]

// Return nil when __INDEX__ is beyond the bounds of the array
#define NSArrayObjectMaybeNil(__ARRAY__, __INDEX__) ((__INDEX__ >= [__ARRAY__ count]) ? nil : [__ARRAY__ objectAtIndex:__INDEX__])

//将数组作为可变参数(va_list)
#define NSArrayToVariableArgumentsList(__ARRAYNAME__)\
NSArrayObjectMaybeNil(__ARRAYNAME__, 0),\
NSArrayObjectMaybeNil(__ARRAYNAME__, 1),\
NSArrayObjectMaybeNil(__ARRAYNAME__, 2),\
NSArrayObjectMaybeNil(__ARRAYNAME__, 3),\
NSArrayObjectMaybeNil(__ARRAYNAME__, 4),\
NSArrayObjectMaybeNil(__ARRAYNAME__, 5),\
NSArrayObjectMaybeNil(__ARRAYNAME__, 6),\
NSArrayObjectMaybeNil(__ARRAYNAME__, 7),\
NSArrayObjectMaybeNil(__ARRAYNAME__, 8),\
NSArrayObjectMaybeNil(__ARRAYNAME__, 9),\
nil

/**
 *  系统颜色
 *
 *  @param r
 *  @param g
 *  @param b
 *  @param a
 *
 *  @return 系统颜色
 */
#define RGBA(r, g, b, a)        [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:(a)]

/**
 *  系统颜色
 *
 *  @param r
 *  @param g
 *  @param b
 *
 *  @return
 */
#define RGB(r, g, b)            RGBA(r, g, b, 1)

/**
 *  颜色值转换（16进制->10进制）
 *  颜色转换(alpha可设置) -> UIColor
 *
 *  @param hexRGBValue    rgb值(0xffffff)
 *  @param alphaValue  alpha值(0.0~1.0)
 */
#define UIColorFromHexRGBA(hexRGBValue, alphaValue) [UIColor colorWithRed:((float)((hexRGBValue & 0xFF0000) >> 16))/255.0 green:((float)((hexRGBValue & 0xFF00) >> 8))/255.0 blue:((float)(hexRGBValue & 0xFF))/255.0 alpha:(alphaValue)]

/**
 *  颜色转换(alpha为1)    -> UIColor
 *
 *  @param hexRGBValue    rgb值(0xffffff)
 */
#define UIColorFromHexRGB(hexRGBValue)        UIColorFromHexRGBA(hexRGBValue, 1.0)

/**
 *  把各种其他类型的数据转换成字符串
 */
#define NSSTRING_CONVERT_FROM_LONGLONG(number)        [NSString stringWithFormat:@"%@", [NSNumber numberWithLongLong:(number)]]
#define NSSTRING_CONVERT_FROM_BOOL(number)            [NSString stringWithFormat:@"%@", [NSNumber numberWithBool:(number)]]
#define NSSTRING_CONVERT_FROM_INTEGER(number)         [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:(number)]]
#define NSSTRING_CONVERT_FROM_UINTEGER(number)        [NSString stringWithFormat:@"%@", [NSNumber numberWithUnsignedInteger:(number)]]
#define NSSTRING_CONVERT_FROM_INT(number)             [NSString stringWithFormat:@"%@", [NSNumber numberWithInt:(number)]]
#define NSSTRING_CONVERT_FROM_DOUBLE(number)          [NSString stringWithFormat:@"%@", [NSNumber numberWithDouble:(number)]]

#pragma mark - 项目自定义宏

/**
 *  黄金分割比例，较长段与比全长的比例
 */
#define MyMacro_GoldenCut_Scale 0.618f

/**
 *  图片分辨率与屏幕点的比例关系
 */
#define MyMacro_ImagePixelScaleToScreenPoint 3.0f

/**
 *  JPEG图片的储存压缩比（决定了图片占用的空间大小，区间0~1）
 */
#define MyMacro_JPEG_CompressionQuality 0.78f

/**
 *  iOS各种控件的默认高度
 */
#define MyMacro_StatusBarViewHeight    20.0f
#define MyMacro_NavigationBarViewHeight  44.0f
#define MyMacro_SearchBarViewHeight      40.0f
#define MyMacro_CellDefaultHeight  44.0f
#define MyMacro_ToolBarViewHeight  49.0f

#define MyMacro_PickerViewHeight       180.0f
#define MyMacro_SeperateLine_Height     1.0f
#define MyMacro_PopView_Controls_Width 270.0f

/**
 *  项目中使用的颜色
 */
#define MyMacro_Color_White                [UIColor whiteColor]
#define MyMacro_Color_Black                [UIColor blackColor]
#define MyMacro_Color_SystemBlue           RGB(0,122,255)
#define MyMacro_Color_SystemLine           RGB(224,224,224)
#define MyMacro_Color_FacebookBrandColor   RGB(66,103,178)//Facebook品牌颜色 https://developers.facebook.com/docs/facebook-login/userexperience

#endif
