//
//  ZWDirectoryWatcher.m
//  ZWProjectsCore
//
//  Created by ZhangWei on 2017/4/24.
//  Copyright © 2017年 ZhangWei. All rights reserved.
//

#import "ZWDirectoryWatcher.h"
#import "ZWProjectsCoreFunctions.h"

NSString* const ZWDirectoryWatcherFileChangedNotification = @"ZWDirectoryWatcherFileChangedNotification";

@interface ZWDirectoryWatcher ()

@property(nonatomic,strong)dispatch_queue_t customQueue;

//与dispatch_queue_t不同的是，dispatch_source_t是可以进行取消的，而且可以添加取消的block回调；
//dispatch_source_t用于监听系统的底层对象，比如文件描述符、Mach端口、信号量、文件目录变化等等，具体请见下表：
//    DISPATCH_SOURCE_TYPE_DATA_ADD	       数据增加
//    DISPATCH_SOURCE_TYPE_DATA_OR	       数据OR
//    DISPATCH_SOURCE_TYPE_MACH_SEND	   Mach端口发送
//    DISPATCH_SOURCE_TYPE_MACH_RECV	   Mach端口接收
//    DISPATCH_SOURCE_TYPE_MEMORYPRESSURE  内存情况
//    DISPATCH_SOURCE_TYPE_PROC	           进程事件
//    DISPATCH_SOURCE_TYPE_READ	           读数据
//    DISPATCH_SOURCE_TYPE_SIGNAL	       信号
//    DISPATCH_SOURCE_TYPE_TIMER	       定时器
//    DISPATCH_SOURCE_TYPE_VNODE	       文件系统变化
//    DISPATCH_SOURCE_TYPE_WRITE	       文件写入
//
//    方法
//    dispatch_source_create：创建dispatch_source_t，创建后会处于挂起状态进行事件接收，需要设置事件处理handler进行事件处理。
//    dispatch_source_set_event_handler：设置事件处理handler
//    dispatch_source_set_cancel_handler：事件取消handler，就是在dispatch_source_t释放前做些清理的事。
//    dispatch_source_cancel：关闭dispatch_source_t，设置的事件处理handler不会被执行，已经执行的事件handler不会取消。
@property(nonatomic,strong)dispatch_source_t customSource;

@end

@implementation ZWDirectoryWatcher

- (void)startObservingDirectoryPathFileChanged;
{
    // 创建 file descriptor (需要将NSString转换成C语言的字符串)
    // open() 函数会建立 file 与 file descriptor 之间的连接

    if (self.customSource)
    {
        [self stopObservingDirectoryPathFileChanged];
        self.customSource = nil;
    }
    
    int filedes = open([self.directoryPath cStringUsingEncoding:NSASCIIStringEncoding], O_EVTONLY);
    
    // 创建 dispatch queue, 当文件改变事件发生时会发送到该 queue
    self.customQueue = dispatch_queue_create([NSStringFromClass([self class]) UTF8String], 0);
    
    // 创建 GCD source. 将用于监听 file descriptor 来判断是否有文件写入操作
    self.customSource = dispatch_source_create(DISPATCH_SOURCE_TYPE_VNODE, filedes, DISPATCH_VNODE_WRITE, self.customQueue);
    
    // 当文件发生改变时会调用该 block
    __weak typeof(self) wSelf = self;
    dispatch_source_set_event_handler(self.customSource, ^ {
        // 在文件发生改变时发出通知
        // 在子线程发送通知, 这个通知触发的方法会在子线程当中执行
        [[NSNotificationCenter defaultCenter] postNotificationName:ZWDirectoryWatcherFileChangedNotification object:wSelf.directoryPath userInfo:nil];
    });
    
    // 当文件监听停止时会调用该 block
    dispatch_source_set_cancel_handler(self.customSource, ^ {
        // 关闭文件监听时, 关闭该 file descriptor
        close(filedes);
    });
    
    // 开始监听文件
    dispatch_resume(self.customSource);
}

- (void)stopObservingDirectoryPathFileChanged;
{
    // 停止监听文件，已经执行的事件不会取消。
    dispatch_cancel(self.customSource);
}
@end
