//
//  ZWDirectoryWatcher.h
//  ZWProjectsCore
//
//  Created by ZhangWei on 2017/4/24.
//  Copyright © 2017年 ZhangWei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

UIKIT_EXTERN NSString* const ZWDirectoryWatcherFileChangedNotification;//一旦指定文件夹路径内的数据发生变化将抛出此通知

@interface ZWDirectoryWatcher : NSObject

@property(nonatomic,copy)NSString* directoryPath;//被监听的文件夹路径（内部子文件夹的数据变动不在监听范围内）

- (void)startObservingDirectoryPathFileChanged;//开始监听

- (void)stopObservingDirectoryPathFileChanged;//停止监听

@end


/*
 *  该模块可以监听指定文件夹路径下的文件变动。
 *  在开启了通过iTunes进行App的Document路径文件共享，当添加了新文件或者删除了已存在的文件时，可以通过该模块及时的通知程式作出逻辑或者界面变化。
 */
