//
//  ZWCountryCodeManager.h
//  ZWProjectsCore
//
//  Created by ZhangWei on 15/10/21.
//  Copyright © 2015年 ZhangWei. All rights reserved.
//

#import <Foundation/Foundation.h>

#define ZWCountryCodeManagerInstance [ZWCountryCodeManager sharedZWCountryCodeManager]

@interface ZWCountryCodeManager : NSObject

+ (ZWCountryCodeManager*)sharedZWCountryCodeManager;

//获取当前设备地区的国际电话码，可以通过【设置】→【通用】→【语言和地区】→【地区】进行修改。
//如【中国】将返回86，如果是【香港（中国）】将返回852
+ (NSString*)phoneCodeForLocalCountryCode;

- (NSDictionary*)allCountryCode;
- (NSArray*)countryCodeWithPhoneCode:(NSString*)phoneCode;
- (NSString*)phoneCodeWithCountryCode:(NSString*)countryCode;

//国家电话码是否存在
- (BOOL)isExistPhoneCode:(NSString*)phoneCode;

//把手机号转换成标准的移动电话码格式 如86-13800138000
- (NSString*)standardFormatMobilePhoneWithPhoneNumber:(NSString*)phoneNumber;

@end
