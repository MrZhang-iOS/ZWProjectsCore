//
//  ZWCountryCodeManager.m
//  ZWProjectsCore
//
//  Created by ZhangWei on 15/10/21.
//  Copyright © 2015年 ZhangWei. All rights reserved.
//

#import "ZWCountryCodeManager.h"
#import "NSArray+ZhangWei.h"
#import "ZWProjectsCoreMacros.h"
#import "ZWProjectsCoreFunctions.h"

static ZWCountryCodeManager* g_pZWCountryCodeManager = nil;

@interface ZWCountryCodeManager ()

@property(nonatomic,strong)NSDictionary* countryCodeDict;

@end
@implementation ZWCountryCodeManager

+ (ZWCountryCodeManager*)sharedZWCountryCodeManager;
{
    static dispatch_once_t pred;
    dispatch_once(&pred, ^ {
        g_pZWCountryCodeManager = [[ZWCountryCodeManager alloc] init];
    });
    return g_pZWCountryCodeManager;
}

+ (NSString*)phoneCodeForLocalCountryCode;
{
    NSLocale *currentLocale = [NSLocale currentLocale];
    NSString *countryCode = [currentLocale objectForKey:NSLocaleCountryCode];
    return [ZWCountryCodeManagerInstance phoneCodeWithCountryCode:countryCode];
}

- (NSDictionary*)allCountryCode;
{
    return self.countryCodeDict;
}

- (NSArray*)countryCodeWithPhoneCode:(NSString*)phoneCode;
{
    if (self.countryCodeDict && phoneCode.length > 0)
    {
        return [self.countryCodeDict allKeysForObject:phoneCode];
    }
    return nil;
}

- (NSString*)phoneCodeWithCountryCode:(NSString*)countryCode;
{
    if (self.countryCodeDict && countryCode.length > 0)
    {
        return [self.countryCodeDict valueForKey:[countryCode uppercaseString]];
    }
    return nil;
}

- (BOOL)isExistPhoneCode:(NSString*)phoneCode;
{
    if (phoneCode.length > 0 && phoneCode.length <= 5)
    {
        if (self.countryCodeDict)
        {
            return [[self.countryCodeDict allValues] zw_containsString:phoneCode];
        }
        return NO;
    }
    else
    {
        return NO;
    }
}

- (NSString*)standardFormatMobilePhoneWithPhoneNumber:(NSString*)phoneNumber;
{
    NSString* standardFormatMobilePhone = nil;
    phoneNumber = [phoneNumber stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([phoneNumber hasPrefix:@"+"])
    {
        NSArray* separatedNumbers = [phoneNumber componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]];
        NSMutableArray* filteredSeparatedNumber = [NSMutableArray array];
        for (NSString* tmpSeparatedNumber in separatedNumbers)
        {
            if (tmpSeparatedNumber.length > 0)
            {
                [filteredSeparatedNumber addObject:tmpSeparatedNumber];
            }
        }
        if (filteredSeparatedNumber.count > 1)
        {
            NSArray* separatedNumberWithoutPhoneCode = [filteredSeparatedNumber zw_getSubArrayWithRange:NSMakeRange(1, filteredSeparatedNumber.count-1)];
            standardFormatMobilePhone = [NSString stringWithFormat:@"%@-%@", [filteredSeparatedNumber zw_getNSStringObjectAtIndex:0],[separatedNumberWithoutPhoneCode componentsJoinedByString:@""]];
        }
        else
        {
            standardFormatMobilePhone = [filteredSeparatedNumber componentsJoinedByString:@""];
            for (NSInteger nIndex = 0; nIndex < 5; ++nIndex)
            {
                if (standardFormatMobilePhone.length > nIndex && [self isExistPhoneCode:[standardFormatMobilePhone substringToIndex:nIndex+1]])
                {
                    standardFormatMobilePhone = [standardFormatMobilePhone stringByReplacingCharactersInRange:NSMakeRange(nIndex+1, 0) withString:@"-"];
                    break;
                }
            }
        }
    }
    else
    {
        NSString* defaultPhoneCode = [ZWCountryCodeManager phoneCodeForLocalCountryCode];
        NSString* filterPhoneNumber = [[phoneNumber componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
        if (defaultPhoneCode.length > 0 && filterPhoneNumber.length > 0)
        {
            standardFormatMobilePhone = [NSString stringWithFormat:@"%@-%@", defaultPhoneCode, filterPhoneNumber];
        }
        else
        {
            standardFormatMobilePhone = filterPhoneNumber;
        }
    }
    NSLog(@"电话号码【%@】转换成的标准格式号码【%@】", phoneNumber, standardFormatMobilePhone);
    return standardFormatMobilePhone;
}

#pragma mark - PrivateFunctions

- (id)init
{
    if (self = [super init])
    {
        //获取当前类的NSBundle，因为该模块可能会被编译到*.framework库中
        //可以把*.framework动态库想象为主程序中的一个文件夹。在没有framework的主程序中，资源是直接放在根目录下的。但是，引入了*.framework动态库之后，就像把资源移动了相应的子目录。这样路径就发生了变化，NSBundle这个类就是为了区分这种变化。
        //*.framework动态库内部的资源如果用mainBundle取，路径就错了，所以一般使用bundleForClass:方法定位指定类所在bundle，然后读取其下的资源文件
        //注：*.framework静态库在主程序中不可以当做一个文件夹，因为*.framework静态库不允许添加资源文件，这也是当把SVProgressHUD编译在*.framework静态库后运行时crash的原因
        self.countryCodeDict = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle bundleForClass:[self class]] pathForResource:@"ZWCountryCode.plist" ofType:nil]];
    }
    return self;
}

@end
