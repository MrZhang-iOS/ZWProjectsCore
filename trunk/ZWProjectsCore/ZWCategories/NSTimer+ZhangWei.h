//
//  NSTimer+ZhangWei.h
//  ZWProjectsCore
//
//  Created by ZhangWei on 15/09/10.
//  Copyright © 2015年 ZhangWei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSTimer (ZhangWei)

+(instancetype)zw_scheduledTimerWithTimeInterval:(NSTimeInterval)timeInterval block:(void (^)(void))arcBlock repeats:(BOOL)yesOrNo;
+(instancetype)zw_timerWithTimeInterval:(NSTimeInterval)timeInterval block:(void (^)(void))arcBlock repeats:(BOOL)yesOrNo;

@end


/*
 *
 *  NSTimer的使用需要主要循环引用导致的不释放问题，封装该类即解决此问题，同时简化代码，使用Block形式。
 *
 */
