//
//  NSDictionary+ZhangWei.m
//  ZWProjectsCore
//
//  Created by ZhangWei on 14-3-31.
//  Copyright (c) 2014年 ZhangWei. All rights reserved.
//

#import "NSDictionary+ZhangWei.h"
#import "NSArray+ZhangWei.h"

@implementation NSDictionary (ZhangWei)

- (BOOL)zw_containsKey:(NSString*)key
{
    NSArray* pAllKeys = [self allKeys];
    BOOL bContain = [pAllKeys zw_containsString:key];
    if (bContain)
    {
        id obj = [self objectForKey:key];
        if ([obj isKindOfClass:[NSNull class]])
        {
            bContain = NO;
        }
    }
    return bContain;
}

- (id)zw_getObjectForKey:(NSString*)key
{
    id obj = nil;
    if ([self zw_containsKey:key])
    {
        //obj不可能为【NSNull】对象,因为【containsKey】方法已经剔除了【NSNull】对象，ZhangWei 2017-12-01
        obj = [self objectForKey:key];
    }
    return obj;
}

- (NSString*)zw_getNSStringObjectForKey:(NSString *)key;
{
    id obj = [self zw_getObjectForKey:key];
    if (obj && ![obj isKindOfClass:[NSString class]])
    {
        obj = nil;
    }
    return obj;
}

- (NSNumber*)zw_getNSNumberObjectForKey:(NSString *)key;
{
    id obj = [self zw_getObjectForKey:key];
    if (obj && ![obj isKindOfClass:[NSNumber class]])
    {
        obj = nil;
    }
    return obj;
}

- (NSArray*)zw_getNSArrayObjectForKey:(NSString *)key;
{
    id obj = [self zw_getObjectForKey:key];
    if (obj && ![obj isKindOfClass:[NSArray class]])
    {
        obj = nil;
    }
    return obj;
}

- (NSDictionary*)zw_getNSDictionaryObjectForKey:(NSString *)key;
{
    id obj = [self zw_getObjectForKey:key];
    if (obj && ![obj isKindOfClass:[NSDictionary class]])
    {
        obj = nil;
    }
    return obj;
}

- (NSData*)zw_getNSDataObjectForKey:(NSString *)key;
{
    id obj = [self zw_getObjectForKey:key];
    if (obj && ![obj isKindOfClass:[NSData class]])
    {
        obj = nil;
    }
    return obj;
}

- (UIColor *)zw_getUIColorObjectForKey:(NSString *)key;
{
    id obj = [self zw_getObjectForKey:key];
    if (obj && ![obj isKindOfClass:[UIColor class]])
    {
        obj = nil;
    }
    return obj;
}

- (NSDictionary *)zw_replaceNullsWithBlanks {
    const NSMutableDictionary *replacedDict = self.mutableCopy;
    const NSString *blank = @"";
    for (NSString *key in self) {
        const id object = [self objectForKey:key];
        if ([object isEqual:[NSNull null]] || [object isKindOfClass:[NSNull class]]) {
            [replacedDict setObject:blank forKey:key];
        } else if ([object isKindOfClass:[NSDictionary class]]) {
            [replacedDict setObject:[(NSDictionary *)object zw_replaceNullsWithBlanks] forKey:key];
        } else if ([object isKindOfClass:[NSArray class]]) {
            [replacedDict setObject:[(NSArray *)object zw_replaceNullsWithBlanks] forKey:key];
        }
    }
    return replacedDict.copy;
}

- (void)zw_autoCreatePropertyCode {
    NSMutableString *codes = [NSMutableString string];
    [self enumerateKeysAndObjectsUsingBlock:^(id _Nonnull key, id _Nonnull value, BOOL *_Nonnull stop) {
        NSString *tmpCode;
        if ([value isKindOfClass:[NSString class]]) {
            tmpCode = [NSString stringWithFormat:@"@property (nonatomic, copy)   NSString *%@;", key];
        } else if ([value isKindOfClass:NSClassFromString(@"__NSCFBoolean")]) {
            tmpCode = [NSString stringWithFormat:@"@property (nonatomic, assign) BOOL %@;", key];
        } else if ([value isKindOfClass:[NSNumber class]]) {
            tmpCode = [NSString stringWithFormat:@"@property (nonatomic, strong) NSNumber *%@;", key];
        } else if ([value isKindOfClass:[NSArray class]]) {
            tmpCode = [NSString stringWithFormat:@"@property (nonatomic, strong) NSArray *%@;", key];
        } else if ([value isKindOfClass:[NSDictionary class]]) {
            tmpCode = [NSString stringWithFormat:@"@property (nonatomic, strong) NSDictionary *%@;", key];
        } else if ([value isKindOfClass:[NSNull class]]) {
            tmpCode = [NSString stringWithFormat:@"@property (nonatomic, strong) NSNull *%@;", key];
        } else {
            tmpCode = [NSString stringWithFormat:@"@property (nonatomic, strong) %@ *%@;", NSStringFromClass([value class]), key];
        }
        [codes appendFormat:@"\n%@\n", tmpCode];
    }];
    NSLog(@"%@", codes);
}

@end
