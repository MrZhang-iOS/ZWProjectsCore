//
//  UIImageView+ZhangWei.m
//  ZWProjectsCore
//
//  Created by ZhangWei on 15/8/12.
//  Copyright (c) 2015年 ZhangWei. All rights reserved.
//

#import "UIImageView+ZhangWei.h"

@implementation UIImageView (ZhangWei)

- (void)zw_setImageWithAnimation:(UIImage*)image
{
    BOOL bAnimation = YES;
    if (self.image)
    {
        bAnimation = NO;
    }
    [self setImage:image];
    
    if (bAnimation)
    {
        CATransition *transition = [CATransition animation];
        transition.duration = 0.16;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionFade;
        [self.layer addAnimation:transition forKey:nil];
    }
}

- (void)zw_setImageWithAlphaFade:(UIImage*)image
{
    [self setImage:image];
    if (self.image == nil && image)
    {
        self.alpha = 0 ;
        __weak typeof(self) wSelf = self;
        [UIView animateWithDuration:0.16 animations:^{
            wSelf.alpha = 1.0;
        }];
    }
}

@end
