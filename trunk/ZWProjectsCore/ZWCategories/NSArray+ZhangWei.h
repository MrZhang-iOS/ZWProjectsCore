//
//  NSArray+ZhangWei.h
//  ZWProjectsCore
//
//  Created by ZhangWei on 14-3-31.
//  Copyright (c) 2014年 ZhangWei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (ZhangWei)

/// 以下方法为获取数组中指定类的对象
- (id)zw_safeObjectAtIndex:(NSInteger)nIndex;
- (NSString *)zw_getNSStringObjectAtIndex:(NSInteger)nIndex;
- (NSNumber *)zw_getNSNumberObjectAtIndex:(NSInteger)nIndex;
- (NSArray *)zw_getNSArrayObjectAtIndex:(NSInteger)nIndex;
- (NSDictionary *)zw_getNSDictionaryObjectAtIndex:(NSInteger)nIndex;
- (NSData *)zw_getNSDataObjectAtIndex:(NSInteger)nIndex;

/// 数组中是否包含指定的字符串
- (BOOL)zw_containsString:(NSString *)anString;

/// 获得子数组，以免越界Crash
- (NSArray *)zw_getSubArrayWithRange:(NSRange)range;

/// 将数据数组逆排序
- (NSArray *)zw_reverseArray;

/// 将数组中包含的【Nul】l对象用【@""】替换掉（递归操作）。
- (NSArray *)zw_replaceNullsWithBlanks;

/// 根据列数，获得行数
- (NSInteger)zw_getRowWithColumn:(NSInteger)nColumn;

/// 将数组随机打乱排序获取一个新的数组
- (NSArray *)zw_getArrayWithRandomOrder;

/**
 *  将【字符串数组】按字母排序
 *  @param enSortType 0表示升序，非0表示降序
 *  @return 经过字母排序后的字符串数组
 */
- (NSArray <NSString *> *)zw_sortArrayByAphabetWithOrderType:(NSInteger)enSortType;

/**
 *  将【字符串数组】按数字大小排序，前提是字符串可以转化成数字
 *  @param enSortType 0表示升序，非0表示降序
 *  @return 经过数字排序后的字符串数组
 */
- (NSArray <NSString *> *)zw_sortArrayByNumbericWithOrderType:(NSInteger)enSortType;

@end
