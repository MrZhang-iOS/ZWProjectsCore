//
//  UIViewController+ZhangWei.m
//  ZWProjectsCore
//
//  Created by zhangwei on 2021/9/1.
//  Copyright © 2021 wei.zhang. All rights reserved.
//

#import "UIViewController+ZhangWei.h"
#import <objc/runtime.h>

static BOOL g_DebugLogInConsole = NO;

@implementation UIViewController (ZhangWei)

+ (void)setDebugLogInConsole:(BOOL)debugLogInConsole {
    if (g_DebugLogInConsole != debugLogInConsole) {
        g_DebugLogInConsole = debugLogInConsole;
    }
}

+ (BOOL)debugLogInConsole {
    return g_DebugLogInConsole;
}

#ifdef DEBUG

typedef NS_ENUM(NSInteger, ZWDebugType) {
    ZWDebugType_Unknown = 0,
    ZWDebugType_Dealloc,
    ZWDebugType_Init,
    ZWDebugType_WillAppear,
    ZWDebugType_DidAppear,
    ZWDebugType_WillDisappear,
    ZWDebugType_DidDisappear,
    ZWDebugTypeCount,
};

//分类的方式重构系统方法时不要直接覆盖系统方法，使用runtime置换掉系统方法，防止与相同分类模块冲突造成异常
+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        Class instanceClass = [self class];
        
#pragma mark - dealloc
        SEL originalInstanceSelector = NSSelectorFromString(@"dealloc");
        SEL swizzledInstanceSelector = @selector(zw_debug_dealloc);
        
        Method originalInstanceMethod = class_getInstanceMethod(instanceClass, originalInstanceSelector);
        Method swizzledInstanceMethod = class_getInstanceMethod(instanceClass, swizzledInstanceSelector);
        
        if (originalInstanceMethod && swizzledInstanceMethod)
        {
            //class_addMethod添加swizzledMethod的实现IMP到originalSelector中（只是添加，不会覆盖originalSelector的原有实现），
            //当且仅当originalSelector为当前类的父类中的函数（无论实例方法还是类方法）时才能添加成功。
            //进行类方法交换式，参数必须为元类
            //1.类本身（分类算作类本身）有实现需要被替换的方法（无论实例方法还是类方法）时，class_addMethod方法返回NO，
            //这种情况交换两个方法的实现就可以了。
            //2.类本身没有实现需要被替换的方法（无论实例方法还是类方法）时，其父类有实现方法（可继承使用），class_addMethod方法返回YES，
            //这种情况重置原方法的实现到新方法名的映射就可以了。
            //
            //也可以直接使用method_exchangeImplementations方法，绕开class_addMethod。
            BOOL isMethodAdd = class_addMethod(instanceClass, originalInstanceSelector, method_getImplementation(swizzledInstanceMethod), method_getTypeEncoding(swizzledInstanceMethod));
            if (isMethodAdd)
            {
                class_replaceMethod(instanceClass, swizzledInstanceSelector, method_getImplementation(originalInstanceMethod), method_getTypeEncoding(originalInstanceMethod));
            }
            else
            {
                method_exchangeImplementations(originalInstanceMethod, swizzledInstanceMethod);
            }
        }
        else
        {
            NSLog(@"dealloc 交换方法失败");
        }
        
#pragma mark - initWithNibName:bundle: 非StoryBoard创建UIViewController都会调用这个方法。
        SEL originalInstanceSelector0 = @selector(initWithNibName:bundle:);
        SEL swizzledInstanceSelector0 = @selector(zw_debug_initWithNibName:bundle:);
        
        Method originalInstanceMethod0 = class_getInstanceMethod(instanceClass, originalInstanceSelector0);
        Method swizzledInstanceMethod0 = class_getInstanceMethod(instanceClass, swizzledInstanceSelector0);
        
        if (originalInstanceMethod0 && swizzledInstanceMethod0)
        {
            //class_addMethod添加swizzledMethod的实现IMP到originalSelector中（只是添加，不会覆盖originalSelector的原有实现），
            //当且仅当originalSelector为当前类的父类中的函数（无论实例方法还是类方法）时才能添加成功。
            //进行类方法交换式，参数必须为元类
            //1.类本身（分类算作类本身）有实现需要被替换的方法（无论实例方法还是类方法）时，class_addMethod方法返回NO，
            //这种情况交换两个方法的实现就可以了。
            //2.类本身没有实现需要被替换的方法（无论实例方法还是类方法）时，其父类有实现方法（可继承使用），class_addMethod方法返回YES，
            //这种情况重置原方法的实现到新方法名的映射就可以了。
            //
            //也可以直接使用method_exchangeImplementations方法，绕开class_addMethod。
            BOOL isMethodAdd = class_addMethod(instanceClass, originalInstanceSelector0, method_getImplementation(swizzledInstanceMethod0), method_getTypeEncoding(swizzledInstanceMethod0));
            if (isMethodAdd)
            {
                class_replaceMethod(instanceClass, swizzledInstanceSelector0, method_getImplementation(originalInstanceMethod0), method_getTypeEncoding(originalInstanceMethod0));
            }
            else
            {
                method_exchangeImplementations(originalInstanceMethod0, swizzledInstanceMethod0);
            }
        }
        else
        {
            NSLog(@"initWithNibName:bundle: 交换方法失败");
        }
        
#pragma mark - initWithCoder: 使用StoryBoard进行视图管理，自动初始化或在segue被触发时自动初始化，因此方法initWithNibName:bundle不会被调用，但是initWithCoder会被调用。
        SEL originalInstanceSelector1 = @selector(initWithCoder:);
        SEL swizzledInstanceSelector1 = @selector(zw_debug_initWithCoder:);
        
        Method originalInstanceMethod1 = class_getInstanceMethod(instanceClass, originalInstanceSelector1);
        Method swizzledInstanceMethod1 = class_getInstanceMethod(instanceClass, swizzledInstanceSelector1);
        
        if (originalInstanceMethod1 && swizzledInstanceMethod1)
        {
            //class_addMethod添加swizzledMethod的实现IMP到originalSelector中（只是添加，不会覆盖originalSelector的原有实现），
            //当且仅当originalSelector为当前类的父类中的函数（无论实例方法还是类方法）时才能添加成功。
            //进行类方法交换式，参数必须为元类
            //1.类本身（分类算作类本身）有实现需要被替换的方法（无论实例方法还是类方法）时，class_addMethod方法返回NO，
            //这种情况交换两个方法的实现就可以了。
            //2.类本身没有实现需要被替换的方法（无论实例方法还是类方法）时，其父类有实现方法（可继承使用），class_addMethod方法返回YES，
            //这种情况重置原方法的实现到新方法名的映射就可以了。
            //
            //也可以直接使用method_exchangeImplementations方法，绕开class_addMethod。
            BOOL isMethodAdd = class_addMethod(instanceClass, originalInstanceSelector1, method_getImplementation(swizzledInstanceMethod1), method_getTypeEncoding(swizzledInstanceMethod1));
            if (isMethodAdd)
            {
                class_replaceMethod(instanceClass, swizzledInstanceSelector1, method_getImplementation(originalInstanceMethod1), method_getTypeEncoding(originalInstanceMethod1));
            }
            else
            {
                method_exchangeImplementations(originalInstanceMethod1, swizzledInstanceMethod1);
            }
        }
        else
        {
            NSLog(@"initWithCoder: 交换方法失败");
        }
   
#pragma mark - viewWillAppear
        SEL originalInstanceSelector2 = @selector(viewWillAppear:);
        SEL swizzledInstanceSelector2 = @selector(zw_debug_viewWillAppear:);
        
        Method originalInstanceMethod2 = class_getInstanceMethod(instanceClass, originalInstanceSelector2);
        Method swizzledInstanceMethod2 = class_getInstanceMethod(instanceClass, swizzledInstanceSelector2);
        
        if (originalInstanceMethod2 && swizzledInstanceMethod2)
        {
            //class_addMethod添加swizzledMethod的实现IMP到originalSelector中（只是添加，不会覆盖originalSelector的原有实现），
            //当且仅当originalSelector为当前类的父类中的函数（无论实例方法还是类方法）时才能添加成功。
            //进行类方法交换式，参数必须为元类
            //1.类本身（分类算作类本身）有实现需要被替换的方法（无论实例方法还是类方法）时，class_addMethod方法返回NO，
            //这种情况交换两个方法的实现就可以了。
            //2.类本身没有实现需要被替换的方法（无论实例方法还是类方法）时，其父类有实现方法（可继承使用），class_addMethod方法返回YES，
            //这种情况重置原方法的实现到新方法名的映射就可以了。
            //
            //也可以直接使用method_exchangeImplementations方法，绕开class_addMethod。
            BOOL isMethodAdd = class_addMethod(instanceClass, originalInstanceSelector2, method_getImplementation(swizzledInstanceMethod2), method_getTypeEncoding(swizzledInstanceMethod2));
            if (isMethodAdd)
            {
                class_replaceMethod(instanceClass, swizzledInstanceSelector2, method_getImplementation(originalInstanceMethod2), method_getTypeEncoding(originalInstanceMethod2));
            }
            else
            {
                method_exchangeImplementations(originalInstanceMethod2, swizzledInstanceMethod2);
            }
        }
        else
        {
            NSLog(@"viewWillAppear 交换方法失败");
        }
        
#pragma mark - viewDidAppear
        SEL originalInstanceSelector3 = @selector(viewDidAppear:);
        SEL swizzledInstanceSelector3 = @selector(zw_debug_viewDidAppear:);
        
        Method originalInstanceMethod3 = class_getInstanceMethod(instanceClass, originalInstanceSelector3);
        Method swizzledInstanceMethod3 = class_getInstanceMethod(instanceClass, swizzledInstanceSelector3);
        
        if (originalInstanceMethod3 && swizzledInstanceMethod3)
        {
            //class_addMethod添加swizzledMethod的实现IMP到originalSelector中（只是添加，不会覆盖originalSelector的原有实现），
            //当且仅当originalSelector为当前类的父类中的函数（无论实例方法还是类方法）时才能添加成功。
            //进行类方法交换式，参数必须为元类
            //1.类本身（分类算作类本身）有实现需要被替换的方法（无论实例方法还是类方法）时，class_addMethod方法返回NO，
            //这种情况交换两个方法的实现就可以了。
            //2.类本身没有实现需要被替换的方法（无论实例方法还是类方法）时，其父类有实现方法（可继承使用），class_addMethod方法返回YES，
            //这种情况重置原方法的实现到新方法名的映射就可以了。
            //
            //也可以直接使用method_exchangeImplementations方法，绕开class_addMethod。
            BOOL isMethodAdd = class_addMethod(instanceClass, originalInstanceSelector3, method_getImplementation(swizzledInstanceMethod3), method_getTypeEncoding(swizzledInstanceMethod3));
            if (isMethodAdd)
            {
                class_replaceMethod(instanceClass, swizzledInstanceSelector3, method_getImplementation(originalInstanceMethod3), method_getTypeEncoding(originalInstanceMethod3));
            }
            else
            {
                method_exchangeImplementations(originalInstanceMethod3, swizzledInstanceMethod3);
            }
        }
        else
        {
            NSLog(@"viewDidAppear 交换方法失败");
        }
        
#pragma mark - viewWillDisappear
        SEL originalInstanceSelector4 = @selector(viewWillDisappear:);
        SEL swizzledInstanceSelector4 = @selector(zw_debug_viewWillDisappear:);
        
        Method originalInstanceMethod4 = class_getInstanceMethod(instanceClass, originalInstanceSelector4);
        Method swizzledInstanceMethod4 = class_getInstanceMethod(instanceClass, swizzledInstanceSelector4);
        
        if (originalInstanceMethod4 && swizzledInstanceMethod4)
        {
            //class_addMethod添加swizzledMethod的实现IMP到originalSelector中（只是添加，不会覆盖originalSelector的原有实现），
            //当且仅当originalSelector为当前类的父类中的函数（无论实例方法还是类方法）时才能添加成功。
            //进行类方法交换式，参数必须为元类
            //1.类本身（分类算作类本身）有实现需要被替换的方法（无论实例方法还是类方法）时，class_addMethod方法返回NO，
            //这种情况交换两个方法的实现就可以了。
            //2.类本身没有实现需要被替换的方法（无论实例方法还是类方法）时，其父类有实现方法（可继承使用），class_addMethod方法返回YES，
            //这种情况重置原方法的实现到新方法名的映射就可以了。
            //
            //也可以直接使用method_exchangeImplementations方法，绕开class_addMethod。
            BOOL isMethodAdd = class_addMethod(instanceClass, originalInstanceSelector4, method_getImplementation(swizzledInstanceMethod4), method_getTypeEncoding(swizzledInstanceMethod4));
            if (isMethodAdd)
            {
                class_replaceMethod(instanceClass, swizzledInstanceSelector4, method_getImplementation(originalInstanceMethod4), method_getTypeEncoding(originalInstanceMethod4));
            }
            else
            {
                method_exchangeImplementations(originalInstanceMethod4, swizzledInstanceMethod4);
            }
        }
        else
        {
            NSLog(@"viewWillDisappear 交换方法失败");
        }
        
#pragma mark - viewDidDisappear
        SEL originalInstanceSelector5 = @selector(viewDidDisappear:);
        SEL swizzledInstanceSelector5 = @selector(zw_debug_viewDidDisappear:);
        
        Method originalInstanceMethod5 = class_getInstanceMethod(instanceClass, originalInstanceSelector5);
        Method swizzledInstanceMethod5 = class_getInstanceMethod(instanceClass, swizzledInstanceSelector5);
        
        if (originalInstanceMethod5 && swizzledInstanceMethod5)
        {
            //class_addMethod添加swizzledMethod的实现IMP到originalSelector中（只是添加，不会覆盖originalSelector的原有实现），
            //当且仅当originalSelector为当前类的父类中的函数（无论实例方法还是类方法）时才能添加成功。
            //进行类方法交换式，参数必须为元类
            //1.类本身（分类算作类本身）有实现需要被替换的方法（无论实例方法还是类方法）时，class_addMethod方法返回NO，
            //这种情况交换两个方法的实现就可以了。
            //2.类本身没有实现需要被替换的方法（无论实例方法还是类方法）时，其父类有实现方法（可继承使用），class_addMethod方法返回YES，
            //这种情况重置原方法的实现到新方法名的映射就可以了。
            //
            //也可以直接使用method_exchangeImplementations方法，绕开class_addMethod。
            BOOL isMethodAdd = class_addMethod(instanceClass, originalInstanceSelector5, method_getImplementation(swizzledInstanceMethod5), method_getTypeEncoding(swizzledInstanceMethod5));
            if (isMethodAdd)
            {
                class_replaceMethod(instanceClass, swizzledInstanceSelector5, method_getImplementation(originalInstanceMethod5), method_getTypeEncoding(originalInstanceMethod5));
            }
            else
            {
                method_exchangeImplementations(originalInstanceMethod5, swizzledInstanceMethod5);
            }
        }
        else
        {
            NSLog(@"viewDidDisappear 交换方法失败");
        }
    });
}

- (void)zw_debug_dealloc;
{
    [self debugWithString:@"[❌] Did dealloc" debugTag:ZWDebugType_Dealloc];
    [self zw_debug_dealloc];
}

- (instancetype)zw_debug_initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    id obj = [self zw_debug_initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (obj) {
        [self debugWithString:@"[✅] Did initWithNibName:bundle:" debugTag:ZWDebugType_Init];
    }
    return obj;
}

- (instancetype)zw_debug_initWithCoder:(NSCoder *)coder {
    id obj = [self zw_debug_initWithCoder:coder];
    if (obj) {
        [self debugWithString:@"[✅] Did initWithCoder:" debugTag:ZWDebugType_Init];
    }
    return obj;
}

- (void)zw_debug_viewWillAppear:(BOOL)animated {
    [self zw_debug_viewWillAppear:animated];
    [self debugWithString:@"➡️ Will appear" debugTag:ZWDebugType_WillAppear];
}

- (void)zw_debug_viewDidAppear:(BOOL)animated {
    [self zw_debug_viewDidAppear:animated];
    [self debugWithString:@"➡️ Did appear" debugTag:ZWDebugType_DidAppear];
}

- (void)zw_debug_viewWillDisappear:(BOOL)animated {
    [self zw_debug_viewWillDisappear:animated];
    [self debugWithString:@"⬅️ Will disappear" debugTag:ZWDebugType_WillDisappear];
}

- (void)zw_debug_viewDidDisappear:(BOOL)animated {
    [self zw_debug_viewDidDisappear:animated];
    [self debugWithString:@"⬅️ Did disappear" debugTag:ZWDebugType_DidDisappear];
}

#pragma mark - PrivateFunctions
- (void)debugWithString:(NSString *)string debugTag:(ZWDebugType)type;
{
    NSDateFormatter *outputFormatter  = [[NSDateFormatter alloc] init] ;
    outputFormatter.dateFormat        = @"HH:mm:ss.SSS";
    
    NSString        *classString = [NSString stringWithFormat:@"%@ %@ [Class:%@ Pointer:%p]", string, [outputFormatter stringFromDate:[NSDate date]], [self class], self];
    NSMutableString *flagString  = [NSMutableString string];
    
    for (int i = 0; i < classString.length; i++)
    {
        switch (type)
        {
            case ZWDebugType_Init:
            {
                [flagString appendString:@"+"];
            }
                break;
            case ZWDebugType_WillAppear:
            {
                [flagString appendString:@"/"];
            }
                break;
            case ZWDebugType_DidAppear:
            {
                [flagString appendString:@">"];
            }
                break;
            case ZWDebugType_WillDisappear:
            {
                [flagString appendString:@"\\"];
            }
                break;
            case ZWDebugType_DidDisappear:
            {
                [flagString appendString:@"<"];
            }
                break;
            case ZWDebugType_Dealloc:
            {
                [flagString appendString:@"-"];
            }
                break;
            default:
                break;
        }
    }
    
    if ([[self class] debugLogInConsole]) {
        NSLog(@"\n%@\n%@\n%@\n", flagString, classString, flagString);
    }    
}

#endif

@end
