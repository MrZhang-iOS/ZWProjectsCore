//
//  NSNumber+ZhangWei.h
//  ZWProjectsCore
//
//  Created by ZhangWei on 2020/12/4.
//  Copyright © 2020 leigod. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (ZhangWei)

- (BOOL)isEqualToString:(id)aString;

- (NSUInteger)length;
 
@end
