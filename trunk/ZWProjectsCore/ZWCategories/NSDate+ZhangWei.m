//
//  NSDate+ZhangWei.m
//  ZWProjectsCore
//
//  Created by ZhangWei on 2018/1/8.
//  Copyright © 2018年 ZhangWei. All rights reserved.
//

#import "NSDate+ZhangWei.h"

@implementation NSDate (ZhangWei)

+ (BOOL)zw_isInSameDayWithDate1:(NSDate*)date1 date2:(NSDate*)date2;
{
    return [NSDate zw_isInSameDayWithDate1:date1 date2:date2 timeZone:nil];
}

+ (BOOL)zw_isInSameDayWithDate1:(NSDate*)date1 date2:(NSDate*)date2 timeZone:(NSTimeZone*)timeZone;
{
    if (fabs([date1 timeIntervalSinceDate:date2]) >= 24*60*60)//如果两个时间的间隔如果超过24小时，则一定不在同一天内
    {
        return NO;
    }
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    if (timeZone)
    {
        calendar.timeZone = timeZone;
    }
    else
    {
        calendar.timeZone = [NSTimeZone systemTimeZone];
    }
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
    
    //NSDateComponents会以系统本地时区转换年月日信息
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:date1];
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:date2];
    
    return ([comp1 day]   == [comp2 day] &&
            [comp1 month] == [comp2 month] &&
            [comp1 year]  == [comp2 year]);
}

+ (BOOL)zw_isInSameDayAtChinaWithDate1:(NSDate*)date1 date2:(NSDate*)date2;
{
    return [NSDate zw_isInSameDayWithDate1:date1 date2:date2 timeZone:[NSTimeZone timeZoneForSecondsFromGMT:8*60*60]];
}

@end
