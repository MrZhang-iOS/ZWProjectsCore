//
//  UIFont+ZhangWei.h
//  ZWProjectsCore
//
//  Created by ZhangWei on 15-1-8.
//  Copyright © 2015年 ZhangWei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (ZhangWei)

+ (UIFont *)zw_getFontForHeight:(CGFloat)height
                       fontName:(NSString *)targetFontName
                  lineBreakMode:(NSLineBreakMode)enMode;

@end
