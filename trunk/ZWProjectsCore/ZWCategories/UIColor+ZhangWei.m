//
//  UIColor+ZhangWei.m
//  ZWProjectsCore
//
//  Created by ZhangWei on 15/10/30.
//  Copyright © 2015年 ZhangWei. All rights reserved.
//

#import "UIColor+ZhangWei.h"
#import "ZWProjectsCoreMacros.h"
#import "ZWProjectsCoreFunctions.h"

@implementation UIColor (ZhangWei)

+ (int)zw_convertToARGBInt:(UIColor *)color;
{
    CGFloat red, green, blue, alpha;
    
    if (![color getRed:&red green:&green blue:&blue alpha:&alpha]) {
        
        CGFloat white;
        if ([color getWhite:&white alpha:&alpha])
        {
            red = green = blue = white;
        }
        else
        {
            NSLog(@"Uh oh, not RGB or greyscale");
        }
    }
    
    int r = red * 255;
    int g = green * 255;
    int b = blue * 255;
    int a = alpha * 255;
    int ARGBInt = ((r << 16) & 0x00FF0000) | ((g << 8) & 0x0000FF00) | (b & 0x000000FF) | ((a << 24) & 0xFF000000);
    return ARGBInt;
}

+ (UIColor *)zw_convertFromARGBInt:(int)ARGBInt;
{
    return [UIColor colorWithRed:((ARGBInt>>16)&0xFF)/255.0 green:((ARGBInt>>8)&0xFF)/255.0 blue:((ARGBInt)&0xFF)/255.0  alpha:((ARGBInt>>24)&0xFF)/255.0];
}

+ (UIColor *)zw_randomColor;
{
    // 取0~1之间的随机浮点数
    // drand48();
    return [UIColor colorWithRed:arc4random_uniform(256)/255.0 green:arc4random_uniform(256)/255.0 blue:arc4random_uniform(256)/255.0 alpha:1];
}

+ (UIColor *)zw_colorWithCSSorHexString:(NSString *)colorStr;
{
    if (colorStr && [colorStr isKindOfClass:[NSString class]] && colorStr.length > 0)
    {
        NSString * cString = [[colorStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
        if ([cString length] < 6 || [cString length] > 10 ) return [UIColor blackColor];
        
        if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
        if ([cString hasPrefix:@"#"]) cString = [cString substringFromIndex:1];
        
        if ([cString length] != 6 && [cString length] != 8) return [UIColor blackColor];
        
        NSRange range;
        range.length = 2;
        
        range.location = 0;
        NSString * rString = [cString substringWithRange:range];
        
        range.location = 2;
        NSString * gString = [cString substringWithRange:range];
        
        range.location = 4;
        NSString * bString = [cString substringWithRange:range];
        
        // Scan values
        unsigned int r, g, b;
        [[NSScanner scannerWithString:rString] scanHexInt:&r];
        [[NSScanner scannerWithString:gString] scanHexInt:&g];
        [[NSScanner scannerWithString:bString] scanHexInt:&b];
        
        unsigned int a = 255;
        if (8 == [cString length])
        {
            range.location = 6;
            NSString * aString = [cString substringWithRange:range];
            [[NSScanner scannerWithString:aString] scanHexInt:&a];
        }
        
        return [UIColor colorWithRed:((float)r / 255.0f)
                               green:((float)g / 255.0f)
                                blue:((float)b / 255.0f)
                               alpha:((float)a / 255.0f)];
    }
    return [UIColor blackColor];
}

+ (UIColor *)zw_colorWithCSSorHexString:(NSString *)colorStr alpha:(CGFloat)alpha {
    if (colorStr && [colorStr isKindOfClass:[NSString class]] && colorStr.length > 0) {
        NSString * cString = [[colorStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
        if ([cString length] < 6 || [cString length] > 10 ) return [UIColor blackColor];
        
        if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
        if ([cString hasPrefix:@"#"]) cString = [cString substringFromIndex:1];
        
        CGFloat alphaValue = alpha;
        if (alphaValue < 0) {
            alphaValue = 0;
        }
        if (alphaValue > 1) {
            alphaValue = 1;
        }
        if (6 == [cString length]) {
            return [UIColor zw_colorWithCSSorHexString:[NSString stringWithFormat:@"%@%02X", cString, (int)(alpha*255)]];
        } else if (8 == [cString length]) {
            return [UIColor zw_colorWithCSSorHexString:[NSString stringWithFormat:@"%@%02X", [cString substringToIndex:5], (int)(alpha*255)]];
        }
        
        return [UIColor blackColor];
    }
    return [UIColor blackColor];
}

@end
