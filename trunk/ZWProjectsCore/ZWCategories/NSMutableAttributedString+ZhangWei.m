//
//  NSMutableAttributedString+ZhangWei.m
//  ZWProjectsCore
//
//  Created by ZhangWei on 22/01/2018.
//  Copyright © 2018 ZhangWei. All rights reserved.
//

#import "NSMutableAttributedString+ZhangWei.h"

@interface NSString(MASAttributes)
-(NSRange)rangeOfStringNoCase:(NSString*)s;
@end

@implementation NSString(MASAttributes)
-(NSRange)rangeOfStringNoCase:(NSString*)s
{
    return  [self rangeOfString:s options:NSCaseInsensitiveSearch];
}
@end

@implementation NSMutableAttributedString (ZhangWei)

+ (NSMutableAttributedString*)zw_createWithString:(NSString*)string
                                             font:(UIFont*)font
                                            color:(UIColor*)color;
{
    if (string && [string isKindOfClass:[NSString class]])
    {
        NSMutableAttributedString* attributedString = [[NSMutableAttributedString alloc] initWithString:string];
        if (font && [font isKindOfClass:[UIFont class]])
        {
            [attributedString addAttribute:NSFontAttributeName
                                     value:font
                                     range:NSMakeRange(0, string.length)];
        }
        if (color && [color isKindOfClass:[UIColor class]])
        {
            [attributedString addAttribute:NSForegroundColorAttributeName
                                     value:color
                                     range:NSMakeRange(0, string.length)];
        }
        return attributedString;
    }
    return nil;
}

- (void)zw_addFont:(UIFont *)font substring:(NSString *)substring;
{
    NSRange range = [self.string rangeOfString:substring];
    if (range.location == NSNotFound)
    {
        range = [[self.string lowercaseString] rangeOfString:[substring lowercaseString]];
    }
    if (range.location != NSNotFound && font != nil)
    {
        [self addAttribute:NSFontAttributeName
                     value:font
                     range:range];
    }
}

- (void)zw_addColor:(UIColor *)color substring:(NSString *)substring;
{
    NSRange range = [self.string rangeOfString:substring];
    if (range.location == NSNotFound)
    {
        range = [[self.string lowercaseString] rangeOfString:[substring lowercaseString]];
    }
    if (range.location != NSNotFound && color != nil) {
        [self addAttribute:NSForegroundColorAttributeName
                     value:color
                     range:range];
    }
}

- (void)zw_addBackgroundColor:(UIColor *)color substring:(NSString *)substring;
{
    NSRange range = [self.string rangeOfString:substring];
    if (range.location == NSNotFound)
    {
        range = [[self.string lowercaseString] rangeOfString:[substring lowercaseString]];
    }
    if (range.location != NSNotFound && color != nil) {
        [self addAttribute:NSBackgroundColorAttributeName
                     value:color
                     range:range];
    }
}

- (void)zw_addCharacterSpacing:(CGFloat)spacing substring:(NSString*)substring;
{
    NSRange range = [self.string rangeOfString:substring];
    if (range.location == NSNotFound)
    {
        range = [[self.string lowercaseString] rangeOfString:[substring lowercaseString]];
    }
    if (range.location != NSNotFound && spacing >= 0) {
        
        [self addAttribute:NSKernAttributeName
                     value:@(spacing)
                     range:range];
    }
}

- (void)zw_addParagraphStyle:(void(^)(NSMutableParagraphStyle *paragraphStyle))block substring:(NSString*)substring;
{
    NSRange range = [self.string rangeOfString:substring];
    if (range.location == NSNotFound)
    {
        range = [[self.string lowercaseString] rangeOfString:[substring lowercaseString]];
    }
    if (range.location != NSNotFound && block)
    {
        NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        
        block(paragraphStyle);
        
        [self addAttribute:NSParagraphStyleAttributeName
                     value:paragraphStyle
                     range:range];
    }

}

@end
