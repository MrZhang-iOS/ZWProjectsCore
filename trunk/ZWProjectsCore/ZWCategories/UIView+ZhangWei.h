//
//  UIView+ZhangWei.h
//  ZWProjectsCore
//
//  Created by ZhangWei on 2013/05/15.
//  Copyright (c) 2013年 ZhangWei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (ZhangWei)

@property (nonatomic) CGFloat top;
@property (nonatomic) CGFloat bottom;
@property (nonatomic) CGFloat right;
@property (nonatomic) CGFloat left;

@property (nonatomic) CGFloat width;
@property (nonatomic) CGFloat height;

/**
 *  获取当前视图所属的控制器
 */
- (UIViewController *)zw_getViewController;

/**
 *  移除当前视图上的所有子视图
 */
- (void)zw_removeAllSubviews;

/**
 *  将当前视图做成图片
 */
- (UIImage*)zw_makeImage;

/**
 * 视图上是由存在正在滚动的控件
 */
- (BOOL)zw_anySubViewScrolling;

/**
 * 判断一个view是否为另一个view的子视图
 */
- (BOOL)zw_isSubViewOfView:(UIView*)view;

/**
 * 仿苹果抖动动画
 */
- (void)zw_startShakeAnimationWithRadians:(NSInteger)degrees;
- (void)zw_stopShakeAnimation;

/**
 * 清除所有布局约束
 */
- (void)zw_uninstallAllConstraints;

/**
 * 使当前视图呈圆形，支持添加边框和边框颜色。
 * ⚠️ 调用此方法的视图不可以包含子视图，否则可能导致Crash。
 * ⚠️ 调用此方法的视图须保证已布局完成且Frame宽高值不为0。
 */
- (void)zw_makeLayerCircleWithBorderWidth:(CGFloat)borderWidth
                              borderColor:(UIColor *)borderColor;

/**
 * 使当前视图呈圆角形状，支持添加边框和边框颜色、指定圆角、圆弧值。
 * ⚠️ 调用此方法的视图不可以包含子视图，否则可能导致Crash。
 * ⚠️ 调用此方法的视图须保证已布局完成且Frame宽高值不为0。
 */
- (void)zw_makeLayerRoundWithCornerRadius:(CGFloat)cornerRadius
                               rectCorner:(UIRectCorner)rectCorner
                              borderWidth:(CGFloat)borderWidth
                              borderColor:(UIColor *)borderColor;
@end
