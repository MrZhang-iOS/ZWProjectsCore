//
//  UIColor+ZhangWei.h
//  ZWProjectsCore
//
//  Created by ZhangWei on 15/10/30.
//  Copyright © 2015年 ZhangWei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (ZhangWei)

/**
 *  把iOS的颜色类实例转换成ARGB格式的Int值。
 *  ⚠️ int类型不可以转换成NSInteger。
 */
+ (int)zw_convertToARGBInt:(UIColor *)color;

/**
 *  把ARGB格式的int值转换成iOS的颜色类实例。
 *  ⚠️ int类型不可以转换成NSInteger。
 */
+ (UIColor *)zw_convertFromARGBInt:(int)ARGBInt;

/**
 *  随机生成一个颜色。
 */
+ (UIColor *)zw_randomColor;

/**
 * 通过十六进制颜色值字符串转成UIColor值。如果字符串非法会抛出黑色。
 * 支持六位RGB、八位RGBA、ox或者#开头的六位八位字符串，不区分大小写，如FD9100、FD9100FF、0xFD9100、#FD9100
 */
+ (UIColor *)zw_colorWithCSSorHexString:(NSString *)colorStr;

/**
 * 通过十六进制颜色值字符串转成UIColor值。透明通道的值由alpha决定。
 * 支持六位RGB、八位RGBA、ox或者#开头的六位八位字符串，不区分大小写，如FD9100、FD9100FF、0xFD9100、#FD9100
 */

/// 通过十六进制RGBA颜色值字符串转成UIColor值。透明通道的值由alpha决定。
/// @param colorStr 十六进制RGBA颜色值字符串。
/// @param alpha 透明通道。取值范围[0~1]。
+ (UIColor *)zw_colorWithCSSorHexString:(NSString *)colorStr alpha:(CGFloat)alpha;

@end
