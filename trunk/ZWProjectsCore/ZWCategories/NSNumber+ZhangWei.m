//
//  NSNumber+ZhangWei.m
//  ZWProjectsCore
//
//  Created by ZhangWei on 2020/12/4.
//  Copyright © 2020 leigod. All rights reserved.
//

#import "NSNumber+ZhangWei.h"
#import "NSString+ZhangWei.h"

@implementation NSNumber (ZhangWei)

- (BOOL)isEqualToString:(id)aString;
{
#ifdef DEBUG
    NSAssert(YES, @"⚠️请确认数据类型是否NSString，为了防止AppCrash故而定义了此方法！！！");
#endif
    if ([aString isKindOfClass:[NSString class]])
    {
        NSString *targetObj = (NSString *)aString;
        
        NSNumber *convertNumber = [targetObj zw_convertToNumber];
        if (convertNumber)
        {
            return [self isEqualToNumber:convertNumber];
        }
        else
        {
            return NO;
        }
    }
    else if ([aString isKindOfClass:[NSNumber class]])
    {
        NSNumber *targetObj = (NSNumber *)aString;
        return [self isEqualToNumber:targetObj];
    }
    else
    {
        return NO;
    }
}

- (NSUInteger)length;
{
#ifdef DEBUG
    NSAssert(YES, @"⚠️请确认数据类型是否NSString，为了防止AppCrash故而定义了此方法！！！");
#endif
    NSString *targetObj = [self stringValue];
    return targetObj.length;
}

@end
