//
//  NSString+ZhangWei.h
//  ZWProjectsCore
//
//  Created by ZhangWei on 16/4/22.
//  Copyright © 2016年 ZhangWei. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, StringSourceType)
{
    StringSourceType_Normal = 0,//默认来源字符串
    StringSourceType_FamilyName = 1,//字符来源于姓氏
    StringSourceType_GeivenName = 2,//字符来源于名字
};//字符串来源类型

@interface NSString (ZhangWei)

/*
 *  获取字符串排序的首字母  如 史蒂夫 -> sdf
 *                         曾国藩 -> zgf
 *                         史蒂夫·乔布斯 -> sdfqbs
 *                         沈从wen -> scwen
 *                         Jobs  -> Jobs
 *                         D.J   -> D.J(不作姓名的转换结果)
 *                         I'm  -> I'm
 *
 *  @param isForSurname 标记字符串是否为姓氏，如果为姓氏将使用姓氏对应的拼音首字母且将去掉姓名中的“·”符号（部分姓氏为多音字，发音独特，如曾、单等）
 *
 *  @return 返回的字符串未处理大小写，汉字系统转拼音默认小写，取每个汉字的拼音首字母，如果字符串本身非汉字则返回值保持不变，如英文字母或字符表情等
 */
- (NSString *)zw_getFirstLettersWithSourceType:(StringSourceType)enSourceType;

/*
 *  获取字符串排序的无音调拼音    如 史蒂夫 -> shi di fu
 *                              曾国藩 -> zeng guo fan
 *                              史蒂夫·乔布斯 -> shi di fu qiao bu si
 *                              沈从wen -> shen congwen
 *                              Jobs  -> Jobs
 *                              D.J   -> D.J(不作姓名的转换结果)
 *                              I'm  -> I'm
 *
 *  @param isForSurname 标记字符串是否为姓氏，如果为姓氏将使用姓氏对应的拼音且将替换掉姓名中的“·”符号为“ ”（部分姓氏为多音字，发音独特，如曾、单等）
 *
 *  @return 返回的字符串未处理大小写，汉字系统转拼音默认小写，取每个汉字的拼音，如果字符串本身非汉字则保持不变，如英文字母或字符等
 */
- (NSString *)zw_getPinyinWithSourceType:(StringSourceType)enSourceType;

/*
 *  获取字符串排序的有音调拼音    如 史蒂夫 -> shǐ dì fu
 *                              曾国藩 -> zēng guó fān
 *                              史蒂夫·乔布斯 -> shǐ dì fu qiáo bù sī
 *                              沈从wen -> shěn cóngwen
 *                              Jobs  -> Jobs
 *                              D.J   -> D.J(不作姓名的转换结果)
 *                              I'm  -> I'm
 *
 *  @param isForSurname 标记字符串是否为姓氏，如果为姓氏将使用姓氏对应的拼音且将替换掉姓名中的“·”符号为“ ”（部分姓氏为多音字，发音独特，如曾、单等）
 *
 *  @return 返回的字符串未处理大小写，汉字系统转拼音默认小写，取每个汉字的拼音，如果字符串本身非汉字则保持不变，如英文字母或字符等
 */
- (NSString *)zw_getTonePinyinWithSourceType:(StringSourceType)enSourceType;

/**
 *  判断是否是纯汉字
 */
- (BOOL)zw_isOnlyChinese;

/**
 *  判断是否包含汉字
 */
- (BOOL)zw_containChinese;

/*
 * 是否是纯英文
 */
- (BOOL)zw_isOnlyAlphabet;

/**
 *  检测是否是只包含汉字字母数字的文字
 */
- (BOOL)zw_isOnlyChineseCharatersOrNumberOrAlphabet;

/**
 *  检测是否是数字，包括小数
 */
- (BOOL)zw_isDecimal;

//判断一个字符串是否为数字
- (BOOL)zw_isDigit;

/*
 * 是否是合法的email
 */
- (BOOL)zw_isValidEmail;

/*
 * 是否是合法的IP地址
 */
- (BOOL)zw_isValidIpAddress;

//    一、车牌号校验规则
//
//    1.常规车牌号：仅允许以汉字开头，后面可录入六个字符，由大写英文字母和阿拉伯数字组成。如：粤B12345；
//    2.武警车牌：允许前两位为大写英文字母，后面可录入五个或六个字符，由大写英文字母和阿拉伯数字组成，其中第三位可录汉字也可录大写英文字母及阿拉伯数字，第三位也可空，如：WJ警00081、WJ京1234J、WJ1234X。
//    3.最后一个为汉字的车牌：允许以汉字开头，后面可录入六个字符，前五位字符，由大写英文字母和阿拉伯数字组成，而最后一个字符为汉字，汉字包括“挂”、“学”、“警”、“军”、“港”、“澳”。如：粤Z1234港。
//    4.新军车牌：以两位为大写英文字母开头，后面以5位阿拉伯数字组成。如：BA12345。
//    5.黑龙江车牌存在08或38开头的情况
/**
 *  是否是中国大陆的合法汽车牌号
 */
- (BOOL)zw_isValidChineseCarLicenseNumber;

/**
 *  把字符串转换成+86地区的手机号  （如参数+8613751033110 返回为 13751033110）
 */
- (NSString*)zw_convertToChineseMobilePhone;

/**
 * 是否精确匹配是中国大陆的合法手机号 即+86地区的
 * 参数exactMatch默认为NO。YES表示使用正则表达式精确匹配是否合法，否则仅判断是否以1开头的11位数字。
 * 移动：134[0-8] 135 136 137 138 139 147 150 151 152 157 158 159 1705(虚拟运营商) 178 182 183 184 187 188 198
 * 联通：130 131 132 145 155 156 166 1706(虚拟运营商) 1707(虚拟运营商) 1708(虚拟运营商) 1709(虚拟运营商) 171 175 176 185 186
 * 电信：133 1349 149 153 1700(虚拟运营商) 1701(虚拟运营商) 1702(虚拟运营商) 173 177 180 181 189 199
 * 💡更新于2018-11-05 ZhangWei
 * 💡因工信部会不定期新批一些号段，请注意更新正则表达式，否则新批的号段可能会被归属到不合法手机号
 */
- (BOOL)zw_isValidChineseMobilePhone:(BOOL)isExactMatch;

/**
 * 字符串是否匹配正则表达式
 */
- (BOOL)zw_isMatchWithRegularExpression:(NSString *)formatString;

typedef NS_ENUM(NSInteger, MobilePhoneType)
{
    MobilePhoneType_ChineseInvalid = 0,
    MobilePhoneType_CM,//中国移动
    MobilePhoneType_CU,//中国联通
    MobilePhoneType_CT,//中国电信
    MobilePhoneType_Unknown,
    MobilePhoneTypeCount,
};//移动手机号类型

/**
 *  获取移动手机号的类型
 */
- (MobilePhoneType)zw_mobilePhoneType;

/*
 我国自1999年实施公民身份号码制度以来，许多公民身份号码末位为“X”的公民，由于不明白“X”的含义，要求给予更换，产生了不必要的误会。目前我国公民身份证号码由18位数字组成：前6位为地址码，第7至14位为出生日期码，第15至17位为顺序码，第18位为校验码。检验码分别是“0、1、2、……10”共11个数字，当检验码为“10”时，为了保证公民身份证号码18位，所以用“X”表示。虽然校验码为“X”不能更换，但若需全用数字表示，只需将18位公民身份号码转换成15位居民身份证号码，去掉第7至8位和最后1位3个数码。
 
 当今的身份证号码有15位和18位之分。1985年我国实行居民身份证制度，当时签发的身份证号码是15位的，1999年签发的身份证由于年份的扩展（由两位变为四位）和末尾加了效验码，就成了18位。这两种身份证号码将在相当长的一段时期内共存。两种身份证号码的含义如下：
 
 18位的身份证号码 如：130429####%%%%0078
 
 1~6位为地区代码，其中1、2位数为各省级政府的代码，3、4位数为地、市级政府的代码，5、6位数为县、区级政府代码。如13（河北省）04（邯郸市）29（永年县）
 
 7~14位为出生年月日
 
 15~17位为顺序号，是县、区级政府所辖派出所的分配码，每个派出所分配码为10个连续号码，例如“000-009”或“060-069”，其中单数为男性分配码，双数为女性分配码，如遇同年同月同日有两人以上时顺延第二、第三、第四、第五个分配码。如：007的就是个男生 而且和他同年月日生的男生至少有两个 他们的后四位是001* 和 003*
 
 18位为效验位（识别码），通过复杂公式算出，普遍采用计算机自动生成。是前面17位的一种检验代码，如果你改变了前面某个数字而后面的效验代码不响应改变就会被计算软件判断为非法身份正号码。X也是效验代码的一中
 
 15位的身份证号码：
 
 （1）1~6位为地区代码
 
 （2）7~8位为出生年份(2位)，9~10位为出生月份，11~12位为出生日期
 
 （3）第13~15位为顺序号，并能够判断性别，奇数为男，偶数为女。
 
 
 
 原来的一代身份证号是15位的，现在用的二代身份证号是18位的，它们之间有一个转换规则。
 一代：340524800101001
 二代：34052419800101001X
 可以看到它们之间的区别是二代在年份前多了19，最后面多了一位校验位
 
 第十八位数字的计算方法为：
 1.将前面的身份证号码17位数分别乘以不同的系数。从第一位到第十七位的系数分别为：7 9 10 5 8 4 2 1 6 3 7 9 10 5 8 4 2
 2.将这17位数字和系数相乘的结果相加。
 3.用加出来和除以11，看余数是多少？
 4余数只可能有0 1 2 3 4 5 6 7 8 9 10这11个数字。其分别对应的最后一位身份证的号码为1 0 X 9 8 7 6 5 4 3 2。
 5.通过上面得知如果余数是2，就会在身份证的第18位数字上出现罗马数字的Ⅹ。如果余数是10，身份证的最后一位号码就是2。
 */

//由身份证的前面17位号码自动算出最后一位校验码
- (NSString*)zw_checkBitForValidateIdentifyCardNumberSince1999;

//验证身份证号码是否合法有效
- (BOOL)zw_isValidIdentityCardSince1999;

/**
 *  把url进行encode转码（内部实现与支付宝官方Demo方法一样）
 */
- (NSString*)zw_urlEncodedString;

/**
 *  把url进行decode转码
 */
- (NSString*)zw_urlDecodedString;

//网络请求拼接中文参数,用户名登陆等很多地方会用到中文,UTF8编码显得颇为重要
- (NSString*)zw_percentEncodingStringByAdding;
- (NSString*)zw_percentEncodingStringByRemoving;

/**
 *  从json字符串转换成NSObject对象
 */
- (id)zw_convertToObjectFromJsonString;

//将html格式的字符串转换成富文本的字符串
+ (NSAttributedString *)zw_getStringFromHtmlString:(NSString *)str;

//从字符串中截取指定字符串之间的子字符串，如果无法截取指定字符串则返回nil。
//⚠️startString从前往后查找，endString则从后往前查找。
- (NSString *)zw_substringFromString:(NSString*)startString toString:(NSString*)endString containSearchString:(BOOL)isContain;

//从16进制字符串转成数据
- (NSData *)zw_convertToDataFromHexString;

//如果字符串不是有效的数字，返回值是 nil。如果是一个有效的数字(包括整型和带小数的的浮点型字符串)，返回值有NSNumber的全部功能，可以搞明白这个数字到底是什么类型。
- (NSNumber *)zw_convertToNumber;

//搜索是否包含关键字
- (BOOL)zw_containsStringCaseInsensitiveSearch:(NSString *)searchText;//不区分大小写比较
- (BOOL)zw_containsString:(NSString *)searchText options:(NSStringCompareOptions)options;

@end





/*
 *   【效率比较】
 *        iPhone6s firstLettersForSort/pinyinForSort/pinyinWithToneForSort几乎没有差别
 *        iPhone5s及iPhone4 firstLettersForSort处理汉字字符串时较快；pinyinForSort/pinyinWithToneForSort处理非汉字字符串时较快
 */

//NSString* test001 = @"繁国藩";
//NSString* test002 = @"区国藩";
//NSString* test003 = @"仇国藩";
//NSString* test004 = @"史蒂夫·Jobs";
//NSString* test005 = @"种国藩";
//NSString* test006 = @"单国藩";
//NSString* test007 = @"解国藩";
//NSString* test008 = @"查国藩";
//NSString* test009 = @"曾国藩";
//NSString* test010 = @"秘国藩";
//NSString* test011 = @"乐国藩";
//NSString* test012 = @"重国藩";
//NSString* test013 = @"朴国藩";
//NSString* test014 = @"缪国藩";
//NSString* test015 = @"冼国藩";
//NSString* test016 = @"翟国藩";
//NSString* test017 = @"折国藩";
//NSString* test018 = @"黑国藩";
//NSString* test019 = @"盖国藩";
//NSString* test020 = @"沈国藩";
//NSString* test021 = @"尉迟国藩";
//NSString* test022 = @"万俟国藩";
//
//NSLog(@"%@", [test001 zw_getFirstLettersWithSourceType:NO]);
//NSLog(@"%@", [test002 zw_getFirstLettersWithSourceType:NO]);
//NSLog(@"%@", [test003 zw_getFirstLettersWithSourceType:NO]);
//NSLog(@"%@", [test004 zw_getFirstLettersWithSourceType:NO]);
//NSLog(@"%@", [test005 zw_getFirstLettersWithSourceType:NO]);
//NSLog(@"%@", [test006 zw_getFirstLettersWithSourceType:NO]);
//NSLog(@"%@", [test007 zw_getFirstLettersWithSourceType:NO]);
//NSLog(@"%@", [test008 zw_getFirstLettersWithSourceType:NO]);
//NSLog(@"%@", [test009 zw_getFirstLettersWithSourceType:NO]);
//NSLog(@"%@", [test010 zw_getFirstLettersWithSourceType:NO]);
//NSLog(@"%@", [test011 zw_getFirstLettersWithSourceType:NO]);
//NSLog(@"%@", [test012 zw_getFirstLettersWithSourceType:NO]);
//NSLog(@"%@", [test013 zw_getFirstLettersWithSourceType:NO]);
//NSLog(@"%@", [test014 zw_getFirstLettersWithSourceType:NO]);
//NSLog(@"%@", [test015 zw_getFirstLettersWithSourceType:NO]);
//NSLog(@"%@", [test016 zw_getFirstLettersWithSourceType:NO]);
//NSLog(@"%@", [test017 zw_getFirstLettersWithSourceType:NO]);
//NSLog(@"%@", [test018 zw_getFirstLettersWithSourceType:NO]);
//NSLog(@"%@", [test019 zw_getFirstLettersWithSourceType:NO]);
//NSLog(@"%@", [test020 zw_getFirstLettersWithSourceType:NO]);
//NSLog(@"%@", [test021 zw_getFirstLettersWithSourceType:NO]);
//NSLog(@"%@", [test022 zw_getFirstLettersWithSourceType:NO]);
//
//NSLog(@"%@", [test001 zw_getPinyinWithSourceType:NO]);
//NSLog(@"%@", [test002 zw_getPinyinWithSourceType:NO]);
//NSLog(@"%@", [test003 zw_getPinyinWithSourceType:NO]);
//NSLog(@"%@", [test004 zw_getPinyinWithSourceType:NO]);
//NSLog(@"%@", [test005 zw_getPinyinWithSourceType:NO]);
//NSLog(@"%@", [test006 zw_getPinyinWithSourceType:NO]);
//NSLog(@"%@", [test007 zw_getPinyinWithSourceType:NO]);
//NSLog(@"%@", [test008 zw_getPinyinWithSourceType:NO]);
//NSLog(@"%@", [test009 zw_getPinyinWithSourceType:NO]);
//NSLog(@"%@", [test010 zw_getPinyinWithSourceType:NO]);
//NSLog(@"%@", [test011 zw_getPinyinWithSourceType:NO]);
//NSLog(@"%@", [test012 zw_getPinyinWithSourceType:NO]);
//NSLog(@"%@", [test013 zw_getPinyinWithSourceType:NO]);
//NSLog(@"%@", [test014 zw_getPinyinWithSourceType:NO]);
//NSLog(@"%@", [test015 zw_getPinyinWithSourceType:NO]);
//NSLog(@"%@", [test016 zw_getPinyinWithSourceType:NO]);
//NSLog(@"%@", [test017 zw_getPinyinWithSourceType:NO]);
//NSLog(@"%@", [test018 zw_getPinyinWithSourceType:NO]);
//NSLog(@"%@", [test019 zw_getPinyinWithSourceType:NO]);
//NSLog(@"%@", [test020 zw_getPinyinWithSourceType:NO]);
//NSLog(@"%@", [test021 zw_getPinyinWithSourceType:NO]);
//NSLog(@"%@", [test022 zw_getPinyinWithSourceType:NO]);
//
//NSLog(@"%@", [test001 zw_getTonePinyinWithSourceType:NO]);
//NSLog(@"%@", [test002 zw_getTonePinyinWithSourceType:NO]);
//NSLog(@"%@", [test003 zw_getTonePinyinWithSourceType:NO]);
//NSLog(@"%@", [test004 zw_getTonePinyinWithSourceType:NO]);
//NSLog(@"%@", [test005 zw_getTonePinyinWithSourceType:NO]);
//NSLog(@"%@", [test006 zw_getTonePinyinWithSourceType:NO]);
//NSLog(@"%@", [test007 zw_getTonePinyinWithSourceType:NO]);
//NSLog(@"%@", [test008 zw_getTonePinyinWithSourceType:NO]);
//NSLog(@"%@", [test009 zw_getTonePinyinWithSourceType:NO]);
//NSLog(@"%@", [test010 zw_getTonePinyinWithSourceType:NO]);
//NSLog(@"%@", [test011 zw_getTonePinyinWithSourceType:NO]);
//NSLog(@"%@", [test012 zw_getTonePinyinWithSourceType:NO]);
//NSLog(@"%@", [test013 zw_getTonePinyinWithSourceType:NO]);
//NSLog(@"%@", [test014 zw_getTonePinyinWithSourceType:NO]);
//NSLog(@"%@", [test015 zw_getTonePinyinWithSourceType:NO]);
//NSLog(@"%@", [test016 zw_getTonePinyinWithSourceType:NO]);
//NSLog(@"%@", [test017 zw_getTonePinyinWithSourceType:NO]);
//NSLog(@"%@", [test018 zw_getTonePinyinWithSourceType:NO]);
//NSLog(@"%@", [test019 zw_getTonePinyinWithSourceType:NO]);
//NSLog(@"%@", [test020 zw_getTonePinyinWithSourceType:NO]);
//NSLog(@"%@", [test021 zw_getTonePinyinWithSourceType:NO]);
//NSLog(@"%@", [test022 zw_getTonePinyinWithSourceType:NO]);
//
//
//
//NSLog(@"%@", [test001 zw_getFirstLettersWithSourceType:YES]);
//NSLog(@"%@", [test002 zw_getFirstLettersWithSourceType:YES]);
//NSLog(@"%@", [test003 zw_getFirstLettersWithSourceType:YES]);
//NSLog(@"%@", [test004 zw_getFirstLettersWithSourceType:YES]);
//NSLog(@"%@", [test005 zw_getFirstLettersWithSourceType:YES]);
//NSLog(@"%@", [test006 zw_getFirstLettersWithSourceType:YES]);
//NSLog(@"%@", [test007 zw_getFirstLettersWithSourceType:YES]);
//NSLog(@"%@", [test008 zw_getFirstLettersWithSourceType:YES]);
//NSLog(@"%@", [test009 zw_getFirstLettersWithSourceType:YES]);
//NSLog(@"%@", [test010 zw_getFirstLettersWithSourceType:YES]);
//NSLog(@"%@", [test011 zw_getFirstLettersWithSourceType:YES]);
//NSLog(@"%@", [test012 zw_getFirstLettersWithSourceType:YES]);
//NSLog(@"%@", [test013 zw_getFirstLettersWithSourceType:YES]);
//NSLog(@"%@", [test014 zw_getFirstLettersWithSourceType:YES]);
//NSLog(@"%@", [test015 zw_getFirstLettersWithSourceType:YES]);
//NSLog(@"%@", [test016 zw_getFirstLettersWithSourceType:YES]);
//NSLog(@"%@", [test017 zw_getFirstLettersWithSourceType:YES]);
//NSLog(@"%@", [test018 zw_getFirstLettersWithSourceType:YES]);
//NSLog(@"%@", [test019 zw_getFirstLettersWithSourceType:YES]);
//NSLog(@"%@", [test020 zw_getFirstLettersWithSourceType:YES]);
//NSLog(@"%@", [test021 zw_getFirstLettersWithSourceType:YES]);
//NSLog(@"%@", [test022 zw_getFirstLettersWithSourceType:YES]);
//
//NSLog(@"%@", [test001 zw_getPinyinWithSourceType:YES]);
//NSLog(@"%@", [test002 zw_getPinyinWithSourceType:YES]);
//NSLog(@"%@", [test003 zw_getPinyinWithSourceType:YES]);
//NSLog(@"%@", [test004 zw_getPinyinWithSourceType:YES]);
//NSLog(@"%@", [test005 zw_getPinyinWithSourceType:YES]);
//NSLog(@"%@", [test006 zw_getPinyinWithSourceType:YES]);
//NSLog(@"%@", [test007 zw_getPinyinWithSourceType:YES]);
//NSLog(@"%@", [test008 zw_getPinyinWithSourceType:YES]);
//NSLog(@"%@", [test009 zw_getPinyinWithSourceType:YES]);
//NSLog(@"%@", [test010 zw_getPinyinWithSourceType:YES]);
//NSLog(@"%@", [test011 zw_getPinyinWithSourceType:YES]);
//NSLog(@"%@", [test012 zw_getPinyinWithSourceType:YES]);
//NSLog(@"%@", [test013 zw_getPinyinWithSourceType:YES]);
//NSLog(@"%@", [test014 zw_getPinyinWithSourceType:YES]);
//NSLog(@"%@", [test015 zw_getPinyinWithSourceType:YES]);
//NSLog(@"%@", [test016 zw_getPinyinWithSourceType:YES]);
//NSLog(@"%@", [test017 zw_getPinyinWithSourceType:YES]);
//NSLog(@"%@", [test018 zw_getPinyinWithSourceType:YES]);
//NSLog(@"%@", [test019 zw_getPinyinWithSourceType:YES]);
//NSLog(@"%@", [test020 zw_getPinyinWithSourceType:YES]);
//NSLog(@"%@", [test021 zw_getPinyinWithSourceType:YES]);
//NSLog(@"%@", [test022 zw_getPinyinWithSourceType:YES]);
//
//NSLog(@"%@", [test001 zw_getTonePinyinWithSourceType:YES]);
//NSLog(@"%@", [test002 zw_getTonePinyinWithSourceType:YES]);
//NSLog(@"%@", [test003 zw_getTonePinyinWithSourceType:YES]);
//NSLog(@"%@", [test004 zw_getTonePinyinWithSourceType:YES]);
//NSLog(@"%@", [test005 zw_getTonePinyinWithSourceType:YES]);
//NSLog(@"%@", [test006 zw_getTonePinyinWithSourceType:YES]);
//NSLog(@"%@", [test007 zw_getTonePinyinWithSourceType:YES]);
//NSLog(@"%@", [test008 zw_getTonePinyinWithSourceType:YES]);
//NSLog(@"%@", [test009 zw_getTonePinyinWithSourceType:YES]);
//NSLog(@"%@", [test010 zw_getTonePinyinWithSourceType:YES]);
//NSLog(@"%@", [test011 zw_getTonePinyinWithSourceType:YES]);
//NSLog(@"%@", [test012 zw_getTonePinyinWithSourceType:YES]);
//NSLog(@"%@", [test013 zw_getTonePinyinWithSourceType:YES]);
//NSLog(@"%@", [test014 zw_getTonePinyinWithSourceType:YES]);
//NSLog(@"%@", [test015 zw_getTonePinyinWithSourceType:YES]);
//NSLog(@"%@", [test016 zw_getTonePinyinWithSourceType:YES]);
//NSLog(@"%@", [test017 zw_getTonePinyinWithSourceType:YES]);
//NSLog(@"%@", [test018 zw_getTonePinyinWithSourceType:YES]);
//NSLog(@"%@", [test019 zw_getTonePinyinWithSourceType:YES]);
//NSLog(@"%@", [test020 zw_getTonePinyinWithSourceType:YES]);
//NSLog(@"%@", [test021 zw_getTonePinyinWithSourceType:YES]);
//NSLog(@"%@", [test022 zw_getTonePinyinWithSourceType:YES]);
