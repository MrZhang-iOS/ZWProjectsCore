//
//  NSString+ZhangWei.m
//  ZWProjectsCore
//
//  Created by ZhangWei on 16/4/22.
//  Copyright © 2016年 ZhangWei. All rights reserved.
//

#import "NSString+ZhangWei.h"
#import "ZWPinyin.h"
#import "ZWProjectsCoreMacros.h"
#import "ZWProjectsCoreFunctions.h"
#import "NSArray+ZhangWei.h"

@implementation NSString (ZhangWei)

//****************************************************************************************************
//*  某些字作为姓氏时，读音比较特别，系统默认转换时有时不能转成正确的读音，以下列出的姓氏常见的多音字姓氏
//*  【如果有其他未列出且系统默认翻译不能匹配正确读音的多音字姓氏时，请补全。以下多音字在iOS7、iOS8、iOS9设备上均已测试】
//*
//*  繁：pó 姓氏时不读fán。汉末诗人繁钦。系统默认将该字翻译成fán
//*  区：ōu 姓氏时不读qū。前国足门将区楚良。系统翻译默认将该字翻译成qū
//*  仇：qiú 姓氏时不读chóu。明代画家仇英。系统翻译默认将该字翻译成chóu
//*  种：chóng 姓氏时不读zhǒng。原中国驻阿拉伯也门大使种汉九。系统翻译默认将该字翻译成zhǒng
//*  单：shàn 姓氏时不读dān。隋唐单雄信。系统翻译默认将该字翻译成dān
//*  解：xiè 姓氏时不读jiě。梁山好汉解珍、解宝。系统翻译默认将该字翻译成jiě
//*  查：zhā 姓氏时不读chá。著名武侠小说作家金庸本名查良镛。系统翻译默认将该字翻译成chá
//*  曾：zēng 姓氏时不读céng。宋代作家曾巩、清代名臣曾国藩，香港特首曾荫权。系统翻译默认将该字翻译成céng
//*  秘：bì 姓氏时不读mì。系统默认将该字翻译成mì
//*  乐：yuè 姓氏时不读lè。系统默认将该字翻译成lè
//*  重：chóng 姓氏时不读zhòng。系统默认将该字翻译成zhòng
//*  朴：piáo 韩国三大姓，李、金、朴，姓氏时不读pǔ。系统翻译默认将该字翻译成pǔ
//*  缪: miào 姓氏时不读móu。系统翻译默认将该字翻译成móu
//【系统翻译就是姓氏发音】冼: xiǎn 姓氏时不读shěng。系统翻译默认将该字翻译成xiǎn
//*  翟：zhái 姓氏时不读dí。系统翻译默认将该字翻译成dí
//*  折：shé  姓氏时不读zhē折腾或者zhé折叠。系统翻译默认将该字翻译成zhé
//*  黑：hè  姓氏时不读hēi。系统翻译默认将该字翻译成hēi
//*  盖：gě 姓氏时不读gài。系统翻译默认将该字翻译成gài
//*  沈：shěn 姓氏时不读chén。系统翻译默认将该字翻译成chén
//*  尉迟：yù chí 复姓。系统翻译默认将该字翻译成wèi chí
//*  万俟：mò qí  复姓。系统翻译默认将该字翻译成wàn qí
//*【谌，读音作chén。因南北方文化差异不同，用作姓氏时即读chén也可读shèn。考虑效率问题所以未录入，以系统翻译为准】
//****************************************************************************************************


#pragma mark - 明确列出转换平台翻译错误的多音字姓氏和名字，并列出正确匹配项
/**
 *  所有的多音字匹配的拼音键值对，每个姓氏多音字对应一个数组，数组中第一个字为正确发音，第二个为错误发音，错误发音用来做字符串替换使用
 *  多音字姓氏使用系统默认转换时，如果转换错误，将使用该键值对匹配替换
 */
+ (NSDictionary*)getDicForAllPolyphoneSurnamePinYin;
{
    static NSDictionary* g_pAllPolyphoneSurnamePinYin = nil;
    static dispatch_once_t pred;
    dispatch_once(&pred, ^
                  {
                      g_pAllPolyphoneSurnamePinYin = [NSMutableDictionary dictionaryWithObjectsAndKeys
                                                      :@[@"po",@"fan"],@"繁"
                                                      ,@[@"ou",@"qu"],@"区"
                                                      ,@[@"qiu",@"chou"],@"仇"
                                                      ,@[@"chong",@"zhong"],@"种"
                                                      ,@[@"shan",@"dan"],@"单"
                                                      ,@[@"xie",@"jie"],@"解"
                                                      ,@[@"zha",@"cha"],@"查"
                                                      ,@[@"zeng",@"ceng"],@"曾"
                                                      ,@[@"bi",@"mi"],@"秘"
                                                      ,@[@"yue",@"le"],@"乐"
                                                      ,@[@"chong",@"zhong"],@"重"
                                                      ,@[@"piao",@"pu"],@"朴"
                                                      ,@[@"miao",@"mou"],@"缪"
                                                      ,@[@"zhai",@"di"],@"翟"
                                                      ,@[@"she",@"zhe"],@"折"
                                                      ,@[@"he",@"hei"],@"黑"
                                                      ,@[@"ge",@"gai"],@"盖"
                                                      ,@[@"shen",@"chen"],@"沈"
                                                      ,@[@"yu chi",@"wei chi"],@"尉迟"
                                                      ,@[@"mo qi",@"wan qi"],@"万俟"
                                                      ,nil];
                  });
    return g_pAllPolyphoneSurnamePinYin;
}

/**
 *  所有的多音字匹配的拼音键值对，每个名字多音字对应一个数组，数组中第一个字为正确发音，第二个为错误发音，错误发音用来做字符串替换使用
 *  多音字名字使用系统默认转换时，如果转换错误，将使用该键值对匹配替换
 */
+ (NSDictionary*)getDicForAllPolyphoneGivenNamePinYin;
{
    static NSDictionary* g_pAllPolyphoneGivenNamePinYin = nil;
    static dispatch_once_t pred;
    dispatch_once(&pred, ^
                  {
                      g_pAllPolyphoneGivenNamePinYin = [NSMutableDictionary dictionaryWithObjectsAndKeys
                                                        :@[@"chang",@"zhang"],@"长"
                                                        ,@[@"xia",@"sha"],@"厦"
                                                        ,@[@"di",@"de"],@"地"
                                                        ,@[@"chong",@"zhong"],@"重"
                                                        ,@[@"shen",@"chen"],@"沈"
                                                        ,nil];
                  });
    return g_pAllPolyphoneGivenNamePinYin;
}

/**
 *  所有多音字匹配的带音调拼音键值对，每个姓氏多音字对应一个数组，数组中第一个字为正确发音，第二个为错误发音，错误发音用来做字符串替换使用
 *  多音字姓氏使用系统默认转换时，如果转换错误，将使用该键值对匹配替换
 */
+ (NSDictionary*)getDicForAllPolyphoneSurnamePinYinWithYinDiao;
{
    static NSDictionary* g_pAllPolyphoneSurnamePinYinWithYinDiao = nil;
    static dispatch_once_t pred;
    dispatch_once(&pred, ^
                  {
                      g_pAllPolyphoneSurnamePinYinWithYinDiao = [NSMutableDictionary dictionaryWithObjectsAndKeys
                                                                 :@[@"pó",@"fán"],@"繁"
                                                                 ,@[@"ōu",@"qū"],@"区"
                                                                 ,@[@"qiú",@"chóu"],@"仇"
                                                                 ,@[@"chóng",@"zhǒng"],@"种"
                                                                 ,@[@"shàn",@"dān"],@"单"
                                                                 ,@[@"xiè",@"jiě"],@"解"
                                                                 ,@[@"zhā",@"chá"],@"查"
                                                                 ,@[@"zēng",@"céng"],@"曾"
                                                                 ,@[@"bì",@"mì"],@"秘"
                                                                 ,@[@"yuè",@"lè"],@"乐"
                                                                 ,@[@"chóng",@"zhòng"],@"重"
                                                                 ,@[@"piáo",@"pǔ"],@"朴"
                                                                 ,@[@"miào",@"móu"],@"缪"
                                                                 ,@[@"xiǎn",@"shěng"],@"冼"
                                                                 ,@[@"zhái",@"dí"],@"翟"
                                                                 ,@[@"shé",@"zhé"],@"折"
                                                                 ,@[@"hè",@"hēi"],@"黑"
                                                                 ,@[@"gě",@"gài"],@"盖"
                                                                 ,@[@"shěn",@"chén"],@"沈"
                                                                 ,@[@"yù chí",@"wèi chí"],@"尉迟"
                                                                 ,@[@"mò qí",@"wàn qí"],@"万俟"
                                                                 ,nil];
                  });
    return g_pAllPolyphoneSurnamePinYinWithYinDiao;
}

/**
 *  所有多音字匹配的带音调拼音键值对，每个名字多音字对应一个数组，数组中第一个字为正确发音，第二个为错误发音，错误发音用来做字符串替换使用
 *  多音字名字使用系统默认转换时，如果转换错误，将使用该键值对匹配替换
 */
+ (NSDictionary*)getDicForAllPolyphoneGivenNamePinYinWithYinDiao;
{
    static NSDictionary* g_pAllPolyphoneGivenNamePinYinWithYinDiao = nil;
    static dispatch_once_t pred;
    dispatch_once(&pred, ^
                  {
                      g_pAllPolyphoneGivenNamePinYinWithYinDiao = [NSMutableDictionary dictionaryWithObjectsAndKeys
                                                                   :@[@"cháng",@"zhǎng"],@"长"
                                                                   ,@[@"xià",@"shà"],@"厦"
                                                                   ,@[@"dì",@"de"],@"地"
                                                                   ,@[@"chóng",@"zhòng"],@"重"
                                                                   ,@[@"shěn",@"chén"],@"沈"
                                                                   ,nil];
                  });
    return g_pAllPolyphoneGivenNamePinYinWithYinDiao;
}

/**
 *  所有的多音字匹配的拼音首字母键值对，每个姓氏多音字对应一字符串
 *  多音字姓氏使用第三方平台转换时，如果转换错误，将使用该键值对匹配替换
 */
+ (NSDictionary*)getDicForAllPolyphoneSurnameFirstLetters;
{
    static NSDictionary* g_pAllPolyphoneSurnameFirstLetters = nil;
    static dispatch_once_t pred;
    dispatch_once(&pred, ^
                  {
                      g_pAllPolyphoneSurnameFirstLetters = [NSMutableDictionary dictionaryWithObjectsAndKeys
                                                            :@"p",@"繁"
                                                            ,@"o",@"区"
                                                            ,@"q",@"仇"
                                                            ,@"c",@"种"
                                                            ,@"s",@"单"
                                                            ,@"x",@"解"
                                                            ,@"z",@"查"
                                                            ,@"z",@"曾"
                                                            ,@"b",@"秘"
                                                            ,@"y",@"乐"
                                                            ,@"c",@"重"
                                                            ,@"x",@"冼"
                                                            ,@"z",@"翟"
                                                            ,@"s",@"折"
                                                            ,@"s",@"沈"
                                                            ,@"q",@"瞿"
                                                            ,@"yc",@"尉迟"
                                                            ,@"mq",@"万俟"
                                                            ,nil];
                  });
    return g_pAllPolyphoneSurnameFirstLetters;
}

/**
 *  所有的多音字匹配的拼音首字母键值对，每个名字多音字对应一字符串
 *  多音字名字使用第三方平台转换时，如果转换错误，将使用该键值对匹配替换
 */
+ (NSDictionary*)getDicForAllPolyphoneGeivenNameFirstLetters;
{
    static NSDictionary* g_pAllPolyphoneGeivenNameFirstLetters = nil;
    static dispatch_once_t pred;
    dispatch_once(&pred, ^
                  {
                      g_pAllPolyphoneGeivenNameFirstLetters = [NSMutableDictionary dictionaryWithObjectsAndKeys
                                                               :@"p",@"繁"
                                                               ,@"o",@"区"
                                                               ,@"q",@"仇"
                                                               ,@"c",@"种"
                                                               ,@"s",@"单"
                                                               ,@"x",@"解"
                                                               ,@"z",@"查"
                                                               ,@"z",@"曾"
                                                               ,@"b",@"秘"
                                                               ,@"y",@"乐"
                                                               ,@"c",@"重"
                                                               ,@"x",@"冼"
                                                               ,@"z",@"翟"
                                                               ,@"s",@"折"
                                                               ,@"s",@"沈"
                                                               ,@"q",@"瞿"
                                                               ,@"yc",@"尉迟"
                                                               ,@"mq",@"万俟"
                                                               ,@"c",@"长"
                                                               ,@"x",@"厦"
                                                               ,@"c",@"晟"
                                                               ,@"q",@"瞿"
                                                               ,@"w",@"蔚"
                                                               ,nil];
                  });
    return g_pAllPolyphoneGeivenNameFirstLetters;
}

/**
 *  把字符串中的汉字转换成拼音
 *
 *  @param enSourceType 明确字符串来源
 *  @param bFlag        明确字符串转换完毕后是否需要拼音音调
 *
 *  @return 返回结果不做特殊处理，请注意系统默认转换汉字时采用小写拼音
 */
- (NSString*)zw_getPinyinWithSourceType:(StringSourceType)enSourceType withYindiao:(BOOL)bFlag;
{
    NSString* convertString = self;
    if (enSourceType == StringSourceType_FamilyName)
    {
        //如果是姓名优先去掉“·”符号
        convertString = [self stringByReplacingOccurrencesOfString:@"·" withString:@""];
    }
    NSMutableString *mutableString = [NSMutableString stringWithString:convertString];
    CFStringTransform((CFMutableStringRef)mutableString, NULL, kCFStringTransformToLatin, false);
    if (bFlag)
    {
        NSString* pinyinString = [NSString stringWithString:mutableString];
        NSMutableString* resultString = [NSMutableString stringWithString:pinyinString];
        
        switch (enSourceType)
        {
            case StringSourceType_FamilyName:
            {
                //处理姓氏多音字
                NSArray* allPolyphoneSurname = [[NSString getDicForAllPolyphoneSurnamePinYinWithYinDiao] allKeys];
                for (NSString* tempPolyphoneSurname in allPolyphoneSurname)
                {
                    if ([convertString hasPrefix:tempPolyphoneSurname])
                    {
                        NSArray* arrayOfPolyphoneSurnamePinYinWithYinDiao = [[NSString getDicForAllPolyphoneSurnamePinYinWithYinDiao] valueForKey:tempPolyphoneSurname];
                        NSString* rightPinYin = [arrayOfPolyphoneSurnamePinYinWithYinDiao zw_safeObjectAtIndex:0];
                        NSString* wrongPinYin = [arrayOfPolyphoneSurnamePinYinWithYinDiao zw_safeObjectAtIndex:1];
                        NSRange rangeOfPinYin = NSMakeRange(0, wrongPinYin.length);
                        if ([[resultString substringWithRange:rangeOfPinYin] isEqualToString:wrongPinYin])
                        {
                            [resultString replaceCharactersInRange:rangeOfPinYin withString:rightPinYin];
                        }
                        break;
                    }
                }
            }
                break;
            case StringSourceType_GeivenName:
            {
                //处理名字多音字
                NSArray* allPolyphoneGivenName = [[NSString getDicForAllPolyphoneGivenNamePinYinWithYinDiao] allKeys];
                for (NSString* tempPolyphoneGivenName in allPolyphoneGivenName)
                {
                    if ([convertString hasPrefix:tempPolyphoneGivenName])
                    {
                        NSArray* arrayOfPolyphoneGivenNamePinYinWithYinDiao = [[NSString getDicForAllPolyphoneSurnamePinYinWithYinDiao] valueForKey:tempPolyphoneGivenName];
                        NSString* rightPinYin = [arrayOfPolyphoneGivenNamePinYinWithYinDiao zw_safeObjectAtIndex:0];
                        NSString* wrongPinYin = [arrayOfPolyphoneGivenNamePinYinWithYinDiao zw_safeObjectAtIndex:1];
                        NSRange rangeOfPinYin = NSMakeRange(0, wrongPinYin.length);
                        if ([[resultString substringWithRange:rangeOfPinYin] isEqualToString:wrongPinYin])
                        {
                            [resultString replaceCharactersInRange:rangeOfPinYin withString:rightPinYin];
                        }
                        break;
                    }
                }
            }
                break;
                
            default:
                break;
        }
        return resultString;
    }
    else
    {
        NSString *pinyinString = [mutableString stringByFoldingWithOptions:NSDiacriticInsensitiveSearch locale:[NSLocale currentLocale]];
        NSMutableString* resultString = [NSMutableString stringWithString:pinyinString];
        
        switch (enSourceType)
        {
            case StringSourceType_FamilyName:
            {
                //处理姓氏多音字
                NSArray* allPolyphoneSurname = [[NSString getDicForAllPolyphoneSurnamePinYin] allKeys];
                for (NSString* tempPolyphoneSurname in allPolyphoneSurname)
                {
                    if ([convertString hasPrefix:tempPolyphoneSurname])
                    {
                        NSArray* arrayOfPolyphoneSurnamePinYin = [[NSString getDicForAllPolyphoneSurnamePinYin] valueForKey:tempPolyphoneSurname];
                        NSString* rightPinYin = [arrayOfPolyphoneSurnamePinYin zw_safeObjectAtIndex:0];
                        NSString* wrongPinYin = [arrayOfPolyphoneSurnamePinYin zw_safeObjectAtIndex:1];
                        NSRange rangeOfPinYin = NSMakeRange(0, wrongPinYin.length);
                        if ([[resultString substringWithRange:rangeOfPinYin] isEqualToString:wrongPinYin])
                        {
                            [resultString replaceCharactersInRange:rangeOfPinYin withString:rightPinYin];
                        }
                        break;
                    }
                }
            }
                break;
            case StringSourceType_GeivenName:
            {
                //处理名字多音字
                NSArray* allPolyphoneGivenName = [[NSString getDicForAllPolyphoneGivenNamePinYin] allKeys];
                for (NSString* tempPolyphoneGivenName in allPolyphoneGivenName)
                {
                    if ([convertString hasPrefix:tempPolyphoneGivenName])
                    {
                        NSArray* arrayOfPolyphoneGivenNamePinYin = [[NSString getDicForAllPolyphoneGivenNamePinYin] valueForKey:tempPolyphoneGivenName];
                        NSString* rightPinYin = [arrayOfPolyphoneGivenNamePinYin zw_safeObjectAtIndex:0];
                        NSString* wrongPinYin = [arrayOfPolyphoneGivenNamePinYin zw_safeObjectAtIndex:1];
                        NSRange rangeOfPinYin = NSMakeRange(0, wrongPinYin.length);
                        if ([[resultString substringWithRange:rangeOfPinYin] isEqualToString:wrongPinYin])
                        {
                            [resultString replaceCharactersInRange:rangeOfPinYin withString:rightPinYin];
                        }
                        break;
                    }
                }
            }
                break;
                
            default:
                break;
        }
        return resultString;
    }
}

#pragma mark - PublicFunctions
- (NSString*)zw_getFirstLettersWithSourceType:(StringSourceType)enSourceType;
{
    NSString* convertString = self;
    if (enSourceType == StringSourceType_FamilyName)
    {
        //如果是姓名优先去掉“·”符号
        convertString = [self stringByReplacingOccurrencesOfString:@"·" withString:@""];
    }
    NSMutableString* resultString = [NSMutableString string];
    if (convertString.length > 0)
    {
        unichar indexChar;
        for (NSUInteger nIndex = 0; nIndex < convertString.length; ++nIndex)
        {
            indexChar = [convertString characterAtIndex:nIndex];
            //判断字符是否为英文字母
            if ((indexChar >= 'A' && indexChar <= 'Z') ||
                (indexChar >= 'a' && indexChar <= 'z'))
            {
                [resultString appendString:[NSString stringWithFormat:@"%c", indexChar]];
            }
            //判断字符是否为汉字
            else if (zw_isUnicharHanzi(indexChar))
            {
                NSString* firstCharacterInPinYin = [NSString stringWithFormat:@"%c",zw_pinyinFirstLetter(indexChar)];//从第三方库中匹配这个汉子的拼音首字母
                if (firstCharacterInPinYin)
                {
                    [resultString appendString:firstCharacterInPinYin];
                }
                else//未识别字符保持不变
                {
                    [resultString appendString:[convertString substringWithRange:NSMakeRange(nIndex, 1)]];
                }
            }
            //其他字符保持不变
            else
            {
                [resultString appendString:[convertString substringWithRange:NSMakeRange(nIndex, 1)]];
            }
        }
        switch (enSourceType)
        {
            case StringSourceType_FamilyName:
            {
                //处理姓氏多音字
                NSArray* allPolyphoneSurname = [[NSString getDicForAllPolyphoneSurnameFirstLetters] allKeys];
                for (NSString* tempPolyphoneSurname in allPolyphoneSurname)
                {
                    if ([convertString hasPrefix:tempPolyphoneSurname])
                    {
                        [resultString replaceCharactersInRange:NSMakeRange(0, tempPolyphoneSurname.length) withString:[[NSString getDicForAllPolyphoneSurnameFirstLetters] valueForKey:tempPolyphoneSurname]];
                        break;
                    }
                }
            }
                break;
            case StringSourceType_GeivenName:
            {
                //处理名字多音字
                NSArray* allPolyphoneGivenName = [[NSString getDicForAllPolyphoneGeivenNameFirstLetters] allKeys];
                for (NSString* tempPolyphoneGivenName in allPolyphoneGivenName)
                {
                    if ([convertString hasPrefix:tempPolyphoneGivenName])
                    {
                        [resultString replaceCharactersInRange:NSMakeRange(0, tempPolyphoneGivenName.length) withString:[[NSString getDicForAllPolyphoneGeivenNameFirstLetters] valueForKey:tempPolyphoneGivenName]];
                        break;
                    }
                }
            }
                break;
            default:
                break;
        }
    }
    return resultString;
}

- (NSString*)zw_getPinyinWithSourceType:(StringSourceType)enSourceType;
{
    return [self zw_getPinyinWithSourceType:enSourceType withYindiao:NO];
}

- (NSString*)zw_getTonePinyinWithSourceType:(StringSourceType)enSourceType;
{
    return [self zw_getPinyinWithSourceType:enSourceType withYindiao:YES];
}

- (BOOL)zw_isOnlyChinese;
{
    //[\u4E00-\u9FCB]为汉字，[\uFE30-\uFFA0]为全角符号
    NSString *match = @"(^[\u4e00-\u9fcb]+$)";
    NSPredicate *regexTest = [NSPredicate predicateWithFormat:@"SELF matches %@", match];
    return [regexTest evaluateWithObject:self];
}

- (BOOL)zw_containChinese;
{
    //这种方法只能判断中文文字(基本集)，不能判断一些特殊符号比如中文状态下的句号“。”等。
    for(int i=0; i< [self length]; ++i)
    {
        int a =[self characterAtIndex:i];
        if( a >= 0x4e00&& a <= 0x9fcb)
        {
            return YES;
        }
    }
    return NO;
}

- (BOOL)zw_isOnlyAlphabet;
{
    NSString *match = @"^[A-Za-z]+$";
    NSPredicate *regexTest = [NSPredicate predicateWithFormat:@"SELF matches %@", match];
    return [regexTest evaluateWithObject:self];
}

- (BOOL)zw_isOnlyChineseCharatersOrNumberOrAlphabet;
{
    NSString* pattern = @"[A-Za-z0-9\u4e00-\u9fcb]";
    NSPredicate* regexTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
    return [regexTest evaluateWithObject:self];
}

- (BOOL)zw_isDecimal;
{
    if (self.length <= 0)
    {
        return NO;
    }
    
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789."] invertedSet];
    NSString *filtered = [[self componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    
    if (![self isEqualToString:filtered])
    {
        return NO;
    }
    return YES;
}

- (BOOL)zw_isDigit;
{
    NSCharacterSet *notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    return ([self rangeOfCharacterFromSet:notDigits].location == NSNotFound);
}

- (BOOL)zw_isValidEmail;
{
    NSString* pattern = @"^[A-Za-z0-9._%+-]+@([A-Za-z0-9-]+\\.)+([A-Za-z0-9]{2,4}|museum)$";
    NSPredicate* regexTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
    return [regexTest evaluateWithObject:self];
}

- (BOOL)zw_isValidIpAddress
{
    if (self.length <= 0)
    {
        return NO;
    }
    
    NSString *urlRegEx = @"^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
    
    NSError *error;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:urlRegEx options:0 error:&error];
    
    if (regex != nil)
    {
        NSTextCheckingResult *firstMatch=[regex firstMatchInString:self options:0 range:NSMakeRange(0, [self length])];
        
        if (firstMatch)
        {
            NSRange resultRange = [firstMatch rangeAtIndex:0];
            NSString *result=[self substringWithRange:resultRange];
            //输出结果
            NSLog(@"%@",result);
            return YES;
        }
    }
    return NO;
}

- (BOOL)zw_isValidChineseCarLicenseNumber;
{
    NSString *chineseCarLicenseNumberFormat = @"(^[京津沪渝冀豫云辽黑湘皖闽鲁新苏浙赣鄂桂甘晋蒙陕吉贵粤青藏川宁琼使领A-Z]{1}[A-Z]{1}[警京津沪渝冀豫云辽黑湘皖闽鲁新苏浙赣鄂桂甘晋蒙陕吉贵粤青藏川宁琼]{0,1}[A-Z0-9]{4}[A-Z0-9挂学警军港澳]{1}$)|(^(08|38){1}[A-Z0-9]{4}[A-Z0-9挂学警军港澳]{1}$)";
    NSPredicate *regexTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", chineseCarLicenseNumberFormat];
    return [regexTest evaluateWithObject:self];
}

- (NSString*)zw_convertToChineseMobilePhone;
{
    NSString* mobilePhone = self;
    mobilePhone = [mobilePhone stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    mobilePhone = [mobilePhone stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobilePhone = [mobilePhone stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (mobilePhone.length == 13 && [mobilePhone hasPrefix:@"861"])
    {
        mobilePhone = [mobilePhone substringFromIndex:2];
    }
    else if (mobilePhone.length == 14 && [mobilePhone hasPrefix:@"+861"])
    {
        mobilePhone = [mobilePhone substringFromIndex:3];
    }
    return mobilePhone;
}

- (BOOL)zw_isValidChineseMobilePhone:(BOOL)isExactMatch;
{
    NSString* mobilePhone = [self zw_convertToChineseMobilePhone];
    if (isExactMatch)
    {
        //精确的按照正则表达式的规则匹配手机号
        NSString *chineseMobilePhoneFormat = @"^1(3[0-9]|4[579]|5[^4]|6[6]|7[0135-8]|8[0-9]|9[89])\\d{8}$";
        return [mobilePhone zw_isMatchWithRegularExpression:chineseMobilePhoneFormat];
    }
    else
    {
        //只要以1️⃣开头的11位数字，都算合法
        NSString *fuzzyFormat = @"^1\\d{10}$";
        return [mobilePhone zw_isMatchWithRegularExpression:fuzzyFormat];
    }
}

- (BOOL)zw_isMatchWithRegularExpression:(NSString *)formatString;
{
    if (formatString && [formatString isKindOfClass:[NSString class]])
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", formatString];
        return [predicate evaluateWithObject:self];
    }
    return NO;
}

- (MobilePhoneType)zw_mobilePhoneType;
{
    NSString* mobilePhone = [self zw_convertToChineseMobilePhone];
    /**
     * 中国移动：China Mobile
     */
    NSString *CM = @"(^1(3[5-9]|4[7]|5[0-27-9]|7[8]|8[2-478]|9[8])\\d{8}$)|(^1705\\d{7}$)|(^134[0-8]\\d{7}$)";
    /**
     * 中国联通：China Unicom
     */
    NSString *CU = @"(^1(3[0-2]|4[5]|5[56]|6[6]|7[156]|8[56])\\d{8}$)|(^170[6-9]\\d{7}$)";
    /**
     * 中国电信：China Telecom
     */
    NSString *CT = @"(^1(3[3]|4[9]|5[3]|7[37]|8[019]|9[9])\\d{8}$)|(^170[0-2]\\d{7}$)|(^1349\\d{7}$)";
    
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
    
    if (![mobilePhone zw_isValidChineseMobilePhone:YES])
    {
        return MobilePhoneType_ChineseInvalid;
    }
    else if ([regextestcm evaluateWithObject:mobilePhone])
    {
        return MobilePhoneType_CM;
    }
    else if ([regextestcu evaluateWithObject:mobilePhone])
    {
        return MobilePhoneType_CU;
    }
    else if ([regextestct evaluateWithObject:mobilePhone])
    {
        return MobilePhoneType_CT;
    }
    else
    {
        return MobilePhoneType_Unknown;
    }
}

- (NSString*)zw_checkBitForValidateIdentifyCardNumberSince1999;
{
    NSString* value = [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([value length] < 17)
    {
        return nil;
    }
    int summary = ([value substringWithRange:NSMakeRange(0,1)].intValue + [value substringWithRange:NSMakeRange(10,1)].intValue) *7 +
                  ([value substringWithRange:NSMakeRange(1,1)].intValue + [value substringWithRange:NSMakeRange(11,1)].intValue) *9 +
                  ([value substringWithRange:NSMakeRange(2,1)].intValue + [value substringWithRange:NSMakeRange(12,1)].intValue) *10 +
                  ([value substringWithRange:NSMakeRange(3,1)].intValue + [value substringWithRange:NSMakeRange(13,1)].intValue) *5 +
                  ([value substringWithRange:NSMakeRange(4,1)].intValue + [value substringWithRange:NSMakeRange(14,1)].intValue) *8 +
                  ([value substringWithRange:NSMakeRange(5,1)].intValue + [value substringWithRange:NSMakeRange(15,1)].intValue) *4 +
                  ([value substringWithRange:NSMakeRange(6,1)].intValue + [value substringWithRange:NSMakeRange(16,1)].intValue) *2 +
                   [value substringWithRange:NSMakeRange(7,1)].intValue *1 +
                   [value substringWithRange:NSMakeRange(8,1)].intValue *6 +
                   [value substringWithRange:NSMakeRange(9,1)].intValue *3;
    NSInteger remainder = summary % 11;
    NSString *checkString = @"10X98765432";
    return [checkString substringWithRange:NSMakeRange(remainder,1)];
}

- (BOOL)zw_isValidIdentityCardSince1999;
{
    NSString* value = [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([value length] != 18)
    {
        return NO;
    }
    NSString *mmdd = @"(((0[13578]|1[02])(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)(0[1-9]|[12][0-9]|30))|(02(0[1-9]|[1][0-9]|2[0-8])))";
    NSString *leapMmdd = @"0229";
    NSString *year = @"(19|20)[0-9]{2}";
    NSString *leapYear = @"(19|20)(0[48]|[2468][048]|[13579][26])";
    NSString *yearMmdd = [NSString stringWithFormat:@"%@%@", year, mmdd];
    NSString *leapyearMmdd = [NSString stringWithFormat:@"%@%@", leapYear, leapMmdd];
    NSString *yyyyMmdd = [NSString stringWithFormat:@"((%@)|(%@)|(%@))", yearMmdd, leapyearMmdd, @"20000229"];
    NSString *area = @"(1[1-5]|2[1-3]|3[1-7]|4[1-6]|5[0-4]|6[1-5]|82|[7-9]1)[0-9]{4}";
    NSString *regex = [NSString stringWithFormat:@"%@%@%@", area, yyyyMmdd  , @"[0-9]{3}[0-9Xx]"];
    
    NSPredicate *regexTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    if (![regexTest evaluateWithObject:value]) {
        return NO;
    }
    
    NSString *checkBit = [self zw_checkBitForValidateIdentifyCardNumberSince1999];
    // 判断校验位
    return [checkBit isEqualToString:[[value substringWithRange:NSMakeRange(17,1)] uppercaseString]];
}

- (NSString*)zw_urlEncodedString;
{
    if ([self respondsToSelector:@selector(stringByAddingPercentEncodingWithAllowedCharacters:)])
    {
        /**
         AFNetworking/AFURLRequestSerialization.m
         
         Returns a percent-escaped string following RFC 3986 for a query string key or value.
         RFC 3986 states that the following characters are "reserved" characters.
         - General Delimiters: ":", "#", "[", "]", "@", "?", "/"
         - Sub-Delimiters: "!", "$", "&", "'", "(", ")", "*", "+", ",", ";", "="
         In RFC 3986 - Section 3.4, it states that the "?" and "/" characters should not be escaped to allow
         query strings to include a URL. Therefore, all "reserved" characters with the exception of "?" and "/"
         should be percent-escaped in the query string.
         - parameter string: The string to be percent-escaped.
         - returns: The percent-escaped string.
         */
        static NSString * const kAFCharactersGeneralDelimitersToEncode = @":#[]@"; // does not include "?" or "/" due to RFC 3986 - Section 3.4
        static NSString * const kAFCharactersSubDelimitersToEncode = @"!$&'()*+,;=";
        
        NSMutableCharacterSet * allowedCharacterSet = [[NSCharacterSet URLQueryAllowedCharacterSet] mutableCopy];
        [allowedCharacterSet removeCharactersInString:[kAFCharactersGeneralDelimitersToEncode stringByAppendingString:kAFCharactersSubDelimitersToEncode]];
        static NSUInteger const batchSize = 50;
        
        NSUInteger index = 0;
        NSMutableString *escaped = @"".mutableCopy;
        
        while (index < self.length) {
            NSUInteger length = MIN(self.length - index, batchSize);
            NSRange range = NSMakeRange(index, length);
            // To avoid breaking up character sequences such as 👴🏻👮🏽
            range = [self rangeOfComposedCharacterSequencesForRange:range];
            NSString *substring = [self substringWithRange:range];
            NSString *encoded = [substring stringByAddingPercentEncodingWithAllowedCharacters:allowedCharacterSet];
            [escaped appendString:encoded];
            
            index += range.length;
        }
        return escaped;
    }
    else
    {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        CFStringEncoding cfEncoding = CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding);
        NSString *encoded = (__bridge_transfer NSString *)
        CFURLCreateStringByAddingPercentEscapes(
                                                kCFAllocatorDefault,
                                                (__bridge CFStringRef)self,
                                                NULL,
                                                CFSTR("!#$&'()*+,/:;=?@[]"),
                                                cfEncoding);
        return encoded;
#pragma clang diagnostic pop
    }
    
    /*
    //修复了iOS9上的Deprecate警告⚠️ ZhangWei 2019-05-14
    __autoreleasing NSString *encodedString;
    
    encodedString = (__bridge_transfer NSString * )
    CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                            (__bridge CFStringRef)self,
                                            NULL,
                                            (__bridge CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                            kCFStringEncodingUTF8);
    return encodedString;
     */
}

- (NSString*)zw_urlDecodedString;
{
    if ([self respondsToSelector:@selector(stringByRemovingPercentEncoding)])
    {
        return [self stringByRemovingPercentEncoding];
    }
    else
    {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        CFStringEncoding en = CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding);
        NSString *decoded = [self stringByReplacingOccurrencesOfString:@"+"
                                                            withString:@" "];
        decoded = (__bridge_transfer NSString *)
        CFURLCreateStringByReplacingPercentEscapesUsingEncoding(
                                                                NULL,
                                                                (__bridge CFStringRef)decoded,
                                                                CFSTR(""),
                                                                en);
        return decoded;
#pragma clang diagnostic pop
    }
    
    /*
    //修复了iOS9上的Deprecate警告⚠️ ZhangWei 2019-05-14
    __autoreleasing NSString *decodedString;
    
    decodedString = (__bridge_transfer NSString * )
    CFURLCreateStringByReplacingPercentEscapesUsingEncoding(
                                                            kCFAllocatorDefault,
                                                            (__bridge CFStringRef)self,
                                                            CFSTR(""),
                                                            kCFStringEncodingUTF8);
    return decodedString;
     */
}

- (NSString*)zw_percentEncodingStringByAdding;
{
    //以下注释这种方式有警告，iOS9之后废弃了此接口，，请更换新的API
    //[self stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return [self stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
}

- (NSString*)zw_percentEncodingStringByRemoving;
{
    //以下注释这种方式有警告，iOS9之后废弃了此接口，，请更换新的API
    //[self stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return [self stringByRemovingPercentEncoding];
}

- (id)zw_convertToObjectFromJsonString;
{
    NSError *error = nil;
    if (self.length > 0)
    {
        //NSJSONReadingMutableContainers 创建出来的数组和字典就是可变
        //NSJSONReadingMutableLeaves     数组或者字典里面的字符串是可变的
        //NSJSONReadingAllowFragments    允许解析出来的对象不是字典或者数组，比如直接是字符串或者NSNumber
        //kNilOptions                    如果不在乎返回的是可变的还是不可变的，它的效率最高！返回的就是不可变的
        id jsonToObject = [NSJSONSerialization JSONObjectWithData:[self dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
        if (error)
        {
            NSLog(@"💡NSJSONSerialization转换报错%@", error);
        }
        return jsonToObject;
    }
    return nil;
}

+(NSAttributedString *)zw_getStringFromHtmlString:(NSString *)str;
{
    
    NSData *stringData = [str dataUsingEncoding:NSUTF8StringEncoding];
    //NSHTMLTextDocumentType
    NSDictionary *options = @{NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute:@(NSUTF8StringEncoding)};
    
    NSAttributedString *decodedString;
    decodedString = [[NSAttributedString alloc] initWithData:stringData
                                                     options:options
                                          documentAttributes:NULL
                                                       error:NULL];
    
    
    return decodedString;
}

- (NSString*)zw_substringFromString:(NSString*)startString toString:(NSString*)endString containSearchString:(BOOL)isContain;
{
    if (startString && [startString isKindOfClass:[NSString class]] && startString.length > 0 &&
        endString && [endString isKindOfClass:[NSString class]] && endString.length > 0)
    {
        NSRange startRange = [self rangeOfString:startString];
        NSRange endRange = [self rangeOfString:endString options:NSBackwardsSearch];
        if (startRange.length > 0 && endRange.length > 0 && startRange.location <= endRange.location)
        {
            NSInteger subStringRangeLocation = startRange.location + (isContain?0:startRange.length);
            NSInteger subStringRangeLength = endRange.location + (isContain?endRange.length:0) - subStringRangeLocation;
            if (subStringRangeLength > 0)
            {
                NSRange range = NSMakeRange(subStringRangeLocation, subStringRangeLength);
                return [self substringWithRange:range];
            }
            return nil;
        }
        else
        {
            return nil;
        }
    }
    return nil;
}

- (NSData *)zw_convertToDataFromHexString;
{
    NSMutableData *hexData = [[NSMutableData alloc] initWithCapacity:20];
    NSRange range;
    if ([self length] % 2 == 0)
    {
        range = NSMakeRange(0, 2);
    }
    else
    {
        range = NSMakeRange(0, 1);
    }
    for (NSInteger i = range.location; i < [self length]; i += 2)
    {
        unsigned int anInt;
        NSString *hexCharStr = [self substringWithRange:range];
        NSScanner *scanner = [[NSScanner alloc] initWithString:hexCharStr];
        
        [scanner scanHexInt:&anInt];
        NSData *entity = [[NSData alloc] initWithBytes:&anInt length:1];
        [hexData appendData:entity];
        
        range.location += range.length;
        range.length = 2;
    }
    return hexData;
}

- (NSNumber *)zw_convertToNumber;
{
    NSNumberFormatter *tmpFormatter = [[NSNumberFormatter alloc] init];
    tmpFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *convertNumber = [tmpFormatter numberFromString:self];
    return convertNumber;
}

//MARK:搜索是否包含关键字
- (BOOL)zw_containsStringCaseInsensitiveSearch:(NSString *)searchText;
{
    if (searchText.length <= 0)
    {
        return NO;
    }
    BOOL suc = [self zw_containsString:searchText options:NSCaseInsensitiveSearch];
    return suc;
}

- (BOOL)zw_containsString:(NSString *)searchText options:(NSStringCompareOptions)options;
{
    if (searchText.length <= 0)
    {
        return NO;
    }
    return [self rangeOfString:searchText options:options].location != NSNotFound;
}

@end
