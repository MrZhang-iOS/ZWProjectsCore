//
//  NSData+ZhangWei.h
//  ZWProjectsCore
//
//  Created by ZhangWei on 16/8/10.
//  Copyright © 2016年 ZhangWei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (ZhangWei)

- (NSString *)zw_byteSize;

- (NSData *)zw_getSubDataWithRange:(NSRange)range;

//将数据进行UTF-8格式转换成字符串
- (NSString *)zw_convertToString;
//有些网络请求返回的数据流不是UTF-8格式的，解析会返回nil，需要通过以下格式解析
- (NSString *)zw_convertToStringWithGBKFormat:(BOOL)isGBKFormat;

//将json数据流转换成模型
- (NSObject *)zw_convertToObjectFromJsonData;
//将GBK格式的json数据流转换成模型
- (NSObject *)zw_convertToObjectFromJsonDataWithGBKFormat:(BOOL)isGBKFormat error:(NSError **)error;

//把数据转换成16进制字符串
- (NSString *)zw_convertToHexString;

@end
