//
//  NSTimer+ZhangWei.h
//  ZWProjectsCore
//
//  Created by ZhangWei on 15/09/10.
//  Copyright © 2015年 ZhangWei. All rights reserved.
//

#import "NSTimer+ZhangWei.h"

@implementation NSTimer (ZhangWei)

+(id)zw_scheduledTimerWithTimeInterval:(NSTimeInterval)timeInterval block:(void (^)(void))arcBlock repeats:(BOOL)yesOrNo;
{
    void (^block)(void) = [arcBlock copy];
    id ret = [self scheduledTimerWithTimeInterval:timeInterval target:self selector:@selector(executeBlock:) userInfo:block repeats:yesOrNo];
    return ret;
}

+(id)zw_timerWithTimeInterval:(NSTimeInterval)timeInterval block:(void (^)(void))arcBlock repeats:(BOOL)yesOrNo;
{
    void (^block)(void) = [arcBlock copy];
    id ret = [self timerWithTimeInterval:timeInterval target:self selector:@selector(executeBlock:) userInfo:block repeats:yesOrNo];
    return ret;
}

+(void)executeBlock:(NSTimer *)arcBlockTimer;
{
    if([arcBlockTimer userInfo])
    {
        void (^block)(void) = (void (^)(void))[arcBlockTimer userInfo];
        block();
    }
}

@end
