//
//  NSDictionary+ZhangWei.h
//  ZWProjectsCore
//
//  Created by ZhangWei on 14-3-31.
//  Copyright (c) 2014年 ZhangWei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSDictionary (ZhangWei)

/// 以下方法均已经剔除了【NSNull】模型
- (BOOL)zw_containsKey:(NSString*)key;
- (id)zw_getObjectForKey:(NSString*)key;
- (NSString *)zw_getNSStringObjectForKey:(NSString *)key;
- (NSNumber *)zw_getNSNumberObjectForKey:(NSString *)key;
- (NSArray *)zw_getNSArrayObjectForKey:(NSString *)key;
- (NSDictionary *)zw_getNSDictionaryObjectForKey:(NSString *)key;
- (NSData *)zw_getNSDataObjectForKey:(NSString *)key;
- (UIColor *)zw_getUIColorObjectForKey:(NSString *)key;

/**
 * 将字典中包含的【Nul】l对象用【@""】替换掉（递归操作）。
 * ⚠️NSUserDefault在存储含NSNull的NSDictionary数据时会导致App崩溃。
 */
- (NSDictionary *)zw_replaceNullsWithBlanks;

/// 根据属性相关的字典数据生成参考代码。
- (void)zw_autoCreatePropertyCode;

@end
