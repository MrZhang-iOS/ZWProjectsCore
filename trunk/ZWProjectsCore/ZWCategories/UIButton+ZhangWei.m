//
//  UIButton+ZhangWei.m
//  ZWProjectsCore
//
//  Created by ZhangWei on 16/3/23.
//  Copyright © 2016年 ZhangWei. All rights reserved.
//

#import "UIButton+ZhangWei.h"
#import <objc/runtime.h>

@implementation UIButton (ZhangWei)

static char expandEventSizeKey;

//分类的方式重构系统方法时不要直接覆盖系统方法，使用runtime置换掉系统方法，防止与相同分类模块冲突造成异常
+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        Class instanceClass = [self class];
        
        SEL originalInstanceSelector = @selector(pointInside:withEvent:);
        SEL swizzledInstanceSelector = @selector(zw_pointInside:withEvent:);
        
        Method originalInstanceMethod = class_getInstanceMethod(instanceClass, originalInstanceSelector);
        Method swizzledInstanceMethod = class_getInstanceMethod(instanceClass, swizzledInstanceSelector);
        
        if (originalInstanceMethod && swizzledInstanceMethod)
        {
            //class_addMethod添加swizzledMethod的实现IMP到originalSelector中（只是添加，不会覆盖originalSelector的原有实现），
            //当且仅当originalSelector为当前类的父类中的函数（无论实例方法还是类方法）时才能添加成功。
            //进行类方法交换时，参数必须为元类
            //1.类本身（分类算作类本身）有实现需要被替换的方法（无论实例方法还是类方法）时，class_addMethod方法返回NO，
            //这种情况交换两个方法的实现就可以了。
            //2.类本身没有实现需要被替换的方法（无论实例方法还是类方法）时，其父类有实现方法（可继承使用），class_addMethod方法返回YES，
            //这种情况重置原方法的实现到新方法名的映射就可以了。
            //
            //也可以直接使用method_exchangeImplementations方法，绕开class_addMethod。
            BOOL isMethodAdd = class_addMethod(instanceClass, originalInstanceSelector, method_getImplementation(swizzledInstanceMethod), method_getTypeEncoding(swizzledInstanceMethod));
            if (isMethodAdd)
            {
                class_replaceMethod(instanceClass, swizzledInstanceSelector, method_getImplementation(originalInstanceMethod), method_getTypeEncoding(originalInstanceMethod));
            }
            else
            {
                method_exchangeImplementations(originalInstanceMethod, swizzledInstanceMethod);
            }
        }
        else
        {
            NSLog(@"❌❌❌没有找到指定的实例方法，交换失败！");
        }
    });
}

- (void)zw_disableContinueThreeSeconds;
{
    [self zw_disableWithDuration:3];
}

- (void)zw_disableWithDuration:(NSTimeInterval)fInterval;
{
    __weak typeof(self) wSelf = self;
    void (^task)(void) = ^{
        wSelf.enabled = NO;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(fInterval * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            wSelf.enabled = YES;
        });
    };
    
    if (strcmp(dispatch_queue_get_label(DISPATCH_CURRENT_QUEUE_LABEL), dispatch_queue_get_label(dispatch_get_main_queue())) == 0) {
        // 主队列
        task();
    } else {
        // 非主队列
        dispatch_async(dispatch_get_main_queue(), task);
    }
}

- (void)zw_expandEventSize:(CGSize)size;
{
    //OBJC_EXPORT void objc_setAssociatedObject(id object, const void *key, id value, objc_AssociationPolicy policy)
    //OBJC_EXPORT 打包lib时，用来说明该函数是暴露给外界调用的。
    //id object 表示关联者，是一个对象
    //id value 表示被关联者，可以理解这个value最后是关联到object上的
    //const void *key 被关联者也许有很多个，所以通过key可以找到指定的那个被关联者
    
    objc_setAssociatedObject(self, &expandEventSizeKey, NSStringFromCGSize(size), OBJC_ASSOCIATION_COPY_NONATOMIC);
}

#pragma mark 增大按钮响应范围的相关方法
/// 获取设置的扩大size，来扩大button的rect
/// 当前只是设置了一个扩大的size，当然也可以设置4个扩大的size，上下左右，具体扩大多少对应button的四个边传入对应的size
- (CGRect)expandEventRect;
{
    NSString *expandEventSizeString = objc_getAssociatedObject(self, &expandEventSizeKey);
    
    if (expandEventSizeString)
    {
        CGSize expandEventSize = CGSizeFromString(expandEventSizeString);
        return CGRectMake(self.bounds.origin.x - expandEventSize.width,
                          self.bounds.origin.y - expandEventSize.height,
                          self.bounds.size.width + expandEventSize.width*2,
                          self.bounds.size.height + expandEventSize.height*2);
    }
    else
    {
        return self.bounds;
    }
}

/// 响应用户的点击事件.
/// ⚠️注意分类中同名方法的调用顺序。
/// ⚠️普通同名方法中优先级高的会覆盖优先级低的（分类>本类>父类）。
/// ⚠️如有多个分类的同名方法，只执行最后一个编译的同名方法，分类的同名方法会覆盖本类的同名方法，子类的同名方法会覆盖父类的同名方法。
/// ⚠️所以此处使用了方法交换，防止工程关联了多个UIButton的分类时，系统方法【-pointInside:withEvent:】被最后一个UIButton分类中同名方法抢占调用而导致此处逻辑无法得到响应。
- (BOOL)zw_pointInside:(CGPoint)point withEvent:(UIEvent *)event;
{
    CGRect buttonEventRect = [self expandEventRect];
    if (CGRectEqualToRect(buttonEventRect, self.bounds))
    {
        return [self zw_pointInside:point withEvent:event];
    }
    return CGRectContainsPoint(buttonEventRect, point) ? YES : NO;
}

@end
