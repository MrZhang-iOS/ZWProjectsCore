//
//  UIViewController+ZWPresentQueue.m
//  ZWProjectsCore
//
//  Created by ZhangWei on 2017/11/6.
//  Copyright © 2017年 ZhangWei. All rights reserved.

#import "UIViewController+ZWPresentQueue.h"
#import <objc/runtime.h>

static dispatch_queue_t lifoQueue = nil;

@implementation UIViewController (ZWPresentQueue)

+ (void)load;
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        Class tmpClass = [self class];
        
        //在iOS13上调用presentViewController:默认不再使用UIModalPresentationFullScreen，可能会导致viewDidDisappear不被执行
        SEL originalSelector = @selector(viewDidDisappear:);
        SEL swizzledSelector = @selector(PresentQueue_viewDidDisappear:);
        
        Method originalMethod = class_getInstanceMethod(tmpClass, originalSelector);
        Method swizzledMethod = class_getInstanceMethod(tmpClass, swizzledSelector);

        if (originalMethod && swizzledMethod)
        {
            //添加IMPswizzlingMethod到SELoriginSel中（只是添加，不会覆盖orginSel的原有实现），当且仅当originSel为当前类的父类方法时才能添加成功。
            BOOL isMethodAdd = class_addMethod(tmpClass, originalSelector, method_getImplementation(swizzledMethod), method_getTypeEncoding(swizzledMethod));
            if (isMethodAdd)
            {
                class_replaceMethod(tmpClass, swizzledSelector, method_getImplementation(originalMethod), method_getTypeEncoding(originalMethod));
            }
            else
            {
                method_exchangeImplementations(originalMethod, swizzledMethod);
            }
        }
        else
        {
            NSLog(@"交换方法失败");
        }
    });
}

- (void)PresentQueue_viewDidDisappear:(BOOL)animated;
{
    [self PresentQueue_viewDidDisappear:animated];
    //只有在非临时关闭时，才需要执行销毁回调
    if ([self isTemporarilyDismissed])
    {
        //只要存在临时释放回调，即执行此操作，一旦执行完马上自毁，防止临时回调被多次执行
        if ([self getTempDismissCompletion])
        {
            [self getTempDismissCompletion](self);
            [self setTempDismissCompletion:nil];
        }
    }
    else
    {
        if ([self getDeallocCompletion])
        {
            [self getDeallocCompletion](self);
            [self setDeallocCompletion:nil];
        }
    }
}

/**
 *将储存队列的数组取消单例创建方式。 ZhangWei 2017-09-19
 *修改的原因:
 *   控制器切换后，其上显示的UIAlertViewCtrl如果没有处理，该控制器即被释放，
 *   将导致队里数组中的其他任务永远无法执行，且新增的任务也会一直处于等待状态。
 *   最直接的例子就是UIAlertViewCtrl弹框不显示。
 */
- (void)setOperationQueue:(NSOperationQueue *)operationQueue;
{
    objc_setAssociatedObject(self, @selector(getOperationQueue), operationQueue, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSOperationQueue *)getOperationQueue;
{
    @synchronized(self)
    {
        NSOperationQueue* pTemp = objc_getAssociatedObject(self, _cmd);
        if (pTemp == nil)
        {
            pTemp = [NSOperationQueue new];
            [self setOperationQueue:pTemp];
        }
        return pTemp;
    }    
}

/**
 *将储存队列的数组取消单例创建方式。 ZhangWei 2017-09-19
 *修改的原因:
 *   控制器切换后，其上显示的UIAlertViewCtrl如果没有处理，该控制器即被释放，
 *   将导致队里数组中的其他任务永远无法执行，且新增的任务也会一直处于等待状态。
 *   最直接的例子就是UIAlertViewCtrl弹框不显示。
 */
- (void)setStackControllers:(NSMutableArray *)stackControllers;
{
    objc_setAssociatedObject(self, @selector(getStackControllers), stackControllers, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSMutableArray *)getStackControllers;
{
    @synchronized(self)
    {
        NSMutableArray* pTemp = objc_getAssociatedObject(self, _cmd);
        if (pTemp == nil)
        {
            pTemp = [NSMutableArray array];
            [self setStackControllers:pTemp];
        }
        return pTemp;
    }
}

- (void)setPresentCompletion:(void (^)(UIViewController *viewController))completion;
{
    objc_setAssociatedObject(self, @selector(getPresentCompletion), completion, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (void (^)(UIViewController *viewController))getPresentCompletion;
{
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setDismissCompletion:(void (^)(UIViewController *viewController))completion;
{
    objc_setAssociatedObject(self, @selector(getDismissCompletion), completion, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (void (^)(UIViewController *viewController))getDismissCompletion;
{
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setDeallocCompletion:(void (^)(UIViewController *viewController))completion;
{
    objc_setAssociatedObject(self, @selector(getDeallocCompletion), completion, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (void (^)(UIViewController *viewController))getDeallocCompletion;
{
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setTempDismissCompletion:(void (^)(UIViewController *viewController))completion;
{
    objc_setAssociatedObject(self, @selector(getTempDismissCompletion), completion, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (void (^)(UIViewController *viewController))getTempDismissCompletion;
{
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setTemporarilyDismissed:(BOOL)temporarilyDismissed;
{
    objc_setAssociatedObject(self, @selector(isTemporarilyDismissed), @(temporarilyDismissed), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (BOOL)isTemporarilyDismissed;
{
    NSNumber *num = objc_getAssociatedObject(self, _cmd);
    return [num boolValue];
}

- (void)setDismissing:(BOOL)dismissing;
{
    objc_setAssociatedObject(self, @selector(isDismissing), @(dismissing), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (BOOL)isDismissing;
{
    NSNumber *num = objc_getAssociatedObject(self, _cmd);
    return [num boolValue];
}

- (void)zw_clearPresentedViewControllers;
{
    @synchronized(self)
    {
        [[self getOperationQueue] cancelAllOperations];
        [[self getStackControllers] removeAllObjects];
    }
}

- (void)zw_presentViewController:(UIViewController *)controller
               presentCompletion:(void (^)(UIViewController *viewController))presentCompletion
               dismissCompletion:(void (^)(UIViewController *viewController))dismissCompletion;
{
    if (controller && [controller isKindOfClass:[UIViewController class]])
    {
        [self lifoPresentViewController:controller presentCompletion:presentCompletion dismissCompletion:dismissCompletion];
    }
}

- (void)lifoPresentViewController:(UIViewController *)controller
                presentCompletion:(void (^)(UIViewController *viewController))presentCompletion
                dismissCompletion:(void (^)(UIViewController *viewController))dismissCompletion;
{
    __weak typeof(self) wSelf = self;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        lifoQueue = dispatch_queue_create("ZWPresentQueue-LIFO", DISPATCH_QUEUE_CONCURRENT);
    });
    
    dispatch_barrier_async(lifoQueue, ^{
        //将需要弹出显示的控制器在指定控制器中存储起来，直接操作视图控制器，防止异步时被释放
        if (controller && ![[wSelf getStackControllers] containsObject:controller])
        {
            [[wSelf getStackControllers] addObject:controller];
            [controller setPresentCompletion:presentCompletion];
            [controller setDismissCompletion:dismissCompletion];
            [controller setDeallocCompletion:^(UIViewController *viewController) {
                [viewController setDismissing:YES];
                
                if (viewController)
                {
                    [[wSelf getStackControllers] removeObject:viewController];
                }
                
                if ([viewController getDismissCompletion])
                {
                    [viewController getDismissCompletion](viewController);
                }
                
                [viewController setDismissing:NO];
                
                [wSelf gotoNextPresentedViewController];
            }];
        }
        
        //为该控制器创建队列任务
        __weak typeof(controller) weakController = controller;
        NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^{
            //创建信号量
            dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
            
            //如果控制器正在执行临时关闭或者关闭动作，则直接中断后续操作
            //增加了当前弹框前，栈中是否还存在其他弹框
            if ([wSelf getStackControllers].count > 1)
            {
                //对可变数组执行变量时容易导致AppCrash（⚠️遍历时可变数组发生增减变更）ZhangWei 2019-06-13
                NSArray* copyStackControllers = [[wSelf getStackControllers] copy];
                for (UIViewController *preController in copyStackControllers)
                {
                    if ([preController isDismissing] || [preController isTemporarilyDismissed])
                    {
                        return ;
                    }
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                //判断当前指定控制器是否存在已经弹出显示了另外一个控制器。如果已经存在，则先临时关闭这个已经弹出显示的控制器，再从当前指定的控制器弹出显示新的控制器；如果不存在，则直接从当前指定的控制器弹出显示新的控制器
                [wSelf temporarilyDismissViewControllerAnimated:YES completion:^{
                    UIPopoverPresentationController* popover = weakController.popoverPresentationController;
                    if (popover)
                    {
                        //箭头所指的对应的视图,sourceRect会以这个视图的左上角为原点.使用这个属性和sourceRect计算箭头所处的锚点.如果不设置,sourceRect就会不起作用
                        popover.sourceView = wSelf.view;
                        popover.permittedArrowDirections = UIPopoverArrowDirectionAny;
                        CGSize screenSize = [UIScreen mainScreen].bounds.size;
                        popover.sourceRect = CGRectMake(screenSize.width/2.0,screenSize.height-44,0,0);
                    }
                    if (weakController)
                    {
                        //参数weakController为nil会导致crash
                        weakController.modalPresentationStyle = UIModalPresentationFullScreen;
                        [wSelf presentViewController:weakController animated:YES completion:^{
                            //释放信号量
                            dispatch_semaphore_signal(semaphore);
                            if (presentCompletion)
                            {
                                presentCompletion(weakController);
                            }
                        }];
                    }
                    else
                    {
                        //释放信号量
                        dispatch_semaphore_signal(semaphore);
                        [wSelf gotoNextPresentedViewController];
                    }
                }];
            });
            
            //阻塞信号量
            dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        }];
        
        NSOperationQueue * pTempQueue = [wSelf getOperationQueue];
        NSOperation* lastOperation = pTempQueue.operations.lastObject;
        if (lastOperation)
        {
            [operation addDependency:lastOperation];
        }
        [pTempQueue addOperation:operation];
    });
}

//临时关闭被当前视图控制器模态出的视图控制器
- (void)temporarilyDismissViewControllerAnimated: (BOOL)flag completion: (void (^)(void))completion;
{
    UIViewController* presentedCtrl = self.presentedViewController;
    if (nil == presentedCtrl || (presentedCtrl && YES == presentedCtrl.isBeingDismissed))
    {
        //模态视图控制器不存在或者正在消失
        if (completion)
        {
            completion();
        }
    }
    else
    {
        [presentedCtrl setTempDismissCompletion:^(UIViewController *viewController) {
            [viewController setTemporarilyDismissed:NO];
            if (completion)
            {
                completion();
            }
        }];
        
        [presentedCtrl setTemporarilyDismissed:YES];
        //💡因为此系统API的completion回调有时不工作，所以自定义了TempDismissCompletion回调
        [self dismissViewControllerAnimated:flag completion:nil];
    }
}

//进入下一个需要被模态出来的视图控制器
- (void)gotoNextPresentedViewController;
{
    //从指定控制器中查找下一个需要弹出显示的控制器
    if ([self getStackControllers].count > 0)
    {
        UIViewController *nextController = [[self getStackControllers] lastObject];
        if (nextController)
        {
            [self lifoPresentViewController:nextController presentCompletion:[nextController getPresentCompletion] dismissCompletion:[nextController getDismissCompletion]];
        }
    }
}

@end
