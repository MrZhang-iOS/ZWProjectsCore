//
//  NSDate+ZhangWei.h
//  ZWProjectsCore
//
//  Created by ZhangWei on 2018/1/8.
//  Copyright © 2018年 ZhangWei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (ZhangWei)

/**
 * 以系统时区为基准，判断date1和date2是不是在同一天内
 */
+ (BOOL)zw_isInSameDayWithDate1:(NSDate*)date1 date2:(NSDate*)date2;

/**
 * 以指定时区为基准，判断date1和date2是不是在同一天内
 * 💡注意：时区如果为nil，则以系统时区为基准
 */
+ (BOOL)zw_isInSameDayWithDate1:(NSDate*)date1 date2:(NSDate*)date2 timeZone:(NSTimeZone*)timeZone;

/**
 * 以中国时区为基准，判断date1和date2是不是在同一天内
 * 💡注意：时区如果为nil，则以系统时区为基准
 */
+ (BOOL)zw_isInSameDayAtChinaWithDate1:(NSDate*)date1 date2:(NSDate*)date2;

@end
