//
//  NSMutableAttributedString+ZhangWei.h
//  ZWProjectsCore
//
//  Created by ZhangWei on 22/01/2018.
//  Copyright © 2018 ZhangWei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSMutableAttributedString (ZhangWei)

/// 为字符串添加指定的字体和颜色。
/// - Parameters:
///   - string: 预期处理的字符串。
///   - font: 预期的文字字体。
///   - color: 预期的文字颜色。
+ (NSMutableAttributedString*)zw_createWithString:(NSString*)string
                                             font:(UIFont*)font
                                            color:(UIColor*)color;

/// 为子字符串添加字体
/// - Parameters:
///   - font: 预期的文字字体。
///   - substring: 预期处理的子字符串。
- (void)zw_addFont:(UIFont *)font substring:(NSString *)substring;

/// 为子字符串添加文字颜色。
/// - Parameters:
///   - color: 预期的文字颜色。
///   - substring: 预期处理的子字符串。
- (void)zw_addColor:(UIColor *)color substring:(NSString *)substring;

/// 为子字符串添加背景颜色。
/// - Parameters:
///   - color: 预期的背景颜色。
///   - substring: 预期处理的子字符串。
- (void)zw_addBackgroundColor:(UIColor *)color substring:(NSString *)substring;

/// 为子字符串设置文字间距。
/// - Parameters:
///   - spacing: 预期的文字间距值。
///   - substring: 预期处理的子字符串。
- (void)zw_addCharacterSpacing:(CGFloat)spacing substring:(NSString*)substring;

/// 为子字符串添加段落样式
/// - Parameters:
///   - block: 设置行高的代码块
///   - substring: 设置行高的子字符串
/// 示例代码如下
///    [aString zw_addParagraphStyle:^(NSMutableParagraphStyle *paragraphStyle) {
///       paragraphStyle.minimumLineHeight = 13;
///       paragraphStyle.maximumLineHeight = 13;
///    }
///                     substring:displayStr];
- (void)zw_addParagraphStyle:(void(^)(NSMutableParagraphStyle *paragraphStyle))block substring:(NSString*)substring;

@end
