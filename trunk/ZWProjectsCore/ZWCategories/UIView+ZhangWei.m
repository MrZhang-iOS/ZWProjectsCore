//
//  UIView+ZhangWei.m
//  ZWProjectsCore
//
//  Created by ZhangWei on 2013/05/15.
//  Copyright (c) 2013年 ZhangWei. All rights reserved.
//

#import "UIView+ZhangWei.h"
#import <Masonry/Masonry.h>

@implementation UIView (ZhangWei)

- (CGFloat)top
{
    return self.frame.origin.y;
}

- (void)setTop:(CGFloat)y
{
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}

- (CGFloat)right
{
    return self.frame.origin.x + self.frame.size.width;
}

- (void)setRight:(CGFloat)right
{
    CGRect frame = self.frame;
    frame.origin.x = right - self.frame.size.width;
    self.frame = frame;
}

- (CGFloat)bottom
{
    return self.frame.origin.y + self.frame.size.height;
}

- (void)setBottom:(CGFloat)bottom
{
    CGRect frame = self.frame;
    frame.origin.y = bottom - self.frame.size.height;
    self.frame = frame;
}

- (CGFloat)left
{
    return self.frame.origin.x;
}

- (void)setLeft:(CGFloat)x
{
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
}

- (CGFloat)width
{
    return self.frame.size.width;
}

- (void)setWidth:(CGFloat)width
{
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}

- (CGFloat)height
{
    return self.frame.size.height;
}

- (void)setHeight:(CGFloat)height
{
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}

- (UIViewController *)zw_getViewController
{
    /// Finds the view's view controller.
    
    // Traverse responder chain. Return first found view controller, which will be the view's view controller.
    UIResponder *responder = self;
    while ((responder = [responder nextResponder]))
    {
        if ([responder isKindOfClass: [UIViewController class]])
        {
            return (UIViewController *)responder;
        }
    }
    
    // If the view controller isn't found, return nil.
    return nil;
}

- (void)zw_removeAllSubviews;
{
    [self.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
}

- (UIImage*)zw_makeImage;
{
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, [UIScreen mainScreen].scale);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage*image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (BOOL)zw_anySubViewScrolling;
{
    if ([self isKindOfClass:[UIScrollView class]])
    {
        UIScrollView *scrollView = (UIScrollView *)self;
        if (scrollView.dragging || scrollView.decelerating)
        {
            return YES;
        }
    }
    for (UIView *theSubView in self.subviews)
    {
        if ([theSubView zw_anySubViewScrolling])
        {
            return YES;
        }
    }
    return NO;
}

- (BOOL)zw_isSubViewOfView:(UIView*)view;
{
    return [self isDescendantOfView:view];
}

- (void)zw_startShakeAnimationWithRadians:(NSInteger)degrees;
{
    void (^task)(void) = ^{
        self.transform = CGAffineTransformRotate(CGAffineTransformIdentity, (((-degrees) * M_PI) / 180.0));
        
        [UIView animateWithDuration:0.08
                              delay:0.0
                            options:(UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse)
                         animations:^
         {
             self.transform = CGAffineTransformRotate(CGAffineTransformIdentity, (((degrees) * M_PI) / 180.0));
         }
                         completion:nil];
    };
    
    if (strcmp(dispatch_queue_get_label(DISPATCH_CURRENT_QUEUE_LABEL), dispatch_queue_get_label(dispatch_get_main_queue())) == 0) {
        // 主队列
        task();
    } else {
        // 非主队列
        dispatch_async(dispatch_get_main_queue(), task);
    }
}

- (void)zw_stopShakeAnimation;
{
    void (^task)(void) = ^{
        [UIView animateWithDuration:0.08
                              delay:0.0
                            options:(UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveLinear)
                         animations:^
         {
             self.transform = CGAffineTransformIdentity;
         }
                         completion:nil];
    };
    
    if (strcmp(dispatch_queue_get_label(DISPATCH_CURRENT_QUEUE_LABEL), dispatch_queue_get_label(dispatch_get_main_queue())) == 0) {
        // 主队列
        task();
    } else {
        // 非主队列
        dispatch_async(dispatch_get_main_queue(), task);
    }
}

- (void)zw_uninstallAllConstraints;
{
    NSArray *installedConstraints = [MASViewConstraint installedConstraintsForView:self];
    for (MASConstraint *constraint in installedConstraints)
    {
        [constraint uninstall];
    }
}

- (void)zw_makeLayerCircleWithBorderWidth:(CGFloat)borderWidth borderColor:(UIColor *)borderColor; {
    CGFloat fWidht = self.bounds.size.width;
    CGFloat fHeight = self.bounds.size.height;
    UIBezierPath *layerPath = [UIBezierPath bezierPathWithArcCenter:CGPointMake(fWidht/2, fHeight/2)
                                                             radius:MIN(fWidht, fHeight)/2
                                                         startAngle:0
                                                           endAngle:(M_PI * 2)
                                                          clockwise:YES];
    /// 为layer的mask 创建一个新的mask时，这个新的mask不能有superlayer 和sublayer。（官方文档的说明）.
    /// mask 可以配合CAGradientLayer、CAShapeLayer 使用。可以实现蒙层透明度、显示不同形状图层、图案镂空、文字变色等等功能。
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = self.bounds;
    maskLayer.path = layerPath.CGPath;
    /// mask 的作用就是让父图层与mask重叠的部分区域可见 , 通俗的说就是mask图层实心的部分将会被保留下来，mask的其余部分则会被抛弃。
    self.layer.mask = maskLayer;
    self.layer.sublayers = nil;
    if (borderWidth > 0 && borderColor) {
        CAShapeLayer *borderLayer = [CAShapeLayer layer];
        borderLayer.frame = self.bounds;
        borderLayer.path = layerPath.CGPath;
        // ⚠️填充成透明色将会透视当前视图的所有子视图。
        borderLayer.fillColor = [UIColor clearColor].CGColor;
        borderLayer.strokeColor = borderColor.CGColor;
        // 用贝赛尔曲线画线，path 其实是在线的中间，这样会被 layer.mask（遮罩层)遮住一半，故在 halfWidth 处新建 path，刚好产生一个内描边
        borderLayer.lineWidth = borderWidth*2;
        [self.layer addSublayer:borderLayer];
    }
}

- (void)zw_makeLayerRoundWithCornerRadius:(CGFloat)cornerRadius rectCorner:(UIRectCorner)rectCorner borderWidth:(CGFloat)borderWidth borderColor:(UIColor *)borderColor; {
    UIBezierPath* layerPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds
                                                    byRoundingCorners:rectCorner
                                                          cornerRadii:CGSizeMake(cornerRadius, cornerRadius)];
    /// 为layer的mask 创建一个新的mask时，这个新的mask不能有superlayer 和sublayer。（官方文档的说明）.
    /// mask 可以配合CAGradientLayer、CAShapeLayer 使用。可以实现蒙层透明度、显示不同形状图层、图案镂空、文字变色等等功能。
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = self.bounds;
    maskLayer.path = layerPath.CGPath;
    /// mask 的作用就是让父图层与mask重叠的部分区域可见 , 通俗的说就是mask图层实心的部分将会被保留下来，mask的其余部分则会被抛弃。
    self.layer.mask = maskLayer;
    self.layer.sublayers = nil;
    if (borderWidth > 0 && borderColor) {
        CAShapeLayer *borderLayer = [CAShapeLayer layer];
        borderLayer.frame = self.bounds;
        borderLayer.path = layerPath.CGPath;
        // ⚠️填充成透明色将会透视当前视图的所有子视图。
        borderLayer.fillColor = [UIColor clearColor].CGColor;
        borderLayer.strokeColor = borderColor.CGColor;
        // 用贝赛尔曲线画线，path 其实是在线的中间，这样会被 layer.mask（遮罩层)遮住一半，故在 halfWidth 处新建 path，刚好产生一个内描边
        borderLayer.lineWidth = borderWidth*2;
        [self.layer addSublayer:borderLayer];
    }
}

@end
