//
//  UIFont+ZhangWei.h
//  ZWProjectsCore
//
//  Created by ZhangWei on 15-1-8.
//  Copyright © 2015年 ZhangWei. All rights reserved.
//

#import "UIFont+ZhangWei.h"
#import "ZWProjectsCoreMacros.h"

@implementation UIFont (ZhangWei)

+ (UIFont *)zw_getFontForHeight:(CGFloat)height fontName:(NSString *)targetFontName lineBreakMode:(NSLineBreakMode)enMode
{
    NSString* pTempString = @"Test";
    CGFloat fTempFontSize = 12;
    UIFont *tempFont;
    if (targetFontName.length > 0)
    {
        tempFont = [UIFont fontWithName:targetFontName size:fTempFontSize];
    }
    if (tempFont == nil)
    {
        tempFont = [UIFont systemFontOfSize:fTempFontSize];
    }
    CGSize testSize = MB_MULTILINE_TEXTSIZE(pTempString, tempFont, CGSizeMake(CGFLOAT_MAX, height));
    if (testSize.height <= 0)
    {
        testSize.height = 12;
    }
    float pointsPerPixel = fTempFontSize / testSize.height; //计算字体大小和高度比例
    float desiredFontSize = height * pointsPerPixel;
    return [UIFont fontWithName:targetFontName size:ceilf(desiredFontSize)];
}

@end
