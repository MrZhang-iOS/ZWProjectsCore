//
//  UIButton+ZhangWei.h
//  ZWProjectsCore
//
//  Created by ZhangWei on 16/3/23.
//  Copyright © 2016年 ZhangWei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (ZhangWei)

/**
 * 使按钮失效3秒或者数秒，再恢复可点击状态。（防止操作太快，按钮被多次点击）
 * ⚠️注意：不适合具有disable状态的按钮。
 */
- (void)zw_disableContinueThreeSeconds;
- (void)zw_disableWithDuration:(NSTimeInterval)fInterval;

/**
 * 缩放 按钮的响应范围
 * @param size 宽值表示水平方向两边各缩放的值，高值表示垂直方向两边各缩放的值。
 */
- (void)zw_expandEventSize:(CGSize)size;

@end
