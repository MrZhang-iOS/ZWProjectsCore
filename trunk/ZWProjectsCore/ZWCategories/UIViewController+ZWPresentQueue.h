//
//  UIViewController+ZWPresentQueue.h
//  ZWProjectsCore
//
//  Created by ZhangWei on 2017/11/6.
//  Copyright © 2017年 ZhangWei. All rights reserved.

#import <UIKit/UIKit.h>

@interface UIViewController (ZWPresentQueue)

- (void)zw_presentViewController:(UIViewController *)controller
               presentCompletion:(void (^)(UIViewController *viewController))presentCompletion
               dismissCompletion:(void (^)(UIViewController *viewController))dismissCompletion;

- (void)zw_clearPresentedViewControllers;

@end

/*
 该分类设计目的：
 基于iOS系统特性：一个视图控制器只能模态出一个视图控制器。
 针对UIAlertController模拟iOS6以前UIAlertView那种弹框效果。（后入先出，即LIFO模式）
 
 参考了Github相关源码：https://github.com/HJaycee/JCAlertController
 */
