//
//  UIImage+ZhangWei.h
//  ZWProjectsCore
//
//  Created by ZhangWei on 14-3-28.
//  Copyright (c) 2014年 ZhangWei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ZhangWei)

//截取原图片指定区域的图像
- (UIImage*)zw_screenShotWithRect:(CGRect)rect;//rect超过原图的区域将被忽略

//生成按原比例缩放的图片,如果newsize大于当前size，图片不处理，直接返回【注：图片的宽高会放大一倍】
- (UIImage*)zw_originalScaleZoomWithSize:(CGSize)newsize;//newsize的比例如果不符,在新的区域上取原比例最大的区域

//和以上方法一样，但当尺寸超过时继续创建
- (UIImage*)zw_originalScaleZoomWithSize:(CGSize)newsize stillWhenSizeBeyond:(BOOL)bFlag;

//生成指定比例和大小的图片
- (UIImage*)zw_zoomWithSize:(CGSize)newsize;//按newsize的比例缩放

//isAcpect为YES时生成原比例和新size大小的图片，比例不同时，与新尺寸同中心
- (UIImage*)zw_zoomWithSize:(CGSize)newsize aspect:(BOOL)isAcpect;

/**
 *  把图片缩放并剪裁到指定大小，比例为newSize的比例，如果图片的比例不同，则将以图片中心裁剪图片
 *  首先按UIViewContentModeScaleAspectFill缩放图片填充newSize
 *  然后裁剪掉newSize区间以外的部分，即返回的新图片
 */
- (UIImage*)zw_zoomAndClipToSize:(CGSize)newSize;

//生成灰化的图片
- (UIImage*)zw_gray;

//生成旋转90度的图片
- (UIImage*)zw_rotate:(BOOL)bLeft;

/**
 *  图片的平均颜色
 */
- (UIColor*)zw_averageColor;

/**
 *  将图片以JPEG形式保存到本地
 *
 *  @param imagePath 图片保存的路径
 *  @param fQuality  压缩系数
 *
 *  @return 保存成功与否
 */
- (BOOL)zw_saveJPEGImageToLacalPath:(NSString*)imagePath withJPEGCompressionQuality:(CGFloat)fQuality;

/**
 *  将图片以PNG形式保存到本地
 *  @param imagePath 图片保存的路径
 *  @return 保存成功与否
 */
- (BOOL)zw_savePNGImageToLacalPath:(NSString*)imagePath;

/**
 *  获取图片上指定像素点的颜色值
 */
- (UIColor*)zw_colorAtPixel:(CGPoint)point;

/**
 *  iOS相机加入了方向传感器，在处理拍照的图片是可能导致方向旋转了
 */
- (UIImage *)zw_fixOrientation;

#pragma mark - 类方法
//用颜色生成图片
+ (UIImage *)zw_createImageWithColor:(UIColor *)color;
+ (UIImage *)zw_createImageWithColor:(UIColor *)color size:(CGSize)size;

/**
 *  从图片的二进制文件中获取图片的类型
 */
+ (NSString *)zw_mimeTypeForImageData:(NSData*)imageData;

typedef NS_ENUM(NSInteger, UIImageMimeType)
{
    UIImageMimeTypeUnknown = 0,
    UIImageMimeTypeJPG,
    UIImageMimeTypePNG,
    UIImageMimeTypeGIF,
    UIImageMimeTypeTIFF,
};
+ (UIImageMimeType)zw_imageMimeTypeForImageData:(NSData*)imageData;

/**
 *  获取Gif文件的第一帧图片，如果不是Gif文件则返回空
 *  @param gifData Gif文件
 */
+ (UIImage*)zw_gifFirstImageFromGifData:(NSData*)gifData;

@end
