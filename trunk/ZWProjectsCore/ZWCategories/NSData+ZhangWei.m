//
//  NSData+ZhangWei.m
//  ZWProjectsCore
//
//  Created by ZhangWei on 16/8/10.
//  Copyright © 2016年 ZhangWei. All rights reserved.
//

#import "NSData+ZhangWei.h"

@implementation NSData (ZhangWei)

- (NSString *)zw_byteSize;
{
    return [NSByteCountFormatter stringFromByteCount:self.length countStyle:NSByteCountFormatterCountStyleFile];
}

- (NSData *)zw_getSubDataWithRange:(NSRange)range
{
    if (range.location >= [self length])
    {
        //开始的位置已越界，返回空
        return nil;
    }
    if ((range.location+range.length) > [self length])
    {
        return [self subdataWithRange:NSMakeRange(range.location, [self length]-range.location)];
    }
    else
    {
        return [self subdataWithRange:range];
    }
    return nil;
}

- (NSString *)zw_convertToString;
{
    return [self zw_convertToStringWithGBKFormat:NO];
}

- (NSString *)zw_convertToStringWithGBKFormat:(BOOL)isGBKFormat;
{
    if (isGBKFormat)
    {
        //定义GBK编码格式
        NSStringEncoding enc = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
        //GBK格式对数据进行转换
        return [[NSString alloc]initWithData:self encoding:enc];
    }
    else
    {
        return [[NSString alloc]initWithData:self encoding:NSUTF8StringEncoding];
    }
}

- (NSObject *)zw_convertToObjectFromJsonData;
{
    NSError* error = nil;
    id object = [self zw_convertToObjectFromJsonDataWithGBKFormat:NO error:&error];
    if (error)
    {
        NSLog(@"%@", error);
    }
    return object;
}

- (NSObject *)zw_convertToObjectFromJsonDataWithGBKFormat:(BOOL)isGBKFormat error:(NSError **)error;
{
    NSString* jsonString = [self zw_convertToStringWithGBKFormat:isGBKFormat];

    if (jsonString.length > 0)
    {
        //NSJSONReadingMutableContainers 创建出来的数组和字典就是可变
        //NSJSONReadingMutableLeaves     数组或者字典里面的字符串是可变的
        //NSJSONReadingAllowFragments    允许解析出来的对象不是字典或者数组，比如直接是字符串或者NSNumber
        //kNilOptions                    如果不在乎返回的是可变的还是不可变的，它的效率最高！返回的就是不可变的
        return [NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:error];
    }
    return nil;
}

- (NSString *)zw_convertToHexString;
{
    unsigned char * bytes = (unsigned char *)(self.bytes);
    NSMutableString *mutableString = @"".mutableCopy;
    for (NSUInteger i = 0; i < self.length; i++)
    {
        [mutableString appendFormat:@"%02x", bytes[i]];
    }
    return [NSString stringWithString:mutableString];
}

@end
