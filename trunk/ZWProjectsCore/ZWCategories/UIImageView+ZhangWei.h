//
//  UIImageView+ZhangWei.h
//  ZWProjectsCore
//
//  Created by ZhangWei on 15/8/12.
//  Copyright (c) 2015年 ZhangWei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (ZhangWei)

/**
 * 渐变的效果显示图片，如果已经存在图片则取消渐变效果
 */
- (void)zw_setImageWithAnimation:(UIImage*)image;
- (void)zw_setImageWithAlphaFade:(UIImage*)image;

@end
