//
//  UIImage+ZhangWei.m
//  ZWProjectsCore
//
//  Created by ZhangWei on 14-3-28.
//  Copyright (c) 2014年 ZhangWei. All rights reserved.
//

#import "UIImage+ZhangWei.h"
#import <ImageIO/ImageIO.h>
#import "ZWProjectsCoreMacros.h"
#import "ZWProjectsCoreFunctions.h"

@implementation UIImage (ZhangWei)

- (UIImage*)zw_screenShotWithRect:(CGRect)rect
{
    //将参数处理为有效的rect
    if (rect.origin.x < 0)
    {
        CGFloat fNewWidth = rect.size.width+rect.origin.x;
        rect.origin.x = 0;
        rect.size.width = fNewWidth;
    }
    if (rect.origin.y < 0)
    {
        CGFloat fNewHeight = rect.size.height+rect.origin.y;
        rect.origin.y = 0;
        rect.size.height = fNewHeight;
    }
    
    float imageWidth = self.size.width;
    float imageHeight = self.size.height;
    
    float fRealWidth = 0;
    float fRealHeight = 0;
    if (imageWidth > rect.origin.x)
    {
        if (imageWidth < rect.origin.x + rect.size.width)
        {
            fRealWidth = imageWidth - rect.origin.x;
        }
        else
        {
            fRealWidth = rect.size.width;
        }
    }
    if (imageHeight > rect.origin.y)
    {
        if (imageHeight < rect.origin.y + rect.size.height)
        {
            fRealHeight = imageHeight - rect.origin.y;
        }
        else
        {
            fRealHeight = rect.size.height;
        }
    }
    
    //实际截图区域
    CGRect newRect = CGRectMake(rect.origin.x, rect.origin.y, fRealWidth, fRealHeight);
    
    CGFloat (^rad)(CGFloat) = ^CGFloat(CGFloat deg) {
        return deg / 180.0f * (CGFloat) M_PI;
    };
    
    // determine the orientation of the image and apply a transformation to the crop rectangle to shift it to the correct position
    CGAffineTransform rectTransform;
    switch (self.imageOrientation)
    {
        case UIImageOrientationLeft:
            rectTransform = CGAffineTransformTranslate(CGAffineTransformMakeRotation(rad(90)), 0, -self.size.height);
            break;
        case UIImageOrientationRight:
            rectTransform = CGAffineTransformTranslate(CGAffineTransformMakeRotation(rad(-90)), -self.size.width, 0);
            break;
        case UIImageOrientationDown:
            rectTransform = CGAffineTransformTranslate(CGAffineTransformMakeRotation(rad(-180)), -self.size.width, -self.size.height);
            break;
        default:
            rectTransform = CGAffineTransformIdentity;
    };
    
    // adjust the transformation scale based on the image scale
    rectTransform = CGAffineTransformScale(rectTransform, self.scale, self.scale);
    CGRect transformedCropSquare = CGRectApplyAffineTransform(newRect, rectTransform);
    
    //截图
    CGImageRef imageRef = CGImageCreateWithImageInRect([self CGImage], transformedCropSquare);
    
    // create a new UIImage and set the scale and orientation appropriately
    UIImage *newImage = [UIImage imageWithCGImage:imageRef scale:self.scale orientation:self.imageOrientation];
    
    // memory cleanup
    CGImageRelease(imageRef);
    
    return newImage;
}

//按比例缩放图片
- (UIImage *)zw_originalScaleZoomWithSize:(CGSize)newsize
{
    return [self zw_originalScaleZoomWithSize:newsize stillWhenSizeBeyond:NO];
}

- (UIImage*)zw_originalScaleZoomWithSize:(CGSize)newsize stillWhenSizeBeyond:(BOOL)bFlag;
{
    if (CGSizeEqualToSize(newsize, CGSizeZero) || newsize.height <= 0 || newsize.width <= 0)
    {
        return self;
    }
    
    CGSize realSize = CGSizeMake(newsize.width, newsize.height);
    //图片比例大小
    CGFloat imageScale = self.size.width/self.size.height;
    //缩成图片的比例
    CGFloat newScale = newsize.width/newsize.height;
    //判断，如果图片比例大于缩成图片比例，说明图片宽度尺寸比缩成图片的要大，那么要改变缩成图片的高度，
    if (imageScale > newScale)
    {
        realSize.height = self.size.height/self.size.width * newsize.width;
    }
    else if (imageScale < newScale)
    {
        //否则，说明图片高度尺寸比缩成图片的要大，那么要改变缩成图片的宽度，
        realSize.width = self.size.width/self.size.height * newsize.height;
    }
    
    CGFloat scale = [UIScreen mainScreen].scale;
    
    if (!bFlag && (realSize.width > self.size.width/scale || realSize.height > self.size.height/scale))
    {
        return self;
    }
    
    // 创建一个bitmap的context
    // 并把它设置成为当前正在使用的context
    UIGraphicsBeginImageContext(realSize);
    UIGraphicsBeginImageContextWithOptions(realSize, NO, scale);
    
    // 绘制改变大小的图片
    [self drawInRect:CGRectMake(0, 0, realSize.width, realSize.height)];
    
    // 从当前context中创建一个改变大小后的图片
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // 使当前的context出堆栈
    UIGraphicsEndImageContext();
    
    // 返回新的改变大小后的图片
    return scaledImage;
}

- (UIImage*)zw_zoomWithSize:(CGSize)newsize
{
    return [self zw_zoomWithSize:newsize aspect:NO];
}

- (UIImage*)zw_zoomWithSize:(CGSize)newsize aspect:(BOOL)isAcpect;
{
    if (CGSizeEqualToSize(newsize, CGSizeZero) || newsize.height <= 0 || newsize.width <= 0)
    {
        return self;
    }
    
    CGFloat scale = [UIScreen mainScreen].scale;
    
    UIGraphicsBeginImageContext(newsize);
    UIGraphicsBeginImageContextWithOptions(newsize, NO, scale);
    
    // 绘制改变大小的图片
    if (isAcpect)
    {
        CGSize realSize = CGSizeMake(newsize.width, newsize.height);
        //图片比例大小
        CGFloat imageScale = self.size.width/self.size.height;
        //缩成图片的比例
        CGFloat newScale = newsize.width/newsize.height;
        //判断，如果图片比例大于缩成图片比例，说明图片宽度尺寸比缩成图片的要大，那么要改变缩成图片的高度，
        if (imageScale > newScale)
        {
            realSize.height = self.size.height/self.size.width * newsize.width;
        }
        else if (imageScale < newScale)
        {
            //否则，说明图片高度尺寸比缩成图片的要大，那么要改变缩成图片的宽度，
            realSize.width = self.size.width/self.size.height * newsize.height;
        }
        
        if (realSize.width == newsize.width && realSize.height < newsize.height)
        {
            [self drawInRect:CGRectMake(0, (newsize.height-realSize.height)/2.0, realSize.width, realSize.height)];
        }
        else if (realSize.height == newsize.height && realSize.width < newsize.width)
        {
            [self drawInRect:CGRectMake((newsize.width-realSize.width)/2.0, 0, realSize.width, realSize.height)];
        }
        else
        {
            [self drawInRect:CGRectMake(0, 0, newsize.width, newsize.height)];
        }
    }
    else
    {
        [self drawInRect:CGRectMake(0, 0, newsize.width, newsize.height)];
    }
    
    // 从当前context中创建一个改变大小后的图片
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // 使当前的context出堆栈
    UIGraphicsEndImageContext();
    
    return scaledImage;
}

- (UIImage*)zw_zoomAndClipToSize:(CGSize)newSize;
{
    if (CGSizeEqualToSize(newSize, CGSizeZero) || newSize.height <= 0 || newSize.width <= 0 || (newSize.height == self.size.height && newSize.width == self.size.width))
    {
        return self;
    }
    
    // 图片上被画的大小，默认是原图
    CGRect clipRect = CGRectMake( 0, 0, self.size.width, self.size.height);
    //图片比例大小
    CGFloat imageScale = self.size.width/self.size.height;
    //新的比例
    CGFloat newScale = newSize.width/newSize.height;
    //判断，如果图片比例大于新比例，说明图片宽度尺寸比缩成图片的要大，那么要裁剪掉图片宽多余的部分
    if (imageScale > newScale)
    {
        CGFloat fWidth = newScale*self.size.height;
        
        clipRect.origin.x = (self.size.width-fWidth)/2.0;
        clipRect.size.width = fWidth;
    }
    else if (imageScale < newScale)
    {
        //否则，说明图片高度尺寸比缩成图片的要大，那么要改变缩成图片的宽度
        CGFloat fHeight = self.size.width/newScale;
        
        clipRect.origin.y = (self.size.height- fHeight)/2.0;
        clipRect.size.height = fHeight;
    }
    
    CGFloat (^rad)(CGFloat) = ^CGFloat(CGFloat deg) {
        return deg / 180.0f * (CGFloat) M_PI;
    };
    
    // determine the orientation of the image and apply a transformation to the crop rectangle to shift it to the correct position
    CGAffineTransform rectTransform;
    switch (self.imageOrientation)
    {
        case UIImageOrientationLeft:
            rectTransform = CGAffineTransformTranslate(CGAffineTransformMakeRotation(rad(90)), 0, -self.size.height);
            break;
        case UIImageOrientationRight:
            rectTransform = CGAffineTransformTranslate(CGAffineTransformMakeRotation(rad(-90)), -self.size.width, 0);
            break;
        case UIImageOrientationDown:
            rectTransform = CGAffineTransformTranslate(CGAffineTransformMakeRotation(rad(-180)), -self.size.width, -self.size.height);
            break;
        default:
            rectTransform = CGAffineTransformIdentity;
    };
    
    // adjust the transformation scale based on the image scale
    rectTransform = CGAffineTransformScale(rectTransform, self.scale, self.scale);
    CGRect transformedCropSquare = CGRectApplyAffineTransform(clipRect, rectTransform);
    
    //截图
    CGImageRef imageRef = CGImageCreateWithImageInRect([self CGImage], transformedCropSquare);
    
    // create a new UIImage and set the scale and orientation appropriately
    UIImage *newImage = [UIImage imageWithCGImage:imageRef scale:self.scale orientation:self.imageOrientation];
    
    // memory cleanup
    CGImageRelease(imageRef);
    
    newImage = [newImage zw_zoomWithSize:newSize];
    
    return newImage;
}

- (UIImage*)zw_gray
{
    const int RED = 1;
    const int GREEN = 2;
    const int BLUE = 3;
    
    // Create image rectangle with current image width/height
    CGRect imageRect = CGRectMake(0, 0, self.size.width * self.scale, self.size.height * self.scale);
    
    int width = imageRect.size.width;
    int height = imageRect.size.height;
    
    // the pixels will be painted to this array
    uint32_t *pixels = (uint32_t *) malloc(width * height * sizeof(uint32_t));
    
    // clear the pixels so any transparency is preserved
    memset(pixels, 0, width * height * sizeof(uint32_t));
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    // create a context with RGBA pixels
    CGContextRef context = CGBitmapContextCreate(pixels, width, height, 8, width * sizeof(uint32_t), colorSpace,
                                                 kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedLast);
    
    // paint the bitmap to our context which will fill in the pixels array
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), [self CGImage]);
    
    for(int y = 0; y < height; y++) {
        for(int x = 0; x < width; x++) {
            uint8_t *rgbaPixel = (uint8_t *) &pixels[y * width + x];
            
            // convert to grayscale using recommended method: http://en.wikipedia.org/wiki/Grayscale#Converting_color_to_grayscale
            uint32_t gray = 0.3 * rgbaPixel[RED] + 0.59 * rgbaPixel[GREEN] + 0.11 * rgbaPixel[BLUE];
            
            // set the pixels to gray
            rgbaPixel[RED] = gray;
            rgbaPixel[GREEN] = gray;
            rgbaPixel[BLUE] = gray;
        }
    }
    
    // create a new CGImageRef from our context with the modified pixels
    CGImageRef image = CGBitmapContextCreateImage(context);
    
    // we're done with the context, color space, and pixels
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    free(pixels);
    
    // make a new UIImage to return
    UIImage *resultUIImage = [UIImage imageWithCGImage:image
                                                 scale:self.scale
                                           orientation:UIImageOrientationUp];
    
    // we're done with image now too
    CGImageRelease(image);
    
    return resultUIImage;
}

- (UIImage*)zw_rotate:(BOOL)bLeft
{
    CGAffineTransform transform = CGAffineTransformIdentity;
    if (bLeft)
    {
        transform = CGAffineTransformTranslate(transform, self.size.width, 0);
        transform = CGAffineTransformRotate(transform, M_PI_2);
    }
    else
    {
        transform = CGAffineTransformTranslate(transform, 0, self.size.height);
        transform = CGAffineTransformRotate(transform, -M_PI_2);
    }
    
    CGContextRef ctx = CGBitmapContextCreate(NULL, self.size.width, self.size.height,
                                             CGImageGetBitsPerComponent(self.CGImage), 0,
                                             CGImageGetColorSpace(self.CGImage),
                                             CGImageGetBitmapInfo(self.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (self.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0,0,self.size.height,self.size.width), self.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,self.size.width,self.size.height), self.CGImage);
            break;
    }
    
    // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}

- (UIColor*)zw_averageColor;
{
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    unsigned char rgba[4];
    CGContextRef context = CGBitmapContextCreate(rgba, 1, 1, 8, 4, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    
    CGContextDrawImage(context, CGRectMake(0, 0, 1, 1), self.CGImage);
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    
    if(rgba[3] > 0)
    {
        CGFloat alpha = ((CGFloat)rgba[3])/255.0;
        CGFloat multiplier = alpha/255.0;
        return [UIColor colorWithRed:((CGFloat)rgba[0])*multiplier
                               green:((CGFloat)rgba[1])*multiplier
                                blue:((CGFloat)rgba[2])*multiplier
                               alpha:alpha];
    }
    else {
        return [UIColor colorWithRed:((CGFloat)rgba[0])/255.0
                               green:((CGFloat)rgba[1])/255.0
                                blue:((CGFloat)rgba[2])/255.0
                               alpha:((CGFloat)rgba[3])/255.0];
    }
}

- (BOOL)zw_saveJPEGImageToLacalPath:(NSString*)imagePath withJPEGCompressionQuality:(CGFloat)fQuality;
{
    if (imagePath.length > 0)
    {
        return [UIImageJPEGRepresentation(self, fQuality) writeToFile:imagePath atomically:YES];
    }
    return NO;
}

- (BOOL)zw_savePNGImageToLacalPath:(NSString*)imagePath;
{
    if (imagePath.length > 0)
    {
        return [UIImagePNGRepresentation(self) writeToFile:imagePath atomically:YES];
    }
    return NO;
}

- (UIColor*)zw_colorAtPixel:(CGPoint)point;
{
    // Cancel if point is outside image coordinates
    if (!CGRectContainsPoint(CGRectMake(0.0f, 0.0f, self.size.width, self.size.height), point)) {
        return nil;
    }
    
    // Create a 1x1 pixel byte array and bitmap context to draw the pixel into.
    // Reference: http://stackoverflow.com/questions/1042830/retrieving-a-pixel-alpha-value-for-a-uiimage
    NSInteger pointX = trunc(point.x);
    NSInteger pointY = trunc(point.y);
    CGImageRef cgImage = self.CGImage;
    NSUInteger width = self.size.width;
    NSUInteger height = self.size.height;
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    int bytesPerPixel = 4;
    int bytesPerRow = bytesPerPixel * 1;
    NSUInteger bitsPerComponent = 8;
    unsigned char pixelData[4] = { 0, 0, 0, 0 };
    CGContextRef context = CGBitmapContextCreate(pixelData,
                                                 1,
                                                 1,
                                                 bitsPerComponent,
                                                 bytesPerRow,
                                                 colorSpace,
                                                 kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGColorSpaceRelease(colorSpace);
    CGContextSetBlendMode(context, kCGBlendModeCopy);
    
    // Draw the pixel we are interested in onto the bitmap context
    CGContextTranslateCTM(context, -pointX, pointY-(CGFloat)height);
    CGContextDrawImage(context, CGRectMake(0.0f, 0.0f, (CGFloat)width, (CGFloat)height), cgImage);
    CGContextRelease(context);
    
    // Convert color values [0..255] to floats [0.0..1.0]
    CGFloat red   = (CGFloat)pixelData[0] / 255.0f;
    CGFloat green = (CGFloat)pixelData[1] / 255.0f;
    CGFloat blue  = (CGFloat)pixelData[2] / 255.0f;
    CGFloat alpha = (CGFloat)pixelData[3] / 255.0f;
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}

- (UIImage *)zw_fixOrientation;
{
    
    // No-op if the orientation is already correct
    if (self.imageOrientation == UIImageOrientationUp) return self;
    
    // We need to calculate the proper transformation to make the image upright.
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (self.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, self.size.width, self.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, self.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, self.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationUpMirrored:
            break;
    }
    
    switch (self.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, self.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, self.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationDown:
        case UIImageOrientationLeft:
        case UIImageOrientationRight:
            break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, self.size.width, self.size.height,
                                             CGImageGetBitsPerComponent(self.CGImage), 0,
                                             CGImageGetColorSpace(self.CGImage),
                                             CGImageGetBitmapInfo(self.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (self.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0,0,self.size.height,self.size.width), self.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,self.size.width,self.size.height), self.CGImage);
            break;
    }
    
    // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}

#pragma mark - 类方法
+ (UIImage *)zw_createImageWithColor:(UIColor *)color;
{
    return [UIImage zw_createImageWithColor:color size:CGSizeMake(2, 2)];
}

+ (UIImage *)zw_createImageWithColor:(UIColor *)color size:(CGSize)size;
{
    CGRect rect=CGRectMake(0.0f, 0.0f, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    theImage = [theImage stretchableImageWithLeftCapWidth:theImage.size.width/2 topCapHeight:theImage.size.height/2];
    return theImage;
}

+ (NSString *)zw_mimeTypeForImageData:(NSData*)imageData;
{
    if (!imageData)
        return nil;
    
    
    
    uint8_t c;
    
    [imageData getBytes:&c length:1];
    
    switch (c) {
            
        case 0xFF:
            
            return @"image/jpeg";
            
        case 0x89:
            
            return @"image/png";
            
        case 0x47:
            
            return @"image/gif";
            
        case 0x49:
            
        case 0x4D:
            
            return @"image/tiff";
    }
    
    return nil;
}

+ (UIImageMimeType)zw_imageMimeTypeForImageData:(NSData*)imageData;
{
    if (!imageData)
        return UIImageMimeTypeUnknown;
    
    
    
    uint8_t c;
    
    [imageData getBytes:&c length:1];
    
    switch (c) {
            
        case 0xFF:
            
            return UIImageMimeTypeJPG;
            
        case 0x89:
            
            return UIImageMimeTypePNG;
            
        case 0x47:
            
            return UIImageMimeTypeGIF;
            
        case 0x49:
            
        case 0x4D:
            
            return UIImageMimeTypeTIFF;
    }
    
    return UIImageMimeTypeUnknown;
}

+ (UIImage*)zw_gifFirstImageFromGifData:(NSData*)gifData;
{
    UIImage* resultImage = nil;
    if (gifData)
    {
        if ([UIImage zw_imageMimeTypeForImageData:gifData] != UIImageMimeTypeGIF)
        {
            return resultImage;
        }
        CGImageSourceRef _gif = CGImageSourceCreateWithData((__bridge CFDataRef)gifData, nil);
        
        size_t _count = CGImageSourceGetCount(_gif);
        
        if (_count > 0)
        {
            CGImageRef ref = CGImageSourceCreateImageAtIndex(_gif, 0, nil);
            resultImage = [UIImage imageWithCGImage:ref];
            CFRelease(ref);
        }
        
        CFRelease(_gif);
        _gif = nil;
    }
    return resultImage;
}

@end
