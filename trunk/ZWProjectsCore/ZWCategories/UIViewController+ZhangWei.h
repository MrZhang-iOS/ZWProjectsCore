//
//  UIViewController+ZhangWei.h
//  ZWProjectsCore
//
//  Created by zhangwei on 2021/9/1.
//  Copyright © 2021 wei.zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (ZhangWei)

/// 在控制台显示页面生命周期的Debug日志，默认NO。
@property (class, nonatomic, assign) BOOL debugLogInConsole;

@end

/**
 * 仅在Debug模式下对
 * dealloc、
 * init、
 * viewWillAppear、
 * viewDidAppear、
 * viewWillDisappear、
 * viewDidDisappear
 * 几个系统方法进行了hook，便于开发的调试。
 * 可以通过类属性查看页面的生命周期日志。
 */
