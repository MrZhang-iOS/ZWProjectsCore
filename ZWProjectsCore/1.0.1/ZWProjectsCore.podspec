Pod::Spec.new do |spec|
  spec.name                  = "ZWProjectsCore"
  spec.version               = "1.0.1"
  spec.summary               = "整理出来的iOS软件开发的核心库，包含了一些常用的方法和模块，可以给开发提供便捷，提升开发效率。"
  spec.description           = "从ProjectsTemplate提取出来"
  spec.homepage              = "https://gitee.com/MrZhang-iOS/ZWProjectsCore"
  spec.license               = { :type => "MIT", :file => "FILE_LICENSE" }
  spec.author                = { "MrZhang-iOS" => "why.20130509@gmail.com" }
  spec.source                = { :git => "https://gitee.com/MrZhang-iOS/ZWProjectsCore.git", :tag => spec.version.to_s }
  spec.source_files          = "trunk/ZWProjectsCore/**/*.{h,m,c}"
  spec.public_header_files   = "trunk/ZWProjectsCore/**/*.h"
  spec.resources             = ['trunk/ZWProjectsCore/FromCustom/CountryCodeManager/Countrycode.plist', 'trunk/ZWProjectsCore/FromCustom/NSLocalizeLanguage/*.{plist,bundle}', 'trunk/ZWProjectsCore/FromCustom/ResourceBundleManager/*.bundle', 'trunk/ZWProjectsCore/FromGithub/MJRefresh/*.bundle', 'trunk/ZWProjectsCore/FromGithub/SAMKeychain/*.bundle']
  spec.ios.deployment_target = "9.0"
  spec.frameworks            = "UIKit", "Foundation", "ImageIO", "Photos", "LocalAuthentication", "SystemConfiguration", "AudioToolbox", "AVFoundation", "Security", "CoreServices", "StoreKit", "MessageUI", "CoreGraphics", "WebKit", "Contacts", "MapKit"
end
