Pod::Spec.new do |spec|
  spec.name                  = "ZWProjectsCore"
  spec.version               = "2.1.0"
  spec.summary               = "iOS软件开发的常用类库，包含了一些常用的方法和模块，避免重复造轮子，提升开发效率。"
  spec.description           = "常用系统类的扩展、常用UI控件的基类封装、加解密、资源打包、语言国际化、Gif图片的创建和显示、国家码区域码匹配、快捷发送Email和短信、路径文件变动监听。"
  spec.homepage              = "https://gitee.com/MrZhang-iOS/ZWProjectsCore"
  spec.license               = { :type => "MIT", :file => "FILE_LICENSE" }
  spec.author                = { "MrZhang-iOS" => "why.20130509@gmail.com" }
  spec.source                = { :git => "https://gitee.com/MrZhang-iOS/ZWProjectsCore.git", :tag => spec.version.to_s }
  spec.source_files          = "trunk/ZWProjectsCore/**/*.{h,m,c}"
  spec.resource_bundle       = {"ZWProjectsCore.privacy"=>"trunk/ZWProjectsCore/Resources/PrivacyInfo.xcprivacy"}
  spec.public_header_files   = "trunk/ZWProjectsCore/**/*.h"
  spec.resources             = ['trunk/ZWProjectsCore/ZWCountryCodeManager/*.plist', 'trunk/ZWProjectsCore/ZWLocalizedLanguage/*.{plist,bundle}', 'trunk/ZWProjectsCore/ZWResourcesManager/*.bundle']
  spec.platform              = :ios, "12.0"
  #spec.frameworks            = 'CoreGraphics', 'Foundation', 'CoreServices', 'Security', 'SystemConfiguration', 'UIKit'
  spec.dependency 'YYModel'
  spec.dependency 'Masonry'
  spec.dependency 'KVOController'
  spec.dependency 'AFNetworking'
  spec.dependency 'ZWRecordTextTool'
end
