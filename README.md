# ZWProjectsCore

#### 介绍
iOS App开发的常用封装库，包含了一些系统类的扩展和模块封装，可以给开发提供便捷，提升开发效率。


#### 安装教程

cd到你的工程路径，在Podfile中添加以下命令
```
pod 'ZWRecordTextTool', :tag => '3.0.0', :git =>'https://gitee.com/MrZhang-iOS/ZWRecordTextTool.git', :branch => 'master'
pod 'ZWProjectsCore', :tag => '2.0.0', :git =>'https://gitee.com/MrZhang-iOS/ZWProjectsCore.git', :branch => 'master'
```

最好先执行以下脚本查询最新的版本
```
pod search ZWRecordTextTool
pod search ZWProjectsCore
```

然后执行
```
pod install
```

#### 使用说明

在需要进行日志记录的类中引用库
```
#import < ZWProjectsCore/ZWProjectsCore.h>
```

*如果有其他问题可以留言，谢谢！*
